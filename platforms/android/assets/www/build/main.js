webpackJsonp([27],{

/***/ 111:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return PopoverPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_barcode_scanner__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_location_accuracy__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_android_permissions__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__config_provider__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_formoutlet_formoutlet__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_storage__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};














var HomePage = (function () {
    function HomePage(navCtrl, alertCtrl, barcodeScanner, popoverCtrl, geolocation, viewCtrl, navParams, postPvdr, storage, locationAccuracy, androidPermissions, modalCtrl) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.barcodeScanner = barcodeScanner;
        this.popoverCtrl = popoverCtrl;
        this.geolocation = geolocation;
        this.viewCtrl = viewCtrl;
        this.navParams = navParams;
        this.postPvdr = postPvdr;
        this.storage = storage;
        this.locationAccuracy = locationAccuracy;
        this.androidPermissions = androidPermissions;
        this.modalCtrl = modalCtrl;
        this.qrData = null;
        this.createdCode = null;
        this.scannedCode = null;
        this.scanCodex(navParams.get("lat"), navParams.get("lng"), navParams.get("outlet"), navParams.get("name"), navParams.get("address"), navParams.get("note"), navParams.get("kode_distributor"), navParams.get("kode_sales"));
        this.lat = navParams.get("lat");
        this.lng = navParams.get("lng");
        this.outlet = navParams.get("outlet");
        this.name = navParams.get("name");
        this.address = navParams.get("address");
        this.note = navParams.get("note");
        this.kode_distributor = navParams.get("kode_distributor");
        this.kode_sales = navParams.get("kode_sales");
    }
    HomePage.prototype.ionViewDidLoad = function () {
        this.initMap();
    };
    //Check if application having GPS access permission  
    HomePage.prototype.checkGPSPermission = function () {
        var _this = this;
        this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(function (result) {
            console.log('status harus melewati permission : ', result.hasPermission);
            if (result.hasPermission) {
                //If having permission show 'Turn On GPS' dialogue
                _this.askToTurnOnGPS();
                console.log('masuk ke pertanyaan permission');
            }
            else {
                //If not having permission ask for permission
                _this.requestGPSPermission();
                console.log('masuk ke request permission');
            }
        }, function (err) {
            console.log(err);
        });
    };
    HomePage.prototype.requestGPSPermission = function () {
        var _this = this;
        this.locationAccuracy.canRequest().then(function (canRequest) {
            if (canRequest) {
                console.log("4");
            }
            else {
                //Show 'GPS Permission Request' dialogue
                _this.androidPermissions.requestPermission(_this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
                    .then(function () {
                    // call method to turn on GPS
                    _this.askToTurnOnGPS();
                    console.log('nyalakan GPS');
                }, function (error) {
                    //Show alert if user click on 'No Thanks'
                    console.log('requestPermission Error requesting location permissions ' + error);
                });
            }
        });
    };
    HomePage.prototype.askToTurnOnGPS = function () {
        var _this = this;
        this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(function () {
            // When GPS Turned ON call method to get Accurate location coordinates
            console.log('masuk ke pencarian koordinat');
            _this.initMap();
        }, function (error) { return console.log('Error requesting location permissions ' + JSON.stringify(error)); });
    };
    // initMap() {
    //   var markerArray = [];
    //   // Instantiate a directions service.
    //   var directionsService = new google.maps.DirectionsService;
    //   this.geolocation.getCurrentPosition().then(pos => {
    //     // this.latitude = pos.coords.latitude;
    //     // this.longitude = pos.coords.longitude;
    //     var myLatlng = { lat: pos.coords.latitude, lng: pos.coords.longitude };
    //     var map = new google.maps.Map(this.mapElement.nativeElement, {
    //       zoom: 15,
    //       center: myLatlng,
    //       rotateControl: false,
    //       fullscreenControl: false,
    //       mapTypeControl: false,
    //       scaleControl: false,
    //       streetViewControl: false,
    //       streetViewControlOptions: {
    //         position: google.maps.ControlPosition.LEFT_TOP
    //       },
    //       zoomControl: false,
    //       zoomControlOptions: {
    //         position: google.maps.ControlPosition.LEFT_TOP
    //       }
    //     });
    //     // Create a renderer for directions and bind it to the map.
    //     var directionsRenderer = new google.maps.DirectionsRenderer({ map: map });
    //     // Instantiate an info window to hold step text.
    //     var stepDisplay = new google.maps.InfoWindow;
    //     // Display the route between the initial start and end selections.
    //     this.calculateAndDisplayRoute(directionsRenderer, directionsService, markerArray, stepDisplay, map, pos.coords.latitude, pos.coords.longitude);
    //     //marker mylocation
    //     // var image = 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-red.png';
    //     // var marker = new google.maps.Marker({
    //     //   position: myLatlng,
    //     //   map: map,
    //     //   icon: image
    //     // });
    //     // var contentString =
    //     //   '<h4>test&nbsp;&nbsp;&nbsp;&nbsp;</h4>' +
    //     //   '<div>' +
    //     //   '<p>test&nbsp;&nbsp;&nbsp;&nbsp;' +
    //     //   '</p>' +
    //     //   '<div id="pic"><img src="https://lh6.googleusercontent.com/_rhuioznu6Us/TU-MTMhCYGI/AAAAAAAAAQs/WJw-khN02AI/s144/vasto-1967.jpeg" width="268" height="188" />' +
    //     //   '<p>Jakarta, 2019</p></div>' +
    //     //   '<input type="submit" id="butSubmit" value="Detail" onclick="detail()"><br><br>' +
    //     //   '</div>';
    //     // marker.content = contentString;
    //     // var infoWindow = new google.maps.InfoWindow();
    //     // google.maps.event.addListener(marker, 'click', function () {
    //     //   infoWindow.setContent(this.content);
    //     //   infoWindow.open(this.getMap(), this);
    //     // });
    //   }).catch(err => console.log(err));
    // }
    HomePage.prototype.initMap = function () {
        var myLatlng = { lat: -6.1741808, lng: 106.8079343 };
        var map = new google.maps.Map(this.mapElement.nativeElement, {
            zoom: 13,
            center: myLatlng,
            zoomControl: false,
            mapTypeControl: false,
            scaleControl: false,
            streetViewControl: false,
            rotateControl: false,
            fullscreenControl: false
        });
    };
    HomePage.prototype.calculateAndDisplayRoute = function (directionsRenderer, directionsService, markerArray, stepDisplay, map, lat, lng) {
        // First, remove any existing markers from the map.
        for (var i = 0; i < markerArray.length; i++) {
            markerArray[i].setMap(null);
        }
        // Retrieve the start and end locations and create a DirectionsRequest using
        // DRIVING directions.
        directionsService.route({
            origin: 'duta indah karya',
            destination: { lat: lat, lng: lng },
            travelMode: 'DRIVING',
            avoidTolls: true
        }, function (response, status) {
            // Route the directions and pass the response to a function to create
            // markers for each step.
            if (status === 'OK') {
                directionsRenderer.setDirections(response);
                // For each step, place a marker, and add the text to the marker's infowindow.
                // Also attach the marker to an array so we can keep track of it and remove it
                // when calculating new routes.
                var myRoute = response.routes[0].legs[0];
                for (var i = 0; i < myRoute.steps.length; i++) {
                    var marker = markerArray[i] = markerArray[i] || new google.maps.Marker;
                    //untuk memunculkan step ( titik langkkah )
                    //marker.setMap(map);
                    marker.setPosition(myRoute.steps[i].start_location);
                    google.maps.event.addListener(marker, 'click', function () {
                        // Open an info window when the marker is clicked on, containing the text
                        // of the step.
                        stepDisplay.setContent(myRoute.steps[i].instructions);
                        stepDisplay.open(map, marker);
                    });
                }
            }
            else {
                // window.alert('Directions request failed due to ' + status);
            }
        });
    };
    HomePage.prototype.detail = function () {
        // alert('test')
    };
    HomePage.prototype.mylocationPage = function (marker) {
        google.maps.event.addListener(marker, 'click', function () {
        });
    };
    HomePage.prototype.createCode = function () {
        this.createdCode = this.qrData;
    };
    // scanCode() {
    //   this.barcodeScanner.scan().then(barcodeData => {
    //     this.scannedCode = barcodeData.text;
    //   }).catch(err => {
    //     console.log('Error', err);
    //   });
    // }
    HomePage.prototype.scanCode = function () {
        var _this = this;
        this.barcodeScanner.scan().then(function (barcodeData) {
            var strArray = barcodeData.text.split("#");
            var prompt = _this.alertCtrl.create({
                title: 'Info!',
                message: "Result QR Code Scanner",
                inputs: [
                    {
                        name: 'outlet_name',
                        placeholder: 'outlet name',
                        value: strArray[0]
                    },
                    {
                        name: 'address',
                        placeholder: 'address',
                        value: strArray[1]
                    },
                    {
                        name: 'latitude',
                        placeholder: 'latitude',
                        value: strArray[2]
                    },
                    {
                        name: 'longitude',
                        placeholder: 'longitude',
                        value: strArray[3]
                    },
                ],
                buttons: [
                    {
                        text: 'Cancel',
                        handler: function (data) {
                            console.log('Cancel clicked');
                        }
                    },
                    {
                        text: 'View',
                        handler: function (data) {
                            console.log('Saved clicked');
                            var myLatlng = { lat: parseFloat(data.latitude), lng: parseFloat(data.longitude) };
                            var map = new google.maps.Map(_this.mapElement.nativeElement, {
                                zoom: 19,
                                center: myLatlng,
                                rotateControl: false,
                                fullscreenControl: false,
                                mapTypeControl: false,
                                scaleControl: false,
                                streetViewControl: true,
                                streetViewControlOptions: {
                                    position: google.maps.ControlPosition.LEFT_TOP
                                },
                                zoomControl: true,
                                zoomControlOptions: {
                                    position: google.maps.ControlPosition.LEFT_TOP
                                }
                            });
                            var image = 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-red.png';
                            var marker = new google.maps.Marker({
                                position: myLatlng,
                                map: map,
                                icon: image
                            });
                            var contentString = '<h4>' + data.outlet_name + '&nbsp;&nbsp;&nbsp;&nbsp;</h4>' +
                                '<div>' +
                                '<p>' + data.address + '&nbsp;&nbsp;&nbsp;&nbsp;' +
                                '</p>' +
                                // '<div id="pic"><img src="https://www.biruindonesia.com/images/articles/project/puri-indah-financiall-towrerrr.jpg" width="188" height="168" />' +
                                '<input type="submit" id="butSubmit" value="Detail" onclick="detail()"><br><br>' +
                                '</div>';
                            marker.content = contentString;
                            var infoWindow = new google.maps.InfoWindow();
                            google.maps.event.addListener(marker, 'click', function () {
                                infoWindow.setContent(this.content);
                                infoWindow.open(this.getMap(), this);
                            });
                            google.maps.event.addListener(marker, 'click', function () {
                                _this.detailLocation('', '', '', '', '', '', '', '', '');
                            });
                        }
                    }
                ]
            });
            prompt.present();
        }).catch(function (err) {
            console.log('Error', err);
        });
    };
    HomePage.prototype.scanCodex = function (lat, lng, outlet, name, address, note, kode_distributor, kode_sales) {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'Info!',
            message: "Result QR Code Scanner",
            inputs: [
                {
                    name: 'outlet_name',
                    placeholder: 'outlet name',
                    value: name
                },
                {
                    name: 'address',
                    placeholder: 'address',
                    value: address
                },
                {
                    name: 'latitude',
                    placeholder: 'latitude',
                    value: lat
                },
                {
                    name: 'longitude',
                    placeholder: 'longitude',
                    value: lng
                },
                {
                    name: 'note',
                    placeholder: 'note',
                    value: note
                },
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'View',
                    handler: function (data) {
                        console.log('Saved clicked');
                        var myLatlng = { lat: parseFloat(lat), lng: parseFloat(lng) };
                        var map = new google.maps.Map(_this.mapElement.nativeElement, {
                            zoom: 17,
                            center: myLatlng,
                            rotateControl: false,
                            fullscreenControl: false,
                            mapTypeControl: false,
                            scaleControl: false,
                            streetViewControl: true,
                            streetViewControlOptions: {
                                position: google.maps.ControlPosition.LEFT_TOP
                            },
                            zoomControl: true,
                            zoomControlOptions: {
                                position: google.maps.ControlPosition.LEFT_TOP
                            }
                        });
                        // var map = new google.maps.Map(this.mapElement.nativeElement, {
                        //   center: myLatlng,
                        //   zoom: 14
                        // });
                        var image = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';
                        var marker = new google.maps.Marker({
                            position: myLatlng,
                            map: map,
                            icon: image
                        });
                        //untuk botton save
                        //google.maps.event.addListener(marker, 'click', () => {
                        //  this.detailLocation(outlet, lat, lng , name, address,'','');
                        //});
                        var contentString = '<h4>' + data.outlet_name + '&nbsp;&nbsp;&nbsp;&nbsp;</h4>' +
                            '<div>' +
                            '<p>' + data.address + '&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '</p>' +
                            '</div>';
                        marker.content = contentString;
                        var infoWindow = new google.maps.InfoWindow();
                        google.maps.event.addListener(marker, 'click', function () {
                            infoWindow.setContent(this.content);
                            infoWindow.open(this.getMap(), this);
                        });
                        var panorama = new google.maps.StreetViewPanorama(document.getElementById('pano'), {
                            position: myLatlng,
                            fullscreenControl: false,
                            zoomControl: false,
                            panControl: false,
                            pov: {
                                heading: 34,
                                pitch: 10
                            }
                        });
                        map.setStreetView(panorama);
                    }
                }
            ]
        });
        prompt.present();
    };
    HomePage.prototype.detailLocation = function (outlet, lat, lng, name, address, typevalidate, note, kode_distributor, kode_sales) {
        var _this = this;
        var tipe;
        if (typevalidate == '1') {
            tipe = "Validate ";
        }
        else {
            tipe = "No Validate ";
        }
        var prompt = this.alertCtrl.create({
            title: tipe,
            message: tipe + "this location. save to server.",
            buttons: [
                {
                    text: 'Cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Yes',
                    handler: function (data) {
                        var prompts = _this.alertCtrl.create({
                            title: 'Info',
                            message: "Add Note For Information",
                            inputs: [
                                {
                                    name: 'sebab',
                                    placeholder: 'Note Here ...'
                                },
                            ],
                            buttons: [
                                {
                                    text: 'Cancel',
                                    handler: function (data) {
                                        console.log('Cancel clicked');
                                    }
                                },
                                {
                                    text: 'Send',
                                    handler: function (data) {
                                        _this.storage.get('user_session_storage').then(function (res) {
                                            var body = {
                                                outlet: outlet,
                                                kdcabang: res[0].cabang_id,
                                                lat: lat,
                                                lng: lng,
                                                name: name,
                                                address: address,
                                                note: note,
                                                notevalidate: data.sebab,
                                                status: typevalidate,
                                                marker: kode_sales,
                                                kdsales: kode_sales,
                                                validator: res[0].kdsales + '-' + res[0].employee_name,
                                                kode_distributor: kode_distributor
                                            };
                                            _this.postPvdr.postData(body, 'outlet/updateoutlet_fromwhatsapp').subscribe(function (data) {
                                                var alert = _this.alertCtrl.create({
                                                    title: 'Info',
                                                    subTitle: data.subtitle,
                                                    message: data.msg,
                                                    buttons: ['OK']
                                                });
                                                alert.present();
                                                var prompt = _this.alertCtrl.create({
                                                    title: 'Info',
                                                    subTitle: data.subtitle,
                                                    message: data.msg,
                                                    buttons: [
                                                        {
                                                            text: 'Save',
                                                            handler: function (data) {
                                                                _this.navCtrl.pop();
                                                            }
                                                        }
                                                    ]
                                                });
                                                prompt.present();
                                            }, function (error) {
                                                var alert = _this.alertCtrl.create({
                                                    title: 'Info',
                                                    subTitle: error,
                                                    message: 'Not Connected To Server',
                                                    buttons: ['OK']
                                                });
                                                alert.present();
                                            });
                                        });
                                    }
                                }
                            ]
                        });
                        prompts.present();
                    }
                }
            ]
        });
        prompt.present();
    };
    HomePage.prototype.more = function (ev) {
        var popover = this.popoverCtrl.create(PopoverPage);
        popover.present({
            ev: ev
        });
    };
    HomePage.prototype.close = function () {
        this.viewCtrl.dismiss();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('map'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], HomePage.prototype, "mapElement", void 0);
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"D:\ionic\Project\qrcode\src\pages\home\home.html"*/`<ion-content no-padding>\n  <ion-fab right top style="background: transparent;">\n    <button ion-button icon-only clear style="background: transparent;" (click)="close()">\n      <ion-icon name=\'close\' color="dark" (click)="close()"></ion-icon>\n    </button>\n  </ion-fab>\n  <div #map id="map" style="width: 100%;height: 50%;"></div>\n  <div id="pano" style="width: 100%;height: 50%;"></div>\n\n\n  <ion-fab left bottom>\n    <button ion-fab color="danger" (click)="detailLocation(outlet, lat, lng , name, address,2,note)">\n      <ion-icon name="close-circle"></ion-icon>\n    </button>\n  </ion-fab>\n\n  <ion-fab right bottom>\n    <button ion-fab color="oren"\n      (click)="detailLocation(outlet, lat, lng , name, address,1,note,kode_distributor,kode_sales)">\n      <ion-icon name="checkmark-circle"></ion-icon>\n    </button>\n  </ion-fab>\n\n</ion-content>`/*ion-inline-end:"D:\ionic\Project\qrcode\src\pages\home\home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_barcode_scanner__["a" /* BarcodeScanner */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* PopoverController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_6__config_provider__["a" /* Provider */],
            __WEBPACK_IMPORTED_MODULE_8__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_location_accuracy__["a" /* LocationAccuracy */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_android_permissions__["a" /* AndroidPermissions */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */]])
    ], HomePage);
    return HomePage;
}());

var PopoverPage = (function () {
    function PopoverPage(navCtrl, alertCtrl, viewCtrl, modalCtrl) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.viewCtrl = viewCtrl;
        this.modalCtrl = modalCtrl;
    }
    PopoverPage.prototype.ionViewDidLoad = function () {
    };
    PopoverPage.prototype.newOutlet = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_7__pages_formoutlet_formoutlet__["a" /* FormoutletPage */]);
        modal.present();
    };
    PopoverPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"D:\ionic\Project\qrcode\src\pages\home\popover.html"*/`<ion-content>\n  <ion-item-group no-lines>\n    <!-- <ion-item-divider color="primary">All About Me</ion-item-divider>\n    <ion-item>\n      <ion-icon name="body" item-start></ion-icon>My Location\n    </ion-item>\n    <ion-item>\n      <ion-icon name="map" item-start></ion-icon>New Place\n    </ion-item>\n    <ion-item>\n      <ion-icon name="locate" item-start></ion-icon>New Coordinates\n    </ion-item>\n    <ion-item>\n      <ion-icon name="trending-up" item-start></ion-icon>Directions\n    </ion-item>\n    <ion-item>\n      <ion-icon name="walk" item-start></ion-icon>Steps\n    </ion-item>\n    <ion-item>\n      <ion-icon name="megaphone" item-start></ion-icon>Voice Rec\n    </ion-item>\n    <ion-item>\n      <ion-icon name="contacts" item-start></ion-icon>Tracking Sales\n    </ion-item>\n    <ion-item>\n      <ion-icon name="home" item-start></ion-icon>All Outlets\n    </ion-item>\n    <ion-item>\n      <ion-icon name="cart" item-start></ion-icon>All Counters\n    </ion-item> -->\n    <ion-item (click)="newOutlet()">\n      <ion-icon name="pin" item-start></ion-icon>New Outlet\n    </ion-item>\n    <!-- <ion-item>\n      <ion-icon name="barcode" item-start></ion-icon>Scan QR Code\n    </ion-item> -->\n    <ion-item>\n      <ion-icon name="clipboard" item-start></ion-icon>History\n    </ion-item>\n    <ion-item>\n      <ion-icon name="settings" item-start></ion-icon>Setting\n    </ion-item>\n  </ion-item-group>\n\n\n  <table>\n    <tr>\n\n    </tr>\n  </table>\n</ion-content>\n`/*ion-inline-end:"D:\ionic\Project\qrcode\src\pages\home\popover.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */]])
    ], PopoverPage);
    return PopoverPage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 133:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ActivitiesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_provider__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_searchactivities_searchactivities__ = __webpack_require__(134);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var ActivitiesPage = (function () {
    function ActivitiesPage(navCtrl, navParams, postPvdr, modalCtrl, alertCtrl, loadingCtrl, storage) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.postPvdr = postPvdr;
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.storage = storage;
        this.base_url = 'http://api.vci.co.id/public/outlets/';
        this.markers = [];
        this.today = new Date();
        this.tanggal = this.today.getFullYear() + '-' + this.benarMonth((this.today.getMonth() + 1)) + '-' + this.benarDate(this.today.getDate());
    }
    ActivitiesPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ActivitiesPage');
        this.getData();
    };
    ActivitiesPage.prototype.getData = function () {
        var _this = this;
        this.storage.get('user_session_storage').then(function (res) {
            //super user dan nasional
            if (res[0].user_level == '-1' || res[0].user_level == '9') {
                _this.getCari('', '', res[0].user_level, '', _this.tanggal, res[0].username, 'nasional', 5.69);
                _this.nama_distributor = 'Semua Distributor Nasional';
                _this.nama_sales = 'Semua';
                //regional
            }
            else if (res[0].user_level == '6') {
                _this.getCari(res[0].cabang_id, '', res[0].user_level, '', _this.tanggal, res[0].username, 'regional', 7);
                _this.nama_distributor = 'Semua Distributor Regional';
                _this.nama_sales = 'Semua';
                //admin dan kepala distribusi
            }
            else if (res[0].user_level == '2' || res[0].user_level == '10') {
                _this.getCari(res[0].cabang_id, res[0].kdoutlet, res[0].user_level, '', _this.tanggal, res[0].username, 'distributor', 11);
                _this.nama_distributor = res[0].distributor_name;
                _this.nama_sales = 'Semua';
            }
        });
    };
    ActivitiesPage.prototype.getCari = function (kdcabang, kdoutlet, userlevel, kdsales, tanggal, username, area, zoom) {
        var _this = this;
        this.kdcabang = kdcabang;
        this.kdoutlet = kdoutlet;
        this.kdsales = kdsales;
        this.tanggal = tanggal;
        var loader = this.loadingCtrl.create({
            content: "Mohon Menunggu..."
        });
        loader.present();
        var body;
        body = {
            kdcabang: kdcabang,
            userlevel: userlevel,
            kdoutlet: kdoutlet,
            kdsales: kdsales,
            tanggal: tanggal,
            username: username
        };
        this.postPvdr.postData(body, 'outlet/getActivities2').subscribe(function (data) {
            console.log('datax : ', data);
            if (data.success) {
                loader.dismiss();
                var myLatlng;
                if (area == "nasional") {
                    myLatlng = { lat: -0.620093, lng: 115.3094217 };
                }
                else if (area == "regional") {
                    myLatlng = { lat: parseFloat(data.result[0]['Lat']), lng: parseFloat(data.result[0]['Lng']) };
                }
                else if (area == "distributor") {
                    myLatlng = { lat: parseFloat(data.result[0]['Lat']), lng: parseFloat(data.result[0]['Lng']) };
                }
                var map = new google.maps.Map(_this.mapElement.nativeElement, {
                    zoom: zoom,
                    center: myLatlng,
                    zoomControl: false,
                    mapTypeControl: false,
                    scaleControl: false,
                    streetViewControl: false,
                    rotateControl: false,
                    fullscreenControl: false
                });
                var lat_from = void 0;
                var lng_from = void 0;
                var lat_from2 = void 0;
                var lng_from2 = void 0;
                _this.animation_directions = [];
                for (var i = 0; i < data.result.length; i++) {
                    //pilih icon
                    var icon;
                    if (data.result[i]['Status'] == '0') {
                        icon = 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-green.png';
                    }
                    else if (data.result[i]['Status'] == '1') {
                        icon = 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-blue.png';
                    }
                    else if (data.result[i]['Status'] == '2') {
                        icon = 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-red.png';
                    }
                    var Latlng = { lat: parseFloat(data.result[i]['Lat']), lng: parseFloat(data.result[i]['Lng']) };
                    //marker
                    var marker = new google.maps.Marker({
                        position: Latlng,
                        animation: google.maps.Animation.DROP,
                        map: map,
                        icon: icon
                    });
                    _this.markers.push(marker);
                    //start rute --------------------------------------------------------
                    // lat_from2 = parseFloat(data.result[i]['Lat']);
                    // lng_from2 = parseFloat(data.result[i]['Lng']);
                    // if ((lat_from2 != lat_from && lng_from2 != lng_from)) {
                    //   this.animation_directions.push(
                    //     {
                    //       lat: parseFloat(data.result[i]['Lat']),
                    //       lng: parseFloat(data.result[i]['Lng'])
                    //     });
                    // }
                    // lat_from = parseFloat(data.result[i]['Lat']);
                    // lng_from = parseFloat(data.result[i]['Lng']);
                    var contentString = '<h4>' + data.result[i]['OutletName'] + '&nbsp;&nbsp;</h4>' +
                        '<div>' +
                        '<p>' + data.result[i]['Address'] + '&nbsp;&nbsp;' + '<br>' +
                        'Sales : ' + data.result[i]['UserUpload'] + '&nbsp;&nbsp;' + '<br>' +
                        'Tanggal Upload : ' + data.result[i]['DateUpload'] + '&nbsp;&nbsp;' + '<br>' +
                        'Catatan Sales :' + data.result[i]['Note'] + '&nbsp;&nbsp;' + '<br>' +
                        'Tanggal Valid : ' + data.result[i]['DateValidate'] + '&nbsp;&nbsp;' + '<br>' +
                        'Catatan Valid : ' + data.result[i]['NoteValidator'] + '&nbsp;&nbsp;' + '</p>' +
                        '<div id="pic"><img src="' + _this.base_url + data.result[i]['Images'] + '" width="200" height="250" />' +
                        '</div>';
                    marker.content = contentString;
                    var infoWindow = new google.maps.InfoWindow();
                    // google.maps.event.addListener(marker, 'mouseover', function () {
                    google.maps.event.addListener(marker, 'click', function () {
                        infoWindow.setContent(this.content);
                        infoWindow.open(this.getMap(), this);
                    });
                }
                // var lineSymbol = {
                //   path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
                //   scale: 5,
                //   strokeColor: '#393'
                // };
                // var line = new google.maps.Polyline({
                //   path: this.animation_directions,
                //   icons: [{
                //     icon: lineSymbol,
                //     offset: '100%'
                //   }],
                //   map: map
                // });
                // var count = 0;
                // window.setInterval(function () {
                //   count = (count + 1) % 200;
                //   var icons = line.get('icons');
                //   icons[0].offset = (count / 2) + '%';
                //   line.set('icons', icons);
                // }, 50);
                //end rute -----------------------------
            }
            else {
                loader.dismiss();
                var alert_1 = _this.alertCtrl.create({
                    title: 'Info',
                    subTitle: 'Mohon Maaf Data Tidak Di Temukan',
                    message: '',
                    buttons: ['OK']
                });
                alert_1.present();
                _this.initMap();
                return false;
            }
        }, function (error) {
            loader.dismiss();
            var alert = _this.alertCtrl.create({
                title: 'Mohon Maaf',
                subTitle: 'Sedang tidak terkoneksi dengan server. Silahkan coba kembali beberapa saat',
                buttons: ['OK']
            });
            alert.present();
            _this.initMap();
            return false;
        });
    };
    ActivitiesPage.prototype.initMap = function () {
        var myLatlng = { lat: -0.620093, lng: 115.3094217 };
        var map = new google.maps.Map(this.mapElement.nativeElement, {
            zoom: 5.69,
            center: myLatlng,
            zoomControl: false,
            mapTypeControl: false,
            scaleControl: false,
            streetViewControl: false,
            rotateControl: false,
            fullscreenControl: false
        });
    };
    ActivitiesPage.prototype.filter = function () {
        var _this = this;
        var area;
        var zooom;
        this.storage.get('user_session_storage').then(function (res) {
            if (res[0].user_level == '-1' || res[0].user_level == '9') {
                var modal = _this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__pages_searchactivities_searchactivities__["a" /* SearchactivitiesPage */], { kdcabang: _this.kdcabang, kdsales: _this.kdsales, kdoutlet: _this.kdoutlet, distributor_name: _this.nama_distributor, sales_name: _this.nama_sales, date_search: _this.tanggal });
                modal.present();
                modal.onDidDismiss(function (val) {
                    console.log('test : ', val);
                    if (val) {
                        if (val.sales_name == 'Semua' && val.distributor_name == "Semua Distributor Nasional") {
                            _this.kdsales = val.kdsales;
                            _this.nama_sales = val.sales_name;
                            _this.kdcabang = val.kdcabang;
                            _this.kdoutlet = val.kdoutlet;
                            _this.nama_distributor = val.distributor_name;
                            _this.tanggal = val.date_search;
                            area = "nasional";
                            zooom = 5.69;
                        }
                        else {
                            _this.kdsales = val.kdsales;
                            _this.nama_sales = val.sales_name;
                            _this.kdcabang = val.kdcabang;
                            _this.kdoutlet = val.kdoutlet;
                            _this.nama_distributor = val.distributor_name;
                            _this.tanggal = val.date_search;
                            area = "distributor";
                            zooom = 11;
                        }
                        _this.getCari(_this.kdcabang, _this.kdoutlet, res[0].user_level, _this.kdsales, _this.tanggal, res[0].username, area, zooom);
                        _this.clearMarker();
                    }
                    else {
                        //coding if else or empty
                    }
                });
            }
            if (res[0].user_level == '6') {
                var modal = _this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__pages_searchactivities_searchactivities__["a" /* SearchactivitiesPage */], { kdcabang: _this.kdcabang, kdsales: _this.kdsales, kdoutlet: _this.kdoutlet, distributor_name: _this.nama_distributor, sales_name: _this.nama_sales, date_search: _this.tanggal });
                modal.present();
                modal.onDidDismiss(function (val) {
                    console.log('test : ', val);
                    if (val) {
                        if (val.sales_name != 'Semua') {
                            _this.kdsales = val.kdsales;
                            _this.nama_sales = val.sales_name;
                            _this.kdcabang = val.kdcabang;
                            _this.kdoutlet = val.kdoutlet;
                            _this.nama_distributor = val.distributor_name;
                            _this.tanggal = val.date_search;
                            area = "distributor";
                            zooom = 11;
                        }
                        else {
                            _this.kdsales = val.kdsales;
                            _this.nama_sales = val.sales_name;
                            _this.kdcabang = val.kdcabang;
                            _this.kdoutlet = val.kdoutlet;
                            _this.nama_distributor = val.distributor_name;
                            _this.tanggal = val.date_search;
                            area = "regional";
                            zooom = 7;
                        }
                        _this.getCari(_this.kdcabang, _this.kdoutlet, res[0].user_level, _this.kdsales, _this.tanggal, res[0].username, area, zooom);
                        _this.clearMarker();
                    }
                    else {
                        //coding if else or empty
                    }
                });
            }
            if (res[0].user_level == '2' || res[0].user_level == '10') {
                var modal = _this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__pages_searchactivities_searchactivities__["a" /* SearchactivitiesPage */], { kdcabang: _this.kdcabang, kdsales: _this.kdsales, kdoutlet: _this.kdoutlet, distributor_name: _this.nama_distributor, sales_name: _this.nama_sales, date_search: _this.tanggal });
                modal.present();
                modal.onDidDismiss(function (val) {
                    console.log('test : ', val);
                    if (val) {
                        if (val.sales_name != 'Semua') {
                            _this.kdsales = val.kdsales;
                            _this.nama_sales = val.sales_name;
                            _this.kdcabang = val.kdcabang;
                            _this.kdoutlet = val.kdoutlet;
                            _this.nama_distributor = val.distributor_name;
                            _this.tanggal = val.date_search;
                            area = "distributor";
                            zooom = 11;
                        }
                        else {
                            _this.kdsales = val.kdsales;
                            _this.nama_sales = val.sales_name;
                            _this.kdcabang = val.kdcabang;
                            _this.kdoutlet = val.kdoutlet;
                            _this.nama_distributor = val.distributor_name;
                            _this.tanggal = val.date_search;
                            area = "distributor";
                            zooom = 11;
                        }
                        _this.getCari(_this.kdcabang, _this.kdoutlet, res[0].user_level, _this.kdsales, _this.tanggal, res[0].username, area, zooom);
                        _this.clearMarker();
                    }
                    else {
                        //coding if else or empty
                    }
                });
            }
        });
    };
    ActivitiesPage.prototype.clearMarker = function () {
        this.setMapOnAll(null);
    };
    ActivitiesPage.prototype.setMapOnAll = function (map) {
        for (var i = 0; i < this.markers.length; i++) {
            this.markers[i].setMap(map);
        }
    };
    ActivitiesPage.prototype.tampilkan = function () {
        document.getElementById('legend').style.display = '';
        document.getElementById('fab').style.display = 'none';
    };
    ActivitiesPage.prototype.sembunyikan = function () {
        document.getElementById('legend').style.display = 'none';
        document.getElementById('fab').style.display = '';
    };
    ActivitiesPage.prototype.benarMonth = function (m) {
        if (m * 1 < 10) {
            m = '0' + m;
        }
        else {
            m = m;
        }
        return m;
    };
    ActivitiesPage.prototype.benarDate = function (d) {
        if (d * 1 < 10) {
            d = '0' + d;
        }
        else {
            d = d;
        }
        return d;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('map'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], ActivitiesPage.prototype, "mapElement", void 0);
    ActivitiesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-activities',template:/*ion-inline-start:"D:\ionic\Project\qrcode\src\pages\activities\activities.html"*/`<!--\n  Generated template for the ActivitiesPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="oren">\n    <ion-title>Aktivitas Sales</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content no-padding class="bgc">\n  <div #map id="map" style="width: 100%;height: 100%;"></div>\n\n  <div ion-fixed id="legend" style="\n      position: absolute;\n      right:3px;\n      bottom: 25px;\n      width: auto;\n      height: auto;">\n    <div id="more_options">\n      <table style="border:0px solid black;">\n        <tr>\n          <td>\n            <ion-card>\n\n              <ion-item-group>\n                <ion-list no-border>\n                  <ion-item-divider color="light">\n                    <table style="width: 100%;">\n                      <tr>\n                        <td>\n                          <table style="width: 33%;">\n                            <tr>\n                              <td style="width: 100%;">\n                                <div\n                                  style="color:#f4f5f8;background-color: #d0465d;border-radius: 50%; width: 10px;height: 10px;">\n                                  .\n                                </div>\n                              </td>\n                              <td style="width: 100%;">&nbsp;&nbsp;Reject</td>\n                            </tr>\n                          </table>\n                        </td>\n                        <td>\n                          <table style="width: 33%;">\n                            <tr>\n                              <td style="width: 100%;">\n                                <div\n                                  style="color:#f4f5f8;background-color: #3abc30;border-radius: 50%; width: 10px;height: 10px;">\n                                  .\n                                </div>\n                              </td>\n                              <td style="width: 100%;">&nbsp;&nbsp;Pending</td>\n                            </tr>\n                          </table>\n                        </td>\n                        <td>\n                          <table style="width: 33%;">\n                            <tr>\n                              <td style="width: 100%;">\n                                <div\n                                  style="color:#f4f5f8;background-color: #4697d0;border-radius: 50%; width: 10px;height: 10px;">\n                                  .\n                                </div>\n                              </td>\n                              <td style="width: 100%;">&nbsp;&nbsp;Validate</td>\n                            </tr>\n                          </table>\n                        </td>\n                      </tr>\n                    </table>\n                  </ion-item-divider>\n                </ion-list>\n              </ion-item-group>\n\n              <ion-item>\n                <h2>{{nama_distributor}}</h2>\n                <table style="width: 100%;">\n                  <tr>\n                    <td style="width: 15%;">\n                      <h3>Tanggal</h3>\n                    </td>\n                    <td style="width: 85%;">\n                      <p>: {{tanggal}}</p>\n                    </td>\n                  </tr>\n                  <tr>\n                    <td style="width: 15%;">\n                      <h3>Sales</h3>\n                    </td>\n                    <td style="width: 85%;">\n                      <p>: {{nama_sales}}</p>\n                    </td>\n                  </tr>\n                </table>\n                <!-- <h3>Sales : {{title}}</h3>\n                  <p>Toko : Semua</p> -->\n              </ion-item>\n\n              <ion-card-content>\n                <p style="color:#ffffff">\n                  <!-- Wait a minute. Wait a minute, Doc. <br> -->\n                  Uhhh... Are you telling me that you built a time machine... <br>\n                  <!-- out of a DeLorean?! Whoa. This is heavy. -->\n                </p>\n              </ion-card-content>\n\n              <ion-grid>\n                <ion-row>\n                  <ion-col col-8>\n                    <div><button ion-button color="primary" outline block (click)="filter()">FILTER</button></div>\n                  </ion-col>\n                  <ion-col col-4>\n                    <div><button ion-button color="danger" outline block (click)="sembunyikan()">SEMBUNYI</button></div>\n                  </ion-col>\n                </ion-row>\n              </ion-grid>\n\n            </ion-card>\n          </td>\n        </tr>\n      </table>\n    </div>\n  </div>\n\n\n  <ion-fab right bottom id="fab" style="display: none;">\n    <button ion-fab color="danger" (click)="tampilkan()">\n      <ion-icon name="clipboard"></ion-icon>\n    </button>\n  </ion-fab>\n</ion-content>`/*ion-inline-end:"D:\ionic\Project\qrcode\src\pages\activities\activities.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__config_provider__["a" /* Provider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */]])
    ], ActivitiesPage);
    return ActivitiesPage;
}());

//# sourceMappingURL=activities.js.map

/***/ }),

/***/ 134:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchactivitiesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_provider__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_usersdistributor_usersdistributor__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_detaillistings_detaillistings__ = __webpack_require__(44);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the SearchactivitiesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SearchactivitiesPage = (function () {
    function SearchactivitiesPage(navCtrl, viewCtrl, postPvdr, storage, modalCtrl, navParams) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.postPvdr = postPvdr;
        this.storage = storage;
        this.modalCtrl = modalCtrl;
        this.navParams = navParams;
        this.distributor_name = navParams.get("distributor_name");
        this.kdcabang = navParams.get("kdcabang");
        this.kdoutlet = navParams.get("kdoutlet");
        this.kdsales = navParams.get("kdsales");
        this.sales_name = navParams.get("sales_name");
        this.date_search = navParams.get("date_search");
        this.storage.get('user_session_storage').then(function (res) {
            if (res[0].user_level == '2' || res[0].user_level == '10') {
                _this.cari_distribusi = "No";
                _this.cari_sales = "Yes";
            }
            else {
                _this.cari_distribusi = "Yes";
                _this.cari_sales = "Yes";
            }
        });
    }
    SearchactivitiesPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SearchactivitiesPage');
    };
    SearchactivitiesPage.prototype.close = function () {
        this.viewCtrl.dismiss();
    };
    SearchactivitiesPage.prototype.search_distributor = function (kdoutlet, kdcabang) {
        var _this = this;
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_5__pages_detaillistings_detaillistings__["a" /* DetaillistingsPage */], { kdoutlet: kdoutlet, kdcabang: kdcabang, storages: 'No', close: 'Yes', bisa_pilih: 'Yes' });
        modal.present();
        modal.onDidDismiss(function (val) {
            console.log('val ', val);
            if (val) {
                _this.distributor_name = val.distributor_name;
                _this.kdoutlet = val.kdoutlet;
                _this.kdsales = '';
                _this.sales_name = 'Semua';
            }
            else {
                //coding if else or empty
            }
        });
    };
    SearchactivitiesPage.prototype.search_sales = function (kdoutlet, kdcabang) {
        var _this = this;
        // untuk mencari toko
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__pages_usersdistributor_usersdistributor__["a" /* UsersdistributorPage */], { kdoutlet: kdoutlet, kdcabang: kdcabang, performa: 'No', close: 'Yes', bisa_pilih: 'Yes', reset_password: 'No' });
        modal.present();
        modal.onDidDismiss(function (val) {
            if (val) {
                _this.kdsales = val.kdsales;
                _this.sales_name = val.sales_name;
            }
            else {
                //coding if else or empty
            }
        });
    };
    SearchactivitiesPage.prototype.terapkan = function () {
        this.viewCtrl.dismiss({ kdcabang: this.kdcabang, kdoutlet: this.kdoutlet, distributor_name: this.distributor_name, kdsales: this.kdsales, sales_name: this.sales_name, date_search: this.date_search });
    };
    SearchactivitiesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-searchactivities',template:/*ion-inline-start:"D:\ionic\Project\qrcode\src\pages\searchactivities\searchactivities.html"*/`<ion-header>\n  <ion-navbar color="oren">\n    <ion-title>Filter</ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="close()">\n        <ion-icon name="close-circle"></ion-icon>&nbsp;&nbsp;\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content class="bgc">\n  <ion-grid fixed class="wrapper">\n\n    <ion-card>\n      <ion-item-divider color="light-shade">Filter Tanggal, Distributor atau Sales\n      </ion-item-divider>\n      <ion-card-content>\n        <ion-list inset>\n          <ion-item>\n            <ion-label>Tanggal : </ion-label>\n            <ion-datetime placeholder="Cari Tanggal..." [(ngModel)]="date_search" displayFormat="YYYY-MM-DD"\n              min="2020-03-01" max="2020-12-31">\n            </ion-datetime>\n          </ion-item>\n          <ion-item-divider color="white">Distributor : {{distributor_name}}<ion-icon\n              (click)="search_distributor(kdoutlet, kdcabang)" name="search" item-end *ngIf="cari_distribusi==\'Yes\'">\n            </ion-icon>\n          </ion-item-divider>\n          <ion-item style="display: none;">\n            <ion-input type="text" placeholder="Distributor Yang Dipilih" [(ngModel)]="kdoutlet"></ion-input>\n          </ion-item>\n          <ion-item style="display: none;">\n            <ion-input readonly type="text" placeholder="Distributor Yang Dipilih" [(ngModel)]="distributor_name">\n            </ion-input>\n          </ion-item>\n          <ion-item-divider color="white">Sales : {{sales_name}}<ion-icon (click)="search_sales(kdoutlet, kdcabang)"\n              name="search" item-end *ngIf="cari_sales==\'Yes\'"></ion-icon>\n          </ion-item-divider>\n          <ion-item style="display: none;">\n            <ion-input type="text" placeholder="Sales Yang Dipilih" [(ngModel)]="kdsales"></ion-input>\n          </ion-item>\n          <ion-item style="display: none;">\n            <ion-input readonly type="text" placeholder="Sales Yang Dipilih" [(ngModel)]="sales_name"></ion-input>\n          </ion-item>\n          <br><br>\n          <button ion-button color="primary" outline block (click)="terapkan()">terapkan</button>\n        </ion-list>\n      </ion-card-content>\n    </ion-card>\n\n  </ion-grid>\n</ion-content>`/*ion-inline-end:"D:\ionic\Project\qrcode\src\pages\searchactivities\searchactivities.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_2__config_provider__["a" /* Provider */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], SearchactivitiesPage);
    return SearchactivitiesPage;
}());

//# sourceMappingURL=searchactivities.js.map

/***/ }),

/***/ 135:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_provider__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_usersdistributor_usersdistributor__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_detaillistings_detaillistings__ = __webpack_require__(44);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










/**
 * Generated class for the SearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SearchPage = (function () {
    function SearchPage(navCtrl, viewCtrl, alertCtrl, postPvdr, storage, modalCtrl, loadingCtrl, navParams) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.alertCtrl = alertCtrl;
        this.postPvdr = postPvdr;
        this.storage = storage;
        this.modalCtrl = modalCtrl;
        this.loadingCtrl = loadingCtrl;
        this.navParams = navParams;
        this.distributor_name = navParams.get("distributor_name");
        this.kdcabang = navParams.get("kdcabang");
        this.kdoutlet = navParams.get("kdoutlet");
        this.kdsales = navParams.get("kdsales");
        this.sales_name = navParams.get("sales_name");
        this.storage.get('user_session_storage').then(function (res) {
            if (res[0].user_level == '2' || res[0].user_level == '10') {
                _this.cari_distribusi = "No";
                _this.cari_sales = "Yes";
            }
            else {
                _this.cari_distribusi = "Yes";
                _this.cari_sales = "Yes";
            }
        });
    }
    SearchPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SearchPage');
    };
    SearchPage.prototype.close = function () {
        this.viewCtrl.dismiss();
    };
    SearchPage.prototype.search_distributor = function (kdoutlet, kdcabang) {
        var _this = this;
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_5__pages_detaillistings_detaillistings__["a" /* DetaillistingsPage */], { kdoutlet: kdoutlet, kdcabang: kdcabang, storages: 'No', close: 'Yes', bisa_pilih: 'Yes' });
        modal.present();
        modal.onDidDismiss(function (val) {
            console.log('val ', val);
            if (val) {
                _this.distributor_name = val.distributor_name;
                _this.kdoutlet = val.kdoutlet;
                _this.kdsales = '';
                _this.sales_name = 'Semua';
            }
            else {
                //coding if else or empty
            }
        });
    };
    SearchPage.prototype.search_sales = function (kdoutlet, kdcabang) {
        var _this = this;
        // untuk mencari toko
        // const modal = this.modalCtrl.create(DetailbangetlistingsPage, { kdoutlet: kdoutlet, kdcabang: kdcabang, kdsales: '', viewuser: 'No', title: '', close: 'Yes', bisa_pilih: 'Yes' });
        // modal.present();
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__pages_usersdistributor_usersdistributor__["a" /* UsersdistributorPage */], { kdoutlet: kdoutlet, kdcabang: kdcabang, performa: 'No', close: 'Yes', bisa_pilih: 'Yes', reset_password: 'No' });
        modal.present();
        modal.onDidDismiss(function (val) {
            if (val) {
                _this.kdsales = val.kdsales;
                _this.sales_name = val.sales_name;
            }
            else {
                //coding if else or empty
            }
        });
    };
    SearchPage.prototype.terapkan = function () {
        this.viewCtrl.dismiss({ kdcabang: this.kdcabang, kdoutlet: this.kdoutlet, distributor_name: this.distributor_name, kdsales: this.kdsales, sales_name: this.sales_name });
    };
    SearchPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-search',template:/*ion-inline-start:"D:\ionic\Project\qrcode\src\pages\search\search.html"*/`<!--\n  Generated template for the SearchPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar color="oren">\n    <ion-title>Filter</ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="close()">\n        <ion-icon name="close-circle"></ion-icon>&nbsp;&nbsp;\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content class="bgc">\n  <ion-grid fixed class="wrapper">\n\n    <ion-card>\n      <ion-item-divider color="light-shade">Filter Distributor atau Sales\n      </ion-item-divider>\n      <ion-card-content>\n        <ion-list inset>\n          <ion-item-divider color="white">Distributor : {{distributor_name}}<ion-icon\n              (click)="search_distributor(kdoutlet, kdcabang)" name="search" item-end *ngIf="cari_distribusi==\'Yes\'">\n            </ion-icon>\n          </ion-item-divider>\n          <ion-item style="display: none;">\n            <ion-input type="text" placeholder="Distributor Yang Dipilih" [(ngModel)]="kdoutlet"></ion-input>\n          </ion-item>\n          <ion-item style="display: none;">\n            <ion-input readonly type="text" placeholder="Distributor Yang Dipilih" [(ngModel)]="distributor_name">\n            </ion-input>\n          </ion-item>\n          <ion-item-divider color="white">Sales : {{sales_name}}<ion-icon (click)="search_sales(kdoutlet, kdcabang)"\n              name="search" item-end *ngIf="cari_sales==\'Yes\'"></ion-icon>\n          </ion-item-divider>\n          <ion-item style="display: none;">\n            <ion-input type="text" placeholder="Sales Yang Dipilih" [(ngModel)]="kdsales"></ion-input>\n          </ion-item>\n          <ion-item style="display: none;">\n            <ion-input readonly type="text" placeholder="Sales Yang Dipilih" [(ngModel)]="sales_name"></ion-input>\n          </ion-item>\n          <br><br>\n          <button ion-button color="primary" outline block (click)="terapkan()">terapkan</button>\n        </ion-list>\n      </ion-card-content>\n    </ion-card>\n  </ion-grid>\n</ion-content>`/*ion-inline-end:"D:\ionic\Project\qrcode\src\pages\search\search.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2__config_provider__["a" /* Provider */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], SearchPage);
    return SearchPage;
}());

//# sourceMappingURL=search.js.map

/***/ }),

/***/ 136:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PetanooPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_provider__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the PetanooPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PetanooPage = (function () {
    function PetanooPage(navCtrl, storage, postPvdr, alertCtrl, toastCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.postPvdr = postPvdr;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.navParams = navParams;
        this.lat = parseFloat(navParams.get("lat"));
        this.lng = parseFloat(navParams.get("lng"));
        this.name_noo = navParams.get("name_noo");
    }
    PetanooPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PetanooPage');
        // this.initMap();
        this.radius_setup(this.lat, this.lng);
    };
    PetanooPage.prototype.radius_setup = function (x, y) {
        this.outlet_radius(x, y, 3000);
    };
    PetanooPage.prototype.outlet_radius = function (x, y, r) {
        var rad = parseFloat(r); //meters
        var theLat = parseFloat(x); //decimal degrees
        var theLng = parseFloat(y); //decimal degrees
        var yMin = theLat - (0.0000065 * rad);
        var xMin = theLng - (-0.0000065 * rad);
        var yMax = theLat + (0.0000065 * rad);
        var xMax = theLng + (-0.0000065 * rad);
        this.allRadiusRectangleOutletsales(yMin, yMax, xMin, xMax, x, y, r);
    };
    PetanooPage.prototype.allRadiusRectangleOutletsales = function (j, k, l, m, x, y, r) {
        var _this = this;
        var body = {
            j: j,
            k: k,
            l: l,
            m: m
        };
        this.postPvdr.postData(body, 'outlet/getAnalyticsNoo').subscribe(function (data) {
            var icon;
            var alloutlet = [];
            if (data.success) {
                for (var i = 0; i < data.result.length; i++) {
                    //pilih icon
                    icon = 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-red.png';
                    alloutlet.push({
                        lat: data.result[i]['Lat'],
                        lng: data.result[i]['Lng'],
                        outletname: data.result[i]['OutletName'],
                        namasalesman: '',
                        lastVisit: '',
                        alamattoko: data.result[i]['Address'],
                        icon: icon
                    });
                }
                _this.circle(x, y, r, alloutlet);
            }
            else {
                _this.circle(x, y, r, '');
                var alert_1 = _this.alertCtrl.create({
                    title: 'Maaf',
                    subTitle: 'Di Area ini tidak ada toko kita.',
                    buttons: ['OK']
                });
                alert_1.present();
            }
        }, function (error) {
            var alert = _this.alertCtrl.create({
                title: 'Opps I am Sorry',
                subTitle: 'Not Connected To Server',
                buttons: ['OK']
            });
            alert.present();
        });
    };
    PetanooPage.prototype.circle = function (x, y, r, alloutlet) {
        var _this = this;
        var latitude;
        var longitude;
        var myLatlng = { lat: x, lng: y };
        var map = new google.maps.Map(this.mapElement.nativeElement, {
            zoom: 13,
            center: myLatlng,
            rotateControl: false,
            fullscreenControl: false,
            mapTypeControl: false,
            scaleControl: false,
            streetViewControl: true,
            streetViewControlOptions: {
                position: google.maps.ControlPosition.LEFT_TOP
            },
            zoomControl: true,
            zoomControlOptions: {
                position: google.maps.ControlPosition.LEFT_TOP
            }
        });
        var tanda = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';
        //market khusus si NOO nya sendiri neglik
        var marker = new google.maps.Marker({
            position: myLatlng,
            animation: google.maps.Animation.DROP,
            map: map,
            icon: tanda
        });
        var contentStringz = '<h4>' + this.name_noo + '&nbsp;&nbsp;&nbsp;&nbsp;</h4>' +
            '<div>' +
            '<p>Toko Potensial&nbsp;&nbsp;&nbsp;&nbsp;' +
            '</p>' +
            '</div>';
        marker.content = contentStringz;
        var infoWindow = new google.maps.InfoWindow();
        google.maps.event.addListener(marker, 'click', function () {
            infoWindow.setContent(this.content);
            infoWindow.open(this.getMap(), this);
        });
        for (var i = 0; i < alloutlet.length; i++) {
            latitude = parseFloat(alloutlet[i]['lat']);
            longitude = parseFloat(alloutlet[i]['lng']);
            var latlng = { lat: latitude, lng: longitude };
            var marker = new google.maps.Marker({
                position: latlng,
                map: map,
                icon: alloutlet[i]['icon']
            });
            var contentString = '<h4>' + alloutlet[i]['outletname'] + '&nbsp;&nbsp;&nbsp;&nbsp;</h4>' +
                '<div>' +
                '<p>' + alloutlet[i]['alamattoko'] + '&nbsp;&nbsp;&nbsp;&nbsp;' +
                '</p>' +
                '</div>';
            marker.content = contentString;
            var infoWindow = new google.maps.InfoWindow();
            google.maps.event.addListener(marker, 'click', function () {
                infoWindow.setContent(this.content);
                infoWindow.open(this.getMap(), this);
            });
        }
        var cityCircle = new google.maps.Circle({
            strokeColor: '#FF0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#FF0000',
            fillOpacity: 0.35,
            map: map,
            center: myLatlng,
            radius: r
        });
        setTimeout(function () {
            var toast = _this.toastCtrl.create({
                message: 'Toko Petonsial Dalam Radius 3 Km',
                showCloseButton: true,
                closeButtonText: 'Ok'
            });
            toast.present();
        }, 1000);
    };
    PetanooPage.prototype.initMap = function () {
        var myLatlng = { lat: this.lat, lng: this.lng };
        var map = new google.maps.Map(this.mapElement.nativeElement, {
            zoom: 13,
            center: myLatlng,
            zoomControl: false,
            mapTypeControl: false,
            scaleControl: false,
            streetViewControl: false,
            rotateControl: false,
            fullscreenControl: false
        });
        //marker
        var marker = new google.maps.Marker({
            position: myLatlng,
            animation: google.maps.Animation.DROP,
            map: map
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('map'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], PetanooPage.prototype, "mapElement", void 0);
    PetanooPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-petanoo',template:/*ion-inline-start:"D:\ionic\Project\qrcode\src\pages\petanoo\petanoo.html"*/`<!--\n  Generated template for the PetanooPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="oren">\n    <ion-title>Peta {{name_noo}}</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content no-padding>\n  <div #map id="map" style="width: 100%;height: 100%;"></div>\n</ion-content>`/*ion-inline-end:"D:\ionic\Project\qrcode\src\pages\petanoo\petanoo.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_2__config_provider__["a" /* Provider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], PetanooPage);
    return PetanooPage;
}());

//# sourceMappingURL=petanoo.js.map

/***/ }),

/***/ 137:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_detaillocationoutlet_detaillocationoutlet__ = __webpack_require__(328);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_provider__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the AdminPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AdminPage = (function () {
    function AdminPage(viewCtrl, postPvdr, storage, alertCtrl, modalCtrl, loadingCtrl, navCtrl, navParams) {
        this.viewCtrl = viewCtrl;
        this.postPvdr = postPvdr;
        this.storage = storage;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.loadingCtrl = loadingCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.filter = 'all';
        this.pending = 0;
        this.validate = 0;
        this.reject = 0;
    }
    AdminPage.prototype.ionViewDidLoad = function () {
        this.listOutlet = new Array();
        this.getData('', 0, 10, 'all', 'No');
    };
    AdminPage.prototype.form_search = function () {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'Cari',
            message: "Masukan kata untuk mencari kode atau nama toko",
            inputs: [
                {
                    name: 'src',
                    placeholder: 'Apa yang anda cari...'
                },
            ],
            buttons: [
                {
                    text: 'Batal',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Cari',
                    handler: function (data) {
                        console.log('Saved clicked');
                        _this.getListOutletsAdmin(data.src);
                    }
                }
            ]
        });
        prompt.present();
    };
    AdminPage.prototype.getListOutletsAdmin = function (src) {
        this.listOutlet = new Array();
        if (src) {
            src = src;
        }
        else {
            src = '';
        }
        this.getData(src, 0, 10, 'all', 'No');
    };
    AdminPage.prototype.getData = function (src, first, offset, filter, infinite) {
        var _this = this;
        this.first = first;
        this.filter = filter;
        this.src = src;
        var loader = this.loadingCtrl.create({
            content: "Mohon Menunggu..."
        });
        if (infinite == 'No') {
            loader.present();
        }
        this.storage.get('user_session_storage').then(function (res) {
            var body = {
                kdcabang: res[0].cabang_id,
                kdoutlet: res[0].kdoutlet,
                user_level: res[0].user_level,
                first: first,
                offset: offset,
                filter: filter,
                keyword: src
            };
            _this.postPvdr.postData(body, 'outlet/gettransgocheck2').subscribe(function (data) {
                if (data.success) {
                    loader.dismiss();
                    _this.pending = data.pending;
                    _this.validate = data.validate;
                    _this.reject = data.reject;
                    for (var i = 0; i < data.result.length; i++) {
                        var tipedata;
                        if (data.result[i]['TypeData'] == 'S') {
                            tipedata = "Server";
                        }
                        else {
                            tipedata = "WhatsApp";
                        }
                        _this.listOutlet.push({
                            id: data.result[i]['id'],
                            KdOutlet: data.result[i]['KdOutlet'],
                            OutletName: data.result[i]['OutletName'],
                            Address: data.result[i]['Address'],
                            UserUpload: data.result[i]['UserUpload'],
                            DateUpload: data.result[i]['DateUpload'],
                            UserValidate: data.result[i]['UserValidate'],
                            DateValidate: data.result[i]['DateValidate'],
                            Lat: data.result[i]['Lat'],
                            Lng: data.result[i]['Lng'],
                            Status: data.result[i]['Status'],
                            Images: data.result[i]['Images'],
                            tipedata: tipedata,
                            Note: data.result[i]['Note'],
                            NoteValidator: data.result[i]['NoteValidator']
                        });
                    }
                }
                else {
                    loader.dismiss();
                    if (infinite == 'No') {
                        var alert_1 = _this.alertCtrl.create({
                            title: 'Info',
                            subTitle: 'Mohon Maaf Data Tidak Di Temukan',
                            message: '',
                            buttons: ['OK']
                        });
                        alert_1.present();
                    }
                    return false;
                }
            }, function (error) {
                loader.dismiss();
                var alert = _this.alertCtrl.create({
                    title: 'Mohon Maaf',
                    subTitle: 'Sedang tidak terkoneksi dengan server. Silahkan coba kembali beberapa saat',
                    buttons: ['OK']
                });
                alert.present();
                return false;
            });
        });
    };
    AdminPage.prototype.detaillist = function (id, lat, lng, outlet, name, address, images) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__pages_detaillocationoutlet_detaillocationoutlet__["a" /* DetaillocationoutletPage */], { id: id, lat: lat, lng: lng, outlet: outlet, name: name, address: address, images: images });
    };
    AdminPage.prototype.doRefresh = function (refresher) {
        this.listOutlet = new Array();
        this.getData('', 0, 10, 'all', 'No');
        setTimeout(function () {
            refresher.complete();
        }, 1000);
    };
    AdminPage.prototype.doInfinite = function (infiniteScroll, src, first, filter) {
        var _this = this;
        first = first + 10;
        console.log('Begin async operation');
        setTimeout(function () {
            _this.getData(src, first, 10, filter, 'Yes');
            infiniteScroll.complete();
        }, 1000);
    };
    AdminPage.prototype.filtering = function () {
        var _this = this;
        var alert = this.alertCtrl.create();
        alert.setTitle('Filter');
        alert.addInput({
            type: 'radio',
            label: 'All',
            value: 'all',
            checked: false
        });
        alert.addInput({
            type: 'radio',
            label: 'Pending',
            value: 'pending',
            checked: false
        });
        alert.addInput({
            type: 'radio',
            label: 'Validate',
            value: 'validate',
            checked: false
        });
        alert.addInput({
            type: 'radio',
            label: 'Reject',
            value: 'reject',
            checked: false
        });
        alert.addInput({
            type: 'radio',
            label: 'N.O.O',
            value: 'noo',
            checked: false
        });
        alert.addButton('Cancel');
        alert.addButton({
            text: 'OK',
            handler: function (data) {
                _this.listOutlet = new Array();
                _this.getData('', 0, 10, data, 'No');
            }
        });
        alert.present();
    };
    AdminPage.prototype.dashboard = function (filter) {
        this.listOutlet = new Array();
        this.getData('', 0, 10, filter, 'No');
    };
    AdminPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-admin',template:/*ion-inline-start:"D:\ionic\Project\qrcode\src\pages\admin\admin.html"*/`<ion-header>\n\n  <ion-navbar color="oren">\n    <ion-title>\n      <div>Halaman Admin</div>\n    </ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="form_search()">\n        <ion-icon name="search"></ion-icon>&nbsp;&nbsp;\n      </button>\n    </ion-buttons>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="filtering()">\n        <ion-icon name="clipboard"></ion-icon>&nbsp;&nbsp;\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content no-padding class="bgc">\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n    <ion-refresher-content pullingIcon="arrow-dropdown" pullingText="Pull to refresh" refreshingSpinner="crescent"\n      refreshingText="">\n    </ion-refresher-content>\n  </ion-refresher>\n\n  <ion-grid fixed class="wrapper">\n\n    <ion-list>\n\n\n      <ion-item-divider color="light">\n        <table style="width: 100%;">\n          <tr>\n            <td (click)="dashboard(\'pending\')">\n              <table style="width: 33%;">\n                <tr>\n                  <td style="width: 100%;">\n                    <div style="color:#f4f5f8;background-color: #ffd31a;border-radius: 50%; width: 10px;height: 10px;">\n                      .\n                    </div>\n                  </td>\n                  <td style="width: 100%;">&nbsp;&nbsp;Pending : {{pending}}</td>\n                </tr>\n              </table>\n            </td>\n            <td (click)="dashboard(\'validate\')">\n              <table style="width: 33%;">\n                <tr>\n                  <td style="width: 100%;">\n                    <div style="color:#f4f5f8;background-color: #ffd31a;border-radius: 50%; width: 10px;height: 10px;">\n                      .\n                    </div>\n                  </td>\n                  <td style="width: 100%;">&nbsp;&nbsp;Validate : {{validate}}</td>\n                </tr>\n              </table>\n            </td>\n            <td (click)="dashboard(\'reject\')">\n              <table style="width: 33%;">\n                <tr>\n                  <td style="width: 100%;">\n                    <div style="color:#f4f5f8;background-color: #ffd31a;border-radius: 50%; width: 10px;height: 10px;">\n                      .\n                    </div>\n                  </td>\n                  <td style="width: 100%;">&nbsp;&nbsp;Reject : {{reject}}</td>\n                </tr>\n              </table>\n            </td>\n          </tr>\n        </table>\n      </ion-item-divider>\n\n\n      <ion-item *ngFor="let val of listOutlet"\n        (click)="detaillist(val.id,val.Lat,val.Lng,val.KdOutlet,val.OutletName,val.Address,val.Images)">\n        <ion-avatar item-start>\n          <img src="assets/imgs/setting.png">\n        </ion-avatar>\n        <h2>{{val.KdOutlet}} - {{val.OutletName}} <ion-icon *ngIf="val.Status==\'1\'" name="checkmark-circle"\n            color="secondary"></ion-icon>\n          <ion-icon *ngIf="val.Status==\'2\'" name="close-circle" color="danger"></ion-icon>\n        </h2>\n        <p>{{val.Address}}</p>\n        <h3>Uploader : {{val.UserUpload}}</h3>\n        <h3>Date Upload : {{val.DateUpload}}</h3>\n        <h3 *ngIf="val.Status!=\'0\'">Validator : {{val.UserValidate}}</h3>\n        <h3 *ngIf="val.Status!=\'0\'">Date Validate : {{val.DateValidate}}</h3>\n        <p *ngIf="val.Note!=\'\'">Note Uploader: {{val.Note}}</p>\n        <p>Source Data: {{val.tipedata}}</p>\n        <p *ngIf="val.NoteValidator!=\'\'">Note Validator: {{val.NoteValidator}}</p>\n\n      </ion-item>\n    </ion-list>\n\n    <ion-infinite-scroll (ionInfinite)="doInfinite($event,src, first,filter)">\n      <ion-infinite-scroll-content loadingSpinner="bubbles" loadingText="Loading more data...">\n      </ion-infinite-scroll-content>\n    </ion-infinite-scroll>\n\n  </ion-grid>\n</ion-content>`/*ion-inline-end:"D:\ionic\Project\qrcode\src\pages\admin\admin.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_3__config_provider__["a" /* Provider */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], AdminPage);
    return AdminPage;
}());

//# sourceMappingURL=admin.js.map

/***/ }),

/***/ 138:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FormoutletpwaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_barcode_scanner__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_file__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_social_sharing__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_file_transfer__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_geolocation__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_android_permissions__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_location_accuracy__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__config_provider__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_listoutlet_listoutlet__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_fix_orientation__ = __webpack_require__(438);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_fix_orientation___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_13_fix_orientation__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

















var QRCode = __webpack_require__(108);
var STORAGE_KEY = 'IMAGE_LIST';
var FormoutletpwaPage = (function () {
    function FormoutletpwaPage(navCtrl, navParams, viewCtrl, camera, barcodeScanner, file, plt, loadingCtrl, storage, renderer, transfer, postPvdr, socialSharing, alertCtrl, modalCtrl, locationAccuracy, androidPermissions, geolocation) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.camera = camera;
        this.barcodeScanner = barcodeScanner;
        this.file = file;
        this.plt = plt;
        this.loadingCtrl = loadingCtrl;
        this.storage = storage;
        this.renderer = renderer;
        this.transfer = transfer;
        this.postPvdr = postPvdr;
        this.socialSharing = socialSharing;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.locationAccuracy = locationAccuracy;
        this.androidPermissions = androidPermissions;
        this.geolocation = geolocation;
        this.outlet_existing = false;
        this.kode_outlet = null;
        this.kode_distributor = null;
        this.kode_sales = null;
        this.outlet_name = null;
        this.address = null;
        this.note = null;
        this.latitude = null;
        this.longitude = null;
        this.createdCode = null;
        this.photo_status = '';
        this.storedImages = [];
        this.img = '';
        this.elementType = 'img';
        this.cssClass = 'qrcode';
        this.value = 'https://www.techiediaries.com';
        this.version = '';
        this.errorCorrectionLevel = 'M';
        this.margin = 3;
        this.scale = 5;
        this.width = 10;
        this.colorDark = '#000000';
        this.colorLight = '#ffffff';
        this.loading = (function () {
            var loadMessage;
            return {
                turnOn: function () {
                    loadMessage = _this.loadingCtrl.create({
                        content: 'Sedang Memproses Gambar dengan QRCode'
                    });
                    loadMessage.present();
                },
                turnOff: function () { return loadMessage.dismiss(); }
            };
        })();
        this.storage.get('user_session_storage').then(function (res) {
            _this.kode_distributor = res[0].kdoutlet;
            _this.kode_sales = res[0].kdsales;
            ;
        });
        if (this.plt.is('android')) {
            console.log('android');
        }
        else {
            console.log('web');
        }
        this.storage.ready().then(function () {
            _this.storage.get(STORAGE_KEY).then(function (data) {
                if (data != undefined) {
                    _this.storedImages = data;
                }
            });
        });
    }
    FormoutletpwaPage.prototype.displayCard = function () {
        return this.img !== '';
    };
    FormoutletpwaPage.prototype.ionViewDidLoad = function () {
        this.canvasElement1 = this.canvas1.nativeElement;
        // this.canvasElement1.width = 3096;
        // this.canvasElement1.height = 4128;
        this.canvasElement1.width = 500;
        this.canvasElement1.height = 500;
        this.getLocation();
        this.myBit();
    };
    FormoutletpwaPage.prototype.myBit = function () {
        //MY BIT
        var _this = this;
        var element = this.cameraInput.nativeElement;
        element.onchange = function () {
            _this.loading.turnOn();
            var reader = new FileReader();
            reader.onload = function (r) {
                //THIS IS THE ORIGINAL BASE64 STRING AS SNAPPED FROM THE CAMERA
                //THIS IS PROBABLY THE ONE TO UPLOAD BACK TO YOUR DB AS IT'S UNALTERED
                //UP TO YOU, NOT REALLY BOTHERED
                var base64 = r.target.result;
                //FIXING ORIENTATION USING NPM PLUGIN fix-orientation
                __WEBPACK_IMPORTED_MODULE_13_fix_orientation___default()(base64, { image: true }, function (fixed, image) {
                    //fixed IS THE NEW VERSION FOR DISPLAY PURPOSES
                    _this.img = fixed;
                    _this.loading.turnOff();
                    //letakkan foto dicanvas
                    var prompt = _this.alertCtrl.create({
                        title: 'Info',
                        message: "Lihat Hasil Photo. Setelah Klik Mohon Tunggu Beberapa Saat.",
                        buttons: [
                            {
                                text: 'Ok',
                                handler: function (data) {
                                    _this.getImages();
                                }
                            }
                        ]
                    });
                    prompt.present();
                });
            };
            reader.readAsDataURL(element.files[0]);
        };
    };
    FormoutletpwaPage.prototype.takingPhoto = function () {
        if (typeof (this.outlet_name) === "undefined" || this.outlet_name == null || this.outlet_name == '') {
            var alert_1 = this.alertCtrl.create({
                title: 'Info!',
                subTitle: 'Nama Toko Tidak Boleh Kosong.',
                buttons: ['OK']
            });
            alert_1.present();
            return false;
        }
        if (typeof (this.note) === "undefined" || this.note == null || this.note == '') {
            var alert_2 = this.alertCtrl.create({
                title: 'Info!',
                subTitle: 'Catatan Harus Diisi, jika tidak ada input strip (-) saja.',
                buttons: ['OK']
            });
            alert_2.present();
            return false;
        }
        //generate QR Code
        this.createCode();
        this.createQRCode();
        this.photo_status = '1';
    };
    FormoutletpwaPage.prototype.ambilPhoto = function () {
        document.getElementById("getFile").click();
    };
    FormoutletpwaPage.prototype.getLocation = function () {
        var _this = this;
        this.geolocation.getCurrentPosition().then(function (pos) {
            _this.latitude = pos.coords.latitude;
            _this.longitude = pos.coords.longitude;
            var myLatlng = { lat: pos.coords.latitude, lng: pos.coords.longitude };
            var map = new google.maps.Map(_this.mapElement.nativeElement, {
                zoom: 17,
                center: myLatlng,
                zoomControl: false,
                mapTypeControl: false,
                scaleControl: false,
                streetViewControl: false,
                rotateControl: false,
                fullscreenControl: false
            });
            // marker mylocation
            var icon = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';
            var marker = new google.maps.Marker({
                position: myLatlng,
                draggable: true,
                map: map,
                icon: icon
            });
            google.maps.event.addListener(marker, 'dragend', function () {
                _this.latitude = marker.getPosition().lat();
                _this.longitude = marker.getPosition().lng();
                //find address draggable
                var geocoder = new google.maps.Geocoder;
                var latlng = { lat: _this.latitude, lng: _this.longitude };
                geocoder.geocode({ 'location': latlng }, function (results, status) {
                    var inputElement = document.getElementById('map-search');
                    inputElement.value = results[0].formatted_address;
                    var PerubahanLokasi = document.getElementById('RLocate').value;
                    if (PerubahanLokasi == '1') {
                        var inputElements = document.getElementById('ReLocate');
                        inputElements.value = '1';
                    }
                });
            });
            //find address fixed
            var geocoder = new google.maps.Geocoder;
            var latlng = { lat: _this.latitude, lng: _this.longitude };
            geocoder.geocode({ 'location': latlng }, function (results, status) {
                var inputElement = document.getElementById('map-search');
                inputElement.value = results[0].formatted_address;
            });
        }).catch(function (err) { return console.log('Error Here : ', err); });
    };
    FormoutletpwaPage.prototype.addForm = function () {
        var _this = this;
        if ((typeof (this.latitude) === "undefined" || this.latitude == null || this.latitude == '') || (typeof (this.longitude) === "undefined" || this.longitude == null || this.longitude == '')) {
            var alert_3 = this.alertCtrl.create({
                title: 'Info!',
                subTitle: 'Harus mendapatkan titik lokasi. Pastikan GPS Handphone anda sudah aktif.',
                buttons: ['OK']
            });
            alert_3.present();
            return false;
        }
        var prompt = this.alertCtrl.create({
            title: 'Info',
            message: "Sebelum isi formulir Tag Lokasi Toko, Pastikan titik lokasi toko sudah tepat.",
            buttons: [
                {
                    text: 'Ulangi',
                    handler: function (data) {
                        //
                    }
                },
                {
                    text: 'Ya',
                    handler: function (data) {
                        _this.tampilkanForm();
                    }
                }
            ]
        });
        prompt.present();
    };
    FormoutletpwaPage.prototype.tampilkanForm = function () {
        var _this = this;
        this.address = document.getElementById('map-search').value;
        document.getElementById('addForm').style.display = '';
        document.getElementById('peta').style.display = 'none';
        document.getElementById('map').style.display = 'none';
        document.getElementById('add').style.display = 'none';
        document.getElementById('maps').style.display = '';
        document.getElementById('tombol_kirim').style.display = '';
        var PerubahanLokasi = document.getElementById('ReLocate').value;
        if (PerubahanLokasi == '1') {
            //nol-kan lagi
            this.photo_status = '';
            var prompt_1 = this.alertCtrl.create({
                title: 'Info',
                message: "Anda ada perubahan titik lokasi, kami menyarankan ambil ulang photo",
                buttons: [
                    {
                        text: 'Ya',
                        handler: function (data) {
                            // PWA
                            _this.ambilPhoto();
                        }
                    }
                ]
            });
            prompt_1.present();
        }
    };
    FormoutletpwaPage.prototype.addMaps = function () {
        var inputElements = document.getElementById('ReLocate');
        inputElements.value = '0';
        var inputElements = document.getElementById('RLocate');
        inputElements.value = '1';
        document.getElementById('addForm').style.display = 'none';
        document.getElementById('peta').style.display = '';
        document.getElementById('map').style.display = '';
        document.getElementById('add').style.display = '';
        document.getElementById('maps').style.display = 'none';
        document.getElementById('tombol_kirim').style.display = 'none';
    };
    FormoutletpwaPage.prototype.myChange = function ($event) {
        var _this = this;
        if (this.outlet_existing) {
            var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_12__pages_listoutlet_listoutlet__["a" /* ListoutletPage */]);
            modal.present();
            modal.onDidDismiss(function (val) {
                if (val) {
                    _this.outlet_existing = true;
                    _this.kode_outlet = val.kdoutlet;
                    _this.outlet_name = val.nama;
                    _this.address = val.alamat;
                    document.getElementById('kode_outlet').style.display = '';
                }
                else {
                    _this.outlet_existing = false;
                    document.getElementById('kode_outlet').style.display = 'none';
                }
            });
        }
        else {
            document.getElementById('kode_outlet').style.display = 'none';
            this.kode_outlet = '';
            this.outlet_name = '';
            this.address = '';
        }
    };
    FormoutletpwaPage.prototype.getImages = function () {
        var ctx = this.canvasElement1.getContext("2d");
        var img = document.getElementById("take_photo");
        var img2 = document.getElementById("qi_ar_code");
        ctx.drawImage(img, 3, 3);
        ctx.drawImage(img2, 3, 3);
        this.getPath();
    };
    FormoutletpwaPage.prototype.getPath = function () {
        var canvas = document.getElementById('myCanvas1');
        ;
        var dataURL = canvas.toDataURL("image/jpg");
        this.hidden_data = dataURL;
    };
    FormoutletpwaPage.prototype.createCode = function () {
        this.createdCode = this.kode_outlet + '#' + this.outlet_name + '#' + this.address + '#' + this.latitude + '#' + this.longitude + '#' + this.note + '#' + this.kode_distributor + '#' + this.kode_sales;
    };
    FormoutletpwaPage.prototype.toDataURL = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            QRCode.toDataURL(_this.value, {
                version: _this.version,
                errorCorrectionLevel: _this.errorCorrectionLevel,
                margin: _this.margin,
                scale: _this.scale,
                width: _this.width,
                color: {
                    dark: _this.colorDark,
                    light: _this.colorLight
                }
            }, function (err, url) {
                if (err) {
                    console.error(err);
                    reject(err);
                }
                else {
                    //console.log(url);
                    resolve(url);
                }
            });
        });
    };
    FormoutletpwaPage.prototype.toCanvas = function (canvas) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            QRCode.toCanvas(canvas, _this.value, {
                version: _this.version,
                errorCorrectionLevel: _this.errorCorrectionLevel,
                margin: _this.margin,
                scale: _this.scale,
                width: _this.width,
                color: {
                    dark: _this.colorDark,
                    light: _this.colorLight
                }
            }, function (error) {
                if (error) {
                    //console.error(error);
                    reject(error);
                }
                else {
                    //console.log('success!');
                    resolve("success");
                }
            });
        });
    };
    FormoutletpwaPage.prototype.renderElement = function (element) {
        for (var _i = 0, _a = this.qrcElement.nativeElement.childNodes; _i < _a.length; _i++) {
            var node = _a[_i];
            this.renderer.removeChild(this.qrcElement.nativeElement, node);
        }
        this.renderer.appendChild(this.qrcElement.nativeElement, element);
    };
    FormoutletpwaPage.prototype.createQRCode = function () {
        var _this = this;
        if (!this.value) {
            return;
        }
        ;
        this.value = this.kode_outlet + '#' + this.outlet_name + '#' + this.address + '#' + this.latitude + '#' + this.longitude + '#' + this.note + '#' + this.kode_distributor + '#' + this.kode_sales;
        var element;
        // switch (this.elementType) {
        //console.log(this.elementType)
        switch (this.elementType) {
            case 'canvas':
                console.log('empty canvas');
                break;
            case 'url':
                element = this.renderer.createElement('canvas');
                this.toCanvas(element).then(function (v) {
                    _this.renderElement(element);
                }).catch(function (e) {
                    console.error(e);
                });
                break;
            case 'img':
                element = this.renderer.createElement('img');
                this.toDataURL().then(function (v) {
                    element.setAttribute("src", v);
                    element.setAttribute("id", "qi_ar_code");
                    _this.renderElement(element);
                }).catch(function (e) {
                    console.error(e);
                });
                break;
            default:
                element = this.renderer.createElement('img');
                this.toDataURL().then(function (v) {
                    element.setAttribute("src", v);
                    _this.renderElement(element);
                }).catch(function (e) {
                    console.error(e);
                });
        }
    };
    FormoutletpwaPage.prototype.metode_upload = function () {
        if (typeof (this.photo_status) === "undefined" || this.photo_status == null || this.photo_status == '') {
            var alert_4 = this.alertCtrl.create({
                title: 'Info!',
                subTitle: 'Harus Melampirkan Photo.',
                buttons: ['OK']
            });
            alert_4.present();
            return false;
        }
        this.upload_data('Disarankan', false, true, true, false);
    };
    FormoutletpwaPage.prototype.upload_data = function (msg, server, whatsapp, s, w) {
        var _this = this;
        var status = '(' + msg + ')';
        var status_server = server;
        var status_whatsapp = whatsapp;
        var alert = this.alertCtrl.create();
        alert.setTitle('Metode Pengiriman');
        alert.addInput({
            type: 'radio',
            label: 'Server VCI ' + status,
            value: 'server',
            checked: s,
            disabled: false
        });
        alert.addInput({
            type: 'radio',
            label: 'WhatsApp',
            value: 'whatsapp',
            checked: w,
            disabled: true
        });
        alert.addButton('Cancel');
        alert.addButton({
            text: 'OK',
            handler: function (data) {
                if (data == 'whatsapp') {
                    //
                }
                else if (data == 'server') {
                    _this.sendToServer();
                }
            }
        });
        alert.present();
    };
    FormoutletpwaPage.prototype.sendToServer = function () {
        var _this = this;
        var name = new Date().getTime() + '.jpg';
        this.nama_image = name;
        //jika menggunakan ala web
        this.uploadImageWeb(this.nama_image);
        this.storage.get('user_session_storage').then(function (res) {
            var body = {
                'kode_outlet': _this.kode_outlet,
                'outlet_name': _this.outlet_name,
                'address': _this.address,
                'note': _this.note,
                'latitude': _this.latitude,
                'longitude': _this.longitude,
                'images': _this.nama_image,
                'marker': res[0].kdsales + ' - ' + res[0].employee_name,
                'kdcabang': res[0].cabang_id,
                'kdsales': _this.kode_sales,
                'kddistributor': _this.kode_distributor
            };
            _this.postPvdr.postData(body, 'outlet/savedata').subscribe(function (data) {
                if (data.success) {
                    var prompt_2 = _this.alertCtrl.create({
                        title: 'Info',
                        message: "Data Berhasil di kirim.",
                        buttons: [
                            {
                                text: 'Ok',
                                handler: function (data) {
                                    _this.navCtrl.pop();
                                }
                            }
                        ]
                    });
                    prompt_2.present();
                }
                else {
                    var prompt_3 = _this.alertCtrl.create({
                        title: 'Info',
                        message: "Data Gagal Dikirim.",
                        buttons: [
                            {
                                text: 'Ok',
                                handler: function (data) {
                                    // this.navCtrl.pop();
                                }
                            }
                        ]
                    });
                    prompt_3.present();
                }
            }, function (error) {
                var prompt = _this.alertCtrl.create({
                    title: 'Info',
                    message: "Tidak konek ke sever, silahkan di coba kembali.",
                    buttons: [
                        {
                            text: 'Ok',
                            handler: function (data) {
                                // this.navCtrl.pop();
                            }
                        }
                    ]
                });
                prompt.present();
                return;
            });
        });
    };
    FormoutletpwaPage.prototype.uploadImageWeb = function (imageName) {
        var body = {
            'namefile': imageName,
            'hisfile': this.hidden_data
        };
        this.postPvdr.postData(body, 'outlet/uploadimages').subscribe(function (data) {
            if (data.success) {
                console.log('images sukses di upload');
            }
            else {
                console.log('images gagal di upload');
            }
        }, function (error) {
            console.log('error : ', error);
            return;
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('inputcamera'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], FormoutletpwaPage.prototype, "cameraInput", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('map'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], FormoutletpwaPage.prototype, "mapElement", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('imageCanvas1'),
        __metadata("design:type", Object)
    ], FormoutletpwaPage.prototype, "canvas1", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('qrcElement'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], FormoutletpwaPage.prototype, "qrcElement", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('qrc-element-type'),
        __metadata("design:type", String)
    ], FormoutletpwaPage.prototype, "elementType", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('qrc-class'),
        __metadata("design:type", Object)
    ], FormoutletpwaPage.prototype, "cssClass", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('qrc-value'),
        __metadata("design:type", Object)
    ], FormoutletpwaPage.prototype, "value", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('qrc-version'),
        __metadata("design:type", String)
    ], FormoutletpwaPage.prototype, "version", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('qrc-errorCorrectionLevel'),
        __metadata("design:type", String)
    ], FormoutletpwaPage.prototype, "errorCorrectionLevel", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('qrc-margin'),
        __metadata("design:type", Object)
    ], FormoutletpwaPage.prototype, "margin", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('qrc-scale'),
        __metadata("design:type", Object)
    ], FormoutletpwaPage.prototype, "scale", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('qrc-width'),
        __metadata("design:type", Object)
    ], FormoutletpwaPage.prototype, "width", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('qrc-colorDark'),
        __metadata("design:type", Object)
    ], FormoutletpwaPage.prototype, "colorDark", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('qrc-colorLight'),
        __metadata("design:type", Object)
    ], FormoutletpwaPage.prototype, "colorLight", void 0);
    FormoutletpwaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-formoutletpwa',template:/*ion-inline-start:"D:\ionic\Project\qrcode\src\pages\formoutletpwa\formoutletpwa.html"*/`<ion-header>\n\n  <ion-navbar color="oren">\n    <ion-title>\n      <div>Formulir Toko PWA</div>\n    </ion-title>\n\n    <ion-buttons end id="maps" style="display: none;">\n      <button ion-button icon-only (click)="addMaps()">\n        <ion-icon name="map"></ion-icon>&nbsp;&nbsp;\n      </button>\n    </ion-buttons>\n\n    <ion-buttons end id="tombol_kirim" style="display: none;">\n      <button ion-button icon-only (click)="metode_upload()">\n        <ion-icon name="cloud-upload"></ion-icon>&nbsp;&nbsp;\n      </button>\n    </ion-buttons>\n\n    <ion-buttons end id="add">\n      <button ion-button icon-only (click)="addForm()">\n        <ion-icon name="add-circle"></ion-icon>&nbsp;&nbsp;\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content class="bgc">\n\n  <div #map id="map"\n    style="box-shadow: 0px 2px 5px 5px #888888;margin: auto; width: 465px ;height: 50%;border-bottom: 3px solid #9831e0;">\n  </div>\n\n  <ion-grid fixed>\n\n\n    <div class="wrapper">\n      <div id="peta">\n        <ion-item-group>\n          <ion-list no-border>\n            <ion-item-divider color="light">Info Koordinat Lokasi Anda <ion-icon (click)="getLocation()" name="locate"\n                item-end></ion-icon>\n            </ion-item-divider>\n            <ion-item>\n              <ion-icon name=\'pin\' item-start></ion-icon>\n              Latitude\n              <ion-note item-end>\n                {{latitude}}\n              </ion-note>\n            </ion-item>\n            <ion-item>\n              <ion-icon name=\'pin\' item-start></ion-icon>\n              Longitude\n              <ion-note item-end>\n                {{longitude}}\n              </ion-note>\n            </ion-item>\n            <ion-item-divider color="light">Alamat Menurut Google Maps</ion-item-divider>\n            <ion-item style="display: none;">\n              <textarea id="ReLocate" rows="3" style="border:none;width: 100%;"></textarea>\n            </ion-item>\n            <ion-item style="display: none;">\n              <textarea id="RLocate" rows="3" style="border:none;width: 100%;" value="0"></textarea>\n            </ion-item>\n            <ion-item>\n              <textarea id="map-search" rows="5" style="border:none;width: 100%;"></textarea>\n            </ion-item>\n          </ion-list>\n        </ion-item-group>\n      </div>\n    </div>\n\n    <div class="wrapper">\n      <div id="addForm" style="display: none;">\n\n\n\n\n        <ion-card>\n          <ion-item-divider color="light-shade">\n            <ion-label>Outlet Yang Sudah Ada</ion-label>\n            <ion-toggle [(ngModel)]="outlet_existing" checked={{outlet_existing}} (ionChange)="myChange($event)">\n            </ion-toggle>\n          </ion-item-divider>\n\n          <ion-card-content>\n            <ion-list inset>\n              <ion-item id="kode_outlet" style="display:none">\n                <ion-label floating>Kode Outlet</ion-label>\n                <ion-input readonly type="text" [(ngModel)]="kode_outlet"></ion-input>\n              </ion-item>\n\n              <ion-item id="kode_distributor" style="display:none">\n                <ion-label floating>Kode Distributor</ion-label>\n                <ion-input readonly type="text" [(ngModel)]="kode_distributor"></ion-input>\n              </ion-item>\n\n              <ion-item id="kode_sales" style="display:none">\n                <ion-label floating>Kode Sales</ion-label>\n                <ion-input readonly type="text" [(ngModel)]="kode_sales"></ion-input>\n              </ion-item>\n\n              <ion-item>\n                <ion-label floating>Nama Toko</ion-label>\n                <ion-input type="text" [(ngModel)]="outlet_name"></ion-input>\n              </ion-item>\n\n              <ion-item>\n                <ion-label floating>Alamat</ion-label>\n                <ion-textarea rows="5" [(ngModel)]="address"></ion-textarea>\n              </ion-item>\n\n              <ion-item>\n                <ion-label floating>Catatan</ion-label>\n                <ion-input type="text" [(ngModel)]="note"></ion-input>\n              </ion-item>\n\n              <ion-item style="display: none;">\n                <ion-label floating>Photo Status</ion-label>\n                <ion-input type="text" [(ngModel)]="photo_status"></ion-input>\n              </ion-item>\n\n              <ion-item style="display: none;">\n                <ion-label floating>Hidden Data</ion-label>\n                <ion-input type="text" [(ngModel)]="hidden_data"></ion-input>\n              </ion-item>\n\n              <ion-item style="display: none;">\n                <ion-label floating>Latitude</ion-label>\n                <ion-input readonly type="text" [(ngModel)]="latitude"></ion-input>\n              </ion-item>\n\n              <ion-item style="display: none;">\n                <ion-label floating>Longitude</ion-label>\n                <ion-input readonly type="text" [(ngModel)]="longitude"></ion-input>\n              </ion-item>\n            </ion-list>\n          </ion-card-content>\n          <ion-item-divider color="oren" style="text-align: center;" (click)="ambilPhoto()">\n            <ion-icon name="camera"></ion-icon>&nbsp;&nbsp;<b>Ambil Photo</b>\n          </ion-item-divider>\n        </ion-card>\n\n        <ion-card>\n          <ion-item-divider color="light-shade">Hasil Photo</ion-item-divider>\n          <ion-card-content>\n            <canvas id="myCanvas1" #imageCanvas1 style="margin: auto; width: 98%;height: 100%;"></canvas>\n          </ion-card-content>\n        </ion-card>\n\n        <div #qrcElement id="qrcElement" style="display: none"></div>\n        <ion-item-group>\n          <ion-grid>\n            <ion-row>\n              <ion-col col-12 col-sm-9 col-md-6 col-lg-4 col-xl-3>\n                <input id="getFile" style="display:none;width: 100%;background-color: #fff;color:#fff" #inputcamera\n                  type="file" accept="image/*" capture="camera" (click)="takingPhoto()" />\n              </ion-col>\n              <ion-col>\n                <ion-card *ngIf="displayCard();" style="display: none;">\n                  <img id="take_photo" #imgresult [src]="img" />\n                </ion-card>\n              </ion-col>\n            </ion-row>\n          </ion-grid>\n        </ion-item-group>\n\n\n\n      </div>\n    </div>\n  </ion-grid>\n</ion-content>`/*ion-inline-end:"D:\ionic\Project\qrcode\src\pages\formoutletpwa\formoutletpwa.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_barcode_scanner__["a" /* BarcodeScanner */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_file__["a" /* File */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["W" /* Renderer2 */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_file_transfer__["a" /* FileTransfer */],
            __WEBPACK_IMPORTED_MODULE_11__config_provider__["a" /* Provider */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_social_sharing__["a" /* SocialSharing */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_10__ionic_native_location_accuracy__["a" /* LocationAccuracy */],
            __WEBPACK_IMPORTED_MODULE_9__ionic_native_android_permissions__["a" /* AndroidPermissions */],
            __WEBPACK_IMPORTED_MODULE_8__ionic_native_geolocation__["a" /* Geolocation */]])
    ], FormoutletpwaPage);
    return FormoutletpwaPage;
}());

//# sourceMappingURL=formoutletpwa.js.map

/***/ }),

/***/ 139:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FormoutletPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_barcode_scanner__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_file__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_social_sharing__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_file_transfer__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_geolocation__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_android_permissions__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_location_accuracy__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__config_provider__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_listoutlet_listoutlet__ = __webpack_require__(73);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
















var QRCode = __webpack_require__(108);
var STORAGE_KEY = 'IMAGE_LIST';
var FormoutletPage = (function () {
    function FormoutletPage(navCtrl, navParams, viewCtrl, camera, barcodeScanner, file, plt, loadingCtrl, storage, renderer, transfer, postPvdr, socialSharing, alertCtrl, modalCtrl, locationAccuracy, androidPermissions, geolocation) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.camera = camera;
        this.barcodeScanner = barcodeScanner;
        this.file = file;
        this.plt = plt;
        this.loadingCtrl = loadingCtrl;
        this.storage = storage;
        this.renderer = renderer;
        this.transfer = transfer;
        this.postPvdr = postPvdr;
        this.socialSharing = socialSharing;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.locationAccuracy = locationAccuracy;
        this.androidPermissions = androidPermissions;
        this.geolocation = geolocation;
        this.upload_url_image = 'http://api.vci.co.id/index.php/gocheck/uploadimage';
        this.outlet_existing = false;
        this.kode_outlet = null;
        this.kode_distributor = null;
        this.kode_sales = null;
        this.outlet_name = null;
        this.address = null;
        this.note = null;
        this.latitude = null;
        this.longitude = null;
        this.createdCode = null;
        this.photo_status = '';
        this.storedImages = [];
        this.elementType = 'img';
        this.cssClass = 'qrcode';
        this.value = 'https://www.techiediaries.com';
        this.version = '';
        this.errorCorrectionLevel = 'M';
        this.margin = 3;
        this.scale = 3;
        this.width = 10;
        this.colorDark = '#000000';
        this.colorLight = '#ffffff';
        this.storage.get('user_session_storage').then(function (res) {
            _this.kode_distributor = res[0].kdoutlet;
            _this.kode_sales = res[0].kdsales;
            ;
        });
        if (this.plt.is('android')) {
            console.log('android');
        }
        else {
            console.log('web');
        }
        this.storage.ready().then(function () {
            _this.storage.get(STORAGE_KEY).then(function (data) {
                if (data != undefined) {
                    _this.storedImages = data;
                }
            });
        });
        //this.checkGPSPermission();
    }
    FormoutletPage.prototype.ionViewDidLoad = function () {
        this.canvasElement1 = this.canvas1.nativeElement;
        this.canvasElement1.width = 720;
        this.canvasElement1.height = 965;
        this.getLocation();
    };
    //Check if application having GPS access permission  
    FormoutletPage.prototype.checkGPSPermission = function () {
        var _this = this;
        this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(function (result) {
            if (result.hasPermission) {
                //If having permission show 'Turn On GPS' dialogue
                _this.askToTurnOnGPS();
            }
            else {
                //If not having permission ask for permission
                _this.requestGPSPermission();
            }
        }, function (err) {
            console.log(err);
        });
    };
    FormoutletPage.prototype.requestGPSPermission = function () {
        var _this = this;
        this.locationAccuracy.canRequest().then(function (canRequest) {
            if (canRequest) {
                console.log("4");
            }
            else {
                //Show 'GPS Permission Request' dialogue
                _this.androidPermissions.requestPermission(_this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
                    .then(function () {
                    // call method to turn on GPS
                    _this.askToTurnOnGPS();
                }, function (error) {
                    //Show alert if user click on 'No Thanks'
                    console.log('requestPermission Error requesting location permissions ' + error);
                });
            }
        });
    };
    FormoutletPage.prototype.askToTurnOnGPS = function () {
        this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(function () {
            // When GPS Turned ON call method to get Accurate location coordinates
        }, function (error) { return console.log('Error requesting location permissions ' + JSON.stringify(error)); });
    };
    FormoutletPage.prototype.getLocation = function () {
        var _this = this;
        this.geolocation.getCurrentPosition().then(function (pos) {
            _this.latitude = pos.coords.latitude;
            _this.longitude = pos.coords.longitude;
            var myLatlng = { lat: pos.coords.latitude, lng: pos.coords.longitude };
            var map = new google.maps.Map(_this.mapElement.nativeElement, {
                zoom: 17,
                center: myLatlng,
                zoomControl: false,
                mapTypeControl: false,
                scaleControl: false,
                streetViewControl: false,
                rotateControl: false,
                fullscreenControl: false
            });
            // marker mylocation
            var icon = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';
            var marker = new google.maps.Marker({
                position: myLatlng,
                draggable: true,
                map: map,
                icon: icon
            });
            google.maps.event.addListener(marker, 'dragend', function () {
                _this.latitude = marker.getPosition().lat();
                _this.longitude = marker.getPosition().lng();
                //find address draggable
                var geocoder = new google.maps.Geocoder;
                var latlng = { lat: _this.latitude, lng: _this.longitude };
                geocoder.geocode({ 'location': latlng }, function (results, status) {
                    var inputElement = document.getElementById('map-search');
                    inputElement.value = results[0].formatted_address;
                    var PerubahanLokasi = document.getElementById('RLocate').value;
                    if (PerubahanLokasi == '1') {
                        var inputElements = document.getElementById('ReLocate');
                        inputElements.value = '1';
                    }
                });
            });
            //find address fixed
            var geocoder = new google.maps.Geocoder;
            var latlng = { lat: _this.latitude, lng: _this.longitude };
            geocoder.geocode({ 'location': latlng }, function (results, status) {
                var inputElement = document.getElementById('map-search');
                inputElement.value = results[0].formatted_address;
            });
        }).catch(function (err) { return console.log('Error Here : ', err); });
    };
    FormoutletPage.prototype.addForm = function () {
        var _this = this;
        if ((typeof (this.latitude) === "undefined" || this.latitude == null || this.latitude == '') || (typeof (this.longitude) === "undefined" || this.longitude == null || this.longitude == '')) {
            var alert_1 = this.alertCtrl.create({
                title: 'Info!',
                subTitle: 'Harus mendapatkan titik lokasi. Pastikan GPS Handphone anda sudah aktif.',
                buttons: ['OK']
            });
            alert_1.present();
            return false;
        }
        var prompt = this.alertCtrl.create({
            title: 'Info',
            message: "Sebelum isi formulir Tag Lokasi Toko, Pastikan titik lokasi toko sudah tepat.",
            buttons: [
                {
                    text: 'Ulangi',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Ya',
                    handler: function (data) {
                        console.log('Saved clicked');
                        _this.tampilkanForm();
                    }
                }
            ]
        });
        prompt.present();
    };
    FormoutletPage.prototype.tampilkanForm = function () {
        var _this = this;
        this.address = document.getElementById('map-search').value;
        document.getElementById('addForm').style.display = '';
        document.getElementById('peta').style.display = 'none';
        document.getElementById('map').style.display = 'none';
        document.getElementById('add').style.display = 'none';
        document.getElementById('maps').style.display = '';
        document.getElementById('tombol_kirim').style.display = '';
        var PerubahanLokasi = document.getElementById('ReLocate').value;
        if (PerubahanLokasi == '1') {
            //hilangkan image
            document.getElementById('hasil_photo').style.display = 'none';
            //nol-kan lagi
            this.photo_status = '';
            var prompt_1 = this.alertCtrl.create({
                title: 'Info',
                message: "Anda ada perubahan titik lokasi, kami menyarankan ambil ulang photo",
                buttons: [
                    {
                        text: 'Ya',
                        handler: function (data) {
                            _this.takePhoto();
                        }
                    }
                ]
            });
            prompt_1.present();
        }
    };
    FormoutletPage.prototype.addMaps = function () {
        var inputElements = document.getElementById('ReLocate');
        inputElements.value = '0';
        var inputElements = document.getElementById('RLocate');
        inputElements.value = '1';
        document.getElementById('addForm').style.display = 'none';
        document.getElementById('peta').style.display = '';
        document.getElementById('map').style.display = '';
        document.getElementById('add').style.display = '';
        document.getElementById('maps').style.display = 'none';
        document.getElementById('tombol_kirim').style.display = 'none';
    };
    FormoutletPage.prototype.myChange = function ($event) {
        var _this = this;
        if (this.outlet_existing) {
            var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_12__pages_listoutlet_listoutlet__["a" /* ListoutletPage */]);
            modal.present();
            modal.onDidDismiss(function (val) {
                if (val) {
                    _this.outlet_existing = true;
                    _this.kode_outlet = val.kdoutlet;
                    _this.outlet_name = val.nama;
                    _this.address = val.alamat;
                    document.getElementById('kode_outlet').style.display = '';
                }
                else {
                    _this.outlet_existing = false;
                    document.getElementById('kode_outlet').style.display = 'none';
                }
            });
        }
        else {
            document.getElementById('kode_outlet').style.display = 'none';
            this.kode_outlet = '';
            this.outlet_name = '';
            this.address = '';
        }
    };
    FormoutletPage.prototype.getImages = function () {
        var ctx = this.canvasElement1.getContext("2d");
        var img = document.getElementById("take_photo");
        var img2 = document.getElementById("qi_ar_code");
        ctx.drawImage(img, 3, 3);
        ctx.drawImage(img2, 3, 3);
    };
    FormoutletPage.prototype.createCode = function () {
        this.createdCode = this.kode_outlet + '#' + this.outlet_name + '#' + this.address + '#' + this.latitude + '#' + this.longitude + '#' + this.note + '#' + this.kode_distributor + '#' + this.kode_sales;
    };
    FormoutletPage.prototype.takePhoto = function () {
        var _this = this;
        if (typeof (this.outlet_name) === "undefined" || this.outlet_name == null || this.outlet_name == '') {
            var alert_2 = this.alertCtrl.create({
                title: 'Info!',
                subTitle: 'Nama Toko Tidak Boleh Kosong.',
                buttons: ['OK']
            });
            alert_2.present();
            return false;
        }
        if (typeof (this.note) === "undefined" || this.note == null || this.note == '') {
            var alert_3 = this.alertCtrl.create({
                title: 'Info!',
                subTitle: 'Catatan Harus Diisi, jika tidak ada input strip (-) saja.',
                buttons: ['OK']
            });
            alert_3.present();
            return false;
        }
        //generate QR Code
        this.createCode();
        this.createQRCode();
        this.photo_status = '1';
        //camera
        var options = {
            quality: 100,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            correctOrientation: true,
            //saveToPhotoAlbum: true,
            targetWidth: 720,
            targetHeight: 1560,
        };
        this.camera.getPicture(options).then(function (imageData) {
            // imageData is either a base64 encoded string or a file URI
            // If it's base64 (DATA_URL):
            _this.photo = imageData;
            //to canvas
            var prompt = _this.alertCtrl.create({
                title: 'Info',
                message: "Tekan Ok Untuk Melihat Gambar.",
                buttons: [
                    {
                        text: 'Ok',
                        handler: function (data) {
                            _this.getImages();
                        }
                    }
                ]
            });
            prompt.present();
        }, function (err) {
            console.log('error : ' + err);
        });
    };
    FormoutletPage.prototype.saveCanvasImage = function () {
        var _this = this;
        var lukisan = this.canvasElement1;
        var dataUrl = lukisan.toDataURL();
        var ctx = lukisan.getContext('2d');
        //ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height); // Clears the canvas
        var name = new Date().getTime() + '.jpg';
        var path = this.file.dataDirectory;
        var options = { replace: true };
        var data = dataUrl.split(',')[1];
        var blob = this.b64toBlob(data, 'image/jpg');
        this.file.writeFile(path, name, blob, options).then(function (res) {
            _this.storeImage(name);
        }, function (err) {
            console.log('error: ' + err);
        });
    };
    FormoutletPage.prototype.saveJoinImage = function () {
        var _this = this;
        var lukisan = this.canvasElement1;
        var dataUrl = lukisan.toDataURL();
        var ctx = lukisan.getContext('2d');
        //ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height); // Clears the canvas
        var name = new Date().getTime() + '.jpg';
        //untuk file upload
        this.nama_image = name;
        var path = this.file.dataDirectory;
        var options = { replace: true };
        var data = dataUrl.split(',')[1];
        var blob = this.b64toBlob(data, 'image/jpg');
        this.file.writeFile(path, name, blob, options).then(function (res) {
            _this.storeImageServer(name);
        }, function (err) {
            console.log('error: ' + err);
        });
    };
    FormoutletPage.prototype.b64toBlob = function (b64Data, contentType) {
        contentType = contentType || '';
        var sliceSize = 512;
        var byteCharacters = atob(b64Data);
        var byteArrays = [];
        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);
            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }
            var byteArray = new Uint8Array(byteNumbers);
            byteArrays.push(byteArray);
        }
        var blob = new Blob(byteArrays, { type: contentType });
        return blob;
    };
    FormoutletPage.prototype.storeImage = function (imageName) {
        var _this = this;
        var saveObj = { img: imageName };
        this.storedImages.push(saveObj);
        this.storage.set(STORAGE_KEY, this.storedImages).then(function () {
            setTimeout(function () {
                _this.shareImg(imageName);
            }, 500);
        });
    };
    FormoutletPage.prototype.storeImageServer = function (imageName) {
        var saveObj = { img: imageName };
        this.storedImages.push(saveObj);
        this.storage.set(STORAGE_KEY, this.storedImages).then(function () {
            setTimeout(function () {
            }, 500);
        });
    };
    FormoutletPage.prototype.shareImg = function (fileImage) {
        var _this = this;
        // let imageName = "1568069382318.bmp";
        var imageName = fileImage;
        // const ROOT_DIRECTORY = 'file:///sdcard//';
        var ROOT_DIRECTORY = 'file:///data/user/0/io.ionic.gocheck/';
        var downloadFolderName = 'tempDownloadFolder';
        //Create a folder in memory location
        this.file.createDir(ROOT_DIRECTORY, downloadFolderName, true)
            .then(function (entries) {
            //Copy our asset/img/FreakyJolly.jpg to folder we created
            //console.log('direktori ', this.file.applicationDirectory);
            // this.file.copyFile(this.file.applicationDirectory + "www/assets/imgs/", imageName, ROOT_DIRECTORY + downloadFolderName + '//', imageName)
            _this.file.copyFile("file:///data/user/0/io.ionic.gocheck/files/", imageName, ROOT_DIRECTORY + downloadFolderName + '//', imageName)
                .then(function (entries) {
                //Common sharing event will open all available application to share
                _this.socialSharing.share("Message", "Subject", ROOT_DIRECTORY + downloadFolderName + "/" + imageName, imageName)
                    .then(function (entries) {
                    //console.log('success ' + JSON.stringify(entries));
                    _this.navCtrl.pop();
                })
                    .catch(function (error) {
                    console.log('error 1 ' + JSON.stringify(error));
                });
            })
                .catch(function (error) {
                console.log('error 2 ' + JSON.stringify(error));
            });
        })
            .catch(function (error) {
            console.log('error 3 ' + JSON.stringify(error));
        });
    };
    FormoutletPage.prototype.toDataURL = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            QRCode.toDataURL(_this.value, {
                version: _this.version,
                errorCorrectionLevel: _this.errorCorrectionLevel,
                margin: _this.margin,
                scale: _this.scale,
                width: _this.width,
                color: {
                    dark: _this.colorDark,
                    light: _this.colorLight
                }
            }, function (err, url) {
                if (err) {
                    console.error(err);
                    reject(err);
                }
                else {
                    //console.log(url);
                    resolve(url);
                }
            });
        });
    };
    FormoutletPage.prototype.toCanvas = function (canvas) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            QRCode.toCanvas(canvas, _this.value, {
                version: _this.version,
                errorCorrectionLevel: _this.errorCorrectionLevel,
                margin: _this.margin,
                scale: _this.scale,
                width: _this.width,
                color: {
                    dark: _this.colorDark,
                    light: _this.colorLight
                }
            }, function (error) {
                if (error) {
                    //console.error(error);
                    reject(error);
                }
                else {
                    //console.log('success!');
                    resolve("success");
                }
            });
        });
    };
    FormoutletPage.prototype.renderElement = function (element) {
        for (var _i = 0, _a = this.qrcElement.nativeElement.childNodes; _i < _a.length; _i++) {
            var node = _a[_i];
            this.renderer.removeChild(this.qrcElement.nativeElement, node);
        }
        this.renderer.appendChild(this.qrcElement.nativeElement, element);
    };
    FormoutletPage.prototype.createQRCode = function () {
        var _this = this;
        if (!this.value) {
            return;
        }
        ;
        this.value = this.kode_outlet + '#' + this.outlet_name + '#' + this.address + '#' + this.latitude + '#' + this.longitude + '#' + this.note + '#' + this.kode_distributor + '#' + this.kode_sales;
        var element;
        // switch (this.elementType) {
        //console.log(this.elementType)
        switch (this.elementType) {
            case 'canvas':
                console.log('empty canvas');
                break;
            case 'url':
                element = this.renderer.createElement('canvas');
                this.toCanvas(element).then(function (v) {
                    _this.renderElement(element);
                }).catch(function (e) {
                    console.error(e);
                });
                break;
            case 'img':
                element = this.renderer.createElement('img');
                this.toDataURL().then(function (v) {
                    console.log('666', v);
                    console.log('999', element);
                    element.setAttribute("src", v);
                    element.setAttribute("id", "qi_ar_code");
                    _this.renderElement(element);
                }).catch(function (e) {
                    console.error(e);
                });
                break;
            default:
                element = this.renderer.createElement('img');
                this.toDataURL().then(function (v) {
                    element.setAttribute("src", v);
                    _this.renderElement(element);
                }).catch(function (e) {
                    console.error(e);
                });
        }
    };
    FormoutletPage.prototype.metode_upload = function () {
        var _this = this;
        if (typeof (this.photo_status) === "undefined" || this.photo_status == null || this.photo_status == '') {
            var alert_4 = this.alertCtrl.create({
                title: 'Info!',
                subTitle: 'Harus Melampirkan Photo.',
                buttons: ['OK']
            });
            alert_4.present();
            return false;
        }
        //save image dulu
        this.saveJoinImage();
        var loader = this.loadingCtrl.create({
            content: "Cek Server Dulu...",
        });
        loader.present();
        var body = {
            ping: 'ping'
        };
        this.postPvdr.postData(body, 'ping').subscribe(function (data) {
            if (data.success) {
                loader.dismiss();
                //recommended server
                _this.upload_data('Disarankan', false, true, true, false);
            }
        }, function (error) {
            loader.dismiss();
            //recommended whatsapp
            _this.upload_data('Tdk Disarankan', true, false, false, true);
        });
    };
    FormoutletPage.prototype.upload_data = function (msg, server, whatsapp, s, w) {
        var _this = this;
        var status = '(' + msg + ')';
        var status_server = server;
        var status_whatsapp = whatsapp;
        var alert = this.alertCtrl.create();
        alert.setTitle('Metode Pengiriman');
        alert.addInput({
            type: 'radio',
            label: 'Server VCI ' + status,
            value: 'server',
            checked: s,
            disabled: false
        });
        alert.addInput({
            type: 'radio',
            label: 'WhatsApp',
            value: 'whatsapp',
            checked: w,
            disabled: false
        });
        alert.addButton('Cancel');
        alert.addButton({
            text: 'OK',
            handler: function (data) {
                if (data == 'whatsapp') {
                    _this.saveCanvasImage();
                }
                else if (data == 'server') {
                    _this.sendToServer();
                }
            }
        });
        alert.present();
    };
    FormoutletPage.prototype.sendToServer = function () {
        var _this = this;
        //upload image
        this.uploadImage(this.nama_image);
        this.storage.get('user_session_storage').then(function (res) {
            var body = {
                'kode_outlet': _this.kode_outlet,
                'outlet_name': _this.outlet_name,
                'address': _this.address,
                'note': _this.note,
                'latitude': _this.latitude,
                'longitude': _this.longitude,
                'images': _this.nama_image,
                'marker': res[0].kdsales + ' - ' + res[0].employee_name,
                'kdcabang': res[0].cabang_id,
                'username': res[0].username,
                'kdsales': _this.kode_sales,
                'kddistributor': _this.kode_distributor
            };
            _this.postPvdr.postData(body, 'outlet/savedata2').subscribe(function (data) {
                if (data.success) {
                    var prompt_2 = _this.alertCtrl.create({
                        title: 'Info',
                        message: "Data Berhasil di kirim.",
                        buttons: [
                            {
                                text: 'Ok',
                                handler: function (data) {
                                    _this.navCtrl.pop();
                                }
                            }
                        ]
                    });
                    prompt_2.present();
                }
                else {
                    var prompt_3 = _this.alertCtrl.create({
                        title: 'Info',
                        message: "Data Gagal Dikirim.",
                        buttons: [
                            {
                                text: 'Ok',
                                handler: function (data) {
                                    // this.navCtrl.pop();
                                }
                            }
                        ]
                    });
                    prompt_3.present();
                }
            }, function (error) {
                var prompt = _this.alertCtrl.create({
                    title: 'Info',
                    message: "Tidak konek ke sever, silahkan di coba kembali.",
                    buttons: [
                        {
                            text: 'Ok',
                            handler: function (data) {
                                // this.navCtrl.pop();
                            }
                        }
                    ]
                });
                prompt.present();
                return;
            });
        });
    };
    FormoutletPage.prototype.uploadImage = function (imageName) {
        var fileTransfer = this.transfer.create();
        var options = {
            fileKey: 'file',
            fileName: imageName,
            chunkedMode: false,
            mimeType: "image/png",
            headers: {}
        };
        fileTransfer.upload("file:///data/user/0/io.ionic.gocheck/files/" + imageName, this.upload_url_image, options)
            .then(function (data) {
            //sukses
        }, function (err) {
            console.log("Error This Image: " + err);
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('map'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], FormoutletPage.prototype, "mapElement", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('imageCanvas1'),
        __metadata("design:type", Object)
    ], FormoutletPage.prototype, "canvas1", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('qrc-element-type'),
        __metadata("design:type", String)
    ], FormoutletPage.prototype, "elementType", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('qrc-class'),
        __metadata("design:type", Object)
    ], FormoutletPage.prototype, "cssClass", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('qrc-value'),
        __metadata("design:type", Object)
    ], FormoutletPage.prototype, "value", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('qrc-version'),
        __metadata("design:type", String)
    ], FormoutletPage.prototype, "version", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('qrc-errorCorrectionLevel'),
        __metadata("design:type", String)
    ], FormoutletPage.prototype, "errorCorrectionLevel", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('qrc-margin'),
        __metadata("design:type", Object)
    ], FormoutletPage.prototype, "margin", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('qrc-scale'),
        __metadata("design:type", Object)
    ], FormoutletPage.prototype, "scale", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('qrc-width'),
        __metadata("design:type", Object)
    ], FormoutletPage.prototype, "width", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('qrc-colorDark'),
        __metadata("design:type", Object)
    ], FormoutletPage.prototype, "colorDark", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('qrc-colorLight'),
        __metadata("design:type", Object)
    ], FormoutletPage.prototype, "colorLight", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('qrcElement'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], FormoutletPage.prototype, "qrcElement", void 0);
    FormoutletPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-formoutlet',template:/*ion-inline-start:"D:\ionic\Project\qrcode\src\pages\formoutlet\formoutlet.html"*/`<ion-header>\n\n  <ion-navbar color="oren">\n    <ion-title>\n      <div>Formulir Outlet Apps</div>\n    </ion-title>\n\n    <ion-buttons end id="maps" style="display: none;">\n      <button ion-button icon-only (click)="addMaps()">\n        <ion-icon name="map"></ion-icon>&nbsp;&nbsp;\n      </button>\n    </ion-buttons>\n\n    <ion-buttons end id="tombol_kirim" style="display: none;">\n      <button ion-button icon-only (click)="metode_upload()">\n        <ion-icon name="cloud-upload"></ion-icon>&nbsp;&nbsp;\n      </button>\n    </ion-buttons>\n\n    <ion-buttons end id="add">\n      <button ion-button icon-only (click)="addForm()">\n        <ion-icon name="add-circle"></ion-icon>&nbsp;&nbsp;\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content no-padding class="bgc">\n\n  <div #map id="map"\n    style="box-shadow: 0px 2px 5px 5px #888888;margin: auto; width: 465px ;height: 50%;border-bottom: 3px solid #9831e0;">\n  </div>\n  <ion-grid fixed>\n\n\n    <div class="wrapper">\n      <div id="peta">\n        <ion-item-group>\n          <ion-list no-border>\n            <ion-item-divider color="light">Info Koordinat Lokasi Anda <ion-icon (click)="getLocation()" name="locate"\n                item-end></ion-icon>\n            </ion-item-divider>\n            <ion-item>\n              <ion-icon name=\'pin\' item-start></ion-icon>\n              Latitude\n              <ion-note item-end>\n                {{latitude}}\n              </ion-note>\n            </ion-item>\n            <ion-item>\n              <ion-icon name=\'pin\' item-start></ion-icon>\n              Longitude\n              <ion-note item-end>\n                {{longitude}}\n              </ion-note>\n            </ion-item>\n\n            <ion-item-divider color="light">Alamat Menurut Google Maps</ion-item-divider>\n            <ion-item style="display: none;">\n              <textarea id="ReLocate" rows="3" style="border:none;width: 100%;"></textarea>\n            </ion-item>\n            <ion-item style="display: none;">\n              <textarea id="RLocate" rows="3" style="border:none;width: 100%;" value="0"></textarea>\n            </ion-item>\n            <ion-item>\n              <textarea id="map-search" rows="5" style="border:none;width: 100%;"></textarea>\n            </ion-item>\n          </ion-list>\n        </ion-item-group>\n      </div>\n    </div>\n\n    <div class="wrapper">\n      <div id="addForm" style="display: none;" no-padding>\n\n        <ion-card>\n          <ion-item-divider color="light-shade">\n            <ion-label>Outlet Yang Sudah Ada</ion-label>\n            <ion-toggle [(ngModel)]="outlet_existing" checked={{outlet_existing}} (ionChange)="myChange($event)">\n            </ion-toggle>\n          </ion-item-divider>\n\n          <ion-card-content>\n            <ion-list inset>\n\n              <ion-item id="kode_outlet" style="display:none">\n                <ion-label floating>Kode Outlet</ion-label>\n                <ion-input readonly type="text" [(ngModel)]="kode_outlet"></ion-input>\n              </ion-item>\n\n              <ion-item id="kode_distributor" style="display:none">\n                <ion-label floating>Kode Distributor</ion-label>\n                <ion-input readonly type="text" [(ngModel)]="kode_distributor"></ion-input>\n              </ion-item>\n\n              <ion-item id="kode_sales" style="display:none">\n                <ion-label floating>Kode Sales</ion-label>\n                <ion-input readonly type="text" [(ngModel)]="kode_sales"></ion-input>\n              </ion-item>\n\n              <ion-item>\n                <ion-label floating>Nama Toko</ion-label>\n                <ion-input type="text" [(ngModel)]="outlet_name"></ion-input>\n              </ion-item>\n\n              <ion-item>\n                <ion-label floating>Alamat</ion-label>\n                <ion-textarea rows="5" [(ngModel)]="address"></ion-textarea>\n              </ion-item>\n\n              <ion-item>\n                <ion-label floating>Catatan</ion-label>\n                <ion-input type="text" [(ngModel)]="note"></ion-input>\n              </ion-item>\n\n              <ion-item style="display: none;">\n                <ion-label floating>Photo Status</ion-label>\n                <ion-input type="text" [(ngModel)]="photo_status"></ion-input>\n              </ion-item>\n\n              <ion-item style="display: none;">\n                <ion-label floating>Hidden Data</ion-label>\n                <ion-input type="text" [(ngModel)]="hidden_data"></ion-input>\n              </ion-item>\n\n              <ion-item style="display: none;">\n                <ion-label floating>Latitude</ion-label>\n                <ion-input readonly type="text" [(ngModel)]="latitude"></ion-input>\n              </ion-item>\n\n              <ion-item style="display: none;">\n                <ion-label floating>Longitude</ion-label>\n                <ion-input readonly type="text" [(ngModel)]="longitude"></ion-input>\n              </ion-item>\n            </ion-list>\n          </ion-card-content>\n          <ion-item-divider color="oren" style="text-align: center;" (click)="takePhoto()">\n            <ion-icon name="camera"></ion-icon>&nbsp;&nbsp;<b>Ambil Photo</b>\n          </ion-item-divider>\n        </ion-card>\n\n        <div #qrcElement id="qrcElement" style="display: none"></div>\n        <p align="center" style="display: none" id="hasil_photo"><img id="take_photo" src="{{photo}}"></p>\n\n        <ion-card>\n          <ion-item-divider color="light-shade">Hasil Photo</ion-item-divider>\n          <ion-card-content>\n            <canvas id="myCanvas1" #imageCanvas1 style="margin: auto; width: 98%;height: 100%;"></canvas>\n          </ion-card-content>\n        </ion-card>\n\n      </div>\n\n    </div>\n  </ion-grid>\n</ion-content>`/*ion-inline-end:"D:\ionic\Project\qrcode\src\pages\formoutlet\formoutlet.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_barcode_scanner__["a" /* BarcodeScanner */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_file__["a" /* File */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["W" /* Renderer2 */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_file_transfer__["a" /* FileTransfer */],
            __WEBPACK_IMPORTED_MODULE_11__config_provider__["a" /* Provider */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_social_sharing__["a" /* SocialSharing */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_10__ionic_native_location_accuracy__["a" /* LocationAccuracy */],
            __WEBPACK_IMPORTED_MODULE_9__ionic_native_android_permissions__["a" /* AndroidPermissions */],
            __WEBPACK_IMPORTED_MODULE_8__ionic_native_geolocation__["a" /* Geolocation */]])
    ], FormoutletPage);
    return FormoutletPage;
}());

//# sourceMappingURL=formoutlet.js.map

/***/ }),

/***/ 140:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HistoryPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_provider__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_detailhistory_detailhistory__ = __webpack_require__(69);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the HistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var HistoryPage = (function () {
    function HistoryPage(navCtrl, postPvdr, storage, viewCtrl, loadingCtrl, alertCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.postPvdr = postPvdr;
        this.storage = storage;
        this.viewCtrl = viewCtrl;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.navParams = navParams;
        this.filter = 'all';
        this.pending = 0;
        this.validate = 0;
        this.reject = 0;
    }
    HistoryPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad HistoryPage');
        this.listOutlet = new Array();
        this.getData('', 0, 10, 'all', 'No');
    };
    HistoryPage.prototype.form_search = function () {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'Cari',
            message: "Masukan kata untuk mencari kode atau nama toko",
            inputs: [
                {
                    name: 'src',
                    placeholder: 'Apa yang anda cari...'
                },
            ],
            buttons: [
                {
                    text: 'Batal',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Cari',
                    handler: function (data) {
                        console.log('Saved clicked');
                        _this.getOutlets(data.src);
                    }
                }
            ]
        });
        prompt.present();
    };
    HistoryPage.prototype.getOutlets = function (src) {
        this.listOutlet = new Array();
        if (src) {
            src = src;
        }
        else {
            src = '';
        }
        this.getData(src, 0, 10, 'all', 'No');
    };
    HistoryPage.prototype.getData = function (src, first, offset, filter, infinite) {
        var _this = this;
        this.first = first;
        this.filter = filter;
        this.src = src;
        var loader = this.loadingCtrl.create({
            content: "Mohon Menunggu..."
        });
        if (infinite == 'No') {
            loader.present();
        }
        this.storage.get('user_session_storage').then(function (res) {
            var body = {
                kdsales: res[0].kdsales,
                kdcabang: res[0].cabang_id,
                user_level: res[0].user_level,
                kdoutlet: res[0].kdoutlet,
                username: res[0].username,
                first: first,
                offset: offset,
                filter: filter,
                keyword: src
            };
            _this.postPvdr.postData(body, 'outlet/getlistoutlet2').subscribe(function (data) {
                if (data.success) {
                    loader.dismiss();
                    _this.pending = data.pending;
                    _this.validate = data.validate;
                    _this.reject = data.reject;
                    for (var i = 0; i < data.result.length; i++) {
                        var tipedata;
                        if (data.result[i]['TypeData'] == 'S') {
                            tipedata = "Server";
                        }
                        else {
                            tipedata = "WhatsApp";
                        }
                        _this.listOutlet.push({
                            id: data.result[i]['id'],
                            KdOutlet: data.result[i]['KdOutlet'],
                            OutletName: data.result[i]['OutletName'],
                            Address: data.result[i]['Address'],
                            UserUpload: data.result[i]['UserUpload'],
                            DateUpload: data.result[i]['DateUpload'],
                            UserValidate: data.result[i]['UserValidate'],
                            DateValidate: data.result[i]['DateValidate'],
                            Lat: data.result[i]['Lat'],
                            Lng: data.result[i]['Lng'],
                            Status: data.result[i]['Status'],
                            Images: data.result[i]['Images'],
                            tipedata: tipedata,
                            Note: data.result[i]['Note'],
                            NoteValidator: data.result[i]['NoteValidator']
                        });
                    }
                }
                else {
                    loader.dismiss();
                    if (infinite == 'No') {
                        var alert_1 = _this.alertCtrl.create({
                            title: 'Info',
                            subTitle: 'Mohon Maaf Data Tidak Di Temukan',
                            message: '',
                            buttons: ['OK']
                        });
                        alert_1.present();
                    }
                    return false;
                }
            }, function (error) {
                loader.dismiss();
                var alert = _this.alertCtrl.create({
                    title: 'Mohon Maaf',
                    subTitle: 'Sedang tidak terkoneksi dengan server. Silahkan coba kembali beberapa saat',
                    buttons: ['OK']
                });
                alert.present();
                return false;
            });
        });
    };
    HistoryPage.prototype.doRefresh = function (refresher) {
        this.listOutlet = new Array();
        this.getData('', 0, 10, 'all', 'No');
        setTimeout(function () {
            refresher.complete();
        }, 1000);
    };
    HistoryPage.prototype.doInfinite = function (infiniteScroll, src, first, filter) {
        var _this = this;
        first = first + 10;
        console.log('Begin async operation');
        setTimeout(function () {
            _this.getData(src, first, 10, filter, 'Yes');
            infiniteScroll.complete();
        }, 1000);
    };
    HistoryPage.prototype.filtering = function () {
        var _this = this;
        var alert = this.alertCtrl.create();
        alert.setTitle('Filter');
        alert.addInput({
            type: 'radio',
            label: 'All',
            value: 'all',
            checked: false
        });
        alert.addInput({
            type: 'radio',
            label: 'Pending',
            value: 'pending',
            checked: false
        });
        alert.addInput({
            type: 'radio',
            label: 'Validate',
            value: 'validate',
            checked: false
        });
        alert.addInput({
            type: 'radio',
            label: 'Reject',
            value: 'reject',
            checked: false
        });
        alert.addButton('Cancel');
        alert.addButton({
            text: 'OK',
            handler: function (data) {
                _this.listOutlet = new Array();
                _this.getData('', 0, 10, data, 'No');
            }
        });
        alert.present();
    };
    HistoryPage.prototype.detail = function (id) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__pages_detailhistory_detailhistory__["a" /* DetailhistoryPage */], { id: id });
    };
    HistoryPage.prototype.dashboard = function (filter) {
        this.listOutlet = new Array();
        this.getData('', 0, 10, filter, 'No');
    };
    HistoryPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-history',template:/*ion-inline-start:"D:\ionic\Project\qrcode\src\pages\history\history.html"*/`<ion-header>\n\n  <ion-navbar color="oren">\n    <ion-title>\n      <div>History {{filter}}</div>\n    </ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="form_search()">\n        <ion-icon name="search"></ion-icon>&nbsp;&nbsp;\n      </button>\n    </ion-buttons>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="filtering()">\n        <ion-icon name="clipboard"></ion-icon>&nbsp;&nbsp;\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content no-padding class="bgc">\n\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n    <ion-refresher-content pullingIcon="arrow-dropdown" pullingText="Pull to refresh" refreshingSpinner="crescent"\n      refreshingText="">\n    </ion-refresher-content>\n  </ion-refresher>\n\n  <ion-grid fixed class="wrapper">\n    <ion-list>\n      <ion-item-divider color="light">\n        <table style="width: 100%;">\n          <tr>\n            <td (click)="dashboard(\'pending\')">\n              <table style="width: 33%;">\n                <tr>\n                  <td style="width: 100%;">\n                    <div style="color:#f4f5f8;background-color: #ffd31a;border-radius: 50%; width: 10px;height: 10px;">\n                      .\n                    </div>\n                  </td>\n                  <td style="width: 100%;">&nbsp;&nbsp;Pending : {{pending}}</td>\n                </tr>\n              </table>\n            </td>\n            <td (click)="dashboard(\'validate\')">\n              <table style="width: 33%;">\n                <tr>\n                  <td style="width: 100%;">\n                    <div style="color:#f4f5f8;background-color: #ffd31a;border-radius: 50%; width: 10px;height: 10px;">\n                      .\n                    </div>\n                  </td>\n                  <td style="width: 100%;">&nbsp;&nbsp;Validate : {{validate}}</td>\n                </tr>\n              </table>\n            </td>\n            <td (click)="dashboard(\'reject\')">\n              <table style="width: 33%;">\n                <tr>\n                  <td style="width: 100%;">\n                    <div style="color:#f4f5f8;background-color: #ffd31a;border-radius: 50%; width: 10px;height: 10px;">\n                      .\n                    </div>\n                  </td>\n                  <td style="width: 100%;">&nbsp;&nbsp;Reject : {{reject}}</td>\n                </tr>\n              </table>\n            </td>\n          </tr>\n        </table>\n      </ion-item-divider>\n\n      <ion-item *ngFor="let val of listOutlet">\n        <ion-avatar item-start>\n          <img src="assets/imgs/setting.png">\n        </ion-avatar>\n        <h2>{{val.KdOutlet}} - {{val.OutletName}} <ion-icon *ngIf="val.Status==\'1\'" name="checkmark-circle"\n            color="secondary"></ion-icon>\n          <ion-icon *ngIf="val.Status==\'2\'" name="close-circle" color="danger"></ion-icon>\n        </h2>\n        <p>{{val.Address}}</p>\n        <h3>Uploader : {{val.UserUpload}}</h3>\n        <h3>Date Upload : {{val.DateUpload}}</h3>\n        <h3 *ngIf="val.Status!=\'0\'">Validator : {{val.UserValidate}}</h3>\n        <h3 *ngIf="val.Status!=\'0\'">Date Validate : {{val.DateValidate}}</h3>\n        <p *ngIf="val.Note!=\'\'">Note Uploader: {{val.Note}}</p>\n        <p>Source Data: {{val.tipedata}}</p>\n        <p *ngIf="val.NoteValidator!=\'\'">Note Validator: {{val.NoteValidator}}</p>\n        <ion-icon name="ios-arrow-forward" item-end (click)="detail(val.id)"></ion-icon>\n      </ion-item>\n    </ion-list>\n\n    <ion-infinite-scroll (ionInfinite)="doInfinite($event,src, first,filter)">\n      <ion-infinite-scroll-content loadingSpinner="bubbles" loadingText="Mencari Data Lagi...">\n      </ion-infinite-scroll-content>\n    </ion-infinite-scroll>\n\n  </ion-grid>\n</ion-content>`/*ion-inline-end:"D:\ionic\Project\qrcode\src\pages\history\history.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__config_provider__["a" /* Provider */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], HistoryPage);
    return HistoryPage;
}());

//# sourceMappingURL=history.js.map

/***/ }),

/***/ 141:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_provider__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_app_update__ = __webpack_require__(335);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_bypass_bypass__ = __webpack_require__(72);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_login_login__ = __webpack_require__(57);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











/**
 * Generated class for the SettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

var SettingsPage = (function () {
    function SettingsPage(storage, alertCtrl, app, toastCtrl, appUpdate, postPvdr, viewCtrl, loadingCtrl, navCtrl, navParams) {
        var _this = this;
        this.storage = storage;
        this.alertCtrl = alertCtrl;
        this.app = app;
        this.toastCtrl = toastCtrl;
        this.appUpdate = appUpdate;
        this.postPvdr = postPvdr;
        this.viewCtrl = viewCtrl;
        this.loadingCtrl = loadingCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.version = '150320';
        this.update = 'No';
        this.press = 0;
        this.storage.get('user_session_storage').then(function (res) {
            if (res[0].jenis_outlet == "distributor" || res[0].username == 'irfan1404') {
                _this.view_change_password = "Yes";
            }
            else {
                _this.view_change_password = "No";
            }
        });
        this.updates();
    }
    SettingsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SettingsPage');
    };
    SettingsPage.prototype.updates = function () {
        var _this = this;
        this.storage.get('user_session_storage').then(function (res) {
            var body = {
                username: res[0].username
            };
            _this.postPvdr.postData(body, 'login/cek_update').subscribe(function (data) {
                console.log('update version : ', data);
                if (data.success) {
                    if (data.result != _this.version) {
                        _this.update = 'Yes';
                    }
                    else {
                        _this.update = 'No';
                    }
                }
                else {
                    _this.update = 'No';
                }
            }, function (error) {
                _this.update = 'No';
            });
        });
    };
    SettingsPage.prototype.check_update = function () {
        var updateUrl = 'http://api.vci.co.id/public/ota/update.xml';
        this.appUpdate.checkAppUpdate(updateUrl).then(function () { console.log('Update available'); });
    };
    SettingsPage.prototype.logout = function () {
        this.viewCtrl.dismiss();
        this.storage.clear();
        this.app.getRootNav().setRoot(__WEBPACK_IMPORTED_MODULE_6__pages_login_login__["a" /* LoginPage */]);
    };
    SettingsPage.prototype.change_password = function () {
        var _this = this;
        this.storage.get('user_session_storage').then(function (res) {
            var prompt = _this.alertCtrl.create({
                title: 'Ganti Password',
                message: "Silahkan mengganti password anda.",
                inputs: [
                    {
                        name: 'old_password',
                        type: 'password',
                        placeholder: 'Password Lama'
                    },
                    {
                        name: 'new_password',
                        type: 'password',
                        placeholder: 'Password Baru'
                    }
                ],
                buttons: [
                    {
                        text: 'Batal',
                        handler: function (data) {
                            console.log('Cancel clicked');
                        }
                    },
                    {
                        text: 'Simpan',
                        handler: function (data) {
                            console.log('Saved clicked');
                            if (data.old_password != res[0].password) {
                                var alert_1 = _this.alertCtrl.create({
                                    title: 'Info',
                                    subTitle: 'Password Lama Anda Salah. Silahkan Ulangi Lagi.',
                                    buttons: ['OK']
                                });
                                alert_1.present();
                            }
                            else {
                                var loader_1 = _this.loadingCtrl.create({
                                    content: "Mohon Menunggu..."
                                });
                                loader_1.present();
                                var body = {
                                    kdcabang: res[0].cabang_id,
                                    kdoutlet: res[0].kdoutlet,
                                    kdsales: res[0].kdsales,
                                    username: res[0].username,
                                    new_password: data.new_password
                                };
                                _this.postPvdr.postData(body, 'login/change_password').subscribe(function (data) {
                                    if (data.success) {
                                        loader_1.dismiss();
                                        var prompts = _this.alertCtrl.create({
                                            title: 'Info',
                                            subTitle: 'Ganti Password Sukses. silahkan login kembali.',
                                            buttons: [
                                                {
                                                    text: 'Oke',
                                                    handler: function (res) {
                                                        _this.logout();
                                                    }
                                                }
                                            ]
                                        });
                                        prompts.present();
                                        return false;
                                    }
                                    else {
                                        loader_1.dismiss();
                                        var alert_2 = _this.alertCtrl.create({
                                            title: 'Info',
                                            subTitle: 'Ganti Password Gagal.',
                                            message: '',
                                            buttons: ['OK']
                                        });
                                        alert_2.present();
                                        return false;
                                    }
                                }, function (error) {
                                    loader_1.dismiss();
                                    var alert = _this.alertCtrl.create({
                                        title: 'Mohon Maaf',
                                        subTitle: 'Sedang tidak terkoneksi dengan server. Silahkan coba kembali beberapa saat',
                                        buttons: ['OK']
                                    });
                                    alert.present();
                                    return false;
                                });
                            }
                        }
                    }
                ]
            });
            prompt.present();
        });
    };
    SettingsPage.prototype.developer_option = function (e) {
        var _this = this;
        var a = this.press++;
        if (a == 3) {
            var prompt_1 = this.alertCtrl.create({
                title: 'Developer Access',
                message: "Enter Your PIN Access Developer",
                inputs: [
                    {
                        name: 'developerid',
                        type: 'password',
                        placeholder: 'ID Developer'
                    },
                ],
                buttons: [
                    {
                        text: 'Cancel',
                        handler: function (data) {
                            //console.log('Cancel clicked');
                        }
                    },
                    {
                        text: 'Access',
                        handler: function (data) {
                            if (data.developerid == '120187') {
                                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__pages_bypass_bypass__["a" /* BypassPage */]);
                            }
                            else {
                                var toast = _this.toastCtrl.create({
                                    message: 'Opps Sorry, you do not have access.',
                                    duration: 3000,
                                    position: 'top'
                                });
                                toast.present();
                            }
                        }
                    }
                ]
            });
            prompt_1.present();
        }
    };
    SettingsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-settings',template:/*ion-inline-start:"D:\ionic\Project\qrcode\src\pages\settings\settings.html"*/`<ion-header>\n\n  <ion-navbar color="oren">\n    <ion-title>\n      <div>Pengaturan</div>\n    </ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content no-padding class="bgc">\n  <ion-grid fixed class="wrapper">\n\n\n\n    <ion-list>\n\n      <ion-item (click)="logout()">\n        <ion-icon name="log-out" item-start></ion-icon>\n        Keluar\n      </ion-item>\n      <ion-item (click)="change_password()" *ngIf="view_change_password==\'Yes\'">\n        <ion-icon name="key" item-start></ion-icon>\n        Ganti Password\n      </ion-item>\n      <ion-item (tap)="developer_option($event)">\n        <ion-icon name="build" item-start></ion-icon>\n        Developer Mode\n      </ion-item>\n      <ion-item>\n        <ion-icon name="ios-cog" item-start></ion-icon>\n        GoCheck V.150320\n        <ion-note item-end *ngIf="update==\'Yes\'">\n          <button ion-button color="danger" round (click)="check_update()">update</button>\n        </ion-note>\n      </ion-item>\n      <ion-item>\n        <ion-icon name="calendar" item-start></ion-icon>\n        First Release At 2020-02-20\n      </ion-item>\n\n\n    </ion-list>\n  </ion-grid>\n</ion-content>`/*ion-inline-end:"D:\ionic\Project\qrcode\src\pages\settings\settings.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_app_update__["a" /* AppUpdate */],
            __WEBPACK_IMPORTED_MODULE_3__config_provider__["a" /* Provider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], SettingsPage);
    return SettingsPage;
}());

//# sourceMappingURL=settings.js.map

/***/ }),

/***/ 142:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LogsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_provider__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the LogsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LogsPage = (function () {
    function LogsPage(navCtrl, viewCtrl, postPvdr, alertCtrl, loadingCtrl, storage, navParams) {
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.postPvdr = postPvdr;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.storage = storage;
        this.navParams = navParams;
    }
    LogsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LogsPage');
        this.listLogs = new Array();
        this.getData('', 0, 10, 'No');
    };
    LogsPage.prototype.form_search = function () {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'Cari',
            message: "Masukan kata untuk mencari username",
            inputs: [
                {
                    name: 'src',
                    placeholder: 'Apa yang anda cari...'
                },
            ],
            buttons: [
                {
                    text: 'Batal',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Cari',
                    handler: function (data) {
                        console.log('Saved clicked');
                        _this.getListLogs(data.src);
                    }
                }
            ]
        });
        prompt.present();
    };
    LogsPage.prototype.getListLogs = function (src) {
        if (src) {
            src = src;
        }
        else {
            src = '';
        }
        this.listLogs = new Array();
        this.getData(src, 0, 10, 'No');
    };
    LogsPage.prototype.getData = function (src, first, offset, infinite) {
        var _this = this;
        this.first = first;
        this.src = src;
        var loader = this.loadingCtrl.create({
            content: "Mohon Menunggu..."
        });
        if (infinite == 'No') {
            loader.present();
        }
        this.storage.get('user_session_storage').then(function (res) {
            var body = {
                kdcabang: res[0].cabang_id,
                first: first,
                offset: offset,
                filter: '',
                keyword: src,
                user_level: res[0].user_level,
                kdoutlet: res[0].kdoutlet
            };
            _this.postPvdr.postData(body, 'outlet/getLogs').subscribe(function (data) {
                if (data.success) {
                    loader.dismiss();
                    for (var i = 0; i < data.result.length; i++) {
                        var platform;
                        if (data.result[i]['platform'] == 'M') {
                            platform = 'Mobile';
                        }
                        else {
                            platform = 'Browser';
                        }
                        _this.listLogs.push({
                            username: data.result[i]['username'],
                            note: data.result[i]['note'],
                            platform: platform,
                            date_log: data.result[i]['date_log']
                        });
                    }
                }
                else {
                    loader.dismiss();
                    if (infinite == 'No') {
                        var alert_1 = _this.alertCtrl.create({
                            title: 'Info',
                            subTitle: 'Mohon Maaf Data Tidak Di Temukan',
                            message: '',
                            buttons: ['OK']
                        });
                        alert_1.present();
                    }
                    return false;
                }
            }, function (error) {
                loader.dismiss();
                var alert = _this.alertCtrl.create({
                    title: 'Mohon Maaf',
                    subTitle: 'Sedang tidak terkoneksi dengan server. Silahkan coba kembali beberapa saat',
                    buttons: ['OK']
                });
                alert.present();
                return false;
            });
        });
    };
    LogsPage.prototype.doRefresh = function (refresher) {
        this.listLogs = new Array();
        this.getData('', 0, 10, 'No');
        setTimeout(function () {
            refresher.complete();
        }, 1000);
    };
    LogsPage.prototype.doInfinite = function (infiniteScroll, src, first, filter) {
        var _this = this;
        first = first + 10;
        console.log('Begin async operation');
        setTimeout(function () {
            _this.getData(src, first, 10, 'Yes');
            infiniteScroll.complete();
        }, 1000);
    };
    LogsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-logs',template:/*ion-inline-start:"D:\ionic\Project\qrcode\src\pages\logs\logs.html"*/`<ion-header>\n\n  <ion-navbar color="oren">\n    <ion-title>\n      <div>Logs</div>\n    </ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="form_search()">\n        <ion-icon name="search"></ion-icon>&nbsp;&nbsp;\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content no-padding class="bgc">\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n    <ion-refresher-content pullingIcon="arrow-dropdown" pullingText="Pull to refresh" refreshingSpinner="crescent"\n      refreshingText="">\n    </ion-refresher-content>\n  </ion-refresher>\n  <ion-grid fixed class="wrapper">\n    <ion-list>\n      <ion-item *ngFor="let val of listLogs">\n        <h2>{{val.username}}</h2>\n        <p>Note : {{val.note}}</p>\n        <p>Platform : {{val.platform}}</p>\n        <p>Time : {{val.date_log}}</p>\n      </ion-item>\n    </ion-list>\n    <ion-infinite-scroll (ionInfinite)="doInfinite($event,src, first)">\n      <ion-infinite-scroll-content loadingSpinner="bubbles" loadingText="Loading more data...">\n      </ion-infinite-scroll-content>\n    </ion-infinite-scroll>\n  </ion-grid>\n</ion-content>`/*ion-inline-end:"D:\ionic\Project\qrcode\src\pages\logs\logs.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_2__config_provider__["a" /* Provider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], LogsPage);
    return LogsPage;
}());

//# sourceMappingURL=logs.js.map

/***/ }),

/***/ 143:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TraficPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_provider__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_chart_js__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_chart_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_chart_js__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the TraficPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TraficPage = (function () {
    function TraficPage(navCtrl, toastCtrl, loadingCtrl, postPvdr) {
        this.navCtrl = navCtrl;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.postPvdr = postPvdr;
    }
    TraficPage.prototype.ngAfterViewInit = function () {
        var _this = this;
        setTimeout(function () {
            _this.barChart = _this.getBarChart();
            _this.lineChart = _this.getLineChart();
        }, 150);
        setTimeout(function () {
            _this.pieChart = _this.getPieChart();
            _this.doughnutChart = _this.getDoughnutChart();
        }, 250);
    };
    TraficPage.prototype.getChart = function (context, chartType, data, options) {
        return new __WEBPACK_IMPORTED_MODULE_3_chart_js___default.a(context, {
            data: data,
            options: options,
            type: chartType
        });
    };
    TraficPage.prototype.getBarChart = function () {
        var loader = this.loadingCtrl.create({
            content: "Please wait..."
        });
        loader.present();
        loader.dismiss();
        //content chart --------------
        var data = {
            labels: ['Anton', 'Imam', 'Haryati', 'Nova', 'Upy'],
            datasets: [{
                    label: 'Traffic Penggunaan Apps',
                    data: [75, 45, 30, 100, 65],
                    backgroundColor: [
                        'rgb(255, 0, 0)',
                        'rgb(20, 0, 255)',
                        'rgb(255, 230, 0)',
                        'rgb(0, 255, 10)',
                        'rgb(60, 0, 70)'
                    ],
                    borderWidth: 1
                }]
        };
        var options = {
            scales: {
                yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
            }
        };
        return this.getChart(this.barCanvas.nativeElement, 'bar', data, options);
    };
    TraficPage.prototype.getLineChart = function () {
        var data = {
            labels: ['Sen', 'Sel', 'Rab', 'Kam'],
            datasets: [{
                    label: 'Traffic',
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: 'rgb(117, 0, 49)',
                    borderColor: 'rgb(51, 50, 46)',
                    borderCapStyle: 'butt',
                    borderJoinStyle: 'miter',
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: [29, 35, 50, 0],
                    scanGaps: false,
                }
            ]
        };
        return this.getChart(this.lineCanvas.nativeElement, 'line', data);
    };
    TraficPage.prototype.getPieChart = function () {
        var data = {
            labels: ['OH', 'BLACK EYE', 'RESTO'],
            datasets: [{
                    data: [300, 75, 224],
                    backgroundColor: ['rgb(200, 6, 0)', 'rgb(36, 0, 255)', 'rgb(242, 255, 0)']
                }]
        };
        return this.getChart(this.pieCanvas.nativeElement, 'pie', data);
    };
    TraficPage.prototype.getDoughnutChart = function () {
        var data = {
            labels: ['Bali Tour', 'Jetwings', 'Abbey'],
            datasets: [{
                    label: 'Teste Chart',
                    data: [12, 65, 32],
                    backgroundColor: [
                        'rgb(0, 244, 97)',
                        'rgb(37, 39, 43)',
                        'rgb(255, 207, 0)'
                    ]
                }]
        };
        return this.getChart(this.doughnutCanvas.nativeElement, 'doughnut', data);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('barCanvas'),
        __metadata("design:type", Object)
    ], TraficPage.prototype, "barCanvas", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('lineCanvas'),
        __metadata("design:type", Object)
    ], TraficPage.prototype, "lineCanvas", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('pieCanvas'),
        __metadata("design:type", Object)
    ], TraficPage.prototype, "pieCanvas", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('doughnutCanvas'),
        __metadata("design:type", Object)
    ], TraficPage.prototype, "doughnutCanvas", void 0);
    TraficPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-trafic',template:/*ion-inline-start:"D:\ionic\Project\qrcode\src\pages\trafic\trafic.html"*/`<!--\n  Generated template for the TraficPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="oren">\n    <ion-title>Traffic Apps</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content no-padding class="bgc">\n  <ion-grid fixed class="wrapper">\n    <ion-card id="visitors">\n      <ion-card-header>\n        Traffic Today<br>\n      </ion-card-header>\n      <ion-card-content>\n        <canvas #barCanvas></canvas>\n      </ion-card-content>\n    </ion-card>\n\n    <ion-card id="omzet">\n      <ion-card-header>\n        Traffic Weekly\n      </ion-card-header>\n      <ion-card-content>\n        <canvas #lineCanvas></canvas>\n      </ion-card-content>\n    </ion-card>\n\n    <ion-card id="omzetDetail" style="display:none">\n      <ion-card-header>\n        Komposisi Omzet Januari 2019\n      </ion-card-header>\n      <ion-card-content>\n        <canvas #pieCanvas></canvas>\n      </ion-card-content>\n    </ion-card>\n\n    <ion-card id="travel" style="display:none">\n      <ion-card-header>\n        Travel Penyumbang Omzet\n      </ion-card-header>\n      <ion-card-content>\n        <canvas #doughnutCanvas></canvas>\n      </ion-card-content>\n    </ion-card>\n  </ion-grid>\n</ion-content>`/*ion-inline-end:"D:\ionic\Project\qrcode\src\pages\trafic\trafic.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_2__config_provider__["a" /* Provider */]])
    ], TraficPage);
    return TraficPage;
}());

//# sourceMappingURL=trafic.js.map

/***/ }),

/***/ 144:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RutesalesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_provider__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_detailrutesales_detailrutesales__ = __webpack_require__(145);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the RutesalesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

var RutesalesPage = (function () {
    function RutesalesPage(navCtrl, viewCtrl, postPvdr, alertCtrl, storage, loadingCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.postPvdr = postPvdr;
        this.alertCtrl = alertCtrl;
        this.storage = storage;
        this.loadingCtrl = loadingCtrl;
        this.navParams = navParams;
        this.kdoutlet = navParams.get("kdoutlet");
        this.kdcabang = navParams.get("kdcabang");
    }
    RutesalesPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RutesalesPage');
        this.listUsers = new Array();
        this.getData('', 0, 10, 'No');
    };
    RutesalesPage.prototype.form_search = function () {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'Cari',
            message: "Masukan kata untuk mencari kode atau nama user",
            inputs: [
                {
                    name: 'src',
                    placeholder: 'Apa yang anda cari...'
                },
            ],
            buttons: [
                {
                    text: 'Batal',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Cari',
                    handler: function (data) {
                        console.log('Saved clicked');
                        _this.getListUserDistributor(data.src);
                    }
                }
            ]
        });
        prompt.present();
    };
    RutesalesPage.prototype.getListUserDistributor = function (src) {
        this.listUsers = new Array();
        if (src) {
            src = src;
        }
        else {
            src = '';
        }
        this.getData(src, 0, 10, 'No');
    };
    RutesalesPage.prototype.getData = function (src, first, offset, infinite) {
        var _this = this;
        this.first = first;
        this.src = src;
        var loader = this.loadingCtrl.create({
            content: "Mohon Menunggu..."
        });
        if (infinite == 'No') {
            loader.present();
        }
        this.storage.get('user_session_storage').then(function (res) {
            var body = {
                kdcabang: _this.kdcabang,
                first: first,
                offset: offset,
                filter: '',
                keyword: src,
                kdoutlet: _this.kdoutlet,
                user_level: res[0].user_level,
                username: res[0].username
            };
            _this.postPvdr.postData(body, 'outlet/getListUserDistributor2').subscribe(function (data) {
                if (data.success) {
                    loader.dismiss();
                    for (var i = 0; i < data.result.length; i++) {
                        _this.listUsers.push({
                            username: data.result[i]['username'],
                            name: data.result[i]['name'],
                            kdsales: data.result[i]['kdsales'],
                            kdoutlet: data.result[i]['kdoutlet'],
                            kdcabang: data.result[i]['kdcabang'],
                            jml_toko: data.result[i]['jml_toko']
                        });
                    }
                }
                else {
                    loader.dismiss();
                    if (infinite == 'No') {
                        var alert_1 = _this.alertCtrl.create({
                            title: 'Info',
                            subTitle: 'Mohon Maaf Data Tidak Di Temukan',
                            message: '',
                            buttons: ['OK']
                        });
                        alert_1.present();
                    }
                    return false;
                }
            }, function (error) {
                loader.dismiss();
                var alert = _this.alertCtrl.create({
                    title: 'Mohon Maaf',
                    subTitle: 'Sedang tidak terkoneksi dengan server. Silahkan coba kembali beberapa saat',
                    buttons: ['OK']
                });
                alert.present();
                return false;
            });
        });
    };
    RutesalesPage.prototype.doRefresh = function (refresher) {
        this.listUsers = new Array();
        this.getData('', 0, 10, 'No');
        setTimeout(function () {
            refresher.complete();
        }, 1000);
    };
    RutesalesPage.prototype.doInfinite = function (infiniteScroll, src, first) {
        var _this = this;
        first = first + 10;
        setTimeout(function () {
            _this.getData(src, first, 10, 'Yes');
            infiniteScroll.complete();
        }, 1000);
    };
    RutesalesPage.prototype.detail_rute = function (kdsales, kdcabang, kdoutlet, name_sales) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__pages_detailrutesales_detailrutesales__["a" /* DetailrutesalesPage */], { kdoutlet: kdoutlet, kdcabang: kdcabang, kdsales: kdsales, title: name_sales });
    };
    RutesalesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-rutesales',template:/*ion-inline-start:"D:\ionic\Project\qrcode\src\pages\rutesales\rutesales.html"*/`<!--\n  Generated template for the RutesalesPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="oren">\n    <ion-title>Rute Sales</ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="form_search()">\n        <ion-icon name="search"></ion-icon>&nbsp;&nbsp;\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content no-padding class="bgc">\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n    <ion-refresher-content pullingIcon="arrow-dropdown" pullingText="Pull to refresh" refreshingSpinner="crescent"\n      refreshingText="">\n    </ion-refresher-content>\n  </ion-refresher>\n  <ion-grid fixed class="wrapper">\n    <ion-list>\n      <ion-item *ngFor="let val of listUsers">\n        <h2>{{val.name}}</h2>\n        <h3>Username : {{val.username}}</h3>\n        <p>Kode Sales : {{val.kdsales}}</p>\n        <button ion-button clear item-end\n          (click)="detail_rute(val.kdsales,val.kdcabang,val.kdoutlet,val.name)">{{val.jml_toko}}\n          toko</button>\n      </ion-item>\n    </ion-list>\n    <ion-infinite-scroll (ionInfinite)="doInfinite($event,src, first)">\n      <ion-infinite-scroll-content loadingSpinner="bubbles" loadingText="Mencari Data Lagi...">\n      </ion-infinite-scroll-content>\n    </ion-infinite-scroll>\n  </ion-grid>\n</ion-content>`/*ion-inline-end:"D:\ionic\Project\qrcode\src\pages\rutesales\rutesales.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_2__config_provider__["a" /* Provider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], RutesalesPage);
    return RutesalesPage;
}());

//# sourceMappingURL=rutesales.js.map

/***/ }),

/***/ 145:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailrutesalesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_provider__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_usersdistributor_usersdistributor__ = __webpack_require__(36);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









/**
 * Generated class for the DetailrutesalesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var DetailrutesalesPage = (function () {
    function DetailrutesalesPage(navCtrl, viewCtrl, modalCtrl, alertCtrl, loadingCtrl, postPvdr, storage, navParams) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.postPvdr = postPvdr;
        this.storage = storage;
        this.navParams = navParams;
        this.kdoutlet = navParams.get("kdoutlet");
        this.kdcabang = navParams.get("kdcabang");
        this.kdsales = navParams.get("kdsales");
        this.title = navParams.get("title");
        this.storage.get('user_session_storage').then(function (res) {
            if ((res[0].user_level == '-1' && res[0].developer == '1') || res[0].user_level == '2') {
                _this.hak_swtiching = 'Yes';
            }
            else {
                _this.hak_swtiching = 'No';
            }
        });
    }
    DetailrutesalesPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DetailrutesalesPage');
        this.listOutlet = new Array();
        this.getData('', 0, 10, 'No');
    };
    DetailrutesalesPage.prototype.form_search = function () {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'Cari',
            message: "Masukan kata untuk mencari kode atau nama toko",
            inputs: [
                {
                    name: 'src',
                    placeholder: 'Apa yang anda cari...'
                },
            ],
            buttons: [
                {
                    text: 'Batal',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Cari',
                    handler: function (data) {
                        console.log('Saved clicked');
                        _this.getListOutletsDistributor(data.src);
                    }
                }
            ]
        });
        prompt.present();
    };
    DetailrutesalesPage.prototype.getListOutletsDistributor = function (src) {
        this.listOutlet = new Array();
        if (src) {
            src = src;
        }
        else {
            src = '';
        }
        this.getData(src, 0, 10, 'No');
    };
    DetailrutesalesPage.prototype.getData = function (src, first, offset, infinite) {
        var _this = this;
        this.first = first;
        this.src = src;
        var loader = this.loadingCtrl.create({
            content: "Mohon Menunggu..."
        });
        if (infinite == 'No') {
            loader.present();
        }
        this.storage.get('user_session_storage').then(function (res) {
            var body = {
                kdcabang: _this.kdcabang,
                first: first,
                offset: offset,
                filter: '',
                keyword: src,
                kdsales: _this.kdsales,
                kdoutlet: _this.kdoutlet,
                user_level: res[0].user_level,
                username: res[0].username
            };
            _this.postPvdr.postData(body, 'outlet/getDetailOutletDistributor2').subscribe(function (data) {
                if (data.success) {
                    loader.dismiss();
                    for (var i = 0; i < data.result.length; i++) {
                        _this.listOutlet.push({
                            KdOutlet: data.result[i]['KdOutlet'],
                            Nama: data.result[i]['Nama'],
                            KotaToko: data.result[i]['KotaToko'],
                            statuz_pending: data.result[i]['statuz_pending'],
                            statuz_valid: data.result[i]['statuz_valid']
                        });
                    }
                }
                else {
                    loader.dismiss();
                    if (infinite == 'No') {
                        var alert_1 = _this.alertCtrl.create({
                            title: 'Info',
                            subTitle: 'Mohon Maaf Data Tidak Di Temukan',
                            message: '',
                            buttons: ['OK']
                        });
                        alert_1.present();
                    }
                    return false;
                }
            }, function (error) {
                loader.dismiss();
                var alert = _this.alertCtrl.create({
                    title: 'Mohon Maaf',
                    message: 'Sedang tidak terkoneksi dengan server. Silahkan coba kembali beberapa saat',
                    buttons: ['OK']
                });
                alert.present();
                return false;
            });
        });
    };
    DetailrutesalesPage.prototype.doRefresh = function (refresher) {
        this.listOutlet = new Array();
        this.getData('', 0, 10, 'No');
        setTimeout(function () {
            refresher.complete();
        }, 1000);
    };
    DetailrutesalesPage.prototype.doInfinite = function (infiniteScroll, src, first) {
        var _this = this;
        first = first + 10;
        setTimeout(function () {
            _this.getData(src, first, 10, 'Yes');
            infiniteScroll.complete();
        }, 1000);
    };
    DetailrutesalesPage.prototype.switching = function (kdoutlet, kdcabang, kddistributor) {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: 'Info',
            message: 'Apakah anda yakin akan melakukan swtiching toko ini dengan sales lain?',
            buttons: [
                {
                    text: 'Batal',
                    handler: function () {
                        console.log('Disagree clicked');
                    }
                },
                {
                    text: 'Ya',
                    handler: function () {
                        var modal = _this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__pages_usersdistributor_usersdistributor__["a" /* UsersdistributorPage */], { kdoutlet: kddistributor, kdcabang: kdcabang, performa: 'No', close: 'Yes', bisa_pilih: 'Yes' });
                        modal.present();
                        modal.onDidDismiss(function (val) {
                            if (val) {
                                if (val.kdsales == '') {
                                    var alert_2 = _this.alertCtrl.create({
                                        title: 'Mohon Maaf',
                                        message: 'Pilhan semua tidak berlaku.',
                                        buttons: ['OK']
                                    });
                                    alert_2.present();
                                    return false;
                                }
                                else {
                                    if (_this.kdsales == val.kdsales) {
                                        var alert_3 = _this.alertCtrl.create({
                                            title: 'Mohon Maaf',
                                            message: 'Tidak Bisa Swtich Dengan Sales Yang Sama.',
                                            buttons: ['OK']
                                        });
                                        alert_3.present();
                                        return false;
                                    }
                                    else {
                                        _this.switch(kdoutlet, val.kdsales, _this.kdsales, kdcabang, kddistributor);
                                    }
                                }
                            }
                            else {
                                //coding if else or empty
                            }
                        });
                    }
                }
            ]
        });
        confirm.present();
    };
    DetailrutesalesPage.prototype.switch = function (kdoutlet, kdsales, kodesales, kdcabang, kddistributor) {
        var _this = this;
        var loader = this.loadingCtrl.create({
            content: "Proses Swtiching..."
        });
        loader.present();
        this.storage.get('user_session_storage').then(function (res) {
            var body = {
                kdcabang: kdcabang,
                kdsales: kdsales,
                kodesales: kodesales,
                kdoutlet: kdoutlet,
                kddistributor: kddistributor,
                username: res[0].username
            };
            _this.postPvdr.postData(body, 'outlet/swtichingRuteSales').subscribe(function (data) {
                if (data.success) {
                    loader.dismiss();
                    var confirm_1 = _this.alertCtrl.create({
                        title: 'Info',
                        message: 'Swtiching Berhasil',
                        buttons: [
                            {
                                text: 'Oke',
                                handler: function () {
                                    _this.ionViewDidLoad();
                                }
                            }
                        ]
                    });
                    confirm_1.present();
                }
                else {
                    loader.dismiss();
                    var alert_4 = _this.alertCtrl.create({
                        title: 'Info',
                        subTitle: 'Swtiching Gagal',
                        message: '',
                        buttons: ['OK']
                    });
                    alert_4.present();
                    return false;
                }
            }, function (error) {
                loader.dismiss();
                var alert = _this.alertCtrl.create({
                    title: 'Mohon Maaf',
                    message: 'Sedang tidak terkoneksi dengan server. Silahkan coba kembali beberapa saat',
                    buttons: ['OK']
                });
                alert.present();
                return false;
            });
        });
    };
    DetailrutesalesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-detailrutesales',template:/*ion-inline-start:"D:\ionic\Project\qrcode\src\pages\detailrutesales\detailrutesales.html"*/`<ion-header>\n  <ion-navbar color="oren">\n    <ion-title>\n      <div>List Toko {{title}}</div>\n    </ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="form_search()">\n        <ion-icon name="search"></ion-icon>&nbsp;&nbsp;\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content no-padding class="bgc">\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n    <ion-refresher-content pullingIcon="arrow-dropdown" pullingText="Pull to refresh" refreshingSpinner="crescent"\n      refreshingText="">\n    </ion-refresher-content>\n  </ion-refresher>\n  <ion-grid fixed class="wrapper">\n    <ion-list>\n      <ion-item *ngFor="let val of listOutlet">\n        <h2>{{val.Nama}}</h2>\n        <h3>Kode Toko : {{val.KdOutlet}}</h3>\n        <p>Alamat Toko : {{val.KotaToko}}</p>\n        <ion-note item-end color="warning" *ngIf="val.statuz_pending==\'1\'">\n          Pending&nbsp;&nbsp;\n        </ion-note>\n        <ion-note item-end color="secondary" *ngIf="val.statuz_valid==\'1\'">\n          Validate&nbsp;&nbsp;\n        </ion-note>\n        <ion-icon *ngIf="hak_swtiching==\'Yes\'" color="danger" style="font-weight:bold" name="ios-shuffle" item-end\n          (click)="switching(val.KdOutlet,kdcabang,kdoutlet)">\n        </ion-icon>\n\n\n      </ion-item>\n\n    </ion-list>\n    <ion-infinite-scroll (ionInfinite)="doInfinite($event,src, first)">\n      <ion-infinite-scroll-content loadingSpinner="bubbles" loadingText="Mencari Data Lagi...">\n      </ion-infinite-scroll-content>\n    </ion-infinite-scroll>\n  </ion-grid>\n</ion-content>`/*ion-inline-end:"D:\ionic\Project\qrcode\src\pages\detailrutesales\detailrutesales.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_2__config_provider__["a" /* Provider */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], DetailrutesalesPage);
    return DetailrutesalesPage;
}());

//# sourceMappingURL=detailrutesales.js.map

/***/ }),

/***/ 155:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 155;

/***/ }),

/***/ 197:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/activities/activities.module": [
		486,
		26
	],
	"../pages/admin/admin.module": [
		488,
		25
	],
	"../pages/analytics/analytics.module": [
		487,
		24
	],
	"../pages/bypass/bypass.module": [
		489,
		23
	],
	"../pages/detailbangetlistings/detailbangetlistings.module": [
		490,
		22
	],
	"../pages/detailhistory/detailhistory.module": [
		491,
		21
	],
	"../pages/detaillistings/detaillistings.module": [
		492,
		20
	],
	"../pages/detailrutesales/detailrutesales.module": [
		493,
		19
	],
	"../pages/formoutlet/formoutlet.module": [
		494,
		18
	],
	"../pages/formoutletpwa/formoutletpwa.module": [
		496,
		17
	],
	"../pages/history/history.module": [
		495,
		16
	],
	"../pages/listing/listing.module": [
		498,
		15
	],
	"../pages/listoutlet/listoutlet.module": [
		497,
		14
	],
	"../pages/login/login.module": [
		500,
		13
	],
	"../pages/logs/logs.module": [
		499,
		12
	],
	"../pages/main/main.module": [
		501,
		11
	],
	"../pages/noolist/noolist.module": [
		502,
		10
	],
	"../pages/performa/performa.module": [
		503,
		9
	],
	"../pages/petanoo/petanoo.module": [
		504,
		8
	],
	"../pages/petatoko/petatoko.module": [
		505,
		7
	],
	"../pages/rutesales/rutesales.module": [
		506,
		6
	],
	"../pages/search/search.module": [
		507,
		5
	],
	"../pages/searchactivities/searchactivities.module": [
		508,
		4
	],
	"../pages/settings/settings.module": [
		509,
		3
	],
	"../pages/tokoplus/tokoplus.module": [
		510,
		2
	],
	"../pages/trafic/trafic.module": [
		511,
		1
	],
	"../pages/usersdistributor/usersdistributor.module": [
		512,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 197;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 328:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetaillocationoutletPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_barcode_scanner__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_location_accuracy__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_android_permissions__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__config_provider__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_storage__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_screen_orientation__ = __webpack_require__(330);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};














var DetaillocationoutletPage = (function () {
    function DetaillocationoutletPage(navCtrl, alertCtrl, barcodeScanner, geolocation, viewCtrl, navParams, postPvdr, storage, plt, screenOrientation, locationAccuracy, androidPermissions, modalCtrl) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.barcodeScanner = barcodeScanner;
        this.geolocation = geolocation;
        this.viewCtrl = viewCtrl;
        this.navParams = navParams;
        this.postPvdr = postPvdr;
        this.storage = storage;
        this.plt = plt;
        this.screenOrientation = screenOrientation;
        this.locationAccuracy = locationAccuracy;
        this.androidPermissions = androidPermissions;
        this.modalCtrl = modalCtrl;
        this.base_url = 'http://api.vci.co.id/public/outlets/';
        this.qrData = null;
        this.createdCode = null;
        this.scannedCode = null;
        this.scanCodex(navParams.get("id"), navParams.get("lat"), navParams.get("lng"), navParams.get("outlet"), navParams.get("name"), navParams.get("address"));
        this.id = navParams.get("id");
        this.lat = navParams.get("lat");
        this.lng = navParams.get("lng");
        this.outlet = navParams.get("outlet");
        this.name = navParams.get("name");
        this.address = navParams.get("address");
        this.images = this.base_url + navParams.get("images");
        //if (this.plt.is('android')) {
        //    this.lockScreenOrientation();
        //}
    }
    DetaillocationoutletPage.prototype.ionViewDidLoad = function () {
        this.initMap();
    };
    DetaillocationoutletPage.prototype.lockScreenOrientation = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                try {
                    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.LANDSCAPE);
                }
                catch (error) {
                    console.log(error);
                }
                return [2 /*return*/];
            });
        });
    };
    //Check if application having GPS access permission  
    DetaillocationoutletPage.prototype.checkGPSPermission = function () {
        var _this = this;
        this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(function (result) {
            console.log('status harus melewati permission : ', result.hasPermission);
            if (result.hasPermission) {
                //If having permission show 'Turn On GPS' dialogue
                _this.askToTurnOnGPS();
                console.log('masuk ke pertanyaan permission');
            }
            else {
                //If not having permission ask for permission
                _this.requestGPSPermission();
                console.log('masuk ke request permission');
            }
        }, function (err) {
            console.log(err);
        });
    };
    DetaillocationoutletPage.prototype.requestGPSPermission = function () {
        var _this = this;
        this.locationAccuracy.canRequest().then(function (canRequest) {
            if (canRequest) {
                console.log("4");
            }
            else {
                //Show 'GPS Permission Request' dialogue
                _this.androidPermissions.requestPermission(_this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
                    .then(function () {
                    // call method to turn on GPS
                    _this.askToTurnOnGPS();
                    console.log('nyalakan GPS');
                }, function (error) {
                    //Show alert if user click on 'No Thanks'
                    console.log('requestPermission Error requesting location permissions ' + error);
                });
            }
        });
    };
    DetaillocationoutletPage.prototype.askToTurnOnGPS = function () {
        var _this = this;
        this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(function () {
            // When GPS Turned ON call method to get Accurate location coordinates
            console.log('masuk ke pencarian koordinat');
            _this.initMap();
        }, function (error) { return console.log('Error requesting location permissions ' + JSON.stringify(error)); });
    };
    DetaillocationoutletPage.prototype.initMap = function () {
        var myLatlng = { lat: -6.1741808, lng: 106.8079343 };
        var map = new google.maps.Map(this.mapElement.nativeElement, {
            zoom: 13,
            center: myLatlng,
            zoomControl: false,
            mapTypeControl: false,
            scaleControl: false,
            streetViewControl: false,
            rotateControl: false,
            fullscreenControl: false
        });
    };
    DetaillocationoutletPage.prototype.mylocationPage = function (marker) {
        google.maps.event.addListener(marker, 'click', function () {
        });
    };
    DetaillocationoutletPage.prototype.createCode = function () {
        this.createdCode = this.qrData;
    };
    DetaillocationoutletPage.prototype.scanCodex = function (id, lat, lng, outlet, name, address) {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'Info!',
            message: "Result QR Code Scanner",
            inputs: [
                {
                    name: 'outlet_name',
                    placeholder: 'outlet name',
                    value: name
                },
                {
                    name: 'address',
                    placeholder: 'address',
                    value: address
                },
                {
                    name: 'latitude',
                    placeholder: 'latitude',
                    value: lat
                },
                {
                    name: 'longitude',
                    placeholder: 'longitude',
                    value: lng
                },
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'View',
                    handler: function (data) {
                        console.log('Saved clicked');
                        var myLatlng = { lat: parseFloat(lat), lng: parseFloat(lng) };
                        var map = new google.maps.Map(_this.mapElement.nativeElement, {
                            zoom: 17,
                            center: myLatlng,
                            rotateControl: false,
                            fullscreenControl: false,
                            mapTypeControl: false,
                            scaleControl: false,
                            streetViewControl: true,
                            streetViewControlOptions: {
                                position: google.maps.ControlPosition.LEFT_TOP
                            },
                            zoomControl: true,
                            zoomControlOptions: {
                                position: google.maps.ControlPosition.LEFT_TOP
                            }
                        });
                        // var map = new google.maps.Map(this.mapElement.nativeElement, {
                        //   center: myLatlng,
                        //   zoom: 14
                        // });
                        var image = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';
                        var marker = new google.maps.Marker({
                            position: myLatlng,
                            map: map,
                            icon: image
                        });
                        //untuk button validasi
                        //google.maps.event.addListener(marker, 'click', () => {
                        //  this.detailLocation(id,outlet, lat, lng , name, address,'');
                        //});
                        var contentString = '<h4>' + data.outlet_name + '&nbsp;&nbsp;&nbsp;&nbsp;</h4>' +
                            '<div>' +
                            '<p>' + data.address + '&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '</p>' +
                            '</div>';
                        marker.content = contentString;
                        var infoWindow = new google.maps.InfoWindow();
                        google.maps.event.addListener(marker, 'click', function () {
                            infoWindow.setContent(this.content);
                            infoWindow.open(this.getMap(), this);
                        });
                        var panorama = new google.maps.StreetViewPanorama(document.getElementById('pano'), {
                            position: myLatlng,
                            fullscreenControl: false,
                            pov: {
                                heading: 34,
                                pitch: 10
                            }
                        });
                        map.setStreetView(panorama);
                    }
                }
            ]
        });
        prompt.present();
    };
    DetaillocationoutletPage.prototype.detailLocation = function (id, outlet, lat, lng, name, address, typevalidate) {
        var _this = this;
        var tipe;
        if (typevalidate == '1') {
            tipe = "Validate ";
        }
        else {
            tipe = "No Validate ";
        }
        var prompt = this.alertCtrl.create({
            title: tipe,
            message: tipe + "this location. save to server.",
            buttons: [
                {
                    text: 'Cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Yes',
                    handler: function (data) {
                        var prompts = _this.alertCtrl.create({
                            title: 'Info',
                            message: "Add Note For Information",
                            inputs: [
                                {
                                    name: 'sebab',
                                    placeholder: 'Note Here ...'
                                },
                            ],
                            buttons: [
                                {
                                    text: 'Cancel',
                                    handler: function (data) {
                                        console.log('Cancel clicked');
                                    }
                                },
                                {
                                    text: 'Send',
                                    handler: function (data) {
                                        _this.storage.get('user_session_storage').then(function (res) {
                                            var body = {
                                                id: id,
                                                outlet: outlet,
                                                kdcabang: res[0].cabang_id,
                                                lat: lat,
                                                lng: lng,
                                                name: name,
                                                address: address,
                                                notevalidate: data.sebab,
                                                status: typevalidate,
                                                marker: res[0].kdsales + ' - ' + res[0].employee_name
                                            };
                                            _this.postPvdr.postData(body, 'outlet/updateoutlet').subscribe(function (data) {
                                                var alert = _this.alertCtrl.create({
                                                    title: 'Info',
                                                    subTitle: data.subtitle,
                                                    message: data.msg,
                                                    buttons: ['OK']
                                                });
                                                alert.present();
                                            }, function (error) {
                                                var alert = _this.alertCtrl.create({
                                                    title: 'Info',
                                                    subTitle: error,
                                                    message: 'Not Connected To Server',
                                                    buttons: ['OK']
                                                });
                                                alert.present();
                                            });
                                        });
                                    }
                                }
                            ]
                        });
                        prompts.present();
                    }
                }
            ]
        });
        prompt.present();
    };
    DetaillocationoutletPage.prototype.close = function () {
        this.viewCtrl.dismiss();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('map'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], DetaillocationoutletPage.prototype, "mapElement", void 0);
    DetaillocationoutletPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-detaillocationoutlet',template:/*ion-inline-start:"D:\ionic\Project\qrcode\src\pages\detaillocationoutlet\detaillocationoutlet.html"*/`<ion-header>\n\n  <ion-navbar color="oren">\n    <ion-title>\n      <div>Lokasi Detail</div>\n    </ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="detailLocation(id,outlet, lat, lng , name, address,2)">\n        <ion-icon name="close-circle"></ion-icon>&nbsp;&nbsp;\n      </button>\n    </ion-buttons>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="detailLocation(id,outlet, lat, lng , name, address,1)">\n        <ion-icon name="checkmark-circle"></ion-icon>&nbsp;&nbsp;\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content no-padding style="background-color: #f5f6f9">\n  <ion-grid style="height: 100%;border:0px solid #000">\n    <ion-row>\n      <ion-col>\n        <img src="{{images}}" style="width: 100%;height: 100%">\n      </ion-col>\n      <ion-col>\n        <div #map id="map" style="width: 100%;height: 100%;"></div>\n      </ion-col>\n      <ion-col>\n        <div id="pano" style="width: 100%;height: 100%;"></div>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>`/*ion-inline-end:"D:\ionic\Project\qrcode\src\pages\detaillocationoutlet\detaillocationoutlet.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_barcode_scanner__["a" /* BarcodeScanner */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_6__config_provider__["a" /* Provider */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_8__ionic_native_screen_orientation__["a" /* ScreenOrientation */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_location_accuracy__["a" /* LocationAccuracy */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_android_permissions__["a" /* AndroidPermissions */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */]])
    ], DetaillocationoutletPage);
    return DetaillocationoutletPage;
}());

//# sourceMappingURL=detaillocationoutlet.js.map

/***/ }),

/***/ 36:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UsersdistributorPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_provider__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_performa_performa__ = __webpack_require__(67);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the UsersdistributorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var UsersdistributorPage = (function () {
    function UsersdistributorPage(navCtrl, viewCtrl, postPvdr, alertCtrl, storage, loadingCtrl, navParams) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.postPvdr = postPvdr;
        this.alertCtrl = alertCtrl;
        this.storage = storage;
        this.loadingCtrl = loadingCtrl;
        this.navParams = navParams;
        this.filter = 'all';
        this.kdoutlet = navParams.get("kdoutlet");
        this.kdcabang = navParams.get("kdcabang");
        this.close_page = navParams.get("close");
        this.bisa_pilih = navParams.get("bisa_pilih");
        this.reset_password = navParams.get("reset_password");
        console.log('reset password : ', this.reset_password);
        this.storage.get('user_session_storage').then(function (res) {
            if (res[0].user_level == '4' || res[0].user_level == '2' || res[0].user_level == '10') {
                _this.performance = navParams.get("performa");
            }
            else {
                _this.performance = 'Yes';
            }
            ;
            if (res[0].user_level == '2' || (res[0].user_level == '-1' && res[0].devloper == '1')) {
                _this.hak_reset_password = 'Yes';
            }
            else {
                _this.hak_reset_password = 'No';
            }
            ;
        });
    }
    UsersdistributorPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad UsersdistributorPage');
        this.listUsers = new Array();
        this.getData('', 0, 10, 'all', 'No');
    };
    UsersdistributorPage.prototype.form_search = function () {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'Cari',
            message: "Masukan kata untuk mencari kode atau nama user",
            inputs: [
                {
                    name: 'src',
                    placeholder: 'Apa yang anda cari...'
                },
            ],
            buttons: [
                {
                    text: 'Batal',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Cari',
                    handler: function (data) {
                        console.log('Saved clicked');
                        _this.getListUserDistributor(data.src);
                    }
                }
            ]
        });
        prompt.present();
    };
    UsersdistributorPage.prototype.getListUserDistributor = function (src) {
        this.listUsers = new Array();
        if (src) {
            src = src;
        }
        else {
            src = '';
        }
        this.getData(src, 0, 10, 'all', 'No');
    };
    UsersdistributorPage.prototype.getData = function (src, first, offset, filter, infinite) {
        var _this = this;
        this.first = first;
        this.src = src;
        var loader = this.loadingCtrl.create({
            content: "Mohon Menunggu..."
        });
        if (infinite == 'No') {
            loader.present();
        }
        this.storage.get('user_session_storage').then(function (res) {
            var body = {
                kdcabang: _this.kdcabang,
                first: first,
                offset: offset,
                filter: filter,
                keyword: src,
                kdoutlet: _this.kdoutlet,
                user_level: res[0].user_level,
                username: res[0].username
            };
            _this.postPvdr.postData(body, 'outlet/getListUserDistributor2').subscribe(function (data) {
                if (data.success) {
                    loader.dismiss();
                    for (var i = 0; i < data.result.length; i++) {
                        _this.listUsers.push({
                            username: data.result[i]['username'],
                            name: data.result[i]['name'],
                            kdsales: data.result[i]['kdsales'],
                            kdoutlet: data.result[i]['kdoutlet'],
                            kdcabang: data.result[i]['kdcabang']
                        });
                    }
                }
                else {
                    loader.dismiss();
                    if (infinite == 'No') {
                        var alert_1 = _this.alertCtrl.create({
                            title: 'Info',
                            subTitle: 'Mohon Maaf Data Tidak Di Temukan',
                            message: '',
                            buttons: ['OK']
                        });
                        alert_1.present();
                    }
                    return false;
                }
            }, function (error) {
                loader.dismiss();
                var alert = _this.alertCtrl.create({
                    title: 'Mohon Maaf',
                    subTitle: 'Sedang tidak terkoneksi dengan server. Silahkan coba kembali beberapa saat',
                    buttons: ['OK']
                });
                alert.present();
                return false;
            });
        });
    };
    UsersdistributorPage.prototype.doRefresh = function (refresher) {
        this.listUsers = new Array();
        this.getData('', 0, 10, 'all', 'No');
        setTimeout(function () {
            refresher.complete();
        }, 1000);
    };
    UsersdistributorPage.prototype.doInfinite = function (infiniteScroll, src, first, filter) {
        var _this = this;
        first = first + 10;
        console.log('filter : ', filter);
        console.log('Begin async operation');
        setTimeout(function () {
            _this.getData(src, first, 10, filter, 'Yes');
            infiniteScroll.complete();
        }, 1000);
    };
    UsersdistributorPage.prototype.performa = function (kdcabang, kdoutlet, kdsales, name) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__pages_performa_performa__["a" /* PerformaPage */], { kdcabang: kdcabang, storage: 'No', user_level: '4', kdoutlet: kdoutlet, kdsales: kdsales, sales_name: name });
    };
    UsersdistributorPage.prototype.close = function () {
        this.viewCtrl.dismiss();
    };
    UsersdistributorPage.prototype.pilih_ini = function (kdsales, Nama) {
        this.viewCtrl.dismiss({ kdsales: kdsales, sales_name: Nama });
    };
    UsersdistributorPage.prototype.resetPassword = function (name, username, kdsales, kdcabang, kdoutlet) {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: 'Info',
            message: 'Apakah anda yakin akan mereset password sales ' + name + '?',
            buttons: [
                {
                    text: 'Batal',
                    handler: function () {
                        console.log('Disagree clicked');
                    }
                },
                {
                    text: 'Ya',
                    handler: function () {
                        _this.reseting_password(username, kdsales, kdcabang, kdoutlet);
                    }
                }
            ]
        });
        confirm.present();
    };
    UsersdistributorPage.prototype.reseting_password = function (username, kdsales, kdcabang, kdoutlet) {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'Reset Password',
            message: "Silahkan Masukkan Password Baru",
            inputs: [
                {
                    name: 'password',
                    placeholder: 'Password'
                },
            ],
            buttons: [
                {
                    text: 'Batal',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Simpan',
                    handler: function (data) {
                        _this.action_reset_password(data.password, username, kdsales, kdcabang, kdoutlet);
                    }
                }
            ]
        });
        prompt.present();
    };
    UsersdistributorPage.prototype.action_reset_password = function (new_password, username, kdsales, kdcabang, kdoutlet) {
        var _this = this;
        var loader = this.loadingCtrl.create({
            content: "Proses Reset..."
        });
        loader.present();
        this.storage.get('user_session_storage').then(function (res) {
            var body = {
                new_password: new_password,
                username_sales: username,
                kdsales: kdsales,
                kdcabang: kdcabang,
                kdoutlet: kdoutlet,
                username_resetor: res[0].username
            };
            _this.postPvdr.postData(body, 'login/resetPassword').subscribe(function (data) {
                if (data.success) {
                    loader.dismiss();
                    var alert_2 = _this.alertCtrl.create({
                        title: 'Info',
                        subTitle: 'Reset Password Berhasil.',
                        message: '',
                        buttons: ['OK']
                    });
                    alert_2.present();
                    return false;
                }
                else {
                    loader.dismiss();
                    var alert_3 = _this.alertCtrl.create({
                        title: 'Info',
                        subTitle: 'Reset Password Gagal.',
                        message: '',
                        buttons: ['OK']
                    });
                    alert_3.present();
                    return false;
                }
            }, function (error) {
                loader.dismiss();
                var alert = _this.alertCtrl.create({
                    title: 'Mohon Maaf',
                    subTitle: 'Sedang tidak terkoneksi dengan server. Silahkan coba kembali beberapa saat',
                    buttons: ['OK']
                });
                alert.present();
                return false;
            });
        });
    };
    UsersdistributorPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-usersdistributor',template:/*ion-inline-start:"D:\ionic\Project\qrcode\src\pages\usersdistributor\usersdistributor.html"*/`<ion-header>\n\n  <ion-navbar color="oren">\n    <ion-title>\n      <div>Users</div>\n    </ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="form_search()">\n        <ion-icon name="search"></ion-icon>&nbsp;&nbsp;\n      </button>\n    </ion-buttons>\n    <ion-buttons end *ngIf="close_page==\'Yes\'">\n      <button ion-button icon-only (click)="close()">\n        <ion-icon name="close-circle"></ion-icon>&nbsp;&nbsp;\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content no-padding class="bgc">\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n    <ion-refresher-content pullingIcon="arrow-dropdown" pullingText="Pull to refresh" refreshingSpinner="crescent"\n      refreshingText="">\n    </ion-refresher-content>\n  </ion-refresher>\n  <ion-grid fixed class="wrapper">\n    <ion-list>\n      <ion-item *ngIf="bisa_pilih==\'Yes\'">\n        <h2>Semua</h2>\n        <ion-icon name="download" item-end (click)="pilih_ini(\'\',\'Semua\')">\n        </ion-icon>\n      </ion-item>\n      <ion-item *ngFor="let val of listUsers">\n        <h2>{{val.name}}</h2>\n        <h3>Username : {{val.username}}</h3>\n        <p>Kode Sales : {{val.kdsales}}</p>\n        <ion-icon name="ios-arrow-forward" item-end *ngIf="performance==\'Yes\' && bisa_pilih==\'No\'"\n          (click)="performa(val.kdcabang,val.kdoutlet,val.kdsales,val.name)"></ion-icon>\n        <ion-icon name="download" item-end *ngIf="bisa_pilih==\'Yes\'" (click)="pilih_ini(val.kdsales,val.name)">\n        </ion-icon>\n        <ion-icon name="key" item-end *ngIf="reset_password==\'Yes\' && hak_reset_password==\'Yes\'"\n          (click)="resetPassword(val.name,val.username,val.kdsales,val.kdcabang,val.kdoutlet)"></ion-icon>\n      </ion-item>\n    </ion-list>\n    <ion-infinite-scroll (ionInfinite)="doInfinite($event,src, first,filter)">\n      <ion-infinite-scroll-content loadingSpinner="bubbles" loadingText="Mencari Data Lagi...">\n      </ion-infinite-scroll-content>\n    </ion-infinite-scroll>\n  </ion-grid>\n</ion-content>`/*ion-inline-end:"D:\ionic\Project\qrcode\src\pages\usersdistributor\usersdistributor.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_2__config_provider__["a" /* Provider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], UsersdistributorPage);
    return UsersdistributorPage;
}());

//# sourceMappingURL=usersdistributor.js.map

/***/ }),

/***/ 376:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_detaillistings_detaillistings__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the ListingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ListingPage = (function () {
    function ListingPage(viewCtrl, navCtrl, modalCtrl, storage, navParams) {
        this.viewCtrl = viewCtrl;
        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl;
        this.storage = storage;
        this.navParams = navParams;
    }
    ListingPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ListingPage');
    };
    ListingPage.prototype.detaillisting = function () {
        var _this = this;
        // const modal = this.modalCtrl.create(DetaillistingsPage);
        // modal.present();
        this.storage.get('user_session_storage').then(function (res) {
            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__pages_detaillistings_detaillistings__["a" /* DetaillistingsPage */], { kdoutlet: res[0].kdoutlet, kdcabang: res[0].cabang_id, storages: 'Yes', close: 'No', bisa_pilih: 'No' });
        });
    };
    ListingPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-listing',template:/*ion-inline-start:"D:\ionic\Project\qrcode\src\pages\listing\listing.html"*/`<ion-header>\n\n  <ion-navbar color="oren">\n    <ion-title>\n      <div>List</div>\n    </ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content no-padding class="bgc">\n  <ion-grid fixed class="wrapper">\n    <ion-list>\n      <ion-item>\n        Cabang\n        <ion-icon name="ios-arrow-forward" item-end></ion-icon>\n      </ion-item>\n      <ion-item>\n        Depo\n        <ion-icon name="ios-arrow-forward" item-end></ion-icon>\n      </ion-item>\n      <ion-item (click)="detaillisting()">\n        Distributor\n        <ion-icon name="ios-arrow-forward" item-end></ion-icon>\n      </ion-item>\n    </ion-list>\n  </ion-grid>\n</ion-content>`/*ion-inline-end:"D:\ionic\Project\qrcode\src\pages\listing\listing.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], ListingPage);
    return ListingPage;
}());

//# sourceMappingURL=listing.js.map

/***/ }),

/***/ 377:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(378);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(401);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 401:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(375);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__(482);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_home_home__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_login_login__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_main_main__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_formoutlet_formoutlet__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_formoutletpwa_formoutletpwa__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_listoutlet_listoutlet__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_history_history__ = __webpack_require__(140);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_settings_settings__ = __webpack_require__(141);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_admin_admin__ = __webpack_require__(137);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_detaillocationoutlet_detaillocationoutlet__ = __webpack_require__(328);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_listing_listing__ = __webpack_require__(376);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_detaillistings_detaillistings__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_detailbangetlistings_detailbangetlistings__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_usersdistributor_usersdistributor__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_logs_logs__ = __webpack_require__(142);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_activities_activities__ = __webpack_require__(133);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_performa_performa__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_petatoko_petatoko__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_search_search__ = __webpack_require__(135);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__pages_detailhistory_detailhistory__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__pages_searchactivities_searchactivities__ = __webpack_require__(134);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pages_analytics_analytics__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__pages_bypass_bypass__ = __webpack_require__(72);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__pages_trafic_trafic__ = __webpack_require__(143);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__pages_noolist_noolist__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__pages_petanoo_petanoo__ = __webpack_require__(136);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__pages_rutesales_rutesales__ = __webpack_require__(144);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__pages_detailrutesales_detailrutesales__ = __webpack_require__(145);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__pages_tokoplus_tokoplus__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__config_provider__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36_ngx_qrcode2__ = __webpack_require__(483);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__ionic_native_barcode_scanner__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__ionic_native_camera__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__ionic_native_file_transfer__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__ionic_native_file__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__ionic_storage__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__ionic_native_social_sharing__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__angular_http__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44__ionic_native_file_opener__ = __webpack_require__(484);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_45__ionic_native_geolocation__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_46__ionic_native_android_permissions__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_47__ionic_native_location_accuracy__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_48__ionic_native_native_geocoder__ = __webpack_require__(485);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_49__ionic_native_screen_orientation__ = __webpack_require__(330);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_50__ionic_native_app_update__ = __webpack_require__(335);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






















































var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_main_main__["a" /* MainPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["b" /* PopoverPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_formoutlet_formoutlet__["a" /* FormoutletPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_formoutletpwa_formoutletpwa__["a" /* FormoutletpwaPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_listoutlet_listoutlet__["a" /* ListoutletPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_history_history__["a" /* HistoryPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_settings_settings__["a" /* SettingsPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_admin_admin__["a" /* AdminPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_detaillocationoutlet_detaillocationoutlet__["a" /* DetaillocationoutletPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_listing_listing__["a" /* ListingPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_detaillistings_detaillistings__["a" /* DetaillistingsPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_detailbangetlistings_detailbangetlistings__["a" /* DetailbangetlistingsPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_logs_logs__["a" /* LogsPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_activities_activities__["a" /* ActivitiesPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_usersdistributor_usersdistributor__["a" /* UsersdistributorPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_performa_performa__["a" /* PerformaPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_petatoko_petatoko__["a" /* PetatokoPage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_search_search__["a" /* SearchPage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_detailhistory_detailhistory__["a" /* DetailhistoryPage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_searchactivities_searchactivities__["a" /* SearchactivitiesPage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_analytics_analytics__["a" /* AnalyticsPage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_analytics_analytics__["b" /* MoveanddragPage */],
                __WEBPACK_IMPORTED_MODULE_28__pages_bypass_bypass__["a" /* BypassPage */],
                __WEBPACK_IMPORTED_MODULE_29__pages_trafic_trafic__["a" /* TraficPage */],
                __WEBPACK_IMPORTED_MODULE_30__pages_noolist_noolist__["b" /* NoolistPage */],
                __WEBPACK_IMPORTED_MODULE_30__pages_noolist_noolist__["a" /* AksiPage */],
                __WEBPACK_IMPORTED_MODULE_31__pages_petanoo_petanoo__["a" /* PetanooPage */],
                __WEBPACK_IMPORTED_MODULE_32__pages_rutesales_rutesales__["a" /* RutesalesPage */],
                __WEBPACK_IMPORTED_MODULE_33__pages_detailrutesales_detailrutesales__["a" /* DetailrutesalesPage */],
                __WEBPACK_IMPORTED_MODULE_34__pages_tokoplus_tokoplus__["a" /* TokoplusPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_43__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_41__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/activities/activities.module#ActivitiesPageModule', name: 'ActivitiesPage', segment: 'activities', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/analytics/analytics.module#AnalyticsPageModule', name: 'AnalyticsPage', segment: 'analytics', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/admin/admin.module#AdminPageModule', name: 'AdminPage', segment: 'admin', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/bypass/bypass.module#BypassPageModule', name: 'BypassPage', segment: 'bypass', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/detailbangetlistings/detailbangetlistings.module#DetailbangetlistingsPageModule', name: 'DetailbangetlistingsPage', segment: 'detailbangetlistings', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/detailhistory/detailhistory.module#DetailhistoryPageModule', name: 'DetailhistoryPage', segment: 'detailhistory', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/detaillistings/detaillistings.module#DetaillistingsPageModule', name: 'DetaillistingsPage', segment: 'detaillistings', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/detailrutesales/detailrutesales.module#DetailrutesalesPageModule', name: 'DetailrutesalesPage', segment: 'detailrutesales', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/formoutlet/formoutlet.module#FormoutletPageModule', name: 'FormoutletPage', segment: 'formoutlet', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/history/history.module#HistoryPageModule', name: 'HistoryPage', segment: 'history', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/formoutletpwa/formoutletpwa.module#FormoutletpwaPageModule', name: 'FormoutletpwaPage', segment: 'formoutletpwa', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/listoutlet/listoutlet.module#ListoutletPageModule', name: 'ListoutletPage', segment: 'listoutlet', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/listing/listing.module#ListingPageModule', name: 'ListingPage', segment: 'listing', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/logs/logs.module#LogsPageModule', name: 'LogsPage', segment: 'logs', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/main/main.module#MainPageModule', name: 'MainPage', segment: 'main', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/noolist/noolist.module#NoolistPageModule', name: 'NoolistPage', segment: 'noolist', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/performa/performa.module#PerformaPageModule', name: 'PerformaPage', segment: 'performa', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/petanoo/petanoo.module#PetanooPageModule', name: 'PetanooPage', segment: 'petanoo', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/petatoko/petatoko.module#PetatokoPageModule', name: 'PetatokoPage', segment: 'petatoko', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/rutesales/rutesales.module#RutesalesPageModule', name: 'RutesalesPage', segment: 'rutesales', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/search/search.module#SearchPageModule', name: 'SearchPage', segment: 'search', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/searchactivities/searchactivities.module#SearchactivitiesPageModule', name: 'SearchactivitiesPage', segment: 'searchactivities', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/settings/settings.module#SettingsPageModule', name: 'SettingsPage', segment: 'settings', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/tokoplus/tokoplus.module#TokoplusPageModule', name: 'TokoplusPage', segment: 'tokoplus', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/trafic/trafic.module#TraficPageModule', name: 'TraficPage', segment: 'trafic', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/usersdistributor/usersdistributor.module#UsersdistributorPageModule', name: 'UsersdistributorPage', segment: 'usersdistributor', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_36_ngx_qrcode2__["a" /* NgxQRCodeModule */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_main_main__["a" /* MainPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["b" /* PopoverPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_formoutlet_formoutlet__["a" /* FormoutletPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_formoutletpwa_formoutletpwa__["a" /* FormoutletpwaPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_listoutlet_listoutlet__["a" /* ListoutletPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_history_history__["a" /* HistoryPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_settings_settings__["a" /* SettingsPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_admin_admin__["a" /* AdminPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_detaillocationoutlet_detaillocationoutlet__["a" /* DetaillocationoutletPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_listing_listing__["a" /* ListingPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_detaillistings_detaillistings__["a" /* DetaillistingsPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_detailbangetlistings_detailbangetlistings__["a" /* DetailbangetlistingsPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_logs_logs__["a" /* LogsPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_activities_activities__["a" /* ActivitiesPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_usersdistributor_usersdistributor__["a" /* UsersdistributorPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_performa_performa__["a" /* PerformaPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_petatoko_petatoko__["a" /* PetatokoPage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_search_search__["a" /* SearchPage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_detailhistory_detailhistory__["a" /* DetailhistoryPage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_searchactivities_searchactivities__["a" /* SearchactivitiesPage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_analytics_analytics__["a" /* AnalyticsPage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_analytics_analytics__["b" /* MoveanddragPage */],
                __WEBPACK_IMPORTED_MODULE_28__pages_bypass_bypass__["a" /* BypassPage */],
                __WEBPACK_IMPORTED_MODULE_29__pages_trafic_trafic__["a" /* TraficPage */],
                __WEBPACK_IMPORTED_MODULE_30__pages_noolist_noolist__["b" /* NoolistPage */],
                __WEBPACK_IMPORTED_MODULE_30__pages_noolist_noolist__["a" /* AksiPage */],
                __WEBPACK_IMPORTED_MODULE_31__pages_petanoo_petanoo__["a" /* PetanooPage */],
                __WEBPACK_IMPORTED_MODULE_32__pages_rutesales_rutesales__["a" /* RutesalesPage */],
                __WEBPACK_IMPORTED_MODULE_33__pages_detailrutesales_detailrutesales__["a" /* DetailrutesalesPage */],
                __WEBPACK_IMPORTED_MODULE_34__pages_tokoplus_tokoplus__["a" /* TokoplusPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_42__ionic_native_social_sharing__["a" /* SocialSharing */],
                __WEBPACK_IMPORTED_MODULE_44__ionic_native_file_opener__["a" /* FileOpener */],
                __WEBPACK_IMPORTED_MODULE_45__ionic_native_geolocation__["a" /* Geolocation */],
                __WEBPACK_IMPORTED_MODULE_46__ionic_native_android_permissions__["a" /* AndroidPermissions */],
                __WEBPACK_IMPORTED_MODULE_47__ionic_native_location_accuracy__["a" /* LocationAccuracy */],
                __WEBPACK_IMPORTED_MODULE_48__ionic_native_native_geocoder__["a" /* NativeGeocoder */],
                __WEBPACK_IMPORTED_MODULE_38__ionic_native_camera__["a" /* Camera */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_37__ionic_native_barcode_scanner__["a" /* BarcodeScanner */],
                __WEBPACK_IMPORTED_MODULE_40__ionic_native_file__["a" /* File */],
                __WEBPACK_IMPORTED_MODULE_39__ionic_native_file_transfer__["a" /* FileTransfer */],
                __WEBPACK_IMPORTED_MODULE_39__ionic_native_file_transfer__["b" /* FileTransferObject */],
                __WEBPACK_IMPORTED_MODULE_35__config_provider__["a" /* Provider */],
                __WEBPACK_IMPORTED_MODULE_49__ionic_native_screen_orientation__["a" /* ScreenOrientation */],
                __WEBPACK_IMPORTED_MODULE_50__ionic_native_app_update__["a" /* AppUpdate */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 429:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": 201,
	"./af.js": 201,
	"./ar": 202,
	"./ar-dz": 203,
	"./ar-dz.js": 203,
	"./ar-kw": 204,
	"./ar-kw.js": 204,
	"./ar-ly": 205,
	"./ar-ly.js": 205,
	"./ar-ma": 206,
	"./ar-ma.js": 206,
	"./ar-sa": 207,
	"./ar-sa.js": 207,
	"./ar-tn": 208,
	"./ar-tn.js": 208,
	"./ar.js": 202,
	"./az": 209,
	"./az.js": 209,
	"./be": 210,
	"./be.js": 210,
	"./bg": 211,
	"./bg.js": 211,
	"./bm": 212,
	"./bm.js": 212,
	"./bn": 213,
	"./bn.js": 213,
	"./bo": 214,
	"./bo.js": 214,
	"./br": 215,
	"./br.js": 215,
	"./bs": 216,
	"./bs.js": 216,
	"./ca": 217,
	"./ca.js": 217,
	"./cs": 218,
	"./cs.js": 218,
	"./cv": 219,
	"./cv.js": 219,
	"./cy": 220,
	"./cy.js": 220,
	"./da": 221,
	"./da.js": 221,
	"./de": 222,
	"./de-at": 223,
	"./de-at.js": 223,
	"./de-ch": 224,
	"./de-ch.js": 224,
	"./de.js": 222,
	"./dv": 225,
	"./dv.js": 225,
	"./el": 226,
	"./el.js": 226,
	"./en-SG": 227,
	"./en-SG.js": 227,
	"./en-au": 228,
	"./en-au.js": 228,
	"./en-ca": 229,
	"./en-ca.js": 229,
	"./en-gb": 230,
	"./en-gb.js": 230,
	"./en-ie": 231,
	"./en-ie.js": 231,
	"./en-il": 232,
	"./en-il.js": 232,
	"./en-nz": 233,
	"./en-nz.js": 233,
	"./eo": 234,
	"./eo.js": 234,
	"./es": 235,
	"./es-do": 236,
	"./es-do.js": 236,
	"./es-us": 237,
	"./es-us.js": 237,
	"./es.js": 235,
	"./et": 238,
	"./et.js": 238,
	"./eu": 239,
	"./eu.js": 239,
	"./fa": 240,
	"./fa.js": 240,
	"./fi": 241,
	"./fi.js": 241,
	"./fo": 242,
	"./fo.js": 242,
	"./fr": 243,
	"./fr-ca": 244,
	"./fr-ca.js": 244,
	"./fr-ch": 245,
	"./fr-ch.js": 245,
	"./fr.js": 243,
	"./fy": 246,
	"./fy.js": 246,
	"./ga": 247,
	"./ga.js": 247,
	"./gd": 248,
	"./gd.js": 248,
	"./gl": 249,
	"./gl.js": 249,
	"./gom-latn": 250,
	"./gom-latn.js": 250,
	"./gu": 251,
	"./gu.js": 251,
	"./he": 252,
	"./he.js": 252,
	"./hi": 253,
	"./hi.js": 253,
	"./hr": 254,
	"./hr.js": 254,
	"./hu": 255,
	"./hu.js": 255,
	"./hy-am": 256,
	"./hy-am.js": 256,
	"./id": 257,
	"./id.js": 257,
	"./is": 258,
	"./is.js": 258,
	"./it": 259,
	"./it-ch": 260,
	"./it-ch.js": 260,
	"./it.js": 259,
	"./ja": 261,
	"./ja.js": 261,
	"./jv": 262,
	"./jv.js": 262,
	"./ka": 263,
	"./ka.js": 263,
	"./kk": 264,
	"./kk.js": 264,
	"./km": 265,
	"./km.js": 265,
	"./kn": 266,
	"./kn.js": 266,
	"./ko": 267,
	"./ko.js": 267,
	"./ku": 268,
	"./ku.js": 268,
	"./ky": 269,
	"./ky.js": 269,
	"./lb": 270,
	"./lb.js": 270,
	"./lo": 271,
	"./lo.js": 271,
	"./lt": 272,
	"./lt.js": 272,
	"./lv": 273,
	"./lv.js": 273,
	"./me": 274,
	"./me.js": 274,
	"./mi": 275,
	"./mi.js": 275,
	"./mk": 276,
	"./mk.js": 276,
	"./ml": 277,
	"./ml.js": 277,
	"./mn": 278,
	"./mn.js": 278,
	"./mr": 279,
	"./mr.js": 279,
	"./ms": 280,
	"./ms-my": 281,
	"./ms-my.js": 281,
	"./ms.js": 280,
	"./mt": 282,
	"./mt.js": 282,
	"./my": 283,
	"./my.js": 283,
	"./nb": 284,
	"./nb.js": 284,
	"./ne": 285,
	"./ne.js": 285,
	"./nl": 286,
	"./nl-be": 287,
	"./nl-be.js": 287,
	"./nl.js": 286,
	"./nn": 288,
	"./nn.js": 288,
	"./pa-in": 289,
	"./pa-in.js": 289,
	"./pl": 290,
	"./pl.js": 290,
	"./pt": 291,
	"./pt-br": 292,
	"./pt-br.js": 292,
	"./pt.js": 291,
	"./ro": 293,
	"./ro.js": 293,
	"./ru": 294,
	"./ru.js": 294,
	"./sd": 295,
	"./sd.js": 295,
	"./se": 296,
	"./se.js": 296,
	"./si": 297,
	"./si.js": 297,
	"./sk": 298,
	"./sk.js": 298,
	"./sl": 299,
	"./sl.js": 299,
	"./sq": 300,
	"./sq.js": 300,
	"./sr": 301,
	"./sr-cyrl": 302,
	"./sr-cyrl.js": 302,
	"./sr.js": 301,
	"./ss": 303,
	"./ss.js": 303,
	"./sv": 304,
	"./sv.js": 304,
	"./sw": 305,
	"./sw.js": 305,
	"./ta": 306,
	"./ta.js": 306,
	"./te": 307,
	"./te.js": 307,
	"./tet": 308,
	"./tet.js": 308,
	"./tg": 309,
	"./tg.js": 309,
	"./th": 310,
	"./th.js": 310,
	"./tl-ph": 311,
	"./tl-ph.js": 311,
	"./tlh": 312,
	"./tlh.js": 312,
	"./tr": 313,
	"./tr.js": 313,
	"./tzl": 314,
	"./tzl.js": 314,
	"./tzm": 315,
	"./tzm-latn": 316,
	"./tzm-latn.js": 316,
	"./tzm.js": 315,
	"./ug-cn": 317,
	"./ug-cn.js": 317,
	"./uk": 318,
	"./uk.js": 318,
	"./ur": 319,
	"./ur.js": 319,
	"./uz": 320,
	"./uz-latn": 321,
	"./uz-latn.js": 321,
	"./uz.js": 320,
	"./vi": 322,
	"./vi.js": 322,
	"./x-pseudo": 323,
	"./x-pseudo.js": 323,
	"./yo": 324,
	"./yo.js": 324,
	"./zh-cn": 325,
	"./zh-cn.js": 325,
	"./zh-hk": 326,
	"./zh-hk.js": 326,
	"./zh-tw": 327,
	"./zh-tw.js": 327
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 429;

/***/ }),

/***/ 44:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetaillistingsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_detailbangetlistings_detailbangetlistings__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_provider__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









/**
 * Generated class for the DetaillistingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var DetaillistingsPage = (function () {
    function DetaillistingsPage(viewCtrl, alertCtrl, postPvdr, storage, navCtrl, loadingCtrl, modalCtrl, navParams) {
        var _this = this;
        this.viewCtrl = viewCtrl;
        this.alertCtrl = alertCtrl;
        this.postPvdr = postPvdr;
        this.storage = storage;
        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
        this.modalCtrl = modalCtrl;
        this.navParams = navParams;
        this.storages = 'Yes';
        this.view_dist_reg = 'No';
        this.view_dist_nas = 'No';
        this.filter = 'all';
        this.kdoutlet = navParams.get("kdoutlet");
        this.kdcabang = navParams.get("kdcabang");
        this.storages = navParams.get("storages");
        this.close_page = navParams.get("close");
        this.bisa_pilih = navParams.get("bisa_pilih");
        this.storage.get('user_session_storage').then(function (res) {
            if (res[0].user_level == '-1' || res[0].user_level == '9') {
                _this.view_dist_reg = 'No';
                _this.view_dist_nas = 'Yes';
            }
            else {
                _this.view_dist_reg = 'Yes';
                _this.view_dist_nas = 'No';
            }
        });
    }
    DetaillistingsPage.prototype.ionViewDidLoad = function () {
        this.listDistributor = new Array();
        this.getData('', 0, 10, 'all', 'No');
    };
    DetaillistingsPage.prototype.form_search = function () {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'Cari',
            message: "Masukan kata untuk mencari kode atau nama distributor",
            inputs: [
                {
                    name: 'src',
                    placeholder: 'Apa yang anda cari...'
                },
            ],
            buttons: [
                {
                    text: 'Batal',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Cari',
                    handler: function (data) {
                        console.log('Saved clicked');
                        _this.getDistributor(data.src);
                    }
                }
            ]
        });
        prompt.present();
    };
    DetaillistingsPage.prototype.getDistributor = function (src) {
        if (src) {
            src = src;
        }
        else {
            src = '';
        }
        this.listDistributor = new Array();
        this.getData(src, 0, 10, 'all', 'No');
    };
    DetaillistingsPage.prototype.getData = function (src, first, offset, filter, infinite) {
        var _this = this;
        this.first = first;
        this.src = src;
        var loader = this.loadingCtrl.create({
            content: "Mohon Menunggu..."
        });
        if (infinite == 'No') {
            loader.present();
        }
        this.storage.get('user_session_storage').then(function (res) {
            var body;
            if (_this.storages == 'No') {
                body = {
                    kdcabang: _this.kdcabang,
                    first: first,
                    offset: offset,
                    filter: filter,
                    keyword: src,
                    user_level: res[0].user_level,
                    kdoutlet: _this.kdoutlet
                };
            }
            else if (_this.storages == 'Yes') {
                body = {
                    kdcabang: res[0].cabang_id,
                    first: first,
                    offset: offset,
                    filter: filter,
                    keyword: src,
                    user_level: res[0].user_level,
                    kdoutlet: res[0].kdoutlet
                };
            }
            _this.postPvdr.postData(body, 'outlet/getDistributor').subscribe(function (data) {
                if (data.success) {
                    loader.dismiss();
                    for (var i = 0; i < data.result.length; i++) {
                        _this.listDistributor.push({
                            KdOutlet: data.result[i]['KdOutlet'],
                            Nama: data.result[i]['Nama'],
                            KdCabang: data.result[i]['KdCabang'],
                            KotaToko: data.result[i]['KotaToko']
                        });
                    }
                }
                else {
                    loader.dismiss();
                    if (infinite == 'No') {
                        var alert_1 = _this.alertCtrl.create({
                            title: 'Info',
                            subTitle: 'Mohon Maaf Data Tidak Di Temukan',
                            message: '',
                            buttons: ['OK']
                        });
                        alert_1.present();
                    }
                    return false;
                }
            }, function (error) {
                loader.dismiss();
                var alert = _this.alertCtrl.create({
                    title: 'Mohon Maaf',
                    subTitle: 'Sedang tidak terkoneksi dengan server. Silahkan coba kembali beberapa saat',
                    buttons: ['OK']
                });
                alert.present();
                return false;
            });
        });
    };
    DetaillistingsPage.prototype.detailbangetlisting = function (kdoutlet, kdcabang) {
        // const modal = this.modalCtrl.create(DetailbangetlistingsPage, { kdoutlet: kdoutlet, kdcabang: kdcabang });
        // modal.present();
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__pages_detailbangetlistings_detailbangetlistings__["a" /* DetailbangetlistingsPage */], { kdoutlet: kdoutlet, kdcabang: kdcabang, kdsales: '', viewuser: 'Yes', title: '', close: 'No', bisa_pilih: 'No' });
    };
    DetaillistingsPage.prototype.doRefresh = function (refresher) {
        this.listDistributor = new Array();
        this.getData('', 0, 10, 'all', 'No');
        setTimeout(function () {
            refresher.complete();
        }, 1000);
    };
    DetaillistingsPage.prototype.doInfinite = function (infiniteScroll, src, first, filter) {
        var _this = this;
        first = first + 10;
        console.log('Begin async operation');
        setTimeout(function () {
            _this.getData(src, first, 10, filter, 'Yes');
            infiniteScroll.complete();
        }, 1000);
    };
    DetaillistingsPage.prototype.close = function () {
        this.viewCtrl.dismiss();
    };
    DetaillistingsPage.prototype.pilih_ini = function (kdoutlet, Nama) {
        this.viewCtrl.dismiss({ kdoutlet: kdoutlet, distributor_name: Nama });
    };
    DetaillistingsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-detaillistings',template:/*ion-inline-start:"D:\ionic\Project\qrcode\src\pages\detaillistings\detaillistings.html"*/`<ion-header>\n\n  <ion-navbar color="oren">\n    <ion-title>\n      <div>List Distributor</div>\n    </ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="form_search()">\n        <ion-icon name="search"></ion-icon>&nbsp;&nbsp;\n      </button>\n    </ion-buttons>\n    <ion-buttons end *ngIf="close_page==\'Yes\'">\n      <button ion-button icon-only (click)="close()">\n        <ion-icon name="close-circle"></ion-icon>&nbsp;&nbsp;\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content no-padding class="bgc">\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n    <ion-refresher-content pullingIcon="arrow-dropdown" pullingText="Pull to refresh" refreshingSpinner="crescent"\n      refreshingText="">\n    </ion-refresher-content>\n  </ion-refresher>\n  <ion-grid fixed class="wrapper">\n    <ion-list>\n      <ion-item *ngIf="bisa_pilih==\'Yes\' && view_dist_reg==\'Yes\'">\n        <h2>Semua Distributor Regional</h2>\n        <ion-icon name="download" item-end (click)="pilih_ini(\'\',\'Semua Distributor Regional\')">\n        </ion-icon>\n      </ion-item>\n      <ion-item *ngIf="bisa_pilih==\'Yes\' && view_dist_nas==\'Yes\'">\n        <h2>Semua Distributor Nasional</h2>\n        <ion-icon name="download" item-end (click)="pilih_ini(\'\',\'Semua Distributor Nasional\')">\n        </ion-icon>\n      </ion-item>\n      <ion-item *ngFor="let val of listDistributor">\n        <h2>{{val.Nama}}</h2>\n        <p>{{val.KotaToko}}</p>\n        <ion-icon name="ios-arrow-forward" item-end *ngIf="bisa_pilih==\'No\'"\n          (click)="detailbangetlisting(val.KdOutlet,val.KdCabang)"></ion-icon>\n        <ion-icon name="download" item-end *ngIf="bisa_pilih==\'Yes\'" (click)="pilih_ini(val.KdOutlet,val.Nama)">\n        </ion-icon>\n      </ion-item>\n    </ion-list>\n    <ion-infinite-scroll (ionInfinite)="doInfinite($event,src, first,filter)">\n      <ion-infinite-scroll-content loadingSpinner="bubbles" loadingText="Mencari Data Lagi...">\n      </ion-infinite-scroll-content>\n    </ion-infinite-scroll>\n  </ion-grid>\n</ion-content>`/*ion-inline-end:"D:\ionic\Project\qrcode\src\pages\detaillistings\detaillistings.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__config_provider__["a" /* Provider */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], DetaillistingsPage);
    return DetaillistingsPage;
}());

//# sourceMappingURL=detaillistings.js.map

/***/ }),

/***/ 482:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(375);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_main_main__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_login_login__ = __webpack_require__(57);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var MyApp = (function () {
    function MyApp(platform, statusBar, storage, splashScreen) {
        var _this = this;
        this.storage = storage;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_6__pages_login_login__["a" /* LoginPage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
            _this.storage.get('user_session_storage').then(function (res) {
                if (res == null) {
                    _this.rootPage = __WEBPACK_IMPORTED_MODULE_6__pages_login_login__["a" /* LoginPage */];
                }
                else {
                    _this.rootPage = __WEBPACK_IMPORTED_MODULE_5__pages_main_main__["a" /* MainPage */];
                }
            });
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"D:\ionic\Project\qrcode\src\app\app.html"*/`<ion-nav [root]="rootPage"></ion-nav>\n`/*ion-inline-end:"D:\ionic\Project\qrcode\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 54:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailbangetlistingsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_usersdistributor_usersdistributor__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_provider__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









/**
 * Generated class for the DetailbangetlistingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var DetailbangetlistingsPage = (function () {
    function DetailbangetlistingsPage(navCtrl, viewCtrl, modalCtrl, alertCtrl, loadingCtrl, postPvdr, storage, navParams) {
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.postPvdr = postPvdr;
        this.storage = storage;
        this.navParams = navParams;
        this.filter = 'all';
        this.kdoutlet = navParams.get("kdoutlet");
        this.kdcabang = navParams.get("kdcabang");
        this.kdsales = navParams.get("kdsales");
        this.viewuser = navParams.get("viewuser");
        this.title = navParams.get("title");
        this.close_page = navParams.get("close");
        this.bisa_pilih = navParams.get("bisa_pilih");
    }
    DetailbangetlistingsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DetailbangetlistingsPage');
        this.listOutlet = new Array();
        this.getData('', 0, 10, 'all', 'No');
    };
    DetailbangetlistingsPage.prototype.form_search = function () {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'Cari',
            message: "Masukan kata untuk mencari kode atau nama toko",
            inputs: [
                {
                    name: 'src',
                    placeholder: 'Apa yang anda cari...'
                },
            ],
            buttons: [
                {
                    text: 'Batal',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Cari',
                    handler: function (data) {
                        console.log('Saved clicked');
                        _this.getListOutletsDistributor(data.src);
                    }
                }
            ]
        });
        prompt.present();
    };
    DetailbangetlistingsPage.prototype.getListOutletsDistributor = function (src) {
        this.listOutlet = new Array();
        if (src) {
            src = src;
        }
        else {
            src = '';
        }
        this.getData(src, 0, 10, 'all', 'No');
    };
    DetailbangetlistingsPage.prototype.getData = function (src, first, offset, filter, infinite) {
        var _this = this;
        this.first = first;
        this.src = src;
        var loader = this.loadingCtrl.create({
            content: "Mohon Menunggu..."
        });
        if (infinite == 'No') {
            loader.present();
        }
        this.storage.get('user_session_storage').then(function (res) {
            var body = {
                kdcabang: _this.kdcabang,
                first: first,
                offset: offset,
                filter: filter,
                keyword: src,
                kdsales: _this.kdsales,
                kdoutlet: _this.kdoutlet,
                user_level: res[0].user_level,
                username: res[0].username
            };
            _this.postPvdr.postData(body, 'outlet/getDetailOutletDistributor2').subscribe(function (data) {
                if (data.success) {
                    loader.dismiss();
                    for (var i = 0; i < data.result.length; i++) {
                        _this.listOutlet.push({
                            KdOutlet: data.result[i]['KdOutlet'],
                            Nama: data.result[i]['Nama'],
                            KotaToko: data.result[i]['KotaToko'],
                            statuz_pending: data.result[i]['statuz_pending'],
                            statuz_valid: data.result[i]['statuz_valid']
                        });
                    }
                }
                else {
                    loader.dismiss();
                    if (infinite == 'No') {
                        var alert_1 = _this.alertCtrl.create({
                            title: 'Info',
                            subTitle: 'Mohon Maaf Data Tidak Di Temukan',
                            message: '',
                            buttons: ['OK']
                        });
                        alert_1.present();
                    }
                    return false;
                }
            }, function (error) {
                loader.dismiss();
                var alert = _this.alertCtrl.create({
                    title: 'Mohon Maaf',
                    message: 'Sedang tidak terkoneksi dengan server. Silahkan coba kembali beberapa saat',
                    buttons: ['OK']
                });
                alert.present();
                return false;
            });
        });
    };
    DetailbangetlistingsPage.prototype.doRefresh = function (refresher) {
        this.listOutlet = new Array();
        this.getData('', 0, 10, 'all', 'No');
        setTimeout(function () {
            refresher.complete();
        }, 1000);
    };
    DetailbangetlistingsPage.prototype.doInfinite = function (infiniteScroll, src, first, filter) {
        var _this = this;
        first = first + 10;
        setTimeout(function () {
            _this.getData(src, first, 10, filter, 'Yes');
            infiniteScroll.complete();
        }, 1000);
    };
    DetailbangetlistingsPage.prototype.salesman = function (kdoutlet, kdcabang) {
        // const modal = this.modalCtrl.create(UsersdistributorPage, { kdoutlet: kdoutlet, kdcabang: kdcabang });
        // modal.present();
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__pages_usersdistributor_usersdistributor__["a" /* UsersdistributorPage */], { kdoutlet: kdoutlet, kdcabang: kdcabang, performa: 'No', close: 'No', bisa_pilih: 'No', reset_password: 'Yes' });
    };
    DetailbangetlistingsPage.prototype.close = function () {
        this.viewCtrl.dismiss();
    };
    DetailbangetlistingsPage.prototype.pilih_ini = function (kdoutlet, Nama) {
        this.viewCtrl.dismiss({ kdoutlet: kdoutlet, Nama: Nama });
    };
    DetailbangetlistingsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-detailbangetlistings',template:/*ion-inline-start:"D:\ionic\Project\qrcode\src\pages\detailbangetlistings\detailbangetlistings.html"*/`<ion-header>\n  <ion-navbar color="oren">\n    <ion-title>\n      <div>Detail List Toko {{title}}</div>\n    </ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="form_search()">\n        <ion-icon name="search"></ion-icon>&nbsp;&nbsp;\n      </button>\n    </ion-buttons>\n    <ion-buttons end *ngIf="viewuser==\'Yes\'">\n      <button ion-button icon-only (click)="salesman(kdoutlet,kdcabang)">\n        <ion-icon name="people"></ion-icon>&nbsp;&nbsp;\n      </button>\n    </ion-buttons>\n    <ion-buttons end *ngIf="close_page==\'Yes\'">\n      <button ion-button icon-only (click)="close()">\n        <ion-icon name="close-circle"></ion-icon>&nbsp;&nbsp;\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content no-padding class="bgc">\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n    <ion-refresher-content pullingIcon="arrow-dropdown" pullingText="Pull to refresh" refreshingSpinner="crescent"\n      refreshingText="">\n    </ion-refresher-content>\n  </ion-refresher>\n  <ion-grid fixed class="wrapper">\n    <ion-list>\n      <ion-item *ngFor="let val of listOutlet">\n        <h2>{{val.Nama}}</h2>\n        <h3>Kode Toko : {{val.KdOutlet}}</h3>\n        <p>Alamat Toko : {{val.KotaToko}}</p>\n        <ion-note item-end color="warning" *ngIf="val.statuz_pending==\'1\'">\n          Pending\n        </ion-note>\n        <ion-note item-end color="secondary" *ngIf="val.statuz_valid==\'1\'">\n          Validate\n        </ion-note>\n        <ion-icon name="download" item-end *ngIf="bisa_pilih==\'Yes\'" (click)="pilih_ini(val.KdOutlet,val.Nama)">\n        </ion-icon>\n      </ion-item>\n\n    </ion-list>\n    <ion-infinite-scroll (ionInfinite)="doInfinite($event,src, first,filter)">\n      <ion-infinite-scroll-content loadingSpinner="bubbles" loadingText="Mencari Data Lagi...">\n      </ion-infinite-scroll-content>\n    </ion-infinite-scroll>\n  </ion-grid>\n</ion-content>`/*ion-inline-end:"D:\ionic\Project\qrcode\src\pages\detailbangetlistings\detailbangetlistings.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_3__config_provider__["a" /* Provider */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], DetailbangetlistingsPage);
    return DetailbangetlistingsPage;
}());

//# sourceMappingURL=detailbangetlistings.js.map

/***/ }),

/***/ 55:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return NoolistPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AksiPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_provider__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_detailhistory_detailhistory__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_petanoo_petanoo__ = __webpack_require__(136);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










/**
 * Generated class for the NoolistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var NoolistPage = (function () {
    function NoolistPage(navCtrl, viewCtrl, postPvdr, storage, alertCtrl, loadingCtrl, popoverCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.postPvdr = postPvdr;
        this.storage = storage;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.popoverCtrl = popoverCtrl;
        this.navParams = navParams;
        this.from_storage = navParams.get("from_storage");
        this.kdoutlet = navParams.get("kdoutlet");
        this.kdcabang = navParams.get("kdcabang");
        this.kdsales = navParams.get("kdsales");
        this.user_level = navParams.get("user_level");
    }
    NoolistPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad NoolistPage');
        this.listNoo = new Array();
        this.getData('', 0, 10, 'No');
    };
    NoolistPage.prototype.form_search = function () {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'Cari',
            message: "Masukan kata untuk mencari Toko N.O.O",
            inputs: [
                {
                    name: 'src',
                    placeholder: 'Apa yang anda cari...'
                },
            ],
            buttons: [
                {
                    text: 'Batal',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Cari',
                    handler: function (data) {
                        console.log('Saved clicked');
                        _this.getlistNoo(data.src);
                    }
                }
            ]
        });
        prompt.present();
    };
    NoolistPage.prototype.getlistNoo = function (src) {
        if (src) {
            src = src;
        }
        else {
            src = '';
        }
        this.listNoo = new Array();
        this.getData(src, 0, 10, 'No');
    };
    NoolistPage.prototype.getData = function (src, first, offset, infinite) {
        var _this = this;
        this.first = first;
        this.src = src;
        var loader = this.loadingCtrl.create({
            content: "Mohon Menunggu..."
        });
        if (infinite == 'No') {
            loader.present();
        }
        this.storage.get('user_session_storage').then(function (res) {
            var body;
            if (_this.from_storage == 'No') {
                body = {
                    first: first,
                    offset: offset,
                    keyword: src,
                    kdcabang: _this.kdcabang,
                    user_level: _this.user_level,
                    kdoutlet: _this.kdoutlet,
                    kdsales: _this.kdsales,
                    username: res[0].username
                };
            }
            else {
                body = {
                    first: first,
                    offset: offset,
                    keyword: src,
                    kdcabang: res[0].cabang_id,
                    user_level: res[0].user_level,
                    kdoutlet: res[0].kdoutlet,
                    kdsales: res[0].kdsales,
                    username: res[0].username
                };
            }
            _this.postPvdr.postData(body, 'outlet/getNoo').subscribe(function (data) {
                if (data.success) {
                    loader.dismiss();
                    for (var i = 0; i < data.result.length; i++) {
                        var tipedata;
                        if (data.result[i]['TypeData'] == 'S') {
                            tipedata = "Server";
                        }
                        else {
                            tipedata = "WhatsApp";
                        }
                        _this.listNoo.push({
                            id: data.result[i]['id'],
                            KdOutlet: data.result[i]['KdOutlet'],
                            OutletName: data.result[i]['OutletName'],
                            Address: data.result[i]['Address'],
                            UserUpload: data.result[i]['UserUpload'],
                            DateUpload: data.result[i]['DateUpload'],
                            UserValidate: data.result[i]['UserValidate'],
                            DateValidate: data.result[i]['DateValidate'],
                            Lat: data.result[i]['Lat'],
                            Lng: data.result[i]['Lng'],
                            Status: data.result[i]['Status'],
                            Images: data.result[i]['Images'],
                            tipedata: tipedata,
                            Note: data.result[i]['Note'],
                            NoteValidator: data.result[i]['NoteValidator']
                        });
                    }
                }
                else {
                    loader.dismiss();
                    if (infinite == 'No') {
                        var alert_1 = _this.alertCtrl.create({
                            title: 'Info',
                            subTitle: 'Mohon Maaf Data Tidak Di Temukan',
                            message: '',
                            buttons: ['OK']
                        });
                        alert_1.present();
                    }
                    return false;
                }
            }, function (error) {
                loader.dismiss();
                var alert = _this.alertCtrl.create({
                    title: 'Mohon Maaf',
                    subTitle: 'Sedang tidak terkoneksi dengan server. Silahkan coba kembali beberapa saat',
                    buttons: ['OK']
                });
                alert.present();
                return false;
            });
        });
    };
    NoolistPage.prototype.doRefresh = function (refresher) {
        this.listNoo = new Array();
        this.getData('', 0, 10, 'No');
        setTimeout(function () {
            refresher.complete();
        }, 1000);
    };
    NoolistPage.prototype.doInfinite = function (infiniteScroll, src, first, filter) {
        var _this = this;
        first = first + 10;
        console.log('Begin async operation');
        setTimeout(function () {
            _this.getData(src, first, 10, 'Yes');
            infiniteScroll.complete();
        }, 1000);
    };
    NoolistPage.prototype.aksi = function (ev, id, lat, lng, name_outlet) {
        var _this = this;
        //popover
        var popover = this.popoverCtrl.create(AksiPage, { id: id, lat: lat, lng: lng, name_noo: name_outlet });
        popover.present({
            ev: ev
        });
        //ketika popover di tutup
        popover.onDidDismiss(function (res) {
            if (res) {
                if (res.edit) {
                    _this.edit(id);
                }
            }
        });
    };
    NoolistPage.prototype.edit = function (id) {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'Edit N.O.O',
            message: "Masukkan Kode Toko Resmi Untuk Toko Ini.",
            inputs: [
                {
                    name: 'new_kdoutlet',
                    placeholder: 'Kode Toko'
                },
            ],
            buttons: [
                {
                    text: 'Batal',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Simpan',
                    handler: function (data) {
                        _this.save_edit(id, data.new_kdoutlet);
                    }
                }
            ]
        });
        prompt.present();
    };
    NoolistPage.prototype.save_edit = function (id, new_kdoutlet) {
        var _this = this;
        var loader = this.loadingCtrl.create({
            content: "Proses Simpan Toko Baru..."
        });
        loader.present();
        this.storage.get('user_session_storage').then(function (res) {
            var body = {
                id: id,
                new_kdoutlet: new_kdoutlet,
                username: res[0].username
            };
            _this.postPvdr.postData(body, 'outlet/saveNewOutlet').subscribe(function (data) {
                if (data.success) {
                    loader.dismiss();
                    var confirm_1 = _this.alertCtrl.create({
                        title: 'Info',
                        message: 'Penyimpanan Kode Toko Baru Berhasil',
                        buttons: [
                            {
                                text: 'Oke',
                                handler: function () {
                                    _this.ionViewDidLoad();
                                }
                            }
                        ]
                    });
                    confirm_1.present();
                    return false;
                }
                else {
                    loader.dismiss();
                    var alert_2 = _this.alertCtrl.create({
                        title: 'Info',
                        subTitle: 'Penyimpanan Kode Toko Baru Gagal',
                        message: '',
                        buttons: ['OK']
                    });
                    alert_2.present();
                    return false;
                }
            }, function (error) {
                loader.dismiss();
                var alert = _this.alertCtrl.create({
                    title: 'Mohon Maaf',
                    subTitle: 'Sedang tidak terkoneksi dengan server. Silahkan coba kembali beberapa saat',
                    buttons: ['OK']
                });
                alert.present();
                return false;
            });
        });
    };
    NoolistPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-noolist',template:/*ion-inline-start:"D:\ionic\Project\qrcode\src\pages\noolist\noolist.html"*/`<!--\n  Generated template for the NoolistPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="oren">\n    <ion-title>N.O.O List</ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="form_search()">\n        <ion-icon name="search"></ion-icon>&nbsp;&nbsp;\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content no-padding class="bgc">\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n    <ion-refresher-content pullingIcon="arrow-dropdown" pullingText="Pull to refresh" refreshingSpinner="crescent"\n      refreshingText="">\n    </ion-refresher-content>\n  </ion-refresher>\n  <ion-grid fixed class="wrapper">\n\n    <ion-list>\n      <ion-item *ngFor="let val of listNoo">\n        <ion-avatar item-start>\n          <img src="assets/imgs/setting.png">\n        </ion-avatar>\n        <h2>{{val.KdOutlet}} - {{val.OutletName}} <ion-icon *ngIf="val.Status==\'1\'" name="checkmark-circle"\n            color="secondary"></ion-icon>\n          <ion-icon *ngIf="val.Status==\'2\'" name="close-circle" color="danger"></ion-icon>\n        </h2>\n        <p>{{val.Address}}</p>\n        <h3>Uploader : {{val.UserUpload}}</h3>\n        <h3>Date Upload : {{val.DateUpload}}</h3>\n        <h3 *ngIf="val.Status!=\'0\'">Validator : {{val.UserValidate}}</h3>\n        <h3 *ngIf="val.Status!=\'0\'">Date Validate : {{val.DateValidate}}</h3>\n        <p *ngIf="val.Note!=\'\'">Note Uploader: {{val.Note}}</p>\n        <p>Source Data: {{val.tipedata}}</p>\n        <p *ngIf="val.NoteValidator!=\'\'">Note Validator: {{val.NoteValidator}}</p>\n        <button ion-button clear item-end (click)="aksi($event,val.id,val.Lat,val.Lng,val.OutletName)">aksi</button>\n      </ion-item>\n    </ion-list>\n\n    <ion-infinite-scroll (ionInfinite)="doInfinite($event,src, first,filter)">\n      <ion-infinite-scroll-content loadingSpinner="bubbles" loadingText="Loading more data...">\n      </ion-infinite-scroll-content>\n    </ion-infinite-scroll>\n\n  </ion-grid>\n</ion-content>`/*ion-inline-end:"D:\ionic\Project\qrcode\src\pages\noolist\noolist.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_2__config_provider__["a" /* Provider */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* PopoverController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], NoolistPage);
    return NoolistPage;
}());

var AksiPage = (function () {
    function AksiPage(alertCtrl, navCtrl, viewCtrl, navParams, postPvdr, storage, loadingCtrl) {
        var _this = this;
        this.alertCtrl = alertCtrl;
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.navParams = navParams;
        this.postPvdr = postPvdr;
        this.storage = storage;
        this.loadingCtrl = loadingCtrl;
        this.id = navParams.get("id");
        this.lat = navParams.get("lat");
        this.lng = navParams.get("lng");
        this.name_noo = navParams.get("name_noo");
        this.storage.get('user_session_storage').then(function (res) {
            if ((res[0].user_level == '-1' && res[0].developer == '1') || res[0].user_level == '2') {
                _this.hak_meresmikan_noo = 'Yes';
            }
            else {
                _this.hak_meresmikan_noo = 'No';
            }
        });
    }
    AksiPage.prototype.ionViewDidLoad = function () {
        //
    };
    AksiPage.prototype.close = function () {
        this.viewCtrl.dismiss();
    };
    AksiPage.prototype.peta = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__pages_petanoo_petanoo__["a" /* PetanooPage */], { lat: this.lat, lng: this.lng, name_noo: this.name_noo });
        this.close();
    };
    AksiPage.prototype.detail = function (id) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__pages_detailhistory_detailhistory__["a" /* DetailhistoryPage */], { id: this.id });
        this.close();
    };
    AksiPage.prototype.edit = function () {
        this.viewCtrl.dismiss({ edit: true });
    };
    AksiPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"D:\ionic\Project\qrcode\src\pages\noolist\aksi.html"*/`<ion-content>\n\n  <ion-item-group>\n\n    <ion-item (click)="detail()">Detail</ion-item>\n\n    <ion-item (click)="peta()">Peta</ion-item>\n\n    <ion-item *ngIf="hak_meresmikan_noo==\'Yes\'" (click)="edit()">Edit</ion-item>\n\n  </ion-item-group>\n\n</ion-content>`/*ion-inline-end:"D:\ionic\Project\qrcode\src\pages\noolist\aksi.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__config_provider__["a" /* Provider */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */]])
    ], AksiPage);
    return AksiPage;
}());

//# sourceMappingURL=noolist.js.map

/***/ }),

/***/ 56:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MainPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_location_accuracy__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_android_permissions__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_barcode_scanner__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_storage__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_status_bar__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__config_provider__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_formoutletpwa_formoutletpwa__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_home_home__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_history_history__ = __webpack_require__(140);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_settings_settings__ = __webpack_require__(141);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_admin_admin__ = __webpack_require__(137);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_logs_logs__ = __webpack_require__(142);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_performa_performa__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_detaillistings_detaillistings__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_activities_activities__ = __webpack_require__(133);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_petatoko_petatoko__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_detailbangetlistings_detailbangetlistings__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_usersdistributor_usersdistributor__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_analytics_analytics__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_bypass_bypass__ = __webpack_require__(72);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_trafic_trafic__ = __webpack_require__(143);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_noolist_noolist__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__pages_rutesales_rutesales__ = __webpack_require__(144);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__pages_tokoplus_tokoplus__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pages_login_login__ = __webpack_require__(57);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};































var MainPage = (function () {
    function MainPage(navCtrl, modalCtrl, geolocation, storage, app, postPvdr, statusBar, alertCtrl, barcodeScanner, locationAccuracy, androidPermissions, navParams) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl;
        this.geolocation = geolocation;
        this.storage = storage;
        this.app = app;
        this.postPvdr = postPvdr;
        this.statusBar = statusBar;
        this.alertCtrl = alertCtrl;
        this.barcodeScanner = barcodeScanner;
        this.locationAccuracy = locationAccuracy;
        this.androidPermissions = androidPermissions;
        this.navParams = navParams;
        this.version = '150320';
        this.update = 'No';
        // set status bar to white
        this.statusBar.backgroundColorByHexString('#633ce0');
        this.storage.get('user_session_storage').then(function (res) {
            _this.name = res[0].employee_name;
            _this.jabatan_name = res[0].jabatan_name;
            _this.level = res[0].user_level;
            _this.corp = res[0].distributor_name;
            _this.developer = res[0].developer;
            _this.jenis_outlet = res[0].jenis_outlet;
        });
        this.updates();
        this.checkGPSPermission();
    }
    MainPage.prototype.ionViewDidLoad = function () {
    };
    MainPage.prototype.updates = function () {
        var _this = this;
        this.storage.get('user_session_storage').then(function (res) {
            var body = {
                username: res[0].username
            };
            _this.postPvdr.postData(body, 'login/cek_update_home').subscribe(function (data) {
                console.log('update version : ', data);
                if (data.success) {
                    if (data.result != _this.version) {
                        _this.update = 'Yes';
                    }
                    else {
                        _this.update = 'No';
                    }
                    //jika sudah tidak aktif maka auto logout
                    if (data.active == 'T') {
                        _this.storage.clear();
                        _this.app.getRootNav().setRoot(__WEBPACK_IMPORTED_MODULE_27__pages_login_login__["a" /* LoginPage */]);
                    }
                }
                else {
                    _this.update = 'No';
                }
            }, function (error) {
                _this.update = 'No';
            });
        });
    };
    MainPage.prototype.checkGPSPermission = function () {
        var _this = this;
        this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(function (result) {
            console.log('status harus melewati permission : ', result.hasPermission);
            if (result.hasPermission) {
                _this.askToTurnOnGPS();
            }
            else {
                _this.requestGPSPermission();
            }
        }, function (err) {
            console.log(err);
        });
    };
    MainPage.prototype.requestGPSPermission = function () {
        var _this = this;
        this.locationAccuracy.canRequest().then(function (canRequest) {
            if (canRequest) {
                //your code
            }
            else {
                _this.androidPermissions.requestPermission(_this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
                    .then(function () {
                    _this.askToTurnOnGPS();
                }, function (error) {
                    console.log('requestPermission Error requesting location permissions ' + error);
                });
            }
        });
    };
    MainPage.prototype.askToTurnOnGPS = function () {
        this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(function () {
            //your code if success
        }, function (error) {
            return console.log('Error requesting location permissions ' + JSON.stringify(error));
        });
    };
    MainPage.prototype.scanCode = function () {
        var _this = this;
        this.storage.get('user_session_storage').then(function (res) {
            if (res[0].user_level == '-1' || res[0].user_level == '2') {
                _this.barcodeScanner.scan().then(function (barcodeData) {
                    var strArray = barcodeData.text.split("#");
                    console.log('test : ', strArray);
                    var modal = _this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_10__pages_home_home__["a" /* HomePage */], { lat: strArray[3], lng: strArray[4], outlet: strArray[0], name: strArray[1], address: strArray[2], note: strArray[5], kode_distributor: strArray[6], kode_sales: strArray[7] });
                    modal.present();
                }).catch(function (err) {
                    console.log('Error', err);
                });
            }
            else {
                var alert_1 = _this.alertCtrl.create({
                    title: 'Mohon Maaf',
                    subTitle: 'Anda Tidak Mempunyai Akses.',
                    buttons: ['OK']
                });
                alert_1.present();
            }
        });
    };
    // apps
    // pin_outlets() {
    //   this.navCtrl.push(FormoutletPage)
    // }
    //pwa
    MainPage.prototype.pin_outlets = function () {
        // const alert = this.alertCtrl.create({
        //   title: 'Info',
        //   subTitle: 'Pastikan GPS Anda sudah Aktif',
        //   buttons: ['OK']
        // });
        // alert.present();
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__pages_formoutletpwa_formoutletpwa__["a" /* FormoutletpwaPage */]);
    };
    MainPage.prototype.history = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_11__pages_history_history__["a" /* HistoryPage */]);
    };
    MainPage.prototype.performa = function () {
        var _this = this;
        this.storage.get('user_session_storage').then(function (res) {
            if (res[0].jenis_outlet == 'distributor') {
                if (_this.level == '4') {
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_15__pages_performa_performa__["a" /* PerformaPage */]);
                }
                else if (_this.level == '2' || _this.level == '10') {
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_20__pages_usersdistributor_usersdistributor__["a" /* UsersdistributorPage */], { kdoutlet: res[0].kdoutlet, kdcabang: res[0].cabang_id, performa: 'Yes', close: 'No', bisa_pilih: 'No', reset_password: 'No' });
                }
                else {
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_16__pages_detaillistings_detaillistings__["a" /* DetaillistingsPage */], { kdoutlet: '', kdcabang: '', storages: 'Yes', close: 'No', bisa_pilih: 'No' });
                }
            }
            else {
                var alert_2 = _this.alertCtrl.create({
                    title: 'Mohon Maaf',
                    subTitle: 'Anda Tidak Mempunyai Akses.',
                    buttons: ['OK']
                });
                alert_2.present();
            }
        });
    };
    MainPage.prototype.settings = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_12__pages_settings_settings__["a" /* SettingsPage */]);
    };
    MainPage.prototype.admin_page = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_13__pages_admin_admin__["a" /* AdminPage */]);
    };
    MainPage.prototype.listing = function () {
        var _this = this;
        // this.navCtrl.push(ListingPage);
        this.storage.get('user_session_storage').then(function (res) {
            //2 dan 10
            if (res[0].user_level == '2' || res[0].user_level == '10') {
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_19__pages_detailbangetlistings_detailbangetlistings__["a" /* DetailbangetlistingsPage */], { kdoutlet: res[0].kdoutlet, kdcabang: res[0].cabang_id, kdsales: '', viewuser: 'Yes', title: '', close: 'No', bisa_pilih: 'No' });
            }
            else {
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_16__pages_detaillistings_detaillistings__["a" /* DetaillistingsPage */], { kdoutlet: '', kdcabang: '', storages: 'Yes', close: 'No', bisa_pilih: 'No' });
            }
        });
    };
    MainPage.prototype.activities = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_17__pages_activities_activities__["a" /* ActivitiesPage */]);
        // const alert = this.alertCtrl.create({
        //   title: 'Mohon Maaf',
        //   subTitle: 'Masih Dalam Tahap Pengembangan',
        //   buttons: ['OK']
        // });
        // alert.present();
    };
    MainPage.prototype.allOutlets = function () {
        var _this = this;
        this.storage.get('user_session_storage').then(function (res) {
            //admin dan kedist
            if (res[0].user_level == '2' || res[0].user_level == '10') {
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_18__pages_petatoko_petatoko__["a" /* PetatokoPage */], {
                    kdcabang: res[0].cabang_id,
                    kdoutlet: res[0].kdoutlet,
                    kdsales: '',
                    distributor_name: res[0].distributor_name,
                    title: res[0].distributor_name,
                    sales_name: 'Semua',
                    legend_type: '2'
                });
                //regional
            }
            else if (res[0].user_level == '6') {
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_18__pages_petatoko_petatoko__["a" /* PetatokoPage */], {
                    kdcabang: res[0].cabang_id,
                    kdoutlet: '',
                    kdsales: '',
                    distributor_name: 'Semua Distributor Regional',
                    title: 'Semua Distributor Regional',
                    sales_name: 'Semua',
                    legend_type: '2'
                });
            }
            else if (res[0].user_level == '-1' || res[0].user_level == '9') {
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_18__pages_petatoko_petatoko__["a" /* PetatokoPage */], {
                    kdcabang: '',
                    kdoutlet: '',
                    kdsales: '',
                    distributor_name: 'Semua Distributor Nasional',
                    title: 'Semua Distributor Nasional',
                    sales_name: 'Semua',
                    legend_type: '2'
                });
            }
        });
    };
    MainPage.prototype.Logs = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_14__pages_logs_logs__["a" /* LogsPage */]);
    };
    MainPage.prototype.bypass = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_22__pages_bypass_bypass__["a" /* BypassPage */]);
    };
    MainPage.prototype.traffic = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_23__pages_trafic_trafic__["a" /* TraficPage */]);
    };
    MainPage.prototype.luar_rute = function () {
        var _this = this;
        this.storage.get('user_session_storage').then(function (res) {
            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_26__pages_tokoplus_tokoplus__["a" /* TokoplusPage */], { from_storage: 'No', kdcabang: res[0].cabang_id, user_level: res[0].user_level, kdoutlet: res[0].kdoutlet, kdsales: res[0].kdsales, performa: 'No' });
        });
    };
    MainPage.prototype.rutesales = function () {
        var _this = this;
        this.storage.get('user_session_storage').then(function (res) {
            if (_this.level == '2' || _this.level == '10') {
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_25__pages_rutesales_rutesales__["a" /* RutesalesPage */], { kdoutlet: res[0].kdoutlet, kdcabang: res[0].cabang_id });
            }
            else {
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_25__pages_rutesales_rutesales__["a" /* RutesalesPage */], { kdoutlet: '', kdcabang: '' });
            }
        });
    };
    MainPage.prototype.analytics = function () {
        var _this = this;
        // this.navCtrl.push(AnalyticsPage);
        this.storage.get('user_session_storage').then(function (res) {
            //2 dan 10
            if (res[0].user_level == '2' || res[0].user_level == '10') {
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_21__pages_analytics_analytics__["a" /* AnalyticsPage */], { kdoutlet: res[0].kdoutlet, kdcabang: res[0].cabang_id, kdsales: '', viewuser: 'Yes', title: '', close: 'No', bisa_pilih: 'No' });
            }
            else {
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_21__pages_analytics_analytics__["a" /* AnalyticsPage */], { kdoutlet: '', kdcabang: '', kdsales: '', storages: 'Yes', close: 'No', bisa_pilih: 'No' });
            }
        });
    };
    MainPage.prototype.noo = function () {
        var _this = this;
        this.storage.get('user_session_storage').then(function (res) {
            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_24__pages_noolist_noolist__["b" /* NoolistPage */], { from_storage: 'No', kdcabang: res[0].cabang_id, user_level: res[0].user_level, kdoutlet: res[0].kdoutlet, kdsales: '' });
        });
    };
    MainPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-main',template:/*ion-inline-start:"D:\ionic\Project\qrcode\src\pages\main\main.html"*/`<!-- <ion-header>\n\n  <ion-navbar color="oren">\n    <ion-title>\n      <div><b>GoCheck</b></div>\n    </ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="settings()">\n        <ion-icon name="settings"></ion-icon>&nbsp;&nbsp;\n      </button>\n    </ion-buttons>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="logout()">\n        <ion-icon style="margin-top: 40px;margin-bottom: 15px;" name="log-out"></ion-icon>&nbsp;&nbsp;\n      </button>\n    </!\n</ion-navbar>\n\n</ion-header> -->\n\n<!-- <ion-content class="bgc"> -->\n<ion-content class="bgc">\n  <ion-grid fixed class="wrapper">\n\n    <!-- <div id="tab-1" class="tab page-content tab-active"> -->\n\n    <!-- title -->\n    <div class="title-apps padding-middle background-primer">\n      <div class="container">\n        <div class="row row-no-margin-bottom">\n          <div class="col">\n            <h3 class="color-white">GoCheck</h3>\n          </div>\n          <div class="col">\n            <span class="icon-middle margin-left-small float-right color-white" (click)="settings()">\n              <h3 class="color-white">\n                <ion-badge id="notifications-badge" *ngIf="update==\'Yes\'">\n                  <span>&nbsp;update&nbsp;</span>\n                </ion-badge>\n                <ion-icon name="settings"></ion-icon>\n              </h3>\n            </span>\n          </div>\n        </div>\n      </div>\n    </div>\n    <!-- end title -->\n\n\n\n    <!-- profile balance -->\n    <div class="border-radius-style background-circle background-primer">\n      <div class="container">\n        <div class="background-white border-radius padding-box-middle box-shadow">\n          <div class="row row-no-margin-bottom">\n            <div class="col-60">\n              <div class="float-left margin-right-small">\n                <img class="people" src="assets/imgs/users.png" alt="">\n              </div>\n              <div class="overflow-hidden">\n                <h4>{{name}}</h4>\n                <p>{{jabatan_name}}<br>\n                  {{corp}}</p>\n              </div>\n            </div>\n            <!-- <div class="col-40">\n                <button class="buttons float-right letter-spacing margin-top-small">$ 650</button>\n              </div> -->\n          </div>\n        </div>\n      </div>\n    </div>\n    <!-- end profile balance -->\n  </ion-grid>\n  <ion-grid fixed class="wrapper_body">\n    <!-- separator -->\n    <div class="separator-small"></div>\n    <div class="separator-small"></div>\n    <div class="separator-small"></div>\n    <div class="separator-small"></div>\n    <!-- end separator -->\n    <!-- menus -->\n    <div class="menus">\n      <div class="container">\n        <div>\n          <!-- sales -->\n          <div class="row" *ngIf="level==\'4\'">\n            <div class="col" (click)="pin_outlets()">\n              <div class="background-white text-center border-radius padding-box box-shadow">\n                <img src="assets/imgs/maps.png" width=\'55px\' height=\'55px\' />\n                <h6 class="font-weight-500">Tag Lokasi</h6>\n              </div>\n            </div>\n            <div class="col" (click)="performa()">\n              <div class="background-white text-center border-radius padding-box box-shadow">\n                <img src="assets/imgs/performance.png" width=\'55px\' height=\'55px\' />\n                <h6 class="font-weight-500">Performa</h6>\n              </div>\n            </div>\n            <div class="col" (click)="history()">\n              <div class="background-white text-center border-radius padding-box box-shadow">\n                <img src="assets/imgs/outlet.png" width=\'55px\' height=\'55px\' />\n                <h6 class="font-weight-500">Histori</h6>\n              </div>\n            </div>\n          </div>\n\n\n          <!-- admin vci  -->\n          <div class="row" *ngIf="level==\'2\' && jenis_outlet==\'vci\'">\n            <div class="col" (click)="pin_outlets()">\n              <div class="background-white text-center border-radius padding-box box-shadow">\n                <img src="assets/imgs/maps.png" width=\'55px\' height=\'55px\' />\n                <h6 class="font-weight-500">Tag Lokasi</h6>\n              </div>\n            </div>\n            <div class="col" (click)="performa()">\n              <div class="background-white text-center border-radius padding-box box-shadow">\n                <img src="assets/imgs/performance.png" width=\'55px\' height=\'55px\' />\n                <h6 class="font-weight-500">Performa</h6>\n              </div>\n            </div>\n            <div class="col" (click)="history()">\n              <div class="background-white text-center border-radius padding-box box-shadow">\n                <img src="assets/imgs/outlet.png" width=\'55px\' height=\'55px\' />\n                <h6 class="font-weight-500">Histori</h6>\n              </div>\n            </div>\n          </div>\n          <div class="row" *ngIf="level==\'2\' && jenis_outlet==\'vci\'">\n            <div class="col" (click)="admin_page()">\n              <div class="background-white text-center border-radius padding-box box-shadow">\n                <img src="assets/imgs/qrscanlogo.png" width=\'55px\' height=\'55px\' />\n                <h6 class="font-weight-500">Admin</h6>\n              </div>\n            </div>\n            <div class="col" (click)="scanCode()">\n              <div class="background-white text-center border-radius padding-box box-shadow">\n                <img src="assets/imgs/analytics.png" width=\'55px\' height=\'55px\' />\n                <h6 class="font-weight-500">Scan QR</h6>\n              </div>\n            </div>\n            <div class="col" (click)="listing()">\n              <div class="background-white text-center border-radius padding-box box-shadow">\n                <img src="assets/imgs/listing.png" width=\'55px\' height=\'55px\' />\n                <h6 class="font-weight-500">List</h6>\n              </div>\n            </div>\n          </div>\n          <div class="row" *ngIf="level==\'2\' && jenis_outlet==\'vci\'">\n            <div class="col" (click)="activities()">\n              <div class="background-white text-center border-radius padding-box box-shadow">\n                <img src="assets/imgs/activities.png" width=\'55px\' height=\'55px\' />\n                <h6 class="font-weight-500">Aktifitas</h6>\n              </div>\n            </div>\n            <div class="col" (click)="allOutlets()">\n              <div class="background-white text-center border-radius padding-box box-shadow">\n                <img src="assets/imgs/outlets.png" width=\'55px\' height=\'55px\' />\n                <h6 class="font-weight-500">Toko</h6>\n              </div>\n            </div>\n            <div class="col" (click)="noo()">\n              <div class="background-white text-center border-radius padding-box box-shadow">\n                <img src="assets/imgs/noo.png" width=\'55px\' height=\'55px\' />\n                <h6 class="font-weight-500">N.O.O</h6>\n              </div>\n            </div>\n          </div>\n\n          <!-- admin non vci -->\n          <div class="row" *ngIf="level==\'2\' && jenis_outlet!=\'vci\'">\n            <div class="col" (click)="admin_page()">\n              <div class="background-white text-center border-radius padding-box box-shadow">\n                <img src="assets/imgs/qrscanlogo.png" width=\'55px\' height=\'55px\' />\n                <h6 class="font-weight-500">Admin</h6>\n              </div>\n            </div>\n            <div class="col" (click)="scanCode()">\n              <div class="background-white text-center border-radius padding-box box-shadow">\n                <img src="assets/imgs/analytics.png" width=\'55px\' height=\'55px\' />\n                <h6 class="font-weight-500">Scan QR</h6>\n              </div>\n            </div>\n            <div class="col" (click)="performa()">\n              <div class="background-white text-center border-radius padding-box box-shadow">\n                <img src="assets/imgs/performance.png" width=\'55px\' height=\'55px\' />\n                <h6 class="font-weight-500">Performa</h6>\n              </div>\n            </div>\n          </div>\n          <div class="row" *ngIf="level==\'2\' && jenis_outlet!=\'vci\'">\n            <div class="col" (click)="listing()">\n              <div class="background-white text-center border-radius padding-box box-shadow">\n                <img src="assets/imgs/listing.png" width=\'55px\' height=\'55px\' />\n                <h6 class="font-weight-500">List</h6>\n              </div>\n            </div>\n            <div class="col" (click)="rutesales()">\n              <div class="background-white text-center border-radius padding-box box-shadow">\n                <img src="assets/imgs/route.png" width=\'55px\' height=\'55px\' />\n                <h6 class="font-weight-500">Rute Sales</h6>\n              </div>\n            </div>\n            <div class="col" (click)="luar_rute()">\n              <div class="background-white text-center border-radius padding-box box-shadow">\n                <img src="assets/imgs/no_route.png" width=\'55px\' height=\'55px\' />\n                <h6 class="font-weight-500">Luar Rute</h6>\n              </div>\n            </div>\n          </div>\n          <div class="row" *ngIf="level==\'2\' && jenis_outlet!=\'vci\'">\n            <div class="col" (click)="activities()">\n              <div class="background-white text-center border-radius padding-box box-shadow">\n                <img src="assets/imgs/activities.png" width=\'55px\' height=\'55px\' />\n                <h6 class="font-weight-500">Aktifitas</h6>\n              </div>\n            </div>\n            <div class="col" (click)="allOutlets()">\n              <div class="background-white text-center border-radius padding-box box-shadow">\n                <img src="assets/imgs/outlets.png" width=\'55px\' height=\'55px\' />\n                <h6 class="font-weight-500">Toko</h6>\n              </div>\n            </div>\n            <div class="col" (click)="noo()">\n              <div class="background-white text-center border-radius padding-box box-shadow">\n                <img src="assets/imgs/noo.png" width=\'55px\' height=\'55px\' />\n                <h6 class="font-weight-500">N.O.O</h6>\n              </div>\n            </div>\n          </div>\n\n          <!-- Kepala sales distributor, regional, nasional  -->\n          <div class="row" *ngIf="level==\'6\' || level==\'9\' || level==\'10\'">\n            <div class="col" (click)="listing()">\n              <div class="background-white text-center border-radius padding-box box-shadow">\n                <img src="assets/imgs/listing.png" width=\'55px\' height=\'55px\' />\n                <h6 class="font-weight-500">List</h6>\n              </div>\n            </div>\n            <div class="col" (click)="activities()">\n              <div class="background-white text-center border-radius padding-box box-shadow">\n                <img src="assets/imgs/activities.png" width=\'55px\' height=\'55px\' />\n                <h6 class="font-weight-500">Aktifitas</h6>\n              </div>\n            </div>\n            <div class="col" (click)="history()">\n              <div class="background-white text-center border-radius padding-box box-shadow">\n                <img src="assets/imgs/outlet.png" width=\'55px\' height=\'55px\' />\n                <h6 class="font-weight-500">Histori</h6>\n              </div>\n            </div>\n          </div>\n          <div class="row" *ngIf="level==\'6\' || level==\'9\' || level==\'10\'">\n            <div class="col" (click)="allOutlets()">\n              <div class="background-white text-center border-radius padding-box box-shadow">\n                <img src="assets/imgs/outlets.png" width=\'55px\' height=\'55px\' />\n                <h6 class="font-weight-500">Toko</h6>\n              </div>\n            </div>\n            <div class="col" (click)="noo()">\n              <div class="background-white text-center border-radius padding-box box-shadow">\n                <img src="assets/imgs/noo.png" width=\'55px\' height=\'55px\' />\n                <h6 class="font-weight-500">N.O.O</h6>\n              </div>\n            </div>\n            <div class="col" (click)="rutesales()">\n              <div class="background-white text-center border-radius padding-box box-shadow">\n                <img src="assets/imgs/route.png" width=\'55px\' height=\'55px\' />\n                <h6 class="font-weight-500">Rute Sales</h6>\n              </div>\n            </div>\n          </div>\n          <div class="row" *ngIf="level==\'6\' || level==\'9\' || level==\'10\'">\n            <div class="col" (click)="luar_rute()">\n              <div class="background-white text-center border-radius padding-box box-shadow">\n                <img src="assets/imgs/no_route.png" width=\'55px\' height=\'55px\' />\n                <h6 class="font-weight-500">Luar Rute</h6>\n              </div>\n            </div>\n            <div class="col" (click)="analytics()">\n              <div class="background-white text-center border-radius padding-box box-shadow">\n                <img src="assets/imgs/analyticss.png" width=\'55px\' height=\'55px\' />\n                <h6 class="font-weight-500">Analisa</h6>\n              </div>\n            </div>\n            <div class="col">\n              <div class="background-white text-center border-radius padding-box box-shadow">\n                <img src="assets/imgs/charts.png" width=\'55px\' height=\'55px\' />\n                <h6 class="font-weight-500">Grafik</h6>\n              </div>\n            </div>\n          </div>\n          <div class="row" *ngIf="level==\'6\' || level==\'9\' || level==\'10\'">\n            <div class="col">\n              <div class="background-white text-center border-radius padding-box box-shadow">\n                <img src="assets/imgs/barang.png" width=\'55px\' height=\'55px\' />\n                <h6 class="font-weight-500">Produk</h6>\n              </div>\n            </div>\n            <div class="col">\n              <div class="background-white text-center border-radius padding-box box-shadow">\n                <img src="assets/imgs/sales.png" width=\'55px\' height=\'55px\' />\n                <h6 class="font-weight-500">Sales</h6>\n              </div>\n            </div>\n            <div class="col">\n              <div class="background-white text-center border-radius padding-box box-shadow">\n                <img src="assets/imgs/bi.png" width=\'55px\' height=\'55px\' />\n                <h6 class="font-weight-500">B.Intel</h6>\n              </div>\n            </div>\n          </div>\n\n          <!-- super user -->\n          <div class="row" *ngIf="level==\'-1\'">\n            <div class="col" (click)="pin_outlets()">\n              <div class="background-white text-center border-radius padding-box box-shadow">\n                <img src="assets/imgs/maps.png" width=\'55px\' height=\'55px\' />\n                <h6 class="font-weight-500">Tag Lokasi</h6>\n              </div>\n            </div>\n            <div class="col" (click)="scanCode()">\n              <div class="background-white text-center border-radius padding-box box-shadow">\n                <img src="assets/imgs/analytics.png" width=\'55px\' height=\'55px\' />\n                <h6 class="font-weight-500">Scan QR</h6>\n              </div>\n            </div>\n            <div class="col" (click)="history()">\n              <div class="background-white text-center border-radius padding-box box-shadow">\n                <img src="assets/imgs/outlet.png" width=\'55px\' height=\'55px\' />\n                <h6 class="font-weight-500">Histori</h6>\n              </div>\n            </div>\n          </div>\n          <div class="row" *ngIf="level==\'-1\'">\n            <div class="col" (click)="admin_page()">\n              <div class="background-white text-center border-radius padding-box box-shadow">\n                <img src="assets/imgs/qrscanlogo.png" width=\'55px\' height=\'55px\' />\n                <h6 class="font-weight-500">Admin</h6>\n              </div>\n            </div>\n            <div class="col" (click)="listing()">\n              <div class="background-white text-center border-radius padding-box box-shadow">\n                <img src="assets/imgs/listing.png" width=\'55px\' height=\'55px\' />\n                <h6 class="font-weight-500">List</h6>\n              </div>\n            </div>\n            <div class="col" (click)="activities()">\n              <div class="background-white text-center border-radius padding-box box-shadow">\n                <img src="assets/imgs/activities.png" width=\'55px\' height=\'55px\' />\n                <h6 class="font-weight-500">Aktifitas</h6>\n              </div>\n            </div>\n          </div>\n          <div class="row" *ngIf="level==\'-1\'">\n            <div class="col" (click)="allOutlets()">\n              <div class="background-white text-center border-radius padding-box box-shadow">\n                <img src="assets/imgs/outlets.png" width=\'55px\' height=\'55px\' />\n                <h6 class="font-weight-500">Toko</h6>\n              </div>\n            </div>\n            <div class="col" (click)="noo()">\n              <div class="background-white text-center border-radius padding-box box-shadow">\n                <img src="assets/imgs/noo.png" width=\'55px\' height=\'55px\' />\n                <h6 class="font-weight-500">N.O.O</h6>\n              </div>\n            </div>\n            <div class="col" (click)="rutesales()">\n              <div class="background-white text-center border-radius padding-box box-shadow">\n                <img src="assets/imgs/route.png" width=\'55px\' height=\'55px\' />\n                <h6 class="font-weight-500">Rute Sales</h6>\n              </div>\n            </div>\n          </div>\n          <div class="row" *ngIf="level==\'-1\'">\n            <div class="col" (click)="luar_rute()">\n              <div class="background-white text-center border-radius padding-box box-shadow">\n                <img src="assets/imgs/no_route.png" width=\'55px\' height=\'55px\' />\n                <h6 class="font-weight-500">Luar Rute</h6>\n              </div>\n            </div>\n            <div class="col" (click)="analytics()">\n              <div class="background-white text-center border-radius padding-box box-shadow">\n                <img src="assets/imgs/analyticss.png" width=\'55px\' height=\'55px\' />\n                <h6 class="font-weight-500">Analisa</h6>\n              </div>\n            </div>\n            <div class="col">\n              <div class="background-white text-center border-radius padding-box box-shadow">\n                <img src="assets/imgs/charts.png" width=\'55px\' height=\'55px\' />\n                <h6 class="font-weight-500">Grafik</h6>\n              </div>\n            </div>\n          </div>\n          <div class="row" *ngIf="level==\'-1\'">\n            <div class="col">\n              <div class="background-white text-center border-radius padding-box box-shadow">\n                <img src="assets/imgs/barang.png" width=\'55px\' height=\'55px\' />\n                <h6 class="font-weight-500">Produk</h6>\n              </div>\n            </div>\n            <div class="col">\n              <div class="background-white text-center border-radius padding-box box-shadow">\n                <img src="assets/imgs/sales.png" width=\'55px\' height=\'55px\' />\n                <h6 class="font-weight-500">Sales</h6>\n              </div>\n            </div>\n            <div class="col">\n              <div class="background-white text-center border-radius padding-box box-shadow">\n                <img src="assets/imgs/bi.png" width=\'55px\' height=\'55px\' />\n                <h6 class="font-weight-500">B.Intel</h6>\n              </div>\n            </div>\n          </div>\n          <div class="row" *ngIf="level==\'-1\' && developer==\'1\'">\n            <div class="col" (click)="Logs()">\n              <div class="background-white text-center border-radius padding-box box-shadow">\n                <img src="assets/imgs/logs.png" width=\'55px\' height=\'55px\' />\n                <h6 class="font-weight-500">Logs</h6>\n              </div>\n            </div>\n            <div class="col" (click)="bypass()">\n              <div class="background-white text-center border-radius padding-box box-shadow">\n                <img src="assets/imgs/bypass.png" width=\'55px\' height=\'55px\' />\n                <h6 class="font-weight-500">ByPass</h6>\n              </div>\n            </div>\n            <div class="col" (click)="traffic()">\n              <div class="background-white text-center border-radius padding-box box-shadow">\n                <img src="assets/imgs/counter.png" width=\'55px\' height=\'55px\' />\n                <h6 class="font-weight-500">Traffic</h6>\n              </div>\n            </div>\n          </div>\n\n        </div>\n      </div>\n    </div>\n    <!-- end menus -->\n    <!-- separator -->\n    <div class="separator"></div>\n    <!-- end separator -->\n    <!-- separator -->\n    <div class="separator-bottom"></div>\n    <!-- end separator -->\n    <!-- </div> -->\n\n\n  </ion-grid>\n</ion-content>`/*ion-inline-end:"D:\ionic\Project\qrcode\src\pages\main\main.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */],
            __WEBPACK_IMPORTED_MODULE_8__config_provider__["a" /* Provider */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_barcode_scanner__["a" /* BarcodeScanner */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_location_accuracy__["a" /* LocationAccuracy */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_android_permissions__["a" /* AndroidPermissions */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], MainPage);
    return MainPage;
}());

//# sourceMappingURL=main.js.map

/***/ }),

/***/ 57:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_main_main__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__config_provider__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var LoginPage = (function () {
    function LoginPage(navCtrl, storage, plt, alertCtrl, postPvdr, navParams) {
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.plt = plt;
        this.alertCtrl = alertCtrl;
        this.postPvdr = postPvdr;
        this.navParams = navParams;
        this.platform = 'M';
        if (this.plt.is('android')) {
            this.platform = 'M';
        }
        else {
            this.platform = 'W';
        }
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
    };
    LoginPage.prototype.login_procces = function () {
        var _this = this;
        var body = {
            username: this.username,
            password: this.password,
            platform: this.platform,
            action: 'cekLogin'
        };
        this.postPvdr.postData(body, 'login/cekuser').subscribe(function (data) {
            if (data.success) {
                _this.storage.set('user_session_storage', data.result);
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__pages_main_main__["a" /* MainPage */]);
            }
            else {
                var alert_1 = _this.alertCtrl.create({
                    title: 'Mohon Maaf',
                    subTitle: 'Anda Tidak Mempunyai Akses.',
                    buttons: ['OK']
                });
                alert_1.present();
            }
        }, function (error) {
            var alert = _this.alertCtrl.create({
                title: 'Mohon Maaf',
                subTitle: 'Sedang tidak terkoneksi dengan server. Silahkan coba kembali beberapa saat',
                buttons: ['OK']
            });
            alert.present();
        });
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"D:\ionic\Project\qrcode\src\pages\login\login.html"*/`<ion-content class="background">\n  <ion-grid id="pop" style="height: 100%;" fixed>\n    <ion-row justify-content-center align-items-center style="height: 100%; flex-direction: column;">\n      <div class="pop">\n        <div class="container">\n          <img src="assets/imgs/gochecks.png" width="150px" height="150px">\n          <ion-list inset>\n            <ion-item>\n              <ion-input type="text" placeholder="Username" [(ngModel)]="username"></ion-input>\n            </ion-item>\n            <ion-item>\n              <ion-input type="password" placeholder="Password" [(ngModel)]="password"></ion-input>\n            </ion-item>\n          </ion-list><br>\n          <button ion-button block round color="oren" (click)="login_procces()">Login</button>\n          <br>\n        </div>\n      </div>\n    </ion-row>\n  </ion-grid>\n</ion-content>`/*ion-inline-end:"D:\ionic\Project\qrcode\src\pages\login\login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_4__config_provider__["a" /* Provider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 67:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PerformaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_provider__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_chart_js__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_chart_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_chart_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_petatoko_petatoko__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_detailbangetlistings_detailbangetlistings__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_noolist_noolist__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_tokoplus_tokoplus__ = __webpack_require__(70);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









/**
 * Generated class for the PerformaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PerformaPage = (function () {
    function PerformaPage(navCtrl, navParams, postPvdr, storage) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.postPvdr = postPvdr;
        this.storage = storage;
        this.from_storage = navParams.get("storage");
        this.kdoutlet = navParams.get("kdoutlet");
        this.kdcabang = navParams.get("kdcabang");
        this.kdsales = navParams.get("kdsales");
        this.user_level = navParams.get("user_level");
        if (this.from_storage == 'No') {
            this.sales_name = navParams.get("sales_name");
        }
        else {
            this.storage.get('user_session_storage').then(function (res) {
                _this.sales_name = res[0].employee_name;
            });
        }
    }
    PerformaPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PerformaPage');
        this.getData();
    };
    PerformaPage.prototype.getData = function () {
        var _this = this;
        this.storage.get('user_session_storage').then(function (res) {
            var body;
            if (_this.from_storage == 'No') {
                body = {
                    kdcabang: _this.kdcabang,
                    user_level: _this.user_level,
                    kdoutlet: _this.kdoutlet,
                    kdsales: _this.kdsales,
                    username: res[0].username
                };
            }
            else {
                body = {
                    kdcabang: res[0].cabang_id,
                    user_level: res[0].user_level,
                    kdoutlet: res[0].kdoutlet,
                    kdsales: res[0].kdsales,
                    username: res[0].username
                };
                //tergantikan
                _this.from_storage = 'Yes';
                _this.kdoutlet = res[0].kdoutlet;
                _this.kdcabang = res[0].cabang_id;
                _this.kdsales = res[0].kdsales;
                _this.user_level = res[0].user_level;
            }
            _this.postPvdr.postData(body, 'outlet/getchart2').subscribe(function (data) {
                if (data.success) {
                    _this.toko_sudah_terpetakan = data.toko_sudah_terpetakan;
                    _this.toko_belum_terpetakan = data.toko_belum_terpetakan;
                    _this.toko_pegangan = data.toko_pegangan;
                    _this.toko_diluar_pegangan_sudah_terpetakan = data.toko_diluar_pegangan_sudah_terpetakan;
                    _this.toko_noo = data.toko_noo;
                    _this.getDoughnutChart1(parseInt(_this.toko_sudah_terpetakan), parseInt(_this.toko_belum_terpetakan));
                    //performa
                    _this.performa = (parseInt(_this.toko_sudah_terpetakan) / parseInt(_this.toko_pegangan) * 100).toFixed(1);
                    _this.seluruh_toko = data.seluruh_toko;
                    _this.seluruh_toko_sudah_terpetakan = data.seluruh_toko_sudah_terpetakan;
                    _this.seluruh_toko_belum_terpetakan = data.seluruh_toko_belum_terpetakan;
                    _this.seluruh_toko_noo = data.seluruh_toko_noo;
                    _this.getDoughnutChart2(parseInt(_this.seluruh_toko_sudah_terpetakan), parseInt(_this.seluruh_toko_belum_terpetakan));
                }
                else {
                    //failed
                    return false;
                }
            }, function (error) {
                //not connect
                return false;
            });
        });
    };
    PerformaPage.prototype.listOutlet = function () {
        var _this = this;
        var kdcabang;
        var kdoutlet;
        this.storage.get('user_session_storage').then(function (res) {
            if (res[0].user_level == '-1' || res[0].user_level == '9') {
                kdcabang = '';
                kdoutlet = '';
            }
            else {
                kdcabang = res[0].cabang_id;
                kdoutlet = res[0].kdoutlet;
            }
            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__pages_detailbangetlistings_detailbangetlistings__["a" /* DetailbangetlistingsPage */], { kdoutlet: kdoutlet, kdcabang: kdcabang, kdsales: _this.kdsales, viewuser: 'No', title: _this.sales_name, close: 'No', bisa_pilih: 'No' });
        });
    };
    PerformaPage.prototype.ngAfterViewInit = function () {
        setTimeout(function () {
            // this.doughnutChart1 = this.getDoughnutChart1();
            // this.doughnutChart2 = this.getDoughnutChart2();
        }, 250);
    };
    PerformaPage.prototype.getChart = function (context, chartType, data, options) {
        return new __WEBPACK_IMPORTED_MODULE_4_chart_js___default.a(context, {
            data: data,
            options: options,
            type: chartType
        });
    };
    PerformaPage.prototype.getDoughnutChart1 = function (sudah, belum) {
        var data = {
            display: true,
            labels: ['Sudah', 'Belum'],
            datasets: [{
                    data: [sudah, belum],
                    backgroundColor: [
                        'rgb(255, 120, 0)',
                        'rgb(37, 39, 43)'
                    ]
                }]
        };
        var options = {
            legend: {
                display: true,
                position: 'bottom'
            },
            title: {
                display: false,
                text: 'Chart Data Terkumpul',
                position: 'bottom'
            }
        };
        return this.getChart(this.doughnutCanvas1.nativeElement, 'doughnut', data, options);
    };
    PerformaPage.prototype.getDoughnutChart2 = function (sudah, belum) {
        var data = {
            display: true,
            labels: ['Sudah', 'Belum'],
            datasets: [{
                    data: [sudah, belum],
                    backgroundColor: [
                        'rgb(255, 120, 0)',
                        'rgb(37, 39, 43)'
                    ]
                }]
        };
        var options = {
            legend: {
                display: true,
                position: 'bottom'
            },
            title: {
                display: false,
                text: 'Chart Data Terkumpul',
                position: 'bottom'
            }
        };
        return this.getChart(this.doughnutCanvas2.nativeElement, 'doughnut', data, options);
    };
    PerformaPage.prototype.peta_sendiri = function (kdcabang, kdoutlet, kdsales) {
        var _this = this;
        this.storage.get('user_session_storage').then(function (res) {
            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__pages_petatoko_petatoko__["a" /* PetatokoPage */], {
                kdcabang: kdcabang,
                kdoutlet: kdoutlet,
                kdsales: kdsales,
                distributor_name: res[0].distributor_name,
                title: _this.sales_name,
                sales_name: _this.sales_name,
                legend_type: '1'
            });
        });
    };
    PerformaPage.prototype.peta_distributor = function (kdcabang, kdoutlet) {
        var _this = this;
        this.storage.get('user_session_storage').then(function (res) {
            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__pages_petatoko_petatoko__["a" /* PetatokoPage */], {
                kdcabang: kdcabang,
                kdoutlet: kdoutlet,
                kdsales: '',
                distributor_name: res[0].distributor_name,
                title: res[0].distributor_name,
                sales_name: 'Semua',
                legend_type: '2'
            });
        });
    };
    PerformaPage.prototype.noo = function () {
        var _this = this;
        this.storage.get('user_session_storage').then(function (res) {
            if (_this.from_storage == 'No') {
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__pages_noolist_noolist__["b" /* NoolistPage */], { from_storage: _this.from_storage, kdcabang: _this.kdcabang, user_level: _this.user_level, kdoutlet: _this.kdoutlet, kdsales: _this.kdsales });
            }
            else {
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__pages_noolist_noolist__["b" /* NoolistPage */], { from_storage: 'Yes', kdcabang: res[0].cabang_id, user_level: res[0].user_level, kdoutlet: res[0].kdoutlet, kdsales: res[0].kdsales });
            }
        });
    };
    PerformaPage.prototype.tokoplus = function () {
        var _this = this;
        this.storage.get('user_session_storage').then(function (res) {
            if (_this.from_storage == 'No') {
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__pages_tokoplus_tokoplus__["a" /* TokoplusPage */], { from_storage: _this.from_storage, kdcabang: _this.kdcabang, user_level: _this.user_level, kdoutlet: _this.kdoutlet, kdsales: _this.kdsales, performa: 'Yes' });
            }
            else {
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__pages_tokoplus_tokoplus__["a" /* TokoplusPage */], { from_storage: 'Yes', kdcabang: res[0].cabang_id, user_level: res[0].user_level, kdoutlet: res[0].kdoutlet, kdsales: res[0].kdsales, performa: 'Yes' });
            }
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('doughnutCanvas1'),
        __metadata("design:type", Object)
    ], PerformaPage.prototype, "doughnutCanvas1", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('doughnutCanvas2'),
        __metadata("design:type", Object)
    ], PerformaPage.prototype, "doughnutCanvas2", void 0);
    PerformaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-performa',template:/*ion-inline-start:"D:\ionic\Project\qrcode\src\pages\performa\performa.html"*/`<!--\n  Generated template for the PerformaPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="oren">\n    <ion-title>Performa</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content no-padding class="bgc">\n  <ion-grid fixed class="wrapper">\n\n\n\n\n\n\n    <!-- <div style="border-bottom:1px solid #d7d8da;">\n      <ion-list>\n        <ion-item>\n          <ion-avatar item-start>\n            <img src="assets/imgs/profilez.png">\n          </ion-avatar>\n          <h2>{{sales_name}}</h2>\n          <p>Performa : {{performa}}%</p>\n          <button ion-button icon-end item-end round color="success-tint" (click)="listOutlet()">\n            &nbsp;&nbsp;List Toko Sales\n            <ion-icon name="clipboard"></ion-icon>&nbsp;&nbsp;\n          </button>\n        </ion-item>\n      </ion-list>\n    </div> -->\n\n    <div style="border-bottom:1px solid #d7d8da;">\n      <ion-card>\n        <ion-item-divider color="light-shade">Pencapaian Sales\n        </ion-item-divider>\n        <ion-card-content><br>\n          <table style="width: 100%;">\n            <tr>\n              <td style="width: 30%">\n                Sales\n              </td>\n              <td style="width: 70%">\n                : {{sales_name}}\n              </td>\n            </tr>\n            <tr>\n              <td style="width: 30%">\n                Performa\n              </td>\n              <td style="width: 70%">\n                : {{performa}}%\n              </td>\n            </tr>\n          </table>\n        </ion-card-content>\n        <ion-item-divider color="light-shade" style="text-align: center;">\n\n          <button ion-button icon-end round color="success-tint" (click)="listOutlet()">\n            &nbsp;&nbsp;Toko Sales\n            <ion-icon name="clipboard"></ion-icon>&nbsp;&nbsp;\n          </button>\n          <button ion-button icon-end round color="danger" (click)="noo()">\n            &nbsp;&nbsp;N.O.O\n            <ion-icon name="clipboard"></ion-icon>&nbsp;&nbsp;\n          </button>\n          <button ion-button icon-end round color="medium-shade" (click)="tokoplus()">\n            &nbsp;&nbsp;Toko Plus\n            <ion-icon name="clipboard"></ion-icon>&nbsp;&nbsp;\n          </button>\n\n        </ion-item-divider>\n      </ion-card>\n    </div>\n\n    <div style="border-bottom:1px solid #d7d8da;">\n      <ion-card>\n        <ion-item-divider color="light-shade">Grafik Pencapain Toko Sendiri\n          <button ion-button icon-end item-end round color="primary-tint"\n            (click)="peta_sendiri(kdcabang,kdoutlet,kdsales)">\n            &nbsp;&nbsp;Lihat Peta\n            <ion-icon name="pin"></ion-icon>&nbsp;&nbsp;\n          </button>\n        </ion-item-divider>\n        <br>\n        <ion-card-content>\n          <canvas #doughnutCanvas1></canvas>\n        </ion-card-content>\n        <ion-card-content>\n          <table style="border: 0px solid #222;width: 100%;">\n            <tr>\n              <td colspan="2">\n                <b>Keterangan Grafik</b>\n              </td>\n              <td colspan="2">\n                <b>Info Tambahan</b>\n              </td>\n            </tr>\n            <tr>\n              <td style="width: 25%;">\n                Semua Toko\n              </td>\n              <td style="width: 25%;">\n                : {{toko_pegangan}} Toko\n              </td>\n              <td style="width: 25%;">\n                N.O.O\n              </td>\n              <td style="width: 25%;">\n                : {{toko_noo}} Toko\n              </td>\n            </tr>\n            <tr>\n              <td style="width: 25%;">\n                Belum\n              </td>\n              <td style="width: 25%;">\n                : {{toko_belum_terpetakan}} Toko\n              </td>\n              <td style="width: 25%;">\n                Toko Plus*\n              </td>\n              <td style="width: 25%;">\n                : {{toko_diluar_pegangan_sudah_terpetakan}} Toko\n              </td>\n            </tr>\n            <tr>\n              <td style="width: 25%;">\n                Sudah\n              </td>\n              <td style="width: 25%;">\n                : {{toko_sudah_terpetakan}} Toko\n              </td>\n              <td style="width: 50%;" colspan="2">\n                <i>*) Toko diluar rute sales</i>\n              </td>\n            </tr>\n          </table>\n        </ion-card-content>\n      </ion-card>\n    </div>\n\n    <div style="border-bottom:1px solid #d7d8da;">\n      <ion-card>\n        <ion-item-divider color="light-shade">Grafik Pencapain Toko Keseluruhan\n          <button ion-button icon-end item-end round color="primary-tint" (click)="peta_distributor(kdcabang,kdoutlet)">\n            &nbsp;&nbsp;Lihat Peta\n            <ion-icon name="pin"></ion-icon>&nbsp;&nbsp;\n          </button>\n        </ion-item-divider>\n        <br>\n        <ion-card-content>\n          <canvas #doughnutCanvas2></canvas>\n        </ion-card-content>\n        <ion-card-content>\n          <table style="border: 0px solid #222;width: 100%;">\n            <tr>\n              <td colspan="2">\n                <b>Keterangan Grafik</b>\n              </td>\n              <td colspan="2">\n                <b>Info Tambahan</b>\n              </td>\n            </tr>\n            <tr>\n              <td style="width: 25%;">\n                Semua Toko\n              </td>\n              <td style="width: 25%;">\n                : {{seluruh_toko}} Toko\n              </td>\n              <td style="width: 25%;">\n                N.O.O\n              </td>\n              <td style="width: 25%;">\n                : {{seluruh_toko_noo}} Toko\n              </td>\n            </tr>\n            <tr>\n              <td style="width: 25%;">\n                Belum\n              </td>\n              <td style="width: 25%;">\n                : {{seluruh_toko_belum_terpetakan}} Toko\n              </td>\n              <td style="width: 25%;">\n                &nbsp;\n              </td>\n              <td style="width: 25%;">\n                &nbsp;\n              </td>\n            </tr>\n            <tr>\n              <td style="width: 25%;">\n                Sudah\n              </td>\n              <td style="width: 25%;">\n                : {{seluruh_toko_sudah_terpetakan}} Toko\n              </td>\n              <td style="width: 25%;">\n                &nbsp;\n              </td>\n              <td style="width: 25%;">\n                &nbsp;\n              </td>\n            </tr>\n          </table>\n        </ion-card-content>\n      </ion-card>\n    </div>\n\n\n  </ion-grid>\n</ion-content>`/*ion-inline-end:"D:\ionic\Project\qrcode\src\pages\performa\performa.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__config_provider__["a" /* Provider */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */]])
    ], PerformaPage);
    return PerformaPage;
}());

//# sourceMappingURL=performa.js.map

/***/ }),

/***/ 68:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PetatokoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_provider__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_search_search__ = __webpack_require__(135);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the PetatokoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PetatokoPage = (function () {
    function PetatokoPage(navCtrl, navParams, postPvdr, modalCtrl, loadingCtrl, alertCtrl, storage) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.postPvdr = postPvdr;
        this.modalCtrl = modalCtrl;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.storage = storage;
        this.base_url = 'http://api.vci.co.id/public/outlets/';
        this.markers = [];
        this.view_filter = 'No';
        this.kdoutlet = navParams.get("kdoutlet");
        this.kdcabang = navParams.get("kdcabang");
        this.kdsales = navParams.get("kdsales");
        this.title = navParams.get("title");
        this.distributor_name = navParams.get("distributor_name");
        this.sales_name = navParams.get("sales_name");
        this.legend_type = navParams.get("legend_type");
        this.storage.get('user_session_storage').then(function (res) {
            if (res[0].user_level != '4') {
                _this.view_filter = 'Yes';
            }
        });
    }
    PetatokoPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PetatokoPage');
        // this.initMap();
        this.getMaps();
        this.getData(this.legend_type);
    };
    PetatokoPage.prototype.getMaps = function () {
        var _this = this;
        this.storage.get('user_session_storage').then(function (res) {
            //super user dan nasional
            if (res[0].user_level == '-1' || res[0].user_level == '9') {
                _this.initMap('nasional', 5.69);
                //regional
            }
            else if (res[0].user_level == '6') {
                _this.initMap('regional', 7);
                //admin dan kepala distribusi
            }
            else if (res[0].user_level == '2' || res[0].user_level == '4' || res[0].user_level == '10') {
                _this.initMap('distributor', 11);
            }
        });
    };
    PetatokoPage.prototype.initMap = function (area, zoom) {
        var _this = this;
        var loader = this.loadingCtrl.create({
            content: "Mohon Menunggu..."
        });
        loader.present();
        this.storage.get('user_session_storage').then(function (res) {
            var body = {
                kdcabang: _this.kdcabang,
                kdsales: _this.kdsales,
                kdoutlet: _this.kdoutlet,
                username: res[0].username
            };
            _this.postPvdr.postData(body, 'outlet/getLocationTagSales2').subscribe(function (data) {
                if (data.success) {
                    loader.dismiss();
                    var myLatlng;
                    if (area == "nasional") {
                        myLatlng = { lat: -0.620093, lng: 115.3094217 };
                    }
                    else if (area == "regional") {
                        myLatlng = { lat: parseFloat(data.result[0]['Lat']), lng: parseFloat(data.result[0]['Lng']) };
                    }
                    else if (area == "distributor") {
                        myLatlng = { lat: parseFloat(data.result[0]['Lat']), lng: parseFloat(data.result[0]['Lng']) };
                    }
                    var map = new google.maps.Map(_this.mapElement.nativeElement, {
                        zoom: zoom,
                        center: myLatlng,
                        zoomControl: true,
                        zoomControlOptions: {
                            position: google.maps.ControlPosition.LEFT_TOP
                        },
                        mapTypeControl: false,
                        scaleControl: false,
                        streetViewControl: false,
                        rotateControl: false,
                        fullscreenControl: false
                    });
                    for (var i = 0; i < data.result.length; i++) {
                        // this.listOutlet.push(
                        //   {
                        //     id: data.result[i]['id'],
                        //     KdOutlet: data.result[i]['KdOutlet'],
                        //     OutletName: data.result[i]['OutletName'],
                        //     Address: data.result[i]['Address'],
                        //     UserUpload: data.result[i]['UserUpload'],
                        //     DateUpload: data.result[i]['DateUpload'],
                        //     UserValidate: data.result[i]['UserValidate'],
                        //     DateValidate: data.result[i]['DateValidate'],
                        //     Lat: data.result[i]['Lat'],
                        //     Lng: data.result[i]['Lng'],
                        //     Status: data.result[i]['Status'],
                        //     NOO: data.result[i]['NOO'],
                        //     Images: this.base_url + data.result[i]['Images'],
                        //     tipedata: data.result[i]['TypeData'],
                        //     Note: data.result[i]['Note'],
                        //     NoteValidator: data.result[i]['NoteValidator'],
                        //     toko_plus: data.result[i]['toko_plus']
                        //   });
                        //pilih icon
                        var icon;
                        if (data.result[i]['toko_plus'] == 'Yes' && data.result[i]['NOO'] == '0') {
                            icon = 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-blue.png';
                        }
                        else if (data.result[i]['toko_plus'] == 'No' && data.result[i]['NOO'] == '0') {
                            icon = 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-red.png';
                        }
                        else if (data.result[i]['NOO'] == '1') {
                            icon = 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-green.png';
                        }
                        var Latlng = { lat: parseFloat(data.result[i]['Lat']), lng: parseFloat(data.result[i]['Lng']) };
                        //marker
                        var marker = new google.maps.Marker({
                            position: Latlng,
                            animation: google.maps.Animation.DROP,
                            map: map,
                            icon: icon
                        });
                        _this.markers.push(marker);
                        var contentString = '<h4>' + data.result[i]['OutletName'] + '&nbsp;&nbsp;</h4>' +
                            '<div>' +
                            '<p>' + data.result[i]['Address'] + '&nbsp;&nbsp;' + '<br>' +
                            'Tanggal Upload : ' + data.result[i]['DateUpload'] + '&nbsp;&nbsp;' + '<br>' +
                            'Catatan Sales :' + data.result[i]['Note'] + '&nbsp;&nbsp;' + '<br>' +
                            'Tanggal Valid : ' + data.result[i]['DateValidate'] + '&nbsp;&nbsp;' + '<br>' +
                            'Catatan Valid : ' + data.result[i]['NoteValidator'] + '&nbsp;&nbsp;' + '</p>' +
                            '<div id="pic"><img src="' + _this.base_url + data.result[i]['Images'] + '" width="200" height="250" />' +
                            '</div>';
                        marker.content = contentString;
                        var infoWindow = new google.maps.InfoWindow();
                        // google.maps.event.addListener(marker, 'mouseover', function () {
                        google.maps.event.addListener(marker, 'click', function () {
                            infoWindow.setContent(this.content);
                            infoWindow.open(this.getMap(), this);
                        });
                    }
                }
                else {
                    loader.dismiss();
                    var alert_1 = _this.alertCtrl.create({
                        title: 'Info',
                        subTitle: 'Mohon Maaf Data Tidak Di Temukan',
                        message: '',
                        buttons: ['OK']
                    });
                    alert_1.present();
                    _this.indonesiaMap();
                    return false;
                }
            }, function (error) {
                loader.dismiss();
                var alert = _this.alertCtrl.create({
                    title: 'Mohon Maaf',
                    subTitle: 'Sedang tidak terkoneksi dengan server. Silahkan coba kembali beberapa saat',
                    buttons: ['OK']
                });
                alert.present();
                _this.indonesiaMap();
                return false;
            });
        });
    };
    PetatokoPage.prototype.indonesiaMap = function () {
        var myLatlng = { lat: -0.620093, lng: 115.3094217 };
        var map = new google.maps.Map(this.mapElement.nativeElement, {
            zoom: 5.69,
            center: myLatlng,
            zoomControl: false,
            mapTypeControl: false,
            scaleControl: false,
            streetViewControl: false,
            rotateControl: false,
            fullscreenControl: false
        });
    };
    PetatokoPage.prototype.getData = function (legend_type) {
        var _this = this;
        console.log('legendnya ini : ', legend_type);
        this.storage.get('user_session_storage').then(function (res) {
            var body;
            body = {
                kdcabang: _this.kdcabang,
                kdsales: _this.kdsales,
                kdoutlet: _this.kdoutlet,
                user_level: res[0].user_level,
                username: res[0].username
            };
            _this.postPvdr.postData(body, 'outlet/getchart2').subscribe(function (data) {
                if (data.success) {
                    if (legend_type == '1') {
                        _this.toko_sudah_terpetakan = data.toko_sudah_terpetakan;
                        _this.toko_belum_terpetakan = data.toko_belum_terpetakan;
                        _this.toko_pegangan = data.toko_pegangan;
                        _this.toko_diluar_pegangan_sudah_terpetakan = data.toko_diluar_pegangan_sudah_terpetakan;
                        _this.toko_noo = data.toko_noo;
                    }
                    else if (legend_type == '2') {
                        _this.toko_sudah_terpetakan = data.seluruh_toko_sudah_terpetakan;
                        _this.toko_belum_terpetakan = data.seluruh_toko_belum_terpetakan;
                        _this.toko_pegangan = data.seluruh_toko;
                        _this.toko_diluar_pegangan_sudah_terpetakan = 0;
                        _this.toko_noo = data.seluruh_toko_noo;
                    }
                }
                else {
                    //failed
                    return false;
                }
            }, function (error) {
                //not connect
                return false;
            });
        });
    };
    PetatokoPage.prototype.filter = function () {
        var _this = this;
        var area;
        var zooming;
        this.storage.get('user_session_storage').then(function (res) {
            if (res[0].user_level == '-1' || res[0].user_level == '9') {
                var modal = _this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__pages_search_search__["a" /* SearchPage */], { kdcabang: _this.kdcabang, kdsales: _this.kdsales, kdoutlet: _this.kdoutlet, distributor_name: _this.distributor_name, sales_name: _this.sales_name });
                modal.present();
                modal.onDidDismiss(function (val) {
                    console.log('val : ', val);
                    if (val) {
                        if (val.distributor_name == 'Semua Distributor Nasional' && val.sales_name == 'Semua') {
                            console.log('Ngapa Kemari Mulu Ya 1 : ', val.distributor_name);
                            _this.kdsales = val.kdsales;
                            _this.sales_name = val.sales_name;
                            _this.kdcabang = val.kdcabang;
                            _this.kdoutlet = val.kdoutlet;
                            _this.distributor_name = val.distributor_name;
                            _this.title = val.distributor_name;
                            _this.legend_type = '2';
                            area = "nasional";
                            zooming = 5.69;
                        }
                        else if (val.distributor_name != 'Semua Distributor Nasional' && val.sales_name == 'Semua') {
                            console.log('Ngapa Kemari Mulu Ya 2 : ', val.distributor_name);
                            _this.kdsales = val.kdsales;
                            _this.sales_name = val.sales_name;
                            _this.kdcabang = val.kdcabang;
                            _this.kdoutlet = val.kdoutlet;
                            _this.distributor_name = val.distributor_name;
                            _this.title = val.distributor_name;
                            _this.legend_type = '2';
                            area = "distributor";
                            zooming = 11;
                        }
                        else {
                            console.log('Ngapa Kemari Mulu Ya 3 : ', val.distributor_name);
                            _this.kdsales = val.kdsales;
                            _this.sales_name = val.sales_name;
                            _this.kdcabang = val.kdcabang;
                            _this.kdoutlet = val.kdoutlet;
                            _this.distributor_name = val.distributor_name;
                            _this.title = val.distributor_name;
                            _this.legend_type = '1';
                            area = "distributor";
                            zooming = 11;
                        }
                        _this.getData(_this.legend_type);
                        _this.initMap(area, zooming);
                        _this.clearMarker();
                    }
                    else {
                        //coding if else or empty
                    }
                });
            }
            if (res[0].user_level == '6') {
                var modal = _this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__pages_search_search__["a" /* SearchPage */], { kdcabang: _this.kdcabang, kdsales: _this.kdsales, kdoutlet: _this.kdoutlet, distributor_name: _this.distributor_name, sales_name: _this.sales_name });
                modal.present();
                modal.onDidDismiss(function (val) {
                    if (val) {
                        if (val.distributor_name == 'Semua Distributor Regional' && val.sales_name == 'Semua') {
                            _this.kdsales = val.kdsales;
                            _this.sales_name = val.sales_name;
                            _this.kdcabang = val.kdcabang;
                            _this.kdoutlet = val.kdoutlet;
                            _this.distributor_name = val.distributor_name;
                            _this.title = val.distributor_name;
                            _this.legend_type = '2';
                            area = "regional";
                            zooming = 7;
                        }
                        else if (val.distributor_name != 'Semua Distributor Regional' && val.sales_name == 'Semua') {
                            _this.kdsales = val.kdsales;
                            _this.sales_name = val.sales_name;
                            _this.kdcabang = val.kdcabang;
                            _this.kdoutlet = val.kdoutlet;
                            _this.distributor_name = val.distributor_name;
                            _this.title = val.distributor_name;
                            _this.legend_type = '2';
                            area = "distributor";
                            zooming = 11;
                        }
                        else {
                            _this.kdsales = val.kdsales;
                            _this.sales_name = val.sales_name;
                            _this.kdcabang = val.kdcabang;
                            _this.kdoutlet = val.kdoutlet;
                            _this.distributor_name = val.distributor_name;
                            _this.title = val.distributor_name;
                            _this.legend_type = '1';
                            area = "distributor";
                            zooming = 11;
                        }
                        _this.getData(_this.legend_type);
                        _this.initMap(area, zooming);
                        _this.clearMarker();
                    }
                    else {
                        //coding if else or empty
                    }
                });
            }
            if (res[0].user_level == '2' || res[0].user_level == '10') {
                var modal = _this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__pages_search_search__["a" /* SearchPage */], { kdcabang: _this.kdcabang, kdsales: _this.kdsales, kdoutlet: _this.kdoutlet, distributor_name: _this.distributor_name, sales_name: _this.sales_name });
                modal.present();
                modal.onDidDismiss(function (val) {
                    if (val) {
                        if (val.sales_name != 'Semua') {
                            _this.kdsales = val.kdsales;
                            _this.sales_name = val.sales_name;
                            _this.kdcabang = val.kdcabang;
                            _this.kdoutlet = val.kdoutlet;
                            _this.distributor_name = val.distributor_name;
                            _this.title = val.distributor_name;
                            _this.legend_type = '1';
                            area = "distributor";
                            zooming = 11;
                        }
                        else {
                            _this.kdsales = val.kdsales;
                            _this.sales_name = val.sales_name;
                            _this.kdcabang = val.kdcabang;
                            _this.kdoutlet = val.kdoutlet;
                            _this.distributor_name = val.distributor_name;
                            _this.title = val.distributor_name;
                            _this.legend_type = '2';
                            area = "distributor";
                            zooming = 11;
                        }
                        _this.getData(_this.legend_type);
                        _this.initMap(area, zooming);
                        _this.clearMarker();
                    }
                    else {
                        //coding if else or empty
                    }
                });
            }
        });
    };
    PetatokoPage.prototype.clearMarker = function () {
        this.setMapOnAll(null);
    };
    PetatokoPage.prototype.setMapOnAll = function (map) {
        for (var i = 0; i < this.markers.length; i++) {
            this.markers[i].setMap(map);
        }
    };
    PetatokoPage.prototype.tampilkan = function () {
        document.getElementById('legend').style.display = '';
        document.getElementById('fab').style.display = 'none';
    };
    PetatokoPage.prototype.sembunyikan = function () {
        document.getElementById('legend').style.display = 'none';
        document.getElementById('fab').style.display = '';
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('map'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], PetatokoPage.prototype, "mapElement", void 0);
    PetatokoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-petatoko',template:/*ion-inline-start:"D:\ionic\Project\qrcode\src\pages\petatoko\petatoko.html"*/`<ion-header>\n  <ion-navbar color="oren">\n    <ion-title>Peta Toko {{title}}</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content no-padding class="bgc">\n  <div #map id="map" style="width: 100%;height: 100%;"></div>\n\n  <div ion-fixed id="legend" style="\n    position: absolute;\n    right:3px;\n    bottom: 25px;\n    width: auto;\n    height: auto;">\n    <div id="more_options">\n      <table style="border:0px solid black;">\n        <tr>\n          <td>\n            <ion-card>\n\n              <ion-item-group>\n                <ion-list no-border>\n                  <ion-item-divider color="light" *ngIf="legend_type==\'1\'">\n                    <table style="width: 100%;">\n                      <tr>\n                        <td>\n                          <table style="width: 33%;">\n                            <tr>\n                              <td style="width: 100%;">\n                                <div\n                                  style="color:#f4f5f8;background-color: #4697d0;border-radius: 50%; width: 10px;height: 10px;">\n                                  .\n                                </div>\n                              </td>\n                              <td style="width: 100%;">&nbsp;&nbsp;Toko Plus</td>\n                            </tr>\n                          </table>\n                        </td>\n                        <td>\n                          <table style="width: 33%;">\n                            <tr>\n                              <td style="width: 100%;">\n                                <div\n                                  style="color:#f4f5f8;background-color: #d0465d;border-radius: 50%; width: 10px;height: 10px;">\n                                  .\n                                </div>\n                              </td>\n                              <td style="width: 100%;">&nbsp;&nbsp;Toko Resmi</td>\n                            </tr>\n                          </table>\n                        </td>\n                        <td>\n                          <table style="width: 33%;">\n                            <tr>\n                              <td style="width: 100%;">\n                                <div\n                                  style="color:#f4f5f8;background-color: #3abc30;border-radius: 50%; width: 10px;height: 10px;">\n                                  .\n                                </div>\n                              </td>\n                              <td style="width: 100%;">&nbsp;&nbsp;Toko N.O.O</td>\n                            </tr>\n                          </table>\n                        </td>\n                      </tr>\n                    </table>\n                  </ion-item-divider>\n\n                  <ion-item-divider color="light" *ngIf="legend_type==\'2\'">\n                    <table style="width: 100%;">\n                      <tr>\n                        <td>\n                          <table style="width: 50%;">\n                            <tr>\n                              <td style="width: 100%;">\n                                <div\n                                  style="color:#f4f5f8;background-color: #d0465d;border-radius: 50%; width: 10px;height: 10px;">\n                                  .\n                                </div>\n                              </td>\n                              <td style="width: 100%;">&nbsp;&nbsp;Toko Resmi</td>\n                            </tr>\n                          </table>\n                        </td>\n                        <td>\n                          <table style="width: 50%;">\n                            <tr>\n                              <td style="width: 100%;">\n                                <div\n                                  style="color:#f4f5f8;background-color: #3abc30;border-radius: 50%; width: 10px;height: 10px;">\n                                  .\n                                </div>\n                              </td>\n                              <td style="width: 100%;">&nbsp;&nbsp;Toko N.O.O</td>\n                            </tr>\n                          </table>\n                        </td>\n                      </tr>\n                    </table>\n                  </ion-item-divider>\n                </ion-list>\n              </ion-item-group>\n\n              <ion-item>\n                <h2>{{distributor_name}}</h2>\n                <table style="width: 100%;">\n                  <tr>\n                    <td style="width: 15%;">\n                      <h3>Sales</h3>\n                    </td>\n                    <td style="width: 85%;">\n                      <p>: {{sales_name}}</p>\n                    </td>\n                  </tr>\n                </table>\n                <!-- <h3>Sales : {{title}}</h3>\n                <p>Toko : Semua</p> -->\n              </ion-item>\n\n              <ion-card-content>\n                <p style="color:#ffffff">\n                  <!-- Wait a minute. Wait a minute, Doc. <br> -->\n                  Uhhh... Are you telling me that you built a time machine... <br>\n                  <!-- out of a DeLorean?! Whoa. This is heavy. -->\n                </p>\n                <table style="border: 0px solid #222;width: 100%;">\n                  <tr>\n                    <td colspan="2">\n                      <b>Keterangan Peta</b>\n                    </td>\n                    <td colspan="2">\n                      <b>Info Tambahan</b>\n                    </td>\n                  </tr>\n                  <tr>\n                    <td style="width: 25%;">\n                      Semua Toko\n                    </td>\n                    <td style="width: 25%;">\n                      : {{toko_pegangan}} Toko\n                    </td>\n                    <td style="width: 25%;">\n                      N.O.O\n                    </td>\n                    <td style="width: 25%;">\n                      : {{toko_noo}} Toko\n                    </td>\n                  </tr>\n                  <tr>\n                    <td style="width: 25%;">\n                      Belum\n                    </td>\n                    <td style="width: 25%;">\n                      : {{toko_belum_terpetakan}} Toko\n                    </td>\n                    <td style="width: 25%;" *ngIf="legend_type==\'1\'">\n                      Toko Plus*\n                    </td>\n                    <td style="width: 25%;" *ngIf="legend_type==\'1\'">\n                      : {{toko_diluar_pegangan_sudah_terpetakan}} Toko\n                    </td>\n                  </tr>\n                  <tr>\n                    <td style="width: 25%;">\n                      Sudah\n                    </td>\n                    <td style="width: 25%;">\n                      : {{toko_sudah_terpetakan}} Toko\n                    </td>\n                    <td style="width: 50%;" colspan="2" *ngIf="legend_type==\'1\'">\n                      <i>*) Toko diluar rute sales</i>\n                    </td>\n                  </tr>\n                </table>\n              </ion-card-content>\n\n              <!-- <ion-row *ngIf="view_filter==\'Yes\'">\n                <ion-col>\n                  <button ion-button color="primary" outline block (click)="filter()">FILTER</button>\n                </ion-col>\n              </ion-row> -->\n\n              <ion-grid *ngIf="view_filter==\'Yes\'">\n                <ion-row>\n                  <ion-col col-8>\n                    <div><button ion-button color="primary" outline block (click)="filter()">FILTER</button></div>\n                  </ion-col>\n                  <ion-col col-4>\n                    <div><button ion-button color="danger" outline block (click)="sembunyikan()">SEMBUNYI</button></div>\n                  </ion-col>\n                </ion-row>\n              </ion-grid>\n\n            </ion-card>\n          </td>\n        </tr>\n      </table>\n    </div>\n  </div>\n\n  <!-- <ion-grid fixed>\n\n\n    <div class="wrapper">\n      <div id="peta">\n        <ion-item-group>\n          <ion-list no-border>\n            <ion-item-divider color="light" *ngIf="legend_type==\'1\'">\n              <table style="width: 100%;">\n                <tr>\n                  <td>\n                    <table style="width: 33%;">\n                      <tr>\n                        <td style="width: 100%;">\n                          <div\n                            style="color:#f4f5f8;background-color: #4697d0;border-radius: 50%; width: 10px;height: 10px;">\n                            .\n                          </div>\n                        </td>\n                        <td style="width: 100%;">&nbsp;&nbsp;Toko Plus</td>\n                      </tr>\n                    </table>\n                  </td>\n                  <td>\n                    <table style="width: 33%;">\n                      <tr>\n                        <td style="width: 100%;">\n                          <div\n                            style="color:#f4f5f8;background-color: #d0465d;border-radius: 50%; width: 10px;height: 10px;">\n                            .\n                          </div>\n                        </td>\n                        <td style="width: 100%;">&nbsp;&nbsp;Toko Resmi</td>\n                      </tr>\n                    </table>\n                  </td>\n                  <td>\n                    <table style="width: 33%;">\n                      <tr>\n                        <td style="width: 100%;">\n                          <div\n                            style="color:#f4f5f8;background-color: #3abc30;border-radius: 50%; width: 10px;height: 10px;">\n                            .\n                          </div>\n                        </td>\n                        <td style="width: 100%;">&nbsp;&nbsp;Toko N.O.O</td>\n                      </tr>\n                    </table>\n                  </td>\n                </tr>\n              </table>\n            </ion-item-divider>\n\n            <ion-item-divider color="light" *ngIf="legend_type==\'2\'">\n              <table style="width: 100%;">\n                <tr>\n                  <td>\n                    <table style="width: 50%;">\n                      <tr>\n                        <td style="width: 100%;">\n                          <div\n                            style="color:#f4f5f8;background-color: #d0465d;border-radius: 50%; width: 10px;height: 10px;">\n                            .\n                          </div>\n                        </td>\n                        <td style="width: 100%;">&nbsp;&nbsp;Toko Resmi</td>\n                      </tr>\n                    </table>\n                  </td>\n                  <td>\n                    <table style="width: 50%;">\n                      <tr>\n                        <td style="width: 100%;">\n                          <div\n                            style="color:#f4f5f8;background-color: #3abc30;border-radius: 50%; width: 10px;height: 10px;">\n                            .\n                          </div>\n                        </td>\n                        <td style="width: 100%;">&nbsp;&nbsp;Toko N.O.O</td>\n                      </tr>\n                    </table>\n                  </td>\n                </tr>\n              </table>\n            </ion-item-divider>\n\n            <ion-item>\n              <ion-icon name=\'search\' item-end *ngIf="ubah_dist==\'Yes\'" >\n              </ion-icon>\n              Distributor : {{distributor_name}}\n            </ion-item>\n            <ion-item>\n              <ion-icon name=\'search\' item-end *ngIf="ubah_sales==\'Yes\'" >\n              </ion-icon>\n              Sales : {{title}}\n            </ion-item>\n            <ion-item>\n              <ion-icon name=\'search\' item-end *ngIf="ubah_toko==\'Yes\'" >\n              </ion-icon>\n              Toko : {{toko}}\n            </ion-item>\n          </ion-list>\n        </ion-item-group>\n      </div>\n    </div>\n  </ion-grid> -->\n\n  <ion-fab right bottom id="fab" style="display: none;">\n    <button ion-fab color="danger" (click)="tampilkan()">\n      <ion-icon name="clipboard"></ion-icon>\n    </button>\n  </ion-fab>\n</ion-content>`/*ion-inline-end:"D:\ionic\Project\qrcode\src\pages\petatoko\petatoko.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__config_provider__["a" /* Provider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */]])
    ], PetatokoPage);
    return PetatokoPage;
}());

//# sourceMappingURL=petatoko.js.map

/***/ }),

/***/ 69:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailhistoryPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_provider__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the DetailhistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var DetailhistoryPage = (function () {
    function DetailhistoryPage(navCtrl, postPvdr, storage, loadingCtrl, alertCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.postPvdr = postPvdr;
        this.storage = storage;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.navParams = navParams;
        // @ViewChild('map') mapElement: ElementRef;
        this.base_url = 'http://api.vci.co.id/public/outlets/';
        this.id = navParams.get("id");
    }
    DetailhistoryPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DetailhistoryPage');
        this.getData(this.id);
    };
    DetailhistoryPage.prototype.getData = function (id) {
        var _this = this;
        var loader = this.loadingCtrl.create({
            content: "Mohon Menunggu..."
        });
        loader.present();
        var body = {
            id: id
        };
        this.postPvdr.postData(body, 'outlet/getdetailhistory').subscribe(function (data) {
            if (data.success) {
                loader.dismiss();
                var tipedata;
                if (data.result[0]['TypeData'] == 'S') {
                    tipedata = "Server";
                }
                else {
                    tipedata = "WhatsApp";
                }
                _this.KdOutlet = data.result[0]['KdOutlet'];
                _this.OutletName = data.result[0]['OutletName'];
                _this.Address = data.result[0]['Address'];
                _this.UserUpload = data.result[0]['UserUpload'];
                _this.DateUpload = data.result[0]['DateUpload'];
                _this.UserValidate = data.result[0]['UserValidate'];
                _this.DateValidate = data.result[0]['DateValidate'];
                _this.Lat = data.result[0]['Lat'];
                _this.Lng = data.result[0]['Lng'];
                _this.Status = data.result[0]['Status'];
                _this.Images = _this.base_url + data.result[0]['Images'];
                _this.tipedata = tipedata;
                _this.Note = data.result[0]['Note'];
                _this.NoteValidator = data.result[0]['NoteValidator'];
                // var myLatlng = { lat: parseFloat(data.result[0]['Lat']), lng: parseFloat(data.result[0]['Lng']) };
                // var map = new google.maps.Map(this.mapElement.nativeElement, {
                //   zoom: 13,
                //   center: myLatlng,
                //   zoomControl: false,
                //   mapTypeControl: false,
                //   scaleControl: false,
                //   streetViewControl: false,
                //   rotateControl: false,
                //   fullscreenControl: false
                // });
                // var icon = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';
                // var marker = new google.maps.Marker({
                //   position: myLatlng,
                //   animation: google.maps.Animation.DROP,
                //   map: map,
                //   icon: icon
                // });
            }
            else {
                loader.dismiss();
                var alert_1 = _this.alertCtrl.create({
                    title: 'Info',
                    subTitle: 'Mohon Maaf Data Tidak Di Temukan',
                    message: '',
                    buttons: ['OK']
                });
                alert_1.present();
                return false;
            }
        }, function (error) {
            loader.dismiss();
            var alert = _this.alertCtrl.create({
                title: 'Mohon Maaf',
                subTitle: 'Sedang tidak terkoneksi dengan server. Silahkan coba kembali beberapa saat',
                buttons: ['OK']
            });
            alert.present();
            return false;
        });
    };
    DetailhistoryPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-detailhistory',template:/*ion-inline-start:"D:\ionic\Project\qrcode\src\pages\detailhistory\detailhistory.html"*/`<!--\n  Generated template for the DetailhistoryPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="oren">\n    <ion-title>Detail Histori</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content no-padding class="bgc">\n  <!-- <div #map id="map"\n    style="box-shadow: 0px 2px 5px 5px #888888;margin: auto; width: 470px ;height:25%;border-bottom: 3px solid #ff7800;">\n  </div> -->\n  <ion-grid fixed class="wrapper">\n    <ion-card>\n\n      <ion-item>\n        <ion-avatar item-start>\n          <img src="assets/imgs/setting.png">\n        </ion-avatar>\n        <h2>{{OutletName}}</h2>\n        <p>{{Address}}</p>\n      </ion-item>\n\n      <img src="{{Images}}">\n\n      <ion-card-content>\n\n        <table style="width: 100%;">\n          <tr>\n            <td style="width: 30%;">\n              Uploader\n            </td>\n            <td style="width: 70%;">\n              : {{UserUpload}}\n            </td>\n          </tr>\n          <tr>\n            <td style="width: 30%;">\n              Date Upload\n            </td>\n            <td style="width: 70%;">\n              : {{DateUpload}}\n            </td>\n          </tr>\n          <tr *ngIf="Status!=\'0\'">\n            <td style="width: 30%;">\n              Validator\n            </td>\n            <td style="width: 70%;">\n              : {{UserValidate}}\n            </td>\n          </tr>\n          <tr *ngIf="Status!=\'0\'">\n            <td style="width: 30%;">\n              Date Validate\n            </td>\n            <td style="width: 70%;">\n              : {{DateValidate}}\n            </td>\n          </tr>\n          <tr *ngIf="Note!=\'\'">\n            <td style="width: 30%;">\n              Note Uploader\n            </td>\n            <td style="width: 70%;">\n              : {{Note}}\n            </td>\n          </tr>\n          <tr>\n            <td style="width: 30%;">\n              Source Data\n            </td>\n            <td style="width: 70%;">\n              : {{tipedata}}\n            </td>\n          </tr>\n          <tr *ngIf="NoteValidator!=\'\'">\n            <td style="width: 30%;">\n              Note Validator\n            </td>\n            <td style="width: 70%;">\n              : {{NoteValidator}}\n            </td>\n          </tr>\n        </table>\n\n        <!-- <h3>Uploader : {{UserUpload}}</h3>\n        <h3>Date Upload : {{DateUpload}}</h3>\n        <h3 *ngIf="Status!=\'0\'">Validator : {{UserValidate}}</h3>\n        <h3 *ngIf="Status!=\'0\'">Date Validate : {{DateValidate}}</h3>\n        <p *ngIf="Note!=\'\'">Note Uploader: {{Note}}</p>\n        <p>Source Data: {{tipedata}}</p>\n        <p *ngIf="NoteValidator!=\'\'">Note Validator: {{NoteValidator}}</p> -->\n      </ion-card-content>\n    </ion-card>\n  </ion-grid>\n</ion-content>`/*ion-inline-end:"D:\ionic\Project\qrcode\src\pages\detailhistory\detailhistory.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__config_provider__["a" /* Provider */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], DetailhistoryPage);
    return DetailhistoryPage;
}());

//# sourceMappingURL=detailhistory.js.map

/***/ }),

/***/ 70:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TokoplusPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_provider__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the TokoplusPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TokoplusPage = (function () {
    function TokoplusPage(navCtrl, viewCtrl, postPvdr, alertCtrl, loadingCtrl, storage, navParams) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.postPvdr = postPvdr;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.storage = storage;
        this.navParams = navParams;
        this.from_storage = navParams.get("from_storage");
        this.kdoutlet = navParams.get("kdoutlet");
        this.kdcabang = navParams.get("kdcabang");
        this.kdsales = navParams.get("kdsales");
        this.user_level = navParams.get("user_level");
        this.performa = navParams.get("performa");
        this.storage.get('user_session_storage').then(function (res) {
            if ((res[0].user_level == '-1' && res[0].developer == '1') || res[0].user_level == '2') {
                _this.hak_swtiching = 'Yes';
            }
            else {
                _this.hak_swtiching = 'No';
            }
        });
    }
    TokoplusPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad NoolistPage');
        this.listTokoplus = new Array();
        this.getData('', 0, 10, 'No');
    };
    TokoplusPage.prototype.form_search = function () {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'Cari',
            message: "Masukan kata untuk mencari Toko Plus",
            inputs: [
                {
                    name: 'src',
                    placeholder: 'Apa yang anda cari...'
                },
            ],
            buttons: [
                {
                    text: 'Batal',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Cari',
                    handler: function (data) {
                        console.log('Saved clicked');
                        _this.getlistTokoplus(data.src);
                    }
                }
            ]
        });
        prompt.present();
    };
    TokoplusPage.prototype.getlistTokoplus = function (src) {
        if (src) {
            src = src;
        }
        else {
            src = '';
        }
        this.listTokoplus = new Array();
        this.getData(src, 0, 10, 'No');
    };
    TokoplusPage.prototype.getData = function (src, first, offset, infinite) {
        var _this = this;
        this.first = first;
        this.src = src;
        var loader = this.loadingCtrl.create({
            content: "Mohon Menunggu..."
        });
        if (infinite == 'No') {
            loader.present();
        }
        this.storage.get('user_session_storage').then(function (res) {
            var body;
            if (_this.from_storage == 'No') {
                body = {
                    first: first,
                    offset: offset,
                    keyword: src,
                    performa: _this.performa,
                    kdcabang: _this.kdcabang,
                    user_level: _this.user_level,
                    kdoutlet: _this.kdoutlet,
                    kdsales: _this.kdsales,
                    username: res[0].username
                };
            }
            else {
                body = {
                    first: first,
                    offset: offset,
                    keyword: src,
                    performa: _this.performa,
                    kdcabang: res[0].cabang_id,
                    user_level: res[0].user_level,
                    kdoutlet: res[0].kdoutlet,
                    kdsales: res[0].kdsales,
                    username: res[0].username
                };
            }
            _this.postPvdr.postData(body, 'outlet/getTokoplus').subscribe(function (data) {
                if (data.success) {
                    loader.dismiss();
                    for (var i = 0; i < data.result.length; i++) {
                        var tipedata;
                        if (data.result[i]['TypeData'] == 'S') {
                            tipedata = "Server";
                        }
                        else {
                            tipedata = "WhatsApp";
                        }
                        _this.listTokoplus.push({
                            id: data.result[i]['id'],
                            KdOutlet: data.result[i]['KdOutlet'],
                            KdCabang: data.result[i]['KdCabang'],
                            KdDistributor: data.result[i]['KdDistributor'],
                            OutletName: data.result[i]['OutletName'],
                            Address: data.result[i]['Address'],
                            UserUpload: data.result[i]['UserUpload'],
                            DateUpload: data.result[i]['DateUpload'],
                            UserValidate: data.result[i]['UserValidate'],
                            DateValidate: data.result[i]['DateValidate'],
                            Lat: data.result[i]['Lat'],
                            Lng: data.result[i]['Lng'],
                            Status: data.result[i]['Status'],
                            Images: data.result[i]['Images'],
                            tipedata: tipedata,
                            Note: data.result[i]['Note'],
                            NoteValidator: data.result[i]['NoteValidator'],
                            sales_asli: data.result[i]['sales_asli'],
                            kdsales_asli: data.result[i]['kdsales_asli'],
                            KdSales: data.result[i]['KdSales']
                        });
                    }
                }
                else {
                    loader.dismiss();
                    if (infinite == 'No') {
                        var alert_1 = _this.alertCtrl.create({
                            title: 'Info',
                            subTitle: 'Mohon Maaf Data Tidak Di Temukan',
                            message: '',
                            buttons: ['OK']
                        });
                        alert_1.present();
                    }
                    return false;
                }
            }, function (error) {
                loader.dismiss();
                var alert = _this.alertCtrl.create({
                    title: 'Mohon Maaf',
                    subTitle: 'Sedang tidak terkoneksi dengan server. Silahkan coba kembali beberapa saat',
                    buttons: ['OK']
                });
                alert.present();
                return false;
            });
        });
    };
    TokoplusPage.prototype.doRefresh = function (refresher) {
        this.listTokoplus = new Array();
        this.getData('', 0, 10, 'No');
        setTimeout(function () {
            refresher.complete();
        }, 1000);
    };
    TokoplusPage.prototype.doInfinite = function (infiniteScroll, src, first, filter) {
        var _this = this;
        first = first + 10;
        console.log('Begin async operation');
        setTimeout(function () {
            _this.getData(src, first, 10, 'Yes');
            infiniteScroll.complete();
        }, 1000);
    };
    TokoplusPage.prototype.switching = function (KdOutlet, KdCabang, KdDistributor, KdSales, kdsales_asli, UserUpload, sales_asli) {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: 'Info',
            message: 'Apakah anda yakin akan melakukan swtiching toko ini dari ' + sales_asli + ' Ke ' + UserUpload + '?',
            buttons: [
                {
                    text: 'Batal',
                    handler: function () {
                        console.log('Disagree clicked');
                    }
                },
                {
                    text: 'Ya',
                    handler: function () {
                        _this.switch(KdOutlet, KdSales, kdsales_asli, KdCabang, KdDistributor);
                    }
                }
            ]
        });
        confirm.present();
    };
    TokoplusPage.prototype.switch = function (kdoutlet, kdsales, kodesales, kdcabang, kddistributor) {
        var _this = this;
        var loader = this.loadingCtrl.create({
            content: "Proses Swtiching..."
        });
        loader.present();
        this.storage.get('user_session_storage').then(function (res) {
            var body = {
                kdcabang: kdcabang,
                kdsales: kdsales,
                kodesales: kodesales,
                kdoutlet: kdoutlet,
                kddistributor: kddistributor,
                username: res[0].username
            };
            _this.postPvdr.postData(body, 'outlet/swtichingRuteSales').subscribe(function (data) {
                if (data.success) {
                    loader.dismiss();
                    var confirm_1 = _this.alertCtrl.create({
                        title: 'Info',
                        message: 'Swtiching Berhasil',
                        buttons: [
                            {
                                text: 'Oke',
                                handler: function () {
                                    _this.ionViewDidLoad();
                                }
                            }
                        ]
                    });
                    confirm_1.present();
                }
                else {
                    loader.dismiss();
                    var alert_2 = _this.alertCtrl.create({
                        title: 'Info',
                        subTitle: 'Swtiching Gagal',
                        message: '',
                        buttons: ['OK']
                    });
                    alert_2.present();
                    return false;
                }
            }, function (error) {
                loader.dismiss();
                var alert = _this.alertCtrl.create({
                    title: 'Mohon Maaf',
                    message: 'Sedang tidak terkoneksi dengan server. Silahkan coba kembali beberapa saat',
                    buttons: ['OK']
                });
                alert.present();
                return false;
            });
        });
    };
    TokoplusPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-tokoplus',template:/*ion-inline-start:"D:\ionic\Project\qrcode\src\pages\tokoplus\tokoplus.html"*/`<!--\n  Generated template for the NoolistPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar color="oren">\n    <ion-title>Luar Rute List</ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="form_search()">\n        <ion-icon name="search"></ion-icon>&nbsp;&nbsp;\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content no-padding class="bgc">\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n    <ion-refresher-content pullingIcon="arrow-dropdown" pullingText="Pull to refresh" refreshingSpinner="crescent"\n      refreshingText="">\n    </ion-refresher-content>\n  </ion-refresher>\n  <ion-grid fixed class="wrapper">\n\n    <ion-list>\n      <ion-item *ngFor="let val of listTokoplus">\n        <ion-avatar item-start>\n          <img src="assets/imgs/setting.png">\n        </ion-avatar>\n        <h2>{{val.KdOutlet}} - {{val.OutletName}} <ion-icon *ngIf="val.Status==\'1\'" name="checkmark-circle"\n            color="secondary"></ion-icon>\n          <ion-icon *ngIf="val.Status==\'2\'" name="close-circle" color="danger"></ion-icon>\n        </h2>\n        <p>{{val.Address}}</p>\n        <h3><i><u><b>Real Rute Sales : {{val.sales_asli}}</b></u></i></h3>\n        <h3><i><u><b>Uploader : {{val.UserUpload}}</b></u></i></h3>\n        <h3>Date Upload : {{val.DateUpload}}</h3>\n        <h3 *ngIf="val.Status!=\'0\'">Validator : {{val.UserValidate}}</h3>\n        <h3 *ngIf="val.Status!=\'0\'">Date Validate : {{val.DateValidate}}</h3>\n        <p *ngIf="val.Note!=\'\'">Note Uploader: {{val.Note}}</p>\n        <p>Source Data: {{val.tipedata}}</p>\n        <p *ngIf="val.NoteValidator!=\'\'">Note Validator: {{val.NoteValidator}}</p>\n        <ion-icon *ngIf="hak_swtiching==\'Yes\'" color="danger" style="font-weight:bold" name="ios-shuffle" item-end\n          (click)="switching(val.KdOutlet,val.KdCabang,val.KdDistributor,val.KdSales,val.kdsales_asli,val.UserUpload,val.sales_asli)">\n        </ion-icon>\n      </ion-item>\n    </ion-list>\n\n    <ion-infinite-scroll (ionInfinite)="doInfinite($event,src, first,filter)">\n      <ion-infinite-scroll-content loadingSpinner="bubbles" loadingText="Loading more data...">\n      </ion-infinite-scroll-content>\n    </ion-infinite-scroll>\n\n  </ion-grid>\n</ion-content>`/*ion-inline-end:"D:\ionic\Project\qrcode\src\pages\tokoplus\tokoplus.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_2__config_provider__["a" /* Provider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], TokoplusPage);
    return TokoplusPage;
}());

//# sourceMappingURL=tokoplus.js.map

/***/ }),

/***/ 71:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AnalyticsPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return MoveanddragPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_provider__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var AnalyticsPage = (function () {
    function AnalyticsPage(navCtrl, navParams, postPvdr, modalCtrl, loadingCtrl, alertCtrl, storage) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.postPvdr = postPvdr;
        this.modalCtrl = modalCtrl;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.storage = storage;
        this.base_url = 'http://api.vci.co.id/public/outlets/';
        this.markers = [];
        this.kdoutlet = navParams.get("kdoutlet");
        this.kdcabang = navParams.get("kdcabang");
        this.kdsales = navParams.get("kdsales");
        this.storage.get('user_session_storage').then(function (res) {
            if (res[0].user_level == '2' || res[0].user_level == '10') {
            }
            else if (res[0].user_level == '6') {
            }
            else {
            }
        });
    }
    AnalyticsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PetatokoPage');
        document.getElementById('legend').style.display = 'none';
        document.getElementById('reload').style.display = 'none';
        //this.initMap();
        this.getMaps();
    };
    AnalyticsPage.prototype.getMaps = function () {
        var _this = this;
        this.storage.get('user_session_storage').then(function (res) {
            //super user dan nasional
            if (res[0].user_level == '-1' || res[0].user_level == '9') {
                _this.initMap('nasional', 5.69);
                //regional
            }
            else if (res[0].user_level == '6') {
                _this.initMap('regional', 7);
                //admin dan kepala distribusi
            }
            else if (res[0].user_level == '2' || res[0].user_level == '10') {
                _this.initMap('distributor', 11);
            }
        });
    };
    AnalyticsPage.prototype.initMap = function (area, zoom) {
        var _this = this;
        var posisi_x;
        var posisi_y;
        var yang_ke = 0;
        var loader = this.loadingCtrl.create({
            content: "Mohon Menunggu..."
        });
        loader.present();
        this.storage.get('user_session_storage').then(function (res) {
            var body = {
                kdcabang: _this.kdcabang,
                kdsales: _this.kdsales,
                kdoutlet: _this.kdoutlet,
                username: res[0].username
            };
            _this.postPvdr.postData(body, 'outlet/getLocationTagSales2').subscribe(function (data) {
                if (data.success) {
                    loader.dismiss();
                    var myLatlng;
                    if (area == "nasional") {
                        myLatlng = { lat: -0.620093, lng: 115.3094217 };
                    }
                    else if (area == "regional") {
                        myLatlng = { lat: parseFloat(data.result[0]['Lat']), lng: parseFloat(data.result[0]['Lng']) };
                    }
                    else if (area == "distributor") {
                        myLatlng = { lat: parseFloat(data.result[0]['Lat']), lng: parseFloat(data.result[0]['Lng']) };
                    }
                    var map = new google.maps.Map(_this.mapElement.nativeElement, {
                        zoom: zoom,
                        center: myLatlng,
                        zoomControl: true,
                        zoomControlOptions: {
                            position: google.maps.ControlPosition.LEFT_TOP
                        },
                        mapTypeControl: false,
                        scaleControl: false,
                        streetViewControl: false,
                        rotateControl: false,
                        fullscreenControl: false
                    });
                    var image = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';
                    var marker = new google.maps.Marker({
                        position: myLatlng,
                        map: map,
                        draggable: true,
                        animation: google.maps.Animation.DROP,
                        icon: image
                    });
                    var content_1 = "<h4>Location</h4>";
                    google.maps.event.addListener(marker, 'dragend', function () {
                        yang_ke++;
                        var inputElement = document.getElementById('yang_ke');
                        inputElement.value = yang_ke.toString();
                        posisi_x = marker.getPosition().lat();
                        posisi_y = marker.getPosition().lng();
                        _this.addInfoWindow(marker, content_1, map, posisi_x, posisi_y, yang_ke);
                    });
                }
                else {
                    loader.dismiss();
                    var alert_1 = _this.alertCtrl.create({
                        title: 'Info',
                        subTitle: 'Mohon Maaf Data Tidak Di Temukan',
                        message: '',
                        buttons: ['OK']
                    });
                    alert_1.present();
                    _this.indonesiaMap();
                    return false;
                }
            }, function (error) {
                loader.dismiss();
                var alert = _this.alertCtrl.create({
                    title: 'Mohon Maaf',
                    subTitle: 'Sedang tidak terkoneksi dengan server. Silahkan coba kembali beberapa saat',
                    buttons: ['OK']
                });
                alert.present();
                _this.indonesiaMap();
                return false;
            });
        });
    };
    AnalyticsPage.prototype.indonesiaMap = function () {
        var myLatlng = { lat: -0.620093, lng: 115.3094217 };
        var map = new google.maps.Map(this.mapElement.nativeElement, {
            zoom: 5.69,
            center: myLatlng,
            zoomControl: false,
            mapTypeControl: false,
            scaleControl: false,
            streetViewControl: false,
            rotateControl: false,
            fullscreenControl: false
        });
    };
    AnalyticsPage.prototype.addInfoWindow = function (marker, content, map, x, y, yang_ke) {
        var _this = this;
        var infoWindow = new google.maps.InfoWindow({
            content: content
        });
        google.maps.event.addListener(marker, 'click', function () {
            //infoWindow.open(map, marker);
            _this.radius_setup(x, y, yang_ke);
        });
    };
    AnalyticsPage.prototype.radius_setup = function (x, y, yang_ke) {
        var _this = this;
        if (parseInt(document.getElementById('yang_ke').value) == yang_ke) {
            var modal = this.modalCtrl.create(MoveanddragPage, { lat: x, long: y });
            modal.present();
            modal.onDidDismiss(function (vue) {
                if (vue) {
                    _this.outlet_radius(vue.lat, vue.long, vue.radius, vue.place_type);
                }
            });
        }
    };
    AnalyticsPage.prototype.outlet_radius = function (x, y, r, place_type) {
        var rad = parseFloat(r); //meters
        var theLat = parseFloat(x); //decimal degrees
        var theLng = parseFloat(y); //decimal degrees
        var yMin = theLat - (0.0000065 * rad);
        var xMin = theLng - (-0.0000065 * rad);
        var yMax = theLat + (0.0000065 * rad);
        var xMax = theLng + (-0.0000065 * rad);
        this.allRadiusRectangleOutletsales(yMin, yMax, xMin, xMax, x, y, r, place_type);
    };
    AnalyticsPage.prototype.allRadiusRectangleOutletsales = function (j, k, l, m, x, y, r, place_type) {
        var _this = this;
        var body = {
            j: j,
            k: k,
            l: l,
            m: m
        };
        this.postPvdr.postData(body, 'outlet/getAnalytics').subscribe(function (data) {
            var icon;
            var alloutlet = [];
            if (data.success) {
                for (var i = 0; i < data.result.length; i++) {
                    //pilih icon
                    icon = 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-red.png';
                    alloutlet.push({
                        lat: data.result[i]['Lat'],
                        lng: data.result[i]['Lng'],
                        outletname: data.result[i]['OutletName'],
                        namasalesman: '',
                        lastVisit: '',
                        alamattoko: data.result[i]['Address'],
                        icon: icon
                    });
                }
                _this.circle(x, y, r, alloutlet, place_type);
            }
            else {
                _this.circle(x, y, r, '', place_type);
                var alert_2 = _this.alertCtrl.create({
                    title: 'Maaf',
                    subTitle: 'Di Area ini tidak ada toko kita.',
                    buttons: ['OK']
                });
                alert_2.present();
            }
        }, function (error) {
            var alert = _this.alertCtrl.create({
                title: 'Opps I am Sorry',
                subTitle: 'Not Connected To Server',
                buttons: ['OK']
            });
            alert.present();
        });
    };
    AnalyticsPage.prototype.circle = function (x, y, r, alloutlet, place_type) {
        var _this = this;
        var service;
        var infowindow;
        var latitude;
        var longitude;
        var jml_toko = 0;
        var myLatlng = { lat: x, lng: y };
        var map = new google.maps.Map(this.mapElement.nativeElement, {
            zoom: 13,
            center: myLatlng,
            rotateControl: false,
            fullscreenControl: false,
            mapTypeControl: false,
            scaleControl: false,
            streetViewControl: true,
            streetViewControlOptions: {
                position: google.maps.ControlPosition.LEFT_TOP
            },
            zoomControl: true,
            zoomControlOptions: {
                position: google.maps.ControlPosition.LEFT_TOP
            }
        });
        for (var i = 0; i < alloutlet.length; i++) {
            latitude = parseFloat(alloutlet[i]['lat']);
            longitude = parseFloat(alloutlet[i]['lng']);
            var latlng = { lat: latitude, lng: longitude };
            var marker = new google.maps.Marker({
                position: latlng,
                map: map,
                icon: alloutlet[i]['icon']
            });
            var contentString = '<h4>' + alloutlet[i]['outletname'] + '&nbsp;&nbsp;&nbsp;&nbsp;</h4>' +
                '<div>' +
                '<p>' + alloutlet[i]['alamattoko'] + '&nbsp;&nbsp;&nbsp;&nbsp;' +
                '</p>' +
                '</div>';
            marker.content = contentString;
            var infoWindow = new google.maps.InfoWindow();
            // google.maps.event.addListener(marker, 'mouseover', function () {
            google.maps.event.addListener(marker, 'click', function () {
                infoWindow.setContent(this.content);
                infoWindow.open(this.getMap(), this);
            });
            jml_toko++;
        }
        var cityCircle = new google.maps.Circle({
            strokeColor: '#FF0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#FF0000',
            fillOpacity: 0.35,
            map: map,
            center: myLatlng,
            radius: r
        });
        google.maps.event.addListener(cityCircle, 'click', function () {
            //percobaan call other function
            //this.getAnalytics(x, y, r, place_type);
        });
        if (place_type.length > 0) {
            var request = {
                location: myLatlng,
                radius: r,
                type: place_type
            };
            service = new google.maps.places.PlacesService(map);
            service.nearbySearch(request, function (results, status) {
                if (status == google.maps.places.PlacesServiceStatus.OK) {
                    var inputElement = document.getElementById('sum_nearby');
                    inputElement.value = results.length;
                    for (var i = 0; i < results.length; i++) {
                        var place = results[i];
                        var image = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';
                        var marker = new google.maps.Marker({
                            map: map,
                            position: place.geometry.location,
                            icon: image
                        });
                        var contentString = '<h4>' + place.name + '&nbsp;&nbsp;&nbsp;&nbsp;</h4>' +
                            '<div>' +
                            '<p>' + place.types[0] + '&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '<p>' + place.vicinity + '&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '</p>' +
                            '</div>';
                        marker.content = contentString;
                        var infoWindow = new google.maps.InfoWindow();
                        // google.maps.event.addListener(marker, 'mouseover', function () {
                        google.maps.event.addListener(marker, 'click', function () {
                            infoWindow.setContent(this.content);
                            infoWindow.open(this.getMap(), this);
                        });
                    }
                }
            });
        }
        setTimeout(function () {
            _this.radius = (parseFloat(r) / 1000).toFixed(2);
            _this.luas = (22 / 7 * (parseFloat(_this.radius) * parseFloat(_this.radius))).toFixed(2);
            _this.sum_toko = jml_toko;
            _this.type_nearby = place_type;
            _this.sum_nearby = parseFloat(document.getElementById('sum_nearby').value);
            _this.persentase = ((jml_toko / (jml_toko + _this.sum_nearby)) * 100).toFixed(1);
            document.getElementById('legend').style.display = '';
            document.getElementById('reload').style.display = '';
        }, 2000);
    };
    AnalyticsPage.prototype.reset = function () {
        this.ionViewDidLoad();
    };
    AnalyticsPage.prototype.clearMarker = function () {
        this.setMapOnAll(null);
    };
    AnalyticsPage.prototype.setMapOnAll = function (map) {
        for (var i = 0; i < this.markers.length; i++) {
            this.markers[i].setMap(map);
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('map'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], AnalyticsPage.prototype, "mapElement", void 0);
    AnalyticsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-analytics',template:/*ion-inline-start:"D:\ionic\Project\qrcode\src\pages\analytics\analytics.html"*/`<ion-header>\n  <ion-navbar color="oren">\n    <ion-title>Analisa Toko {{title}}</ion-title>\n    <ion-buttons end>\n      <button id="reload" ion-button icon-only (click)="reset()" style="display: none;">\n        <ion-icon name="sync"></ion-icon>&nbsp;&nbsp;\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content no-padding class="bgc">\n  <input type="hidden" id="sum_nearby" value="0">\n  <input type="hidden" id="yang_ke" value="0">\n\n  <div #map id="map" style="width: 100%;height: 100%;"></div>\n\n  <div ion-fixed id="legend" style="\n    position: absolute;\n    right:8px;\n    top:5px;\n    width: auto;\n    height: auto;\n    display:none">\n    <div id="more_options">\n      <table style="border:0px solid black;">\n        <tr>\n          <td>\n            <ion-card>\n\n              <ion-item-group>\n                <ion-list no-border>\n                  <ion-item-divider color="light">\n                    <table style="width: 100%;">\n                      <tr>\n                        <td>\n                          <b>Analisa Peta</b>\n                        </td>\n                      </tr>\n                    </table>\n                  </ion-item-divider>\n                </ion-list>\n              </ion-item-group>\n\n              <ion-card-content>\n                <table style="border: 0px solid #222;width: 100%;">\n                  <tr>\n                    <td>\n                      <b>Keterangan Peta</b>\n                    </td>\n                  </tr>\n                  <tr>\n                    <td style="width: 40%;">\n                      Radius\n                    </td>\n                    <td style="width: 60%;">\n                      : {{radius}} Km\n                    </td>\n                  </tr>\n                  <tr>\n                    <td style="width: 40%;">\n                      Luas\n                    </td>\n                    <td style="width: 60%;">\n                      : {{luas}} Km2\n                    </td>\n                  </tr>\n                  <tr>\n                    <td style="width: 40%;">\n                      Toko Tercover\n                    </td>\n                    <td style="width: 60%;">\n                      : {{sum_toko}} Toko\n                    </td>\n                  </tr>\n                  <tr *ngIf="type_nearby!=\'\'">\n                    <td style="width: 40%;">\n                      {{type_nearby}}\n                    </td>\n                    <td style="width: 60%;">\n                      : {{sum_nearby}} Toko\n                    </td>\n                  </tr>\n                </table>\n                <br>\n                <table style="border: 0px solid #222;width: 100%;" *ngIf="type_nearby!=\'\'">\n                  <tr>\n                    <td>\n                      <b>Kesimpulan</b>\n                    </td>\n                  </tr>\n                  <tr>\n                    <td>\n                      <p>Berdasarkan data - data tersebut dapat disimpulkan <br>\n                        bahwa Toko yang sudah tersebar di wilayah radius<br>\n                        tersebut baru mencapai <b>{{persentase}}% dibanding {{type_nearby}} <br>\n                          menurut database Google</b>.</p>\n                    </td>\n                  </tr>\n                </table>\n                <p style="color:#ffffff">\n                  <!-- Wait a minute. Wait a minute, Doc. <br> -->\n                  Uhhh... Are you telling me that you built a time machine... <br>\n                  <!-- out of a DeLorean?! Whoa. This is heavy. -->\n                </p>\n              </ion-card-content>\n\n            </ion-card>\n          </td>\n        </tr>\n      </table>\n    </div>\n  </div>\n\n</ion-content>`/*ion-inline-end:"D:\ionic\Project\qrcode\src\pages\analytics\analytics.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__config_provider__["a" /* Provider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */]])
    ], AnalyticsPage);
    return AnalyticsPage;
}());

var MoveanddragPage = (function () {
    function MoveanddragPage(viewCtrl, navParams) {
        this.viewCtrl = viewCtrl;
        this.navParams = navParams;
        this.radius = 0;
        this.getCurrentData(navParams.get("lat"), navParams.get("long"));
        this.langForm = new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["b" /* FormGroup */]({
            "langs": new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["a" /* FormControl */]({ value: 'none', disabled: false })
        });
    }
    MoveanddragPage.prototype.getCurrentData = function (lat, long) {
        this.lat = lat;
        this.long = long;
    };
    MoveanddragPage.prototype.doSubmit = function (event) {
        alert('Submitting form ' + this.langForm.value + ' Radius : ' + this.radius);
        event.preventDefault();
    };
    MoveanddragPage.prototype.apply = function () {
        var place_type;
        if (this.place_type == 'none') {
            place_type = [];
        }
        else {
            place_type = [this.place_type];
        }
        this.viewCtrl.dismiss({ lat: this.lat, long: this.long, radius: this.radius, place_type: place_type });
    };
    MoveanddragPage.prototype.close = function () {
        this.viewCtrl.dismiss();
    };
    MoveanddragPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"D:\ionic\Project\qrcode\src\pages\analytics\moveanddrag.html"*/`<ion-header>\n\n  <ion-navbar color="oren">\n\n    <ion-title>\n\n      Radius Setup\n\n    </ion-title>\n\n    <ion-buttons end>\n\n      <button ion-button icon-only (click)="apply($event)">\n\n        <ion-icon name="checkmark-circle"></ion-icon>&nbsp;&nbsp;\n\n      </button>\n\n    </ion-buttons>\n\n    <ion-buttons end>\n\n      <button ion-button icon-only (click)="close()">\n\n        <ion-icon name="close-circle"></ion-icon>&nbsp;&nbsp;\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding class="outer-content">\n\n\n\n  <ion-item-group>\n\n    <ion-item-divider color="light">Slide Radius</ion-item-divider>\n\n    <ion-item>\n\n      <ion-range min="0" max="10000" pin="true" [(ngModel)]="radius" color="secondary">\n\n        <ion-icon range-left small name="radius"></ion-icon>\n\n        <ion-icon range-right name="radius"></ion-icon>\n\n      </ion-range>\n\n    </ion-item>\n\n  </ion-item-group>\n\n\n\n  <form (submit)="doSubmit($event)" [formGroup]="langForm">\n\n    <ion-list radio-group formControlName="langs" [(ngModel)]="place_type">\n\n\n\n      <ion-item-divider color="light">Place Type Nearby System By Google</ion-item-divider>\n\n\n\n      <ion-item>\n\n        <ion-label>None</ion-label>\n\n        <ion-radio value="none"></ion-radio>\n\n      </ion-item>\n\n      <ion-item>\n\n        <ion-label>Beauty Salon</ion-label>\n\n        <ion-radio value="beauty_salon"></ion-radio>\n\n      </ion-item>\n\n      <ion-item>\n\n        <ion-label>Cafe</ion-label>\n\n        <ion-radio value="cafe"></ion-radio>\n\n      </ion-item>\n\n      <ion-item>\n\n        <ion-label>Convenience Store </ion-label>\n\n        <ion-radio value="convenience_store "></ion-radio>\n\n      </ion-item>\n\n      <ion-item>\n\n        <ion-label>Department Store </ion-label>\n\n        <ion-radio value="department_store "></ion-radio>\n\n      </ion-item>\n\n      <ion-item>\n\n        <ion-label>Grocery Or Supermarket</ion-label>\n\n        <ion-radio value="grocery_or_supermarket"></ion-radio>\n\n      </ion-item>\n\n      <ion-item>\n\n        <ion-label>Hair Care</ion-label>\n\n        <ion-radio value="hair_care "></ion-radio>\n\n      </ion-item>\n\n      <ion-item>\n\n        <ion-label>Pharmacy</ion-label>\n\n        <ion-radio value="pharmacy"></ion-radio>\n\n      </ion-item>\n\n      <ion-item>\n\n        <ion-label>Shopping Mall </ion-label>\n\n        <ion-radio value="shopping_mall"></ion-radio>\n\n      </ion-item>\n\n      <ion-item>\n\n        <ion-label>Spa</ion-label>\n\n        <ion-radio value="spa"></ion-radio>\n\n      </ion-item>\n\n      <ion-item>\n\n        <ion-label>Scala</ion-label>\n\n        <ion-radio value="scala"></ion-radio>\n\n      </ion-item>\n\n      <ion-item>\n\n        <ion-label>Supermarket</ion-label>\n\n        <ion-radio value="supermarket"></ion-radio>\n\n      </ion-item>\n\n    </ion-list>\n\n\n\n\n\n  </form>\n\n</ion-content>\n\n\n\n<style>\n\n  ion-list+ion-list {\n\n    margin-top: 0;\n\n  }\n\n\n\n  .small-text {\n\n    font-size: 10px;\n\n  }\n\n</style>`/*ion-inline-end:"D:\ionic\Project\qrcode\src\pages\analytics\moveanddrag.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], MoveanddragPage);
    return MoveanddragPage;
}());

//# sourceMappingURL=analytics.js.map

/***/ }),

/***/ 72:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BypassPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_provider__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_main_main__ = __webpack_require__(56);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the BypassPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var BypassPage = (function () {
    function BypassPage(navCtrl, viewCtrl, postPvdr, alertCtrl, storage, loadingCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.postPvdr = postPvdr;
        this.alertCtrl = alertCtrl;
        this.storage = storage;
        this.loadingCtrl = loadingCtrl;
        this.navParams = navParams;
        this.filter = 'all';
    }
    BypassPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad UsersdistributorPage');
        this.listUsers = new Array();
        this.getData('', 0, 25, 'No');
    };
    BypassPage.prototype.form_search = function () {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'Cari',
            message: "Masukan kata untuk mencari nama atau username",
            inputs: [
                {
                    name: 'src',
                    placeholder: 'Apa yang anda cari...'
                },
            ],
            buttons: [
                {
                    text: 'Batal',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Cari',
                    handler: function (data) {
                        console.log('Saved clicked');
                        _this.getListUserDistributor(data.src);
                    }
                }
            ]
        });
        prompt.present();
    };
    BypassPage.prototype.getListUserDistributor = function (src) {
        this.listUsers = new Array();
        if (src) {
            src = src;
        }
        else {
            src = '';
        }
        this.getData(src, 0, 25, 'No');
    };
    BypassPage.prototype.getData = function (src, first, offset, infinite) {
        var _this = this;
        this.first = first;
        this.src = src;
        var loader = this.loadingCtrl.create({
            content: "Mohon Menunggu..."
        });
        if (infinite == 'No') {
            loader.present();
        }
        this.storage.get('user_session_storage').then(function (res) {
            var body = {
                first: first,
                offset: offset,
                keyword: src
            };
            _this.postPvdr.postData(body, 'outlet/getListUserDistributorAll').subscribe(function (data) {
                if (data.success) {
                    loader.dismiss();
                    for (var i = 0; i < data.result.length; i++) {
                        _this.listUsers.push({
                            username: data.result[i]['username'],
                            name: data.result[i]['nama'],
                            password: data.result[i]['password'],
                            outlet: data.result[i]['outlet'],
                            jabatan: data.result[i]['jabatan']
                        });
                    }
                }
                else {
                    loader.dismiss();
                    if (infinite == 'No') {
                        var alert_1 = _this.alertCtrl.create({
                            title: 'Info',
                            subTitle: 'Mohon Maaf Data Tidak Di Temukan',
                            message: '',
                            buttons: ['OK']
                        });
                        alert_1.present();
                    }
                    return false;
                }
            }, function (error) {
                loader.dismiss();
                var alert = _this.alertCtrl.create({
                    title: 'Mohon Maaf',
                    subTitle: 'Sedang tidak terkoneksi dengan server. Silahkan coba kembali beberapa saat',
                    buttons: ['OK']
                });
                alert.present();
                return false;
            });
        });
    };
    BypassPage.prototype.doRefresh = function (refresher) {
        this.listUsers = new Array();
        this.getData('', 0, 25, 'No');
        setTimeout(function () {
            refresher.complete();
        }, 1000);
    };
    BypassPage.prototype.doInfinite = function (infiniteScroll, src, first) {
        var _this = this;
        first = first + 10;
        console.log('Begin async operation');
        setTimeout(function () {
            _this.getData(src, first, 25, 'Yes');
            infiniteScroll.complete();
        }, 1000);
    };
    BypassPage.prototype.bypass = function (username, password) {
        var _this = this;
        var body = {
            username: username,
            password: password,
            platform: 'M',
            action: 'cekLogin'
        };
        this.postPvdr.postData(body, 'login/bypass').subscribe(function (data) {
            if (data.success) {
                _this.storage.set('user_session_storage', data.result);
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__pages_main_main__["a" /* MainPage */]);
            }
            else {
                var alert_2 = _this.alertCtrl.create({
                    title: 'Mohon Maaf',
                    subTitle: 'Anda Tidak Mempunyai Akses.',
                    buttons: ['OK']
                });
                alert_2.present();
            }
        }, function (error) {
            var alert = _this.alertCtrl.create({
                title: 'Mohon Maaf',
                subTitle: 'Sedang tidak terkoneksi dengan server. Silahkan coba kembali beberapa saat',
                buttons: ['OK']
            });
            alert.present();
        });
    };
    BypassPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-bypass',template:/*ion-inline-start:"D:\ionic\Project\qrcode\src\pages\bypass\bypass.html"*/`<!--\n  Generated template for the BypassPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="oren">\n    <ion-title>By Pass UI</ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="form_search()">\n        <ion-icon name="search"></ion-icon>&nbsp;&nbsp;\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content no-padding class="bgc">\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n    <ion-refresher-content pullingIcon="arrow-dropdown" pullingText="Pull to refresh" refreshingSpinner="crescent"\n      refreshingText="">\n    </ion-refresher-content>\n  </ion-refresher>\n  <ion-grid fixed class="wrapper">\n    <ion-list>\n      <ion-item *ngIf="bisa_pilih==\'Yes\'">\n        <h2>Semua</h2>\n        <ion-icon name="download" item-end (click)="pilih_ini(\'\',\'Semua\')">\n        </ion-icon>\n      </ion-item>\n      <ion-item *ngFor="let val of listUsers" (click)="bypass(val.username,val.password)">\n        <h2>{{val.name}}</h2>\n        <h3>Username : {{val.username}}</h3>\n        <p>Outlet : {{val.outlet}}</p>\n        <p>Jabatan : {{val.jabatan}}</p>\n      </ion-item>\n    </ion-list>\n    <ion-infinite-scroll (ionInfinite)="doInfinite($event,src, first)">\n      <ion-infinite-scroll-content loadingSpinner="bubbles" loadingText="Mencari Data Lagi...">\n      </ion-infinite-scroll-content>\n    </ion-infinite-scroll>\n  </ion-grid>\n</ion-content>`/*ion-inline-end:"D:\ionic\Project\qrcode\src\pages\bypass\bypass.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_2__config_provider__["a" /* Provider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], BypassPage);
    return BypassPage;
}());

//# sourceMappingURL=bypass.js.map

/***/ }),

/***/ 73:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListoutletPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_provider__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the ListoutletPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ListoutletPage = (function () {
    function ListoutletPage(navCtrl, postPvdr, storage, navParams, viewCtrl, loadingCtrl, alertCtrl) {
        this.navCtrl = navCtrl;
        this.postPvdr = postPvdr;
        this.storage = storage;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
    }
    ListoutletPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ListoutletPage');
        this.form_search();
    };
    ListoutletPage.prototype.form_search = function () {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'Cari',
            message: "Masukan kata untuk mencari toko",
            inputs: [
                {
                    name: 'src',
                    placeholder: 'Apa yang anda cari...'
                },
            ],
            buttons: [
                {
                    text: 'Batal',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Cari',
                    handler: function (data) {
                        console.log('Saved clicked');
                        _this.getOutlets(data.src);
                    }
                }
            ]
        });
        prompt.present();
    };
    ListoutletPage.prototype.getOutlets = function (src) {
        if (src) {
            src = src;
        }
        else {
            src = '';
        }
        this.getData(src);
    };
    ListoutletPage.prototype.getData = function (src) {
        var _this = this;
        var loader = this.loadingCtrl.create({
            content: "Mohon Menunggu..."
        });
        loader.present();
        this.storage.get('user_session_storage').then(function (res) {
            var body = {
                jenis_outlet: res[0].jenis_outlet,
                kdoutlet: res[0].kdoutlet,
                kdcabang: res[0].cabang_id,
                keyword: src
            };
            _this.postPvdr.postData(body, 'outlet/getoutlet').subscribe(function (data) {
                console.log(data);
                if (data.success) {
                    loader.dismiss();
                    _this.listOutlet = [];
                    for (var i = 0; i < data.result.length; i++) {
                        _this.listOutlet.push({
                            kdoutlet: data.result[i]['KdOutlet'],
                            nama: data.result[i]['Nama'],
                            alamat: data.result[i]['Alm1Toko'],
                            statuz_pending: data.result[i]['statuz_pending'],
                            statuz_valid: data.result[i]['statuz_valid']
                        });
                    }
                }
                else {
                    loader.dismiss();
                    var alert_1 = _this.alertCtrl.create({
                        title: 'Info',
                        subTitle: 'Mohon Maaf Data Tidak Di Temukan',
                        message: '',
                        buttons: ['OK']
                    });
                    alert_1.present();
                    return false;
                }
            }, function (error) {
                loader.dismiss();
                var alert = _this.alertCtrl.create({
                    title: 'Mohon Maaf',
                    subTitle: 'Sedang tidak terkoneksi dengan server. Silahkan coba kembali beberapa saat',
                    buttons: ['OK']
                });
                alert.present();
                return false;
            });
        });
    };
    ListoutletPage.prototype.pilih_outlet = function (kdoutlet, nama, alamat) {
        this.viewCtrl.dismiss({ kdoutlet: kdoutlet, nama: nama, alamat: alamat });
    };
    ListoutletPage.prototype.close = function () {
        this.viewCtrl.dismiss();
    };
    ListoutletPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-listoutlet',template:/*ion-inline-start:"D:\ionic\Project\qrcode\src\pages\listoutlet\listoutlet.html"*/`<ion-header>\n\n  <ion-navbar color="oren">\n    <ion-title>\n      <div>List Toko</div>\n    </ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="form_search()">\n        <ion-icon name="search"></ion-icon>&nbsp;&nbsp;\n      </button>\n    </ion-buttons>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="close()">\n        <ion-icon name="close-circle"></ion-icon>&nbsp;&nbsp;\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content class="bgc">\n  <ion-grid fixed class="wrapper">\n\n    <ion-list>\n      <ion-item *ngFor="let val of listOutlet" (click)="pilih_outlet(val.kdoutlet,val.nama,val.alamat)">\n\n        <h2>{{val.nama}} ({{val.kdoutlet}})</h2>\n        <p>{{val.alamat}}</p>\n        <ion-note item-end color="warning" *ngIf="val.statuz_pending==\'1\'">\n          Pending\n        </ion-note>\n        <ion-note item-end color="secondary" *ngIf="val.statuz_valid==\'1\'">\n          Validate\n        </ion-note>\n\n      </ion-item>\n    </ion-list>\n  </ion-grid>\n</ion-content>`/*ion-inline-end:"D:\ionic\Project\qrcode\src\pages\listoutlet\listoutlet.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__config_provider__["a" /* Provider */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], ListoutletPage);
    return ListoutletPage;
}());

//# sourceMappingURL=listoutlet.js.map

/***/ }),

/***/ 9:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Provider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(426);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
//provider api
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var Provider = (function () {
    function Provider(http) {
        this.http = http;
        // server: string = "http://api.vci.co.id/gocheck_api/index.php/gocheck/";
        this.server = "http://api.vci.co.id/index.php/gocheck/";
    }
    Provider.prototype.postData = function (body, file) {
        var type = "application/x-www-form-urlencoded";
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({
            'Content-Type': type
        });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.post(this.server + file, JSON.stringify(body), options)
            .map(function (res) { return res.json(); });
    };
    Provider.prototype.getData = function (file) {
        return this.http.get(this.server + file)
            .map(function (res) { return res.json(); });
    };
    Provider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
    ], Provider);
    return Provider;
}());

//# sourceMappingURL=provider.js.map

/***/ })

},[377]);
//# sourceMappingURL=main.js.map