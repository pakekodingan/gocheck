//provider api

import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class Provider {

	// server: string = "http://api.vci.co.id/gocheck_api/index.php/gocheck/";
	server: string = "http://api.vci.co.id/index.php/gocheck/";

	constructor(public http: Http) {

	}

	postData(body, file) {

		let type = "application/x-www-form-urlencoded";
		let headers = new Headers({
			'Content-Type': type
		});

		let options = new RequestOptions({ headers: headers });

		return this.http.post(this.server + file, JSON.stringify(body), options)
			.map(res => res.json());
	}

	getData(file) {

		return this.http.get(this.server + file)
			.map(res => res.json());


	}








}