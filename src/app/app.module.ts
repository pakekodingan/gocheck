import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { MainPage } from '../pages/main/main';
import { PopoverPage } from '../pages/home/home';
import { FormoutletPage } from '../pages/formoutlet/formoutlet';
import { FormoutletpwaPage } from '../pages/formoutletpwa/formoutletpwa';
import { ListoutletPage } from '../pages/listoutlet/listoutlet';
import { HistoryPage } from '../pages/history/history';
import { SettingsPage } from '../pages/settings/settings';
import { AdminPage } from '../pages/admin/admin';
import { DetaillocationoutletPage } from '../pages/detaillocationoutlet/detaillocationoutlet';
import { ListingPage } from '../pages/listing/listing';
import { DetaillistingsPage } from '../pages/detaillistings/detaillistings';
import { DetailbangetlistingsPage } from '../pages/detailbangetlistings/detailbangetlistings';
import { UsersdistributorPage } from '../pages/usersdistributor/usersdistributor';
import { LogsPage } from '../pages/logs/logs';
import { ActivitiesPage } from '../pages/activities/activities';
import { PerformaPage } from '../pages/performa/performa';
import { PetatokoPage } from '../pages/petatoko/petatoko';
import { SearchPage } from '../pages/search/search';
import { DetailhistoryPage } from '../pages/detailhistory/detailhistory';
import { SearchactivitiesPage } from '../pages/searchactivities/searchactivities';
import { AnalyticsPage } from '../pages/analytics/analytics';
import { MoveanddragPage } from '../pages/analytics/analytics';
import { BypassPage } from '../pages/bypass/bypass';
import { TraficPage } from '../pages/trafic/trafic';
import { NoolistPage } from '../pages/noolist/noolist';
import { PetanooPage } from '../pages/petanoo/petanoo';
import { AksiPage } from '../pages/noolist/noolist';
import { RutesalesPage } from '../pages/rutesales/rutesales';
import { DetailrutesalesPage } from '../pages/detailrutesales/detailrutesales';
import { TokoplusPage } from '../pages/tokoplus/tokoplus';

import { Provider } from '../config/provider';

import { NgxQRCodeModule } from 'ngx-qrcode2';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Camera } from '@ionic-native/camera';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { IonicStorageModule } from '@ionic/storage';
import { SocialSharing } from '@ionic-native/social-sharing';
import { HttpModule } from '@angular/http';
import { FileOpener } from '@ionic-native/file-opener';
import { Geolocation } from '@ionic-native/geolocation';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { NativeGeocoder } from '@ionic-native/native-geocoder';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { AppUpdate } from '@ionic-native/app-update';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    MainPage,
    LoginPage,
    PopoverPage,
    FormoutletPage,
    FormoutletpwaPage,
    ListoutletPage,
    HistoryPage,
    SettingsPage,
    AdminPage,
    DetaillocationoutletPage,
    ListingPage,
    DetaillistingsPage,
    DetailbangetlistingsPage,
    LogsPage,
    ActivitiesPage,
    UsersdistributorPage,
    PerformaPage,
    PetatokoPage,
    SearchPage,
    DetailhistoryPage,
    SearchactivitiesPage,
    AnalyticsPage,
    MoveanddragPage,
    BypassPage,
    TraficPage,
    NoolistPage,
    AksiPage,
    PetanooPage,
    RutesalesPage,
    DetailrutesalesPage,
    TokoplusPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp),
    NgxQRCodeModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    MainPage,
    LoginPage,
    PopoverPage,
    FormoutletPage,
    FormoutletpwaPage,
    ListoutletPage,
    HistoryPage,
    SettingsPage,
    AdminPage,
    DetaillocationoutletPage,
    ListingPage,
    DetaillistingsPage,
    DetailbangetlistingsPage,
    LogsPage,
    ActivitiesPage,
    UsersdistributorPage,
    PerformaPage,
    PetatokoPage,
    SearchPage,
    DetailhistoryPage,
    SearchactivitiesPage,
    AnalyticsPage,
    MoveanddragPage,
    BypassPage,
    TraficPage,
    NoolistPage,
    AksiPage,
    PetanooPage,
    RutesalesPage,
    DetailrutesalesPage,
    TokoplusPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    SocialSharing,
    FileOpener,
    Geolocation,
    AndroidPermissions,
    LocationAccuracy,
    NativeGeocoder,
    Camera,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    BarcodeScanner,
    File,
    FileTransfer,
    FileTransferObject,
    Provider,
    ScreenOrientation,
    AppUpdate
  ]
})
export class AppModule { }
