import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { ModalController } from 'ionic-angular';
import { ViewController } from 'ionic-angular';
import { Provider } from '../../config/provider';
import { Storage } from '@ionic/storage';
import { FormGroup, FormControl } from '@angular/forms';

import { SearchPage } from '../../pages/search/search';
declare var google;

@IonicPage()
@Component({
  selector: 'page-analytics',
  templateUrl: 'analytics.html',
})
export class AnalyticsPage {
  base_url = 'http://api.vci.co.id/public/outlets/';

  kdoutlet: any;
  kdcabang: any;
  kdsales: any;
  title: any;
  markers = [];

  toko_sudah_terpetakan: any;
  toko_belum_terpetakan: any;
  toko_pegangan: any;
  toko_diluar_pegangan_sudah_terpetakan: any;
  toko_noo: any;

  listOutlet: any;

  radius: any;
  luas: any;
  sum_toko: any;
  sum_nearby: any;
  persentase: any;
  type_nearby: any;

  @ViewChild('map') mapElement: ElementRef;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public postPvdr: Provider,
    public modalCtrl: ModalController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public storage: Storage
  ) {
    this.kdoutlet = navParams.get("kdoutlet");
    this.kdcabang = navParams.get("kdcabang");
    this.kdsales = navParams.get("kdsales");

    this.storage.get('user_session_storage').then((res) => {
      if (res[0].user_level == '2' || res[0].user_level == '10') {

      } else if (res[0].user_level == '6') {

      } else {

      }

    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PetatokoPage');
    document.getElementById('legend').style.display = 'none';
    document.getElementById('reload').style.display = 'none';
    //this.initMap();
    this.getMaps();
  }

  getMaps() {

    this.storage.get('user_session_storage').then((res) => {
      //super user dan nasional
      if (res[0].user_level == '-1' || res[0].user_level == '9') {
        this.initMap('nasional', 5.69);
        //regional
      } else if (res[0].user_level == '6') {
        this.initMap('regional', 7);

        //admin dan kepala distribusi
      } else if (res[0].user_level == '2' || res[0].user_level == '10') {
        this.initMap('distributor', 11);
      }
    });
  }

  initMap(area, zoom) {
    var posisi_x;
    var posisi_y;
    let yang_ke = 0;

    const loader = this.loadingCtrl.create({
      content: "Mohon Menunggu..."
    });
    loader.present();

    this.storage.get('user_session_storage').then((res) => {
      let body = {
        kdcabang: this.kdcabang,
        kdsales: this.kdsales,
        kdoutlet: this.kdoutlet,
        username: res[0].username
      };

      this.postPvdr.postData(body, 'outlet/getLocationTagSales2').subscribe((data) => {

        if (data.success) {
          loader.dismiss();

          var myLatlng;

          if (area == "nasional") {
            myLatlng = { lat: -0.620093, lng: 115.3094217 };
          } else if (area == "regional") {
            myLatlng = { lat: parseFloat(data.result[0]['Lat']), lng: parseFloat(data.result[0]['Lng']) };
          } else if (area == "distributor") {
            myLatlng = { lat: parseFloat(data.result[0]['Lat']), lng: parseFloat(data.result[0]['Lng']) };
          }

          var map = new google.maps.Map(this.mapElement.nativeElement, {
            zoom: zoom,
            center: myLatlng,
            zoomControl: true,
            zoomControlOptions: {
              position: google.maps.ControlPosition.LEFT_TOP
            },
            mapTypeControl: false,
            scaleControl: false,
            streetViewControl: false,
            rotateControl: false,
            fullscreenControl: false
          });

          var image = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';
          var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            draggable: true,
            animation: google.maps.Animation.DROP,
            icon: image
          });

          let content = "<h4>Location</h4>";


          google.maps.event.addListener(marker, 'dragend', () => {
            yang_ke++;
            var inputElement = <HTMLInputElement>document.getElementById('yang_ke');
            inputElement.value = yang_ke.toString();

            posisi_x = marker.getPosition().lat();
            posisi_y = marker.getPosition().lng();
            this.addInfoWindow(marker, content, map, posisi_x, posisi_y, yang_ke);
          });

        } else {
          loader.dismiss();
          const alert = this.alertCtrl.create({
            title: 'Info',
            subTitle: 'Mohon Maaf Data Tidak Di Temukan',
            message: '',
            buttons: ['OK']
          });
          alert.present();
          this.indonesiaMap();
          return false;
        }

      }, error => {
        loader.dismiss();
        const alert = this.alertCtrl.create({
          title: 'Mohon Maaf',
          subTitle: 'Sedang tidak terkoneksi dengan server. Silahkan coba kembali beberapa saat',
          buttons: ['OK']
        });
        alert.present();
        this.indonesiaMap();
        return false;
      });
    });

  }

  indonesiaMap() {
    var myLatlng = { lat: -0.620093, lng: 115.3094217 };
    var map = new google.maps.Map(this.mapElement.nativeElement, {
      zoom: 5.69,
      center: myLatlng,
      zoomControl: false,
      mapTypeControl: false,
      scaleControl: false,
      streetViewControl: false,
      rotateControl: false,
      fullscreenControl: false
    });
  }

  addInfoWindow(marker, content, map, x, y, yang_ke) {
    let infoWindow = new google.maps.InfoWindow({
      content: content
    });

    google.maps.event.addListener(marker, 'click', () => {
      //infoWindow.open(map, marker);
      this.radius_setup(x, y, yang_ke);
    });
  }

  radius_setup(x, y, yang_ke) {
    if (parseInt((document.getElementById('yang_ke') as HTMLInputElement).value) == yang_ke) {
      const modal = this.modalCtrl.create(MoveanddragPage, { lat: x, long: y });
      modal.present();
      modal.onDidDismiss(vue => {
        if (vue) {
          this.outlet_radius(vue.lat, vue.long, vue.radius, vue.place_type);
        }
      });
    }
  }

  outlet_radius(x, y, r, place_type) {
    var rad = parseFloat(r)  //meters
    var theLat = parseFloat(x);  //decimal degrees
    var theLng = parseFloat(y);  //decimal degrees

    var yMin = theLat - (0.0000065 * rad);
    var xMin = theLng - (-0.0000065 * rad);

    var yMax = theLat + (0.0000065 * rad);
    var xMax = theLng + (-0.0000065 * rad);

    this.allRadiusRectangleOutletsales(yMin, yMax, xMin, xMax, x, y, r, place_type);
  }

  allRadiusRectangleOutletsales(j, k, l, m, x, y, r, place_type) {
    let body = {
      j: j,
      k: k,
      l: l,
      m: m
    };

    this.postPvdr.postData(body, 'outlet/getAnalytics').subscribe((data) => {
      var icon;
      var alloutlet = [];


      if (data.success) {

        for (let i = 0; i < data.result.length; i++) {

          //pilih icon
          icon = 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-red.png';

          alloutlet.push({
            lat: data.result[i]['Lat'],
            lng: data.result[i]['Lng'],
            outletname: data.result[i]['OutletName'],
            namasalesman: '',
            lastVisit: '',
            alamattoko: data.result[i]['Address'],
            icon: icon
          }
          );

        }

        this.circle(x, y, r, alloutlet, place_type);

      } else {
        this.circle(x, y, r, '', place_type);
        const alert = this.alertCtrl.create({
          title: 'Maaf',
          subTitle: 'Di Area ini tidak ada toko kita.',
          buttons: ['OK']
        });
        alert.present();
      }

    }, error => {

      const alert = this.alertCtrl.create({
        title: 'Opps I am Sorry',
        subTitle: 'Not Connected To Server',
        buttons: ['OK']
      });
      alert.present();
    });
  }

  circle(x, y, r, alloutlet, place_type) {

    var service;
    var infowindow;
    var latitude;
    var longitude;

    let jml_toko = 0;

    var myLatlng = { lat: x, lng: y };
    var map = new google.maps.Map(this.mapElement.nativeElement, {
      zoom: 13,
      center: myLatlng,
      rotateControl: false,
      fullscreenControl: false,
      mapTypeControl: false,
      scaleControl: false,
      streetViewControl: true,
      streetViewControlOptions: {
        position: google.maps.ControlPosition.LEFT_TOP
      },
      zoomControl: true,
      zoomControlOptions: {
        position: google.maps.ControlPosition.LEFT_TOP
      }
    });


    for (let i = 0; i < alloutlet.length; i++) {
      latitude = parseFloat(alloutlet[i]['lat']);
      longitude = parseFloat(alloutlet[i]['lng']);
      var latlng = { lat: latitude, lng: longitude };

      var marker = new google.maps.Marker({
        position: latlng,
        map: map,
        icon: alloutlet[i]['icon']
      });

      var contentString =
        '<h4>' + alloutlet[i]['outletname'] + '&nbsp;&nbsp;&nbsp;&nbsp;</h4>' +
        '<div>' +
        '<p>' + alloutlet[i]['alamattoko'] + '&nbsp;&nbsp;&nbsp;&nbsp;' +
        '</p>' +
        '</div>';

      marker.content = contentString;

      var infoWindow = new google.maps.InfoWindow();
      // google.maps.event.addListener(marker, 'mouseover', function () {
      google.maps.event.addListener(marker, 'click', function () {
        infoWindow.setContent(this.content);
        infoWindow.open(this.getMap(), this);
      });
      jml_toko++;
    }


    var cityCircle = new google.maps.Circle({
      strokeColor: '#FF0000',
      strokeOpacity: 0.8,
      strokeWeight: 2,
      fillColor: '#FF0000',
      fillOpacity: 0.35,
      map: map,
      center: myLatlng,
      radius: r
    });

    google.maps.event.addListener(cityCircle, 'click', () => {
      //percobaan call other function
      //this.getAnalytics(x, y, r, place_type);
    });


    if (place_type.length > 0) {

      var request = {
        location: myLatlng,
        radius: r,
        type: place_type
      };

      service = new google.maps.places.PlacesService(map);
      service.nearbySearch(request, function (results, status) {

        if (status == google.maps.places.PlacesServiceStatus.OK) {

          var inputElement = <HTMLInputElement>document.getElementById('sum_nearby');
          inputElement.value = results.length;

          for (var i = 0; i < results.length; i++) {
            var place = results[i];

            var image = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';
            var marker = new google.maps.Marker({
              map: map,
              position: place.geometry.location,
              icon: image
            });

            var contentString =
              '<h4>' + place.name + '&nbsp;&nbsp;&nbsp;&nbsp;</h4>' +
              '<div>' +
              '<p>' + place.types[0] + '&nbsp;&nbsp;&nbsp;&nbsp;' +
              '<p>' + place.vicinity + '&nbsp;&nbsp;&nbsp;&nbsp;' +
              '</p>' +
              '</div>';

            marker.content = contentString;

            var infoWindow = new google.maps.InfoWindow();
            // google.maps.event.addListener(marker, 'mouseover', function () {
            google.maps.event.addListener(marker, 'click', function () {
              infoWindow.setContent(this.content);
              infoWindow.open(this.getMap(), this);
            });



          }
        }
      });

    }



    setTimeout(() => {
      this.radius = (parseFloat(r) / 1000).toFixed(2);
      this.luas = (22 / 7 * (parseFloat(this.radius) * parseFloat(this.radius))).toFixed(2);
      this.sum_toko = jml_toko;
      this.type_nearby = place_type;
      this.sum_nearby = parseFloat((document.getElementById('sum_nearby') as HTMLInputElement).value);
      this.persentase = ((jml_toko / (jml_toko + this.sum_nearby)) * 100).toFixed(1);
      document.getElementById('legend').style.display = '';
      document.getElementById('reload').style.display = '';
    }, 2000)

  }


  reset() {
    this.ionViewDidLoad();
  }

  clearMarker() {
    this.setMapOnAll(null);
  }

  setMapOnAll(map) {
    for (var i = 0; i < this.markers.length; i++) {
      this.markers[i].setMap(map);
    }
  }

}

@Component({
  templateUrl: 'moveanddrag.html'
})
export class MoveanddragPage {

  radius: number = 0;
  lat: any;
  long: any;
  place_type: any;
  langs;
  langForm;

  constructor(public viewCtrl: ViewController, public navParams: NavParams) {
    this.getCurrentData(navParams.get("lat"), navParams.get("long"));

    this.langForm = new FormGroup({
      "langs": new FormControl({ value: 'none', disabled: false })
    });

  }

  getCurrentData(lat, long) {
    this.lat = lat;
    this.long = long;
  }

  doSubmit(event) {
    alert('Submitting form ' + this.langForm.value + ' Radius : ' + this.radius);
    event.preventDefault();
  }

  apply() {
    var place_type;
    if (this.place_type == 'none') {
      place_type = []
    } else {
      place_type = [this.place_type]
    }

    this.viewCtrl.dismiss({ lat: this.lat, long: this.long, radius: this.radius, place_type: place_type });
  }

  close() {
    this.viewCtrl.dismiss();
  }

}
