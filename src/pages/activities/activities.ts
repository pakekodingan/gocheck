import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { ModalController } from 'ionic-angular';
import { Provider } from '../../config/provider';
import { Storage } from '@ionic/storage';

import { SearchactivitiesPage } from '../../pages/searchactivities/searchactivities';

/**
 * Generated class for the ActivitiesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

declare var google;

@IonicPage()
@Component({
  selector: 'page-activities',
  templateUrl: 'activities.html',
})
export class ActivitiesPage {

  @ViewChild('map') mapElement: ElementRef;

  base_url = 'http://api.vci.co.id/public/outlets/';

  nama_distributor: any;
  nama_sales: any;
  markers = [];
  animation_directions: any;

  kdoutlet: any;
  kdcabang: any;
  kdsales: any;
  distributor_name: any;
  sales_name: any;

  today = new Date();
  tanggal = this.today.getFullYear() + '-' + this.benarMonth((this.today.getMonth() + 1)) + '-' + this.benarDate(this.today.getDate());

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public postPvdr: Provider,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public storage: Storage
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ActivitiesPage');
    this.getData();
  }

  getData() {
    this.storage.get('user_session_storage').then((res) => {

      //super user dan nasional
      if (res[0].user_level == '-1' || res[0].user_level == '9') {
        this.getCari('', '', res[0].user_level, '', this.tanggal, res[0].username, 'nasional', 5.69);
        this.nama_distributor = 'Semua Distributor Nasional';
        this.nama_sales = 'Semua';

        //regional
      } else if (res[0].user_level == '6') {
        this.getCari(res[0].cabang_id, '', res[0].user_level, '', this.tanggal, res[0].username, 'regional', 7);
        this.nama_distributor = 'Semua Distributor Regional';
        this.nama_sales = 'Semua';

        //admin dan kepala distribusi
      } else if (res[0].user_level == '2' || res[0].user_level == '10') {
        this.getCari(res[0].cabang_id, res[0].kdoutlet, res[0].user_level, '', this.tanggal, res[0].username, 'distributor', 11);
        this.nama_distributor = res[0].distributor_name;
        this.nama_sales = 'Semua';
      }

    });
  }

  getCari(kdcabang, kdoutlet, userlevel, kdsales, tanggal, username, area, zoom) {

    this.kdcabang = kdcabang;
    this.kdoutlet = kdoutlet;
    this.kdsales = kdsales;
    this.tanggal = tanggal;

    const loader = this.loadingCtrl.create({
      content: "Mohon Menunggu..."
    });
    loader.present();

    let body;
    body = {
      kdcabang: kdcabang,
      userlevel: userlevel,
      kdoutlet: kdoutlet,
      kdsales: kdsales,
      tanggal: tanggal,
      username: username
    };

    this.postPvdr.postData(body, 'outlet/getActivities2').subscribe((data) => {
      console.log('datax : ', data)
      if (data.success) {
        loader.dismiss();

        var myLatlng;

        if (area == "nasional") {
          myLatlng = { lat: -0.620093, lng: 115.3094217 };
        } else if (area == "regional") {
          myLatlng = { lat: parseFloat(data.result[0]['Lat']), lng: parseFloat(data.result[0]['Lng']) };
        } else if (area == "distributor") {
          myLatlng = { lat: parseFloat(data.result[0]['Lat']), lng: parseFloat(data.result[0]['Lng']) };
        }

        var map = new google.maps.Map(this.mapElement.nativeElement, {
          zoom: zoom,
          center: myLatlng,
          zoomControl: false,
          mapTypeControl: false,
          scaleControl: false,
          streetViewControl: false,
          rotateControl: false,
          fullscreenControl: false
        });

        let lat_from;
        let lng_from;
        let lat_from2;
        let lng_from2;

        this.animation_directions = [];

        for (let i = 0; i < data.result.length; i++) {

          //pilih icon
          var icon;
          if (data.result[i]['Status'] == '0') {
            icon = 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-green.png';
          } else if (data.result[i]['Status'] == '1') {
            icon = 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-blue.png';
          } else if (data.result[i]['Status'] == '2') {
            icon = 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-red.png';
          }


          var Latlng = { lat: parseFloat(data.result[i]['Lat']), lng: parseFloat(data.result[i]['Lng']) };
          //marker
          var marker = new google.maps.Marker({
            position: Latlng,
            animation: google.maps.Animation.DROP,
            map: map,
            icon: icon
          });
          this.markers.push(marker);

          //start rute --------------------------------------------------------

          // lat_from2 = parseFloat(data.result[i]['Lat']);
          // lng_from2 = parseFloat(data.result[i]['Lng']);

          // if ((lat_from2 != lat_from && lng_from2 != lng_from)) {

          //   this.animation_directions.push(
          //     {
          //       lat: parseFloat(data.result[i]['Lat']),
          //       lng: parseFloat(data.result[i]['Lng'])
          //     });

          // }

          // lat_from = parseFloat(data.result[i]['Lat']);
          // lng_from = parseFloat(data.result[i]['Lng']);

          var contentString =
            '<h4>' + data.result[i]['OutletName'] + '&nbsp;&nbsp;</h4>' +
            '<div>' +
            '<p>' + data.result[i]['Address'] + '&nbsp;&nbsp;' + '<br>' +
            'Sales : ' + data.result[i]['UserUpload'] + '&nbsp;&nbsp;' + '<br>' +
            'Tanggal Upload : ' + data.result[i]['DateUpload'] + '&nbsp;&nbsp;' + '<br>' +
            'Catatan Sales :' + data.result[i]['Note'] + '&nbsp;&nbsp;' + '<br>' +
            'Tanggal Valid : ' + data.result[i]['DateValidate'] + '&nbsp;&nbsp;' + '<br>' +
            'Catatan Valid : ' + data.result[i]['NoteValidator'] + '&nbsp;&nbsp;' + '</p>' +
            '<div id="pic"><img src="' + this.base_url + data.result[i]['Images'] + '" width="200" height="250" />' +
            '</div>';

          marker.content = contentString;

          var infoWindow = new google.maps.InfoWindow();
          // google.maps.event.addListener(marker, 'mouseover', function () {
          google.maps.event.addListener(marker, 'click', function () {
            infoWindow.setContent(this.content);
            infoWindow.open(this.getMap(), this);
          });

        }

        // var lineSymbol = {
        //   path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
        //   scale: 5,
        //   strokeColor: '#393'
        // };

        // var line = new google.maps.Polyline({
        //   path: this.animation_directions,
        //   icons: [{
        //     icon: lineSymbol,
        //     offset: '100%'
        //   }],
        //   map: map
        // });

        // var count = 0;
        // window.setInterval(function () {
        //   count = (count + 1) % 200;

        //   var icons = line.get('icons');
        //   icons[0].offset = (count / 2) + '%';
        //   line.set('icons', icons);
        // }, 50);

        //end rute -----------------------------

      } else {
        loader.dismiss();
        const alert = this.alertCtrl.create({
          title: 'Info',
          subTitle: 'Mohon Maaf Data Tidak Di Temukan',
          message: '',
          buttons: ['OK']
        });
        alert.present();
        this.initMap();
        return false;
      }

    }, error => {
      loader.dismiss();
      const alert = this.alertCtrl.create({
        title: 'Mohon Maaf',
        subTitle: 'Sedang tidak terkoneksi dengan server. Silahkan coba kembali beberapa saat',
        buttons: ['OK']
      });
      alert.present();
      this.initMap();
      return false;
    });
  }

  initMap() {

    var myLatlng = { lat: -0.620093, lng: 115.3094217 };
    var map = new google.maps.Map(this.mapElement.nativeElement, {
      zoom: 5.69,
      center: myLatlng,
      zoomControl: false,
      mapTypeControl: false,
      scaleControl: false,
      streetViewControl: false,
      rotateControl: false,
      fullscreenControl: false
    });
  }

  filter() {
    var area;
    var zooom;
    this.storage.get('user_session_storage').then((res) => {

      if (res[0].user_level == '-1' || res[0].user_level == '9') {
        const modal = this.modalCtrl.create(SearchactivitiesPage, { kdcabang: this.kdcabang, kdsales: this.kdsales, kdoutlet: this.kdoutlet, distributor_name: this.nama_distributor, sales_name: this.nama_sales, date_search: this.tanggal });
        modal.present();
        modal.onDidDismiss(val => {
          console.log('test : ', val)
          if (val) {
            if (val.sales_name == 'Semua' && val.distributor_name == "Semua Distributor Nasional") {
              this.kdsales = val.kdsales;
              this.nama_sales = val.sales_name;
              this.kdcabang = val.kdcabang;
              this.kdoutlet = val.kdoutlet;
              this.nama_distributor = val.distributor_name;
              this.tanggal = val.date_search;
              area = "nasional";
              zooom = 5.69;

            } else {
              this.kdsales = val.kdsales;
              this.nama_sales = val.sales_name;
              this.kdcabang = val.kdcabang;
              this.kdoutlet = val.kdoutlet;
              this.nama_distributor = val.distributor_name;
              this.tanggal = val.date_search;
              area = "distributor";
              zooom = 11;


            }
            this.getCari(this.kdcabang, this.kdoutlet, res[0].user_level, this.kdsales, this.tanggal, res[0].username, area, zooom);
            this.clearMarker();
          } else {
            //coding if else or empty
          }
        });
      }

      if (res[0].user_level == '6') {
        const modal = this.modalCtrl.create(SearchactivitiesPage, { kdcabang: this.kdcabang, kdsales: this.kdsales, kdoutlet: this.kdoutlet, distributor_name: this.nama_distributor, sales_name: this.nama_sales, date_search: this.tanggal });
        modal.present();
        modal.onDidDismiss(val => {
          console.log('test : ', val)
          if (val) {
            if (val.sales_name != 'Semua') {
              this.kdsales = val.kdsales;
              this.nama_sales = val.sales_name;
              this.kdcabang = val.kdcabang;
              this.kdoutlet = val.kdoutlet;
              this.nama_distributor = val.distributor_name;
              this.tanggal = val.date_search;
              area = "distributor";
              zooom = 11;
            } else {
              this.kdsales = val.kdsales;
              this.nama_sales = val.sales_name;
              this.kdcabang = val.kdcabang;
              this.kdoutlet = val.kdoutlet;
              this.nama_distributor = val.distributor_name;
              this.tanggal = val.date_search;
              area = "regional";
              zooom = 7;
            }
            this.getCari(this.kdcabang, this.kdoutlet, res[0].user_level, this.kdsales, this.tanggal, res[0].username, area, zooom);
            this.clearMarker();
          } else {
            //coding if else or empty
          }
        });

      }

      if (res[0].user_level == '2' || res[0].user_level == '10') {
        const modal = this.modalCtrl.create(SearchactivitiesPage, { kdcabang: this.kdcabang, kdsales: this.kdsales, kdoutlet: this.kdoutlet, distributor_name: this.nama_distributor, sales_name: this.nama_sales, date_search: this.tanggal });
        modal.present();
        modal.onDidDismiss(val => {
          console.log('test : ', val)
          if (val) {
            if (val.sales_name != 'Semua') {
              this.kdsales = val.kdsales;
              this.nama_sales = val.sales_name;
              this.kdcabang = val.kdcabang;
              this.kdoutlet = val.kdoutlet;
              this.nama_distributor = val.distributor_name;
              this.tanggal = val.date_search;
              area = "distributor";
              zooom = 11;
            } else {
              this.kdsales = val.kdsales;
              this.nama_sales = val.sales_name;
              this.kdcabang = val.kdcabang;
              this.kdoutlet = val.kdoutlet;
              this.nama_distributor = val.distributor_name;
              this.tanggal = val.date_search;
              area = "distributor";
              zooom = 11;
            }
            this.getCari(this.kdcabang, this.kdoutlet, res[0].user_level, this.kdsales, this.tanggal, res[0].username, area, zooom);
            this.clearMarker();
          } else {
            //coding if else or empty
          }
        });
      }




    });
  }

  clearMarker() {
    this.setMapOnAll(null);
  }

  setMapOnAll(map) {
    for (var i = 0; i < this.markers.length; i++) {
      this.markers[i].setMap(map);
    }
  }

  tampilkan() {
    document.getElementById('legend').style.display = '';
    document.getElementById('fab').style.display = 'none';
  }

  sembunyikan() {
    document.getElementById('legend').style.display = 'none';
    document.getElementById('fab').style.display = '';
  }

  benarMonth(m) {
    if (m * 1 < 10) {
      m = '0' + m;
    } else {
      m = m;
    }

    return m;
  }

  benarDate(d) {
    if (d * 1 < 10) {
      d = '0' + d;
    } else {
      d = d;
    }

    return d;
  }

}
