import { Component, Input, ViewChild, Renderer2, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewController, AlertController } from 'ionic-angular';
import { ModalController, LoadingController, Loading } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { File, IWriteOptions } from '@ionic-native/file';
import { Storage } from '@ionic/storage';
import { SocialSharing } from '@ionic-native/social-sharing';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { Geolocation } from '@ionic-native/geolocation';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { Platform } from 'ionic-angular';
import { Provider } from '../../config/provider';
import { ListoutletPage } from '../../pages/listoutlet/listoutlet';
import fixOrientation from 'fix-orientation';

declare var google;
declare var require: any;
let QRCode = require('qrcode');
const STORAGE_KEY = 'IMAGE_LIST';

@IonicPage()
@Component({
  selector: 'page-formoutletpwa',
  templateUrl: 'formoutletpwa.html',
})
export class FormoutletpwaPage {

  @ViewChild('inputcamera') cameraInput: ElementRef;
  @ViewChild('map') mapElement: ElementRef;
  @ViewChild('imageCanvas1') canvas1: any;
  @ViewChild('qrcElement') qrcElement: ElementRef;
  canvasElement1: any;

  photo: any;
  nama_image: any;
  hidden_data: any;
  outlet_existing: any = false;
  kode_outlet = null;
  kode_distributor = null;
  kode_sales = null;
  outlet_name = null;
  address = null;
  note = null;
  latitude = null;
  longitude = null;
  createdCode = null;
  photo_status: String = '';
  storedImages = [];
  img = '';

  displayCard() {
    return this.img !== '';
  }

  @Input('qrc-element-type') elementType: 'url' | 'img' | 'canvas' = 'img';
  @Input('qrc-class') cssClass = 'qrcode';
  @Input('qrc-value') value = 'https://www.techiediaries.com';
  @Input('qrc-version') version: '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' | '10' | '11' | '12' | '13' | '14' | '15' | '16' | '17' | '18' | '19' | '20' | '21' | '22' | '23' | '24' | '25' | '26' | '27' | '28' | '29' | '30' | '31' | '32' | '33' | '34' | '35' | '36' | '37' | '38' | '39' | '40' | '' = '';
  @Input('qrc-errorCorrectionLevel') errorCorrectionLevel: 'L' | 'M' | 'Q' | 'H' = 'M';
  @Input('qrc-margin') margin = 3;
  @Input('qrc-scale') scale = 12;
  @Input('qrc-width') width = 10;
  @Input('qrc-colorDark') colorDark = '#000000';
  @Input('qrc-colorLight') colorLight = '#ffffff';

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    private camera: Camera,
    public barcodeScanner: BarcodeScanner,
    private file: File,
    public plt: Platform,
    public loadingCtrl: LoadingController,
    private storage: Storage,
    public renderer: Renderer2,
    public transfer: FileTransfer,
    public postPvdr: Provider,
    private socialSharing: SocialSharing,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    private locationAccuracy: LocationAccuracy,
    private androidPermissions: AndroidPermissions,
    public geolocation: Geolocation,
  ) {

    this.storage.get('user_session_storage').then((res) => {
      this.kode_distributor = res[0].kdoutlet;
      this.kode_sales = res[0].kdsales;
      ;
    });

    if (this.plt.is('android')) {
      console.log('android')
    } else {
      console.log('web')
    }

    this.storage.ready().then(() => {
      this.storage.get(STORAGE_KEY).then(data => {
        if (data != undefined) {
          this.storedImages = data;
        }
      });
    });

  }

  ionViewDidLoad() {
    this.canvasElement1 = this.canvas1.nativeElement;
    // this.canvasElement1.width = 3096;
    // this.canvasElement1.height = 4128;
    this.canvasElement1.width = 2500;
    this.canvasElement1.height = 3500;

    // this.canvasElement1.width = 500;
    // this.canvasElement1.height = 500;

    // this.canvasElement1.width = 720;
    // this.canvasElement1.height = 1560;

    this.getLocation();
    this.myBit();
  }

  myBit() {
    //MY BIT

    const element = this.cameraInput.nativeElement as HTMLInputElement;
    element.onchange = () => {

      this.loading.turnOn();

      const reader = new FileReader();

      reader.onload = (r: any) => {

        //THIS IS THE ORIGINAL BASE64 STRING AS SNAPPED FROM THE CAMERA
        //THIS IS PROBABLY THE ONE TO UPLOAD BACK TO YOUR DB AS IT'S UNALTERED
        //UP TO YOU, NOT REALLY BOTHERED
        let base64 = r.target.result as string;

        //FIXING ORIENTATION USING NPM PLUGIN fix-orientation
        fixOrientation(base64, { image: true }, (fixed: string, image: any) => {
          //fixed IS THE NEW VERSION FOR DISPLAY PURPOSES
          this.img = fixed;

          this.loading.turnOff();

          //letakkan foto dicanvas
          const prompt = this.alertCtrl.create({
            title: 'Info',
            message: "Lihat Hasil Photo. Setelah Klik Mohon Tunggu Beberapa Saat.",
            buttons: [
              {
                text: 'Ok',
                handler: data => {
                  this.getImages();
                }
              }
            ]
          });
          prompt.present();

        });

      };

      reader.readAsDataURL(element.files[0]);

    };
  }

  loading = (() => {
    let loadMessage: Loading;

    return {
      turnOn: () => {
        loadMessage = this.loadingCtrl.create({
          content: 'Sedang Memproses Gambar dengan QRCode'
        });
        loadMessage.present();
      },
      turnOff: () => loadMessage.dismiss()
    };

  })();

  takingPhoto() {

    if (typeof (this.outlet_name) === "undefined" || this.outlet_name == null || this.outlet_name == '') {
      const alert = this.alertCtrl.create({
        title: 'Info!',
        subTitle: 'Nama Toko Tidak Boleh Kosong.',
        buttons: ['OK']
      });
      alert.present();
      return false;
    }

    if (typeof (this.note) === "undefined" || this.note == null || this.note == '') {
      const alert = this.alertCtrl.create({
        title: 'Info!',
        subTitle: 'Catatan Harus Diisi, jika tidak ada input strip (-) saja.',
        buttons: ['OK']
      });
      alert.present();
      return false;
    }

    //generate QR Code
    this.createCode();
    this.createQRCode();

    this.photo_status = '1';

  }

  ambilPhoto() {
    document.getElementById("getFile").click();
  }

  getLocation() {
    this.geolocation.getCurrentPosition().then(pos => {
      this.latitude = pos.coords.latitude;
      this.longitude = pos.coords.longitude;

      var myLatlng = { lat: pos.coords.latitude, lng: pos.coords.longitude };
      var map = new google.maps.Map(this.mapElement.nativeElement, {
        zoom: 17,
        center: myLatlng,
        zoomControl: false,
        mapTypeControl: false,
        scaleControl: false,
        streetViewControl: false,
        rotateControl: false,
        fullscreenControl: false
      });

      // marker mylocation
      var icon = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';
      var marker = new google.maps.Marker({
        position: myLatlng,
        draggable: true,
        map: map,
        icon: icon
      });

      google.maps.event.addListener(marker, 'dragend', () => {
        this.latitude = marker.getPosition().lat();
        this.longitude = marker.getPosition().lng();

        //find address draggable
        var geocoder = new google.maps.Geocoder;
        var latlng = { lat: this.latitude, lng: this.longitude };
        geocoder.geocode({ 'location': latlng }, function (results, status) {
          var inputElement = <HTMLInputElement>document.getElementById('map-search');
          inputElement.value = results[0].formatted_address;

          var PerubahanLokasi = (document.getElementById('RLocate') as HTMLInputElement).value;
          if (PerubahanLokasi == '1') {
            var inputElements = <HTMLInputElement>document.getElementById('ReLocate');
            inputElements.value = '1';
          }

        });

      });

      //find address fixed

      var geocoder = new google.maps.Geocoder;
      var latlng = { lat: this.latitude, lng: this.longitude };
      geocoder.geocode({ 'location': latlng }, function (results, status) {
        var inputElement = <HTMLInputElement>document.getElementById('map-search');
        inputElement.value = results[0].formatted_address;
      });

    }).catch(err => console.log('Error Here : ', err));
  }

  addForm() {

    if ((typeof (this.latitude) === "undefined" || this.latitude == null || this.latitude == '') || (typeof (this.longitude) === "undefined" || this.longitude == null || this.longitude == '')) {
      const alert = this.alertCtrl.create({
        title: 'Info!',
        subTitle: 'Harus mendapatkan titik lokasi. Pastikan GPS Handphone anda sudah aktif.',
        buttons: ['OK']
      });
      alert.present();
      return false;
    }

    const prompt = this.alertCtrl.create({
      title: 'Info',
      message: "Sebelum isi formulir Tag Lokasi Toko, Pastikan titik lokasi toko sudah tepat.",
      buttons: [
        {
          text: 'Ulangi',
          handler: data => {
            //
          }
        },
        {
          text: 'Ya',
          handler: data => {
            this.tampilkanForm();
          }
        }
      ]
    });
    prompt.present();
  }

  tampilkanForm() {

    this.address = (document.getElementById('map-search') as HTMLInputElement).value;
    document.getElementById('addForm').style.display = '';
    document.getElementById('peta').style.display = 'none';
    document.getElementById('map').style.display = 'none';
    document.getElementById('add').style.display = 'none';
    document.getElementById('maps').style.display = '';
    document.getElementById('tombol_kirim').style.display = '';

    var PerubahanLokasi = (document.getElementById('ReLocate') as HTMLInputElement).value;
    if (PerubahanLokasi == '1') {

      //nol-kan lagi
      this.photo_status = '';
      const prompt = this.alertCtrl.create({
        title: 'Info',
        message: "Anda ada perubahan titik lokasi, kami menyarankan ambil ulang photo",
        buttons: [
          {
            text: 'Ya',
            handler: data => {

              // PWA
              this.ambilPhoto()

            }
          }
        ]
      });
      prompt.present();
    }

  }

  addMaps() {

    var inputElements = <HTMLInputElement>document.getElementById('ReLocate');
    inputElements.value = '0';

    var inputElements = <HTMLInputElement>document.getElementById('RLocate');
    inputElements.value = '1';

    document.getElementById('addForm').style.display = 'none';
    document.getElementById('peta').style.display = '';
    document.getElementById('map').style.display = '';
    document.getElementById('add').style.display = '';
    document.getElementById('maps').style.display = 'none';
    document.getElementById('tombol_kirim').style.display = 'none';
  }

  myChange($event) {
    if (this.outlet_existing) {
      const modal = this.modalCtrl.create(ListoutletPage);
      modal.present();
      modal.onDidDismiss(val => {
        if (val) {
          this.outlet_existing = true;
          this.kode_outlet = val.kdoutlet;
          this.outlet_name = val.nama;
          this.address = val.alamat;
          document.getElementById('kode_outlet').style.display = '';
        } else {
          this.outlet_existing = false;
          document.getElementById('kode_outlet').style.display = 'none';
        }
      });
    } else {
      document.getElementById('kode_outlet').style.display = 'none';
      this.kode_outlet = '';
      this.outlet_name = '';
      this.address = '';
    }
  }

  getImages() {

    var ctx = this.canvasElement1.getContext("2d");
    var img = document.getElementById("take_photo");
    var img2 = document.getElementById("qi_ar_code");
    ctx.drawImage(img, 3, 3);
    ctx.drawImage(img2, 3, 3);

    this.getPath();

  }

  getPath() {
    var canvas = document.getElementById('myCanvas1') as HTMLCanvasElement;;
    var dataURL = canvas.toDataURL("image/jpg");
    this.hidden_data = dataURL;
  }

  createCode() {
    this.createdCode = this.kode_outlet + '#' + this.outlet_name + '#' + this.address + '#' + this.latitude + '#' + this.longitude + '#' + this.note + '#' + this.kode_distributor + '#' + this.kode_sales;
  }

  toDataURL() {
    return new Promise((resolve, reject) => {
      QRCode.toDataURL(this.value,
        {
          version: this.version,
          errorCorrectionLevel: this.errorCorrectionLevel,
          margin: this.margin,
          scale: this.scale,
          width: this.width,
          color: {
            dark: this.colorDark,
            light: this.colorLight
          }
        }, function (err, url) {
          if (err) {
            console.error(err);
            reject(err);
          } else {
            //console.log(url);
            resolve(url);
          }
        })
    });
  }
  toCanvas(canvas) {
    return new Promise((resolve, reject) => {
      QRCode.toCanvas(canvas, this.value, {
        version: this.version,
        errorCorrectionLevel: this.errorCorrectionLevel,
        margin: this.margin,
        scale: this.scale,
        width: this.width,
        color: {
          dark: this.colorDark,
          light: this.colorLight
        }
      }, function (error) {
        if (error) {
          //console.error(error);
          reject(error);
        }
        else {
          //console.log('success!');
          resolve("success");
        }
      })
    });
  }
  renderElement(element) {
    for (let node of this.qrcElement.nativeElement.childNodes) {
      this.renderer.removeChild(this.qrcElement.nativeElement, node);
    }
    this.renderer.appendChild(this.qrcElement.nativeElement, element);
  }
  createQRCode() {
    if (!this.value) {
      return;
    };

    this.value = this.kode_outlet + '#' + this.outlet_name + '#' + this.address + '#' + this.latitude + '#' + this.longitude + '#' + this.note + '#' + this.kode_distributor + '#' + this.kode_sales;
    let element: Element;

    // switch (this.elementType) {
    //console.log(this.elementType)
    switch (this.elementType) {

      case 'canvas':
        console.log('empty canvas');
        break;

      case 'url':
        element = this.renderer.createElement('canvas');
        this.toCanvas(element).then((v) => {
          this.renderElement(element);
        }).catch((e) => {
          console.error(e);
        });
        break;

      case 'img':

        element = this.renderer.createElement('img');
        this.toDataURL().then((v: string) => {
          element.setAttribute("src", v);
          element.setAttribute("id", "qi_ar_code");
          this.renderElement(element);
        }).catch((e) => {
          console.error(e);
        })
        break;

      default:

        element = this.renderer.createElement('img');
        this.toDataURL().then((v: string) => {

          element.setAttribute("src", v);
          this.renderElement(element);
        }).catch((e) => {
          console.error(e);
        })

    }


  }

  metode_upload() {

    if (typeof (this.photo_status) === "undefined" || this.photo_status == null || this.photo_status == '') {
      const alert = this.alertCtrl.create({
        title: 'Info!',
        subTitle: 'Harus Melampirkan Photo.',
        buttons: ['OK']
      });
      alert.present();
      return false;
    }

    this.upload_data('Disarankan', false, true, true, false);


  }

  upload_data(msg, server, whatsapp, s, w) {
    var status = '(' + msg + ')';
    let status_server = server;
    let status_whatsapp = whatsapp;

    let alert = this.alertCtrl.create();
    alert.setTitle('Metode Pengiriman');

    alert.addInput({
      type: 'radio',
      label: 'Server VCI ' + status,
      value: 'server',
      checked: s,
      disabled: false
    });

    alert.addInput({
      type: 'radio',
      label: 'WhatsApp',
      value: 'whatsapp',
      checked: w,
      disabled: true
    });

    alert.addButton('Cancel');
    alert.addButton({
      text: 'OK',
      handler: data => {
        if (data == 'whatsapp') {
          //
        } else if (data == 'server') {
          this.sendToServer();
        }
      }
    });
    alert.present();
  }

  sendToServer() {

    let name = new Date().getTime() + '.jpg';
    this.nama_image = name;

    //jika menggunakan ala web
    this.uploadImageWeb(this.nama_image);

    this.storage.get('user_session_storage').then((res) => {

      let body = {
        'kode_outlet': this.kode_outlet,
        'outlet_name': this.outlet_name,
        'address': this.address,
        'note': this.note,
        'latitude': this.latitude,
        'longitude': this.longitude,
        'images': this.nama_image,
        'marker': res[0].kdsales + ' - ' + res[0].employee_name,
        'kdcabang': res[0].cabang_id,
        'kdsales': this.kode_sales,
        'kddistributor': this.kode_distributor
      };
      this.postPvdr.postData(body, 'outlet/savedata').subscribe((data) => {
        if (data.success) {
          const prompt = this.alertCtrl.create({
            title: 'Info',
            message: "Data Berhasil di kirim.",
            buttons: [
              {
                text: 'Ok',
                handler: data => {
                  this.navCtrl.pop();
                }
              }
            ]
          });
          prompt.present();
        } else {
          const prompt = this.alertCtrl.create({
            title: 'Info',
            message: "Data Gagal Dikirim.",
            buttons: [
              {
                text: 'Ok',
                handler: data => {
                  // this.navCtrl.pop();
                }
              }
            ]
          });
          prompt.present();
        }
      }, error => {
        const prompt = this.alertCtrl.create({
          title: 'Info',
          message: "Tidak konek ke sever, silahkan di coba kembali.",
          buttons: [
            {
              text: 'Ok',
              handler: data => {
                // this.navCtrl.pop();
              }
            }
          ]
        });
        prompt.present();
        return;
      });

    });
  }

  uploadImageWeb(imageName) {

    let body = {
      'namefile': imageName,
      'hisfile': this.hidden_data
    };
    this.postPvdr.postData(body, 'outlet/uploadimages').subscribe((data) => {
      if (data.success) {
        console.log('images sukses di upload')
      } else {
        console.log('images gagal di upload')
      }

    }, error => {
      console.log('error : ', error)
      return;
    });
  }

}
