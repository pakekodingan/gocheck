import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormoutletpwaPage } from './formoutletpwa';

@NgModule({
  declarations: [
    FormoutletpwaPage,
  ],
  imports: [
    IonicPageModule.forChild(FormoutletpwaPage),
  ],
})
export class FormoutletpwaPageModule { }
