import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetaillistingsPage } from './detaillistings';

@NgModule({
  declarations: [
    DetaillistingsPage,
  ],
  imports: [
    IonicPageModule.forChild(DetaillistingsPage),
  ],
})
export class DetaillistingsPageModule {}
