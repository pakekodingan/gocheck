import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewController } from 'ionic-angular';
import { ModalController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { DetailbangetlistingsPage } from '../../pages/detailbangetlistings/detailbangetlistings';
import { Provider } from '../../config/provider';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the DetaillistingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detaillistings',
  templateUrl: 'detaillistings.html',
})
export class DetaillistingsPage {

  kdoutlet: any;
  kdcabang: any;
  storages: String = 'Yes';
  close_page: any;
  bisa_pilih: any;
  view_dist_reg: String = 'No';
  view_dist_nas: String = 'No';

  listDistributor: Array<any>;
  src: any;
  filter: String = 'all';
  first: number;

  constructor(
    public viewCtrl: ViewController,
    public alertCtrl: AlertController,
    public postPvdr: Provider,
    public storage: Storage,
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    public navParams: NavParams) {

    this.kdoutlet = navParams.get("kdoutlet");
    this.kdcabang = navParams.get("kdcabang");
    this.storages = navParams.get("storages");
    this.close_page = navParams.get("close");
    this.bisa_pilih = navParams.get("bisa_pilih");

    this.storage.get('user_session_storage').then((res) => {
      if (res[0].user_level == '-1' || res[0].user_level == '9') {
        this.view_dist_reg = 'No';
        this.view_dist_nas = 'Yes';
      } else {
        this.view_dist_reg = 'Yes';
        this.view_dist_nas = 'No';
      }
    });

  }

  ionViewDidLoad() {
    this.listDistributor = new Array();
    this.getData('', 0, 10, 'all', 'No');
  }

  form_search() {
    const prompt = this.alertCtrl.create({
      title: 'Cari',
      message: "Masukan kata untuk mencari kode atau nama distributor",
      inputs: [
        {
          name: 'src',
          placeholder: 'Apa yang anda cari...'
        },
      ],
      buttons: [
        {
          text: 'Batal',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Cari',
          handler: data => {
            console.log('Saved clicked');
            this.getDistributor(data.src);
          }
        }
      ]
    });
    prompt.present();
  }

  getDistributor(src) {
    if (src) {
      src = src;
    } else {
      src = '';
    }
    this.listDistributor = new Array();
    this.getData(src, 0, 10, 'all', 'No');
  }

  getData(src, first, offset, filter, infinite) {

    this.first = first;
    this.src = src;

    const loader = this.loadingCtrl.create({
      content: "Mohon Menunggu..."
    });
    if (infinite == 'No') {
      loader.present();
    }

    this.storage.get('user_session_storage').then((res) => {
      let body;
      if (this.storages == 'No') {
        body = {
          kdcabang: this.kdcabang,
          first: first,
          offset: offset,
          filter: filter,
          keyword: src,
          user_level: res[0].user_level,
          kdoutlet: this.kdoutlet
        };
      } else if (this.storages == 'Yes') {
        body = {
          kdcabang: res[0].cabang_id,
          first: first,
          offset: offset,
          filter: filter,
          keyword: src,
          user_level: res[0].user_level,
          kdoutlet: res[0].kdoutlet
        };
      }


      this.postPvdr.postData(body, 'outlet/getDistributor').subscribe((data) => {


        if (data.success) {
          loader.dismiss();
          for (let i = 0; i < data.result.length; i++) {

            this.listDistributor.push(
              {
                KdOutlet: data.result[i]['KdOutlet'],
                Nama: data.result[i]['Nama'],
                KdCabang: data.result[i]['KdCabang'],
                KotaToko: data.result[i]['KotaToko']
              });

          }
        } else {
          loader.dismiss();
          if (infinite == 'No') {
            const alert = this.alertCtrl.create({
              title: 'Info',
              subTitle: 'Mohon Maaf Data Tidak Di Temukan',
              message: '',
              buttons: ['OK']
            });
            alert.present();
          }
          return false;
        }

      }, error => {

        loader.dismiss();
        const alert = this.alertCtrl.create({
          title: 'Mohon Maaf',
          subTitle: 'Sedang tidak terkoneksi dengan server. Silahkan coba kembali beberapa saat',
          buttons: ['OK']
        });
        alert.present();
        return false;
      });

    });

  }

  detailbangetlisting(kdoutlet, kdcabang) {
    // const modal = this.modalCtrl.create(DetailbangetlistingsPage, { kdoutlet: kdoutlet, kdcabang: kdcabang });
    // modal.present();
    this.navCtrl.push(DetailbangetlistingsPage, { kdoutlet: kdoutlet, kdcabang: kdcabang, kdsales: '', viewuser: 'Yes', title: '', close: 'No', bisa_pilih: 'No' });
  }

  doRefresh(refresher) {
    this.listDistributor = new Array();
    this.getData('', 0, 10, 'all', 'No');
    setTimeout(() => {
      refresher.complete();
    }, 1000);
  }

  doInfinite(infiniteScroll, src, first, filter) {


    first = first + 10;

    console.log('Begin async operation');

    setTimeout(() => {
      this.getData(src, first, 10, filter, 'Yes');
      infiniteScroll.complete();
    }, 1000);

  }

  close() {
    this.viewCtrl.dismiss();
  }

  pilih_ini(kdoutlet, Nama) {
    this.viewCtrl.dismiss({ kdoutlet: kdoutlet, distributor_name: Nama });
  }

}
