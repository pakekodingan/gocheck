import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailbangetlistingsPage } from './detailbangetlistings';

@NgModule({
  declarations: [
    DetailbangetlistingsPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailbangetlistingsPage),
  ],
})
export class DetailbangetlistingsPageModule {}
