import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewController } from 'ionic-angular';
import { ModalController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { UsersdistributorPage } from '../../pages/usersdistributor/usersdistributor';

import { Provider } from '../../config/provider';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the DetailbangetlistingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detailbangetlistings',
  templateUrl: 'detailbangetlistings.html',
})
export class DetailbangetlistingsPage {

  kdoutlet: any;
  kdcabang: any;
  kdsales: any;
  viewuser: any;
  title: any;
  listOutlet: Array<any>;
  src: any;
  filter: String = 'all';
  first: number;
  close_page: any;
  bisa_pilih: any;

  constructor(
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public postPvdr: Provider,
    public storage: Storage,
    public navParams: NavParams) {
    this.kdoutlet = navParams.get("kdoutlet");
    this.kdcabang = navParams.get("kdcabang");
    this.kdsales = navParams.get("kdsales");
    this.viewuser = navParams.get("viewuser");
    this.title = navParams.get("title");
    this.close_page = navParams.get("close");
    this.bisa_pilih = navParams.get("bisa_pilih");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailbangetlistingsPage');
    this.listOutlet = new Array();
    this.getData('', 0, 10, 'all', 'No');
  }

  form_search() {
    const prompt = this.alertCtrl.create({
      title: 'Cari',
      message: "Masukan kata untuk mencari kode atau nama toko",
      inputs: [
        {
          name: 'src',
          placeholder: 'Apa yang anda cari...'
        },
      ],
      buttons: [
        {
          text: 'Batal',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Cari',
          handler: data => {
            console.log('Saved clicked');
            this.getListOutletsDistributor(data.src);
          }
        }
      ]
    });
    prompt.present();
  }

  getListOutletsDistributor(src) {
    this.listOutlet = new Array();
    if (src) {
      src = src;
    } else {
      src = '';
    }
    this.getData(src, 0, 10, 'all', 'No');
  }

  getData(src, first, offset, filter, infinite) {

    this.first = first;
    this.src = src;

    const loader = this.loadingCtrl.create({
      content: "Mohon Menunggu..."
    });
    if (infinite == 'No') {
      loader.present();
    }

    this.storage.get('user_session_storage').then((res) => {
      let body = {
        kdcabang: this.kdcabang,
        first: first,
        offset: offset,
        filter: filter,
        keyword: src,
        kdsales: this.kdsales,
        kdoutlet: this.kdoutlet,
        user_level: res[0].user_level,
        username: res[0].username
      };

      this.postPvdr.postData(body, 'outlet/getDetailOutletDistributor2').subscribe((data) => {


        if (data.success) {
          loader.dismiss();
          for (let i = 0; i < data.result.length; i++) {
            this.listOutlet.push(
              {
                KdOutlet: data.result[i]['KdOutlet'],
                Nama: data.result[i]['Nama'],
                KotaToko: data.result[i]['KotaToko'],
                statuz_pending: data.result[i]['statuz_pending'],
                statuz_valid: data.result[i]['statuz_valid']
              });

          }
        } else {
          loader.dismiss();
          if (infinite == 'No') {
            const alert = this.alertCtrl.create({
              title: 'Info',
              subTitle: 'Mohon Maaf Data Tidak Di Temukan',
              message: '',
              buttons: ['OK']
            });
            alert.present();
          }
          return false;
        }

      }, error => {

        loader.dismiss();
        const alert = this.alertCtrl.create({
          title: 'Mohon Maaf',
          message: 'Sedang tidak terkoneksi dengan server. Silahkan coba kembali beberapa saat',
          buttons: ['OK']
        });
        alert.present();
        return false;

      });

    });
  }

  doRefresh(refresher) {
    this.listOutlet = new Array();
    this.getData('', 0, 10, 'all', 'No');
    setTimeout(() => {
      refresher.complete();
    }, 1000);
  }

  doInfinite(infiniteScroll, src, first, filter) {

    first = first + 10;

    setTimeout(() => {
      this.getData(src, first, 10, filter, 'Yes');
      infiniteScroll.complete();
    }, 1000);

  }

  salesman(kdoutlet, kdcabang) {
    // const modal = this.modalCtrl.create(UsersdistributorPage, { kdoutlet: kdoutlet, kdcabang: kdcabang });
    // modal.present();
    this.navCtrl.push(UsersdistributorPage, { kdoutlet: kdoutlet, kdcabang: kdcabang, performa: 'No', close: 'No', bisa_pilih: 'No', reset_password: 'Yes' });
  }

  close() {
    this.viewCtrl.dismiss();
  }

  pilih_ini(kdoutlet, Nama) {
    this.viewCtrl.dismiss({ kdoutlet: kdoutlet, Nama: Nama });
  }

}
