import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { Provider } from '../../config/provider';
import { Storage } from '@ionic/storage';

declare var google;
/**
 * Generated class for the PetanooPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-petanoo',
  templateUrl: 'petanoo.html',
})
export class PetanooPage {

  @ViewChild('map') mapElement: ElementRef;

  lat: any;
  lng: any;
  name_noo: any;

  constructor(
    public navCtrl: NavController,
    public storage: Storage,
    public postPvdr: Provider,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public navParams: NavParams) {
    this.lat = parseFloat(navParams.get("lat"));
    this.lng = parseFloat(navParams.get("lng"));
    this.name_noo = navParams.get("name_noo");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PetanooPage');
    // this.initMap();
    this.radius_setup(this.lat, this.lng);
  }

  radius_setup(x, y) {
    this.outlet_radius(x, y, 3000);
  }

  outlet_radius(x, y, r) {
    var rad = parseFloat(r)  //meters
    var theLat = parseFloat(x);  //decimal degrees
    var theLng = parseFloat(y);  //decimal degrees

    var yMin = theLat - (0.0000065 * rad);
    var xMin = theLng - (-0.0000065 * rad);

    var yMax = theLat + (0.0000065 * rad);
    var xMax = theLng + (-0.0000065 * rad);

    this.allRadiusRectangleOutletsales(yMin, yMax, xMin, xMax, x, y, r);
  }

  allRadiusRectangleOutletsales(j, k, l, m, x, y, r) {
    let body = {
      j: j,
      k: k,
      l: l,
      m: m
    };

    this.postPvdr.postData(body, 'outlet/getAnalyticsNoo').subscribe((data) => {
      var icon;
      var alloutlet = [];


      if (data.success) {

        for (let i = 0; i < data.result.length; i++) {

          //pilih icon
          icon = 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-red.png';

          alloutlet.push({
            lat: data.result[i]['Lat'],
            lng: data.result[i]['Lng'],
            outletname: data.result[i]['OutletName'],
            namasalesman: '',
            lastVisit: '',
            alamattoko: data.result[i]['Address'],
            icon: icon
          }
          );

        }

        this.circle(x, y, r, alloutlet);

      } else {
        this.circle(x, y, r, '');
        const alert = this.alertCtrl.create({
          title: 'Maaf',
          subTitle: 'Di Area ini tidak ada toko kita.',
          buttons: ['OK']
        });
        alert.present();
      }

    }, error => {

      const alert = this.alertCtrl.create({
        title: 'Opps I am Sorry',
        subTitle: 'Not Connected To Server',
        buttons: ['OK']
      });
      alert.present();
    });
  }

  circle(x, y, r, alloutlet) {


    var latitude;
    var longitude;



    var myLatlng = { lat: x, lng: y };
    var map = new google.maps.Map(this.mapElement.nativeElement, {
      zoom: 13,
      center: myLatlng,
      rotateControl: false,
      fullscreenControl: false,
      mapTypeControl: false,
      scaleControl: false,
      streetViewControl: true,
      streetViewControlOptions: {
        position: google.maps.ControlPosition.LEFT_TOP
      },
      zoomControl: true,
      zoomControlOptions: {
        position: google.maps.ControlPosition.LEFT_TOP
      }
    });

    var tanda = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';
    //market khusus si NOO nya sendiri neglik
    var marker = new google.maps.Marker({
      position: myLatlng,
      animation: google.maps.Animation.DROP,
      map: map,
      icon: tanda
    });

    var contentStringz =
      '<h4>' + this.name_noo + '&nbsp;&nbsp;&nbsp;&nbsp;</h4>' +
      '<div>' +
      '<p>Toko Potensial&nbsp;&nbsp;&nbsp;&nbsp;' +
      '</p>' +
      '</div>';

    marker.content = contentStringz;

    var infoWindow = new google.maps.InfoWindow();
    google.maps.event.addListener(marker, 'click', function () {
      infoWindow.setContent(this.content);
      infoWindow.open(this.getMap(), this);
    });


    for (let i = 0; i < alloutlet.length; i++) {
      latitude = parseFloat(alloutlet[i]['lat']);
      longitude = parseFloat(alloutlet[i]['lng']);
      var latlng = { lat: latitude, lng: longitude };

      var marker = new google.maps.Marker({
        position: latlng,
        map: map,
        icon: alloutlet[i]['icon']
      });

      var contentString =
        '<h4>' + alloutlet[i]['outletname'] + '&nbsp;&nbsp;&nbsp;&nbsp;</h4>' +
        '<div>' +
        '<p>' + alloutlet[i]['alamattoko'] + '&nbsp;&nbsp;&nbsp;&nbsp;' +
        '</p>' +
        '</div>';

      marker.content = contentString;

      var infoWindow = new google.maps.InfoWindow();
      google.maps.event.addListener(marker, 'click', function () {
        infoWindow.setContent(this.content);
        infoWindow.open(this.getMap(), this);
      });

    }


    var cityCircle = new google.maps.Circle({
      strokeColor: '#FF0000',
      strokeOpacity: 0.8,
      strokeWeight: 2,
      fillColor: '#FF0000',
      fillOpacity: 0.35,
      map: map,
      center: myLatlng,
      radius: r
    });


    setTimeout(() => {
      const toast = this.toastCtrl.create({
        message: 'Toko Petonsial Dalam Radius 3 Km',
        showCloseButton: true,
        closeButtonText: 'Ok'
      });
      toast.present();
    }, 1000)

  }

  initMap() {

    var myLatlng = { lat: this.lat, lng: this.lng };
    var map = new google.maps.Map(this.mapElement.nativeElement, {
      zoom: 13,
      center: myLatlng,
      zoomControl: false,
      mapTypeControl: false,
      scaleControl: false,
      streetViewControl: false,
      rotateControl: false,
      fullscreenControl: false
    });

    //marker
    var marker = new google.maps.Marker({
      position: myLatlng,
      animation: google.maps.Animation.DROP,
      map: map
    });
  }

}
