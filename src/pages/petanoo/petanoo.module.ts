import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PetanooPage } from './petanoo';

@NgModule({
  declarations: [
    PetanooPage,
  ],
  imports: [
    IonicPageModule.forChild(PetanooPage),
  ],
})
export class PetanooPageModule {}
