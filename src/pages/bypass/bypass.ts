import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { Provider } from '../../config/provider';
import { Storage } from '@ionic/storage';
import { MainPage } from '../../pages/main/main';
/**
 * Generated class for the BypassPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-bypass',
  templateUrl: 'bypass.html',
})
export class BypassPage {

  listUsers: Array<any>;
  src: any;
  filter: String = 'all';
  first: number;

  constructor(
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    public postPvdr: Provider,
    public alertCtrl: AlertController,
    public storage: Storage,
    public loadingCtrl: LoadingController,
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UsersdistributorPage');
    this.listUsers = new Array();
    this.getData('', 0, 25, 'No');
  }

  form_search() {
    const prompt = this.alertCtrl.create({
      title: 'Cari',
      message: "Masukan kata untuk mencari nama atau username",
      inputs: [
        {
          name: 'src',
          placeholder: 'Apa yang anda cari...'
        },
      ],
      buttons: [
        {
          text: 'Batal',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Cari',
          handler: data => {
            console.log('Saved clicked');
            this.getListUserDistributor(data.src);
          }
        }
      ]
    });
    prompt.present();
  }

  getListUserDistributor(src) {
    this.listUsers = new Array();
    if (src) {
      src = src;
    } else {
      src = '';
    }
    this.getData(src, 0, 25, 'No');
  }

  getData(src, first, offset, infinite) {

    this.first = first;
    this.src = src;

    const loader = this.loadingCtrl.create({
      content: "Mohon Menunggu..."
    });
    if (infinite == 'No') {
      loader.present();
    }

    this.storage.get('user_session_storage').then((res) => {
      let body = {
        first: first,
        offset: offset,
        keyword: src
      };

      this.postPvdr.postData(body, 'outlet/getListUserDistributorAll').subscribe((data) => {


        if (data.success) {
          loader.dismiss();
          for (let i = 0; i < data.result.length; i++) {
            this.listUsers.push(
              {
                username: data.result[i]['username'],
                name: data.result[i]['nama'],
                password: data.result[i]['password'],
                outlet: data.result[i]['outlet'],
                jabatan: data.result[i]['jabatan']
              });

          }
        } else {
          loader.dismiss();
          if (infinite == 'No') {
            const alert = this.alertCtrl.create({
              title: 'Info',
              subTitle: 'Mohon Maaf Data Tidak Di Temukan',
              message: '',
              buttons: ['OK']
            });
            alert.present();
          }
          return false;
        }

      }, error => {

        loader.dismiss();
        const alert = this.alertCtrl.create({
          title: 'Mohon Maaf',
          subTitle: 'Sedang tidak terkoneksi dengan server. Silahkan coba kembali beberapa saat',
          buttons: ['OK']
        });
        alert.present();
        return false;

      });

    });
  }

  doRefresh(refresher) {
    this.listUsers = new Array();
    this.getData('', 0, 25, 'No');
    setTimeout(() => {
      refresher.complete();
    }, 1000);
  }

  doInfinite(infiniteScroll, src, first) {

    first = first + 10;

    console.log('Begin async operation');

    setTimeout(() => {
      this.getData(src, first, 25, 'Yes');
      infiniteScroll.complete();
    }, 1000);

  }

  bypass(username, password) {

    let body = {
      username: username,
      password: password,
      platform: 'M',
      action: 'cekLogin'
    };

    this.postPvdr.postData(body, 'login/bypass').subscribe((data) => {


      if (data.success) {
        this.storage.set('user_session_storage', data.result);
        this.navCtrl.setRoot(MainPage);
      } else {
        const alert = this.alertCtrl.create({
          title: 'Mohon Maaf',
          subTitle: 'Anda Tidak Mempunyai Akses.',
          buttons: ['OK']
        });
        alert.present();
      }

    }, error => {
      const alert = this.alertCtrl.create({
        title: 'Mohon Maaf',
        subTitle: 'Sedang tidak terkoneksi dengan server. Silahkan coba kembali beberapa saat',
        buttons: ['OK']
      });
      alert.present();
    });
  }

}
