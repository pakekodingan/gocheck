import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BypassPage } from './bypass';

@NgModule({
  declarations: [
    BypassPage,
  ],
  imports: [
    IonicPageModule.forChild(BypassPage),
  ],
})
export class BypassPageModule {}
