import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { Provider } from '../../config/provider';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the ListoutletPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-listoutlet',
  templateUrl: 'listoutlet.html',
})
export class ListoutletPage {

  listOutlet: any;

  constructor(
    public navCtrl: NavController,
    public postPvdr: Provider,
    public storage: Storage,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListoutletPage');
    this.form_search();
  }

  form_search() {
    const prompt = this.alertCtrl.create({
      title: 'Cari',
      message: "Masukan kata untuk mencari toko",
      inputs: [
        {
          name: 'src',
          placeholder: 'Apa yang anda cari...'
        },
      ],
      buttons: [
        {
          text: 'Batal',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Cari',
          handler: data => {
            console.log('Saved clicked');
            this.getOutlets(data.src);
          }
        }
      ]
    });
    prompt.present();
  }

  getOutlets(src) {
    if (src) {
      src = src;
    } else {
      src = '';
    }
    this.getData(src);
  }

  getData(src) {

    const loader = this.loadingCtrl.create({
      content: "Mohon Menunggu..."
    });
    loader.present();


    this.storage.get('user_session_storage').then((res) => {

      let body = {
        jenis_outlet: res[0].jenis_outlet,
        kdoutlet: res[0].kdoutlet,
        kdcabang: res[0].cabang_id,
        keyword: src
      };

      this.postPvdr.postData(body, 'outlet/getoutlet').subscribe((data) => {
        console.log(data)
        if (data.success) {
          loader.dismiss();
          this.listOutlet = [];
          for (let i = 0; i < data.result.length; i++) {
            
            this.listOutlet.push(
              {
                kdoutlet: data.result[i]['KdOutlet'],
                nama: data.result[i]['Nama'],
                alamat: data.result[i]['Alm1Toko'],
                statuz_pending: data.result[i]['statuz_pending'],
                statuz_valid: data.result[i]['statuz_valid']
              });

          }
        } else {
          loader.dismiss();
          const alert = this.alertCtrl.create({
            title: 'Info',
            subTitle: 'Mohon Maaf Data Tidak Di Temukan',
            message: '',
            buttons: ['OK']
          });
          alert.present();
          return false;
        }

      }, error => {

        loader.dismiss();
        const alert = this.alertCtrl.create({
          title: 'Mohon Maaf',
          subTitle: 'Sedang tidak terkoneksi dengan server. Silahkan coba kembali beberapa saat',
          buttons: ['OK']
        });
        alert.present();
        return false;
      });

    });
  }

  pilih_outlet(kdoutlet, nama, alamat) {
    this.viewCtrl.dismiss({ kdoutlet: kdoutlet, nama: nama, alamat: alamat })
  }

  close() {
    this.viewCtrl.dismiss();
  }

}
