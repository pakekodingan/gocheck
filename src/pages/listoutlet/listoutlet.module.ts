import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListoutletPage } from './listoutlet';

@NgModule({
  declarations: [
    ListoutletPage,
  ],
  imports: [
    IonicPageModule.forChild(ListoutletPage),
  ],
})
export class ListoutletPageModule {}
