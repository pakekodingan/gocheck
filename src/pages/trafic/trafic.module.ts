import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TraficPage } from './trafic';

@NgModule({
  declarations: [
    TraficPage,
  ],
  imports: [
    IonicPageModule.forChild(TraficPage),
  ],
})
export class TraficPageModule {}
