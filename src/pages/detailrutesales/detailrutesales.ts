import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewController } from 'ionic-angular';
import { ModalController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';

import { Provider } from '../../config/provider';
import { Storage } from '@ionic/storage';

import { UsersdistributorPage } from '../../pages/usersdistributor/usersdistributor';
/**
 * Generated class for the DetailrutesalesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detailrutesales',
  templateUrl: 'detailrutesales.html',
})
export class DetailrutesalesPage {

  kdoutlet: any;
  kdcabang: any;
  kdsales: any;
  title: any;
  listOutlet: Array<any>;
  src: any;
  first: number;

  hak_swtiching: any;

  constructor(
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public postPvdr: Provider,
    public storage: Storage,
    public navParams: NavParams
  ) {
    this.kdoutlet = navParams.get("kdoutlet");
    this.kdcabang = navParams.get("kdcabang");
    this.kdsales = navParams.get("kdsales");
    this.title = navParams.get("title");

    this.storage.get('user_session_storage').then((res) => {
      if ((res[0].user_level == '-1' && res[0].developer == '1') || res[0].user_level == '2') {
        this.hak_swtiching = 'Yes';
      } else {
        this.hak_swtiching = 'No';
      }
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailrutesalesPage');
    this.listOutlet = new Array();
    this.getData('', 0, 10, 'No');
  }

  form_search() {
    const prompt = this.alertCtrl.create({
      title: 'Cari',
      message: "Masukan kata untuk mencari kode atau nama toko",
      inputs: [
        {
          name: 'src',
          placeholder: 'Apa yang anda cari...'
        },
      ],
      buttons: [
        {
          text: 'Batal',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Cari',
          handler: data => {
            console.log('Saved clicked');
            this.getListOutletsDistributor(data.src);
          }
        }
      ]
    });
    prompt.present();
  }

  getListOutletsDistributor(src) {
    this.listOutlet = new Array();
    if (src) {
      src = src;
    } else {
      src = '';
    }
    this.getData(src, 0, 10, 'No');
  }

  getData(src, first, offset, infinite) {

    this.first = first;
    this.src = src;

    const loader = this.loadingCtrl.create({
      content: "Mohon Menunggu..."
    });
    if (infinite == 'No') {
      loader.present();
    }

    this.storage.get('user_session_storage').then((res) => {
      let body = {
        kdcabang: this.kdcabang,
        first: first,
        offset: offset,
        filter: '',
        keyword: src,
        kdsales: this.kdsales,
        kdoutlet: this.kdoutlet,
        user_level: res[0].user_level,
        username: res[0].username
      };

      this.postPvdr.postData(body, 'outlet/getDetailOutletDistributor2').subscribe((data) => {


        if (data.success) {
          loader.dismiss();
          for (let i = 0; i < data.result.length; i++) {
            this.listOutlet.push(
              {
                KdOutlet: data.result[i]['KdOutlet'],
                Nama: data.result[i]['Nama'],
                KotaToko: data.result[i]['KotaToko'],
                statuz_pending: data.result[i]['statuz_pending'],
                statuz_valid: data.result[i]['statuz_valid']
              });

          }
        } else {
          loader.dismiss();
          if (infinite == 'No') {
            const alert = this.alertCtrl.create({
              title: 'Info',
              subTitle: 'Mohon Maaf Data Tidak Di Temukan',
              message: '',
              buttons: ['OK']
            });
            alert.present();
          }
          return false;
        }

      }, error => {

        loader.dismiss();
        const alert = this.alertCtrl.create({
          title: 'Mohon Maaf',
          message: 'Sedang tidak terkoneksi dengan server. Silahkan coba kembali beberapa saat',
          buttons: ['OK']
        });
        alert.present();
        return false;

      });

    });
  }

  doRefresh(refresher) {
    this.listOutlet = new Array();
    this.getData('', 0, 10, 'No');
    setTimeout(() => {
      refresher.complete();
    }, 1000);
  }

  doInfinite(infiniteScroll, src, first) {

    first = first + 10;

    setTimeout(() => {
      this.getData(src, first, 10, 'Yes');
      infiniteScroll.complete();
    }, 1000);

  }

  switching(kdoutlet, kdcabang, kddistributor) {

    const confirm = this.alertCtrl.create({
      title: 'Info',
      message: 'Apakah anda yakin akan melakukan swtiching toko ini dengan sales lain?',
      buttons: [
        {
          text: 'Batal',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Ya',
          handler: () => {

            const modal = this.modalCtrl.create(UsersdistributorPage, { kdoutlet: kddistributor, kdcabang: kdcabang, performa: 'No', close: 'Yes', bisa_pilih: 'Yes' });
            modal.present();
            modal.onDidDismiss(val => {
              if (val) {
                if (val.kdsales == '') {
                  const alert = this.alertCtrl.create({
                    title: 'Mohon Maaf',
                    message: 'Pilhan semua tidak berlaku.',
                    buttons: ['OK']
                  });
                  alert.present();
                  return false;
                } else {
                  if (this.kdsales == val.kdsales) {
                    const alert = this.alertCtrl.create({
                      title: 'Mohon Maaf',
                      message: 'Tidak Bisa Swtich Dengan Sales Yang Sama.',
                      buttons: ['OK']
                    });
                    alert.present();
                    return false;
                  } else {
                    this.switch(kdoutlet, val.kdsales, this.kdsales, kdcabang, kddistributor);
                  }
                }
              } else {
                //coding if else or empty
              }
            });

          }
        }
      ]
    });
    confirm.present();


  }

  switch(kdoutlet, kdsales, kodesales, kdcabang, kddistributor) {
    const loader = this.loadingCtrl.create({
      content: "Proses Swtiching..."
    });
    loader.present();


    this.storage.get('user_session_storage').then((res) => {
      let body = {
        kdcabang: kdcabang,
        kdsales: kdsales,
        kodesales: kodesales,
        kdoutlet: kdoutlet,
        kddistributor: kddistributor,
        username: res[0].username
      };

      this.postPvdr.postData(body, 'outlet/swtichingRuteSales').subscribe((data) => {


        if (data.success) {
          loader.dismiss();
          const confirm = this.alertCtrl.create({
            title: 'Info',
            message: 'Swtiching Berhasil',
            buttons: [
              {
                text: 'Oke',
                handler: () => {
                  this.ionViewDidLoad();
                }
              }
            ]
          });
          confirm.present();
        } else {
          loader.dismiss();

          const alert = this.alertCtrl.create({
            title: 'Info',
            subTitle: 'Swtiching Gagal',
            message: '',
            buttons: ['OK']
          });
          alert.present();

          return false;
        }

      }, error => {

        loader.dismiss();
        const alert = this.alertCtrl.create({
          title: 'Mohon Maaf',
          message: 'Sedang tidak terkoneksi dengan server. Silahkan coba kembali beberapa saat',
          buttons: ['OK']
        });
        alert.present();
        return false;

      });

    });
  }

}
