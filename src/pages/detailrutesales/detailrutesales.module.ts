import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailrutesalesPage } from './detailrutesales';

@NgModule({
  declarations: [
    DetailrutesalesPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailrutesalesPage),
  ],
})
export class DetailrutesalesPageModule {}
