import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { Provider } from '../../config/provider';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the LogsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-logs',
  templateUrl: 'logs.html',
})
export class LogsPage {

  listLogs: Array<any>;
  src: any;
  first: number;

  constructor(
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    public postPvdr: Provider,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public storage: Storage,
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LogsPage');
    this.listLogs = new Array();
    this.getData('', 0, 10, 'No');
  }

  form_search() {
    const prompt = this.alertCtrl.create({
      title: 'Cari',
      message: "Masukan kata untuk mencari username",
      inputs: [
        {
          name: 'src',
          placeholder: 'Apa yang anda cari...'
        },
      ],
      buttons: [
        {
          text: 'Batal',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Cari',
          handler: data => {
            console.log('Saved clicked');
            this.getListLogs(data.src);
          }
        }
      ]
    });
    prompt.present();
  }


  getListLogs(src) {
    if (src) {
      src = src;
    } else {
      src = '';
    }
    this.listLogs = new Array();
    this.getData(src, 0, 10, 'No');
  }

  getData(src, first, offset, infinite) {

    this.first = first;
    this.src = src;


    const loader = this.loadingCtrl.create({
      content: "Mohon Menunggu..."
    });

    if (infinite == 'No') {
      loader.present();
    }


    this.storage.get('user_session_storage').then((res) => {
      let body = {
        kdcabang: res[0].cabang_id,
        first: first,
        offset: offset,
        filter: '',
        keyword: src,
        user_level: res[0].user_level,
        kdoutlet: res[0].kdoutlet
      };

      this.postPvdr.postData(body, 'outlet/getLogs').subscribe((data) => {


        if (data.success) {
          loader.dismiss();
          for (let i = 0; i < data.result.length; i++) {
            var platform;
            if (data.result[i]['platform'] == 'M') {
              platform = 'Mobile';
            } else {
              platform = 'Browser';
            }
            this.listLogs.push(
              {
                username: data.result[i]['username'],
                note: data.result[i]['note'],
                platform: platform,
                date_log: data.result[i]['date_log']
              });
          }
        } else {
          loader.dismiss();
          if (infinite == 'No') {
            const alert = this.alertCtrl.create({
              title: 'Info',
              subTitle: 'Mohon Maaf Data Tidak Di Temukan',
              message: '',
              buttons: ['OK']
            });
            alert.present();
          }
          return false;
        }

      }, error => {
        loader.dismiss();
        const alert = this.alertCtrl.create({
          title: 'Mohon Maaf',
          subTitle: 'Sedang tidak terkoneksi dengan server. Silahkan coba kembali beberapa saat',
          buttons: ['OK']
        });
        alert.present();
        return false;
      });

    });

  }

  doRefresh(refresher) {
    this.listLogs = new Array();
    this.getData('', 0, 10, 'No');
    setTimeout(() => {
      refresher.complete();
    }, 1000);
  }

  doInfinite(infiniteScroll, src, first, filter) {

    first = first + 10;

    console.log('Begin async operation');

    setTimeout(() => {
      this.getData(src, first, 10, 'Yes');
      infiniteScroll.complete();
    }, 1000);

  }

}
