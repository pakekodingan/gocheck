import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ModalController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { App } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { Provider } from '../../config/provider';
import { FormoutletPage } from '../../pages/formoutlet/formoutlet';
import { FormoutletpwaPage } from '../../pages/formoutletpwa/formoutletpwa';
import { HomePage } from '../../pages/home/home';
import { HistoryPage } from '../../pages/history/history';
import { SettingsPage } from '../../pages/settings/settings';
import { AdminPage } from '../../pages/admin/admin';
import { ListingPage } from '../../pages/listing/listing';
import { LogsPage } from '../../pages/logs/logs';
import { PerformaPage } from '../../pages/performa/performa';
import { DetaillistingsPage } from '../../pages/detaillistings/detaillistings';
import { ActivitiesPage } from '../../pages/activities/activities';
import { PetatokoPage } from '../../pages/petatoko/petatoko';
import { DetailbangetlistingsPage } from '../../pages/detailbangetlistings/detailbangetlistings';
import { UsersdistributorPage } from '../../pages/usersdistributor/usersdistributor';
import { AnalyticsPage } from '../../pages/analytics/analytics';
import { BypassPage } from '../../pages/bypass/bypass';
import { TraficPage } from '../../pages/trafic/trafic';
import { NoolistPage } from '../../pages/noolist/noolist';
import { RutesalesPage } from '../../pages/rutesales/rutesales';
import { TokoplusPage } from '../../pages/tokoplus/tokoplus';
import { LoginPage } from '../../pages/login/login';

@IonicPage()
@Component({
  selector: 'page-main',
  templateUrl: 'main.html',
})
export class MainPage {

  name: any;
  jabatan_name: any;
  level: any;
  corp: any;
  jenis_outlet: any;
  developer: any;
  version: String = '150320';
  update: String = 'No';

  constructor(
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    public geolocation: Geolocation,
    public storage: Storage,
    public app: App,
    public postPvdr: Provider,
    private statusBar: StatusBar,
    public alertCtrl: AlertController,
    public barcodeScanner: BarcodeScanner,
    private locationAccuracy: LocationAccuracy,
    private androidPermissions: AndroidPermissions,
    public navParams: NavParams) {
    // set status bar to white
    this.statusBar.backgroundColorByHexString('#633ce0');

    this.storage.get('user_session_storage').then((res) => {
      this.name = res[0].employee_name;
      this.jabatan_name = res[0].jabatan_name;
      this.level = res[0].user_level;
      this.corp = res[0].distributor_name;
      this.developer = res[0].developer;
      this.jenis_outlet = res[0].jenis_outlet;
    });

    this.updates();

    this.checkGPSPermission();
  }

  ionViewDidLoad() {

  }

  updates() {
    this.storage.get('user_session_storage').then((res) => {
      let body = {
        username: res[0].username
      };

      this.postPvdr.postData(body, 'login/cek_update_home').subscribe((data) => {
        console.log('update version : ', data)
        if (data.success) {
          if (data.result != this.version) {
            this.update = 'Yes';
          } else {
            this.update = 'No';
          }

          //jika sudah tidak aktif maka auto logout
          if (data.active == 'T') {
            this.storage.clear();
            this.app.getRootNav().setRoot(LoginPage);
          }

        } else {
          this.update = 'No';
        }

      }, error => {
        this.update = 'No';
      });
    });
  }

  checkGPSPermission() {
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
      result => {
        console.log('status harus melewati permission : ', result.hasPermission);
        if (result.hasPermission) {
          this.askToTurnOnGPS();
        } else {
          this.requestGPSPermission();
        }
      },
      err => {
        console.log(err);
      }
    );

  }

  requestGPSPermission() {
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {
      if (canRequest) {
        //your code
      } else {
        this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
          .then(
            () => {
              this.askToTurnOnGPS();
            },
            error => {
              console.log('requestPermission Error requesting location permissions ' + error)
            }
          );
      }
    });
  }

  askToTurnOnGPS() {
    this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
      () => {
        //your code if success
      },
      error =>
        console.log('Error requesting location permissions ' + JSON.stringify(error))
    );
  }

  scanCode() {
    this.storage.get('user_session_storage').then((res) => {

      if (res[0].user_level == '-1' || res[0].user_level == '2') {
        this.barcodeScanner.scan().then(barcodeData => {

          var strArray = barcodeData.text.split("#");
          console.log('test : ', strArray);
          const modal = this.modalCtrl.create(HomePage, { lat: strArray[3], lng: strArray[4], outlet: strArray[0], name: strArray[1], address: strArray[2], note: strArray[5], kode_distributor: strArray[6], kode_sales: strArray[7] });
          modal.present();


        }).catch(err => {
          console.log('Error', err);
        });
      } else {
        const alert = this.alertCtrl.create({
          title: 'Mohon Maaf',
          subTitle: 'Anda Tidak Mempunyai Akses.',
          buttons: ['OK']
        });
        alert.present();
      }
    });

  }

  // apps
  // pin_outlets() {
  //   this.navCtrl.push(FormoutletPage)
  // }

  //pwa
  pin_outlets() {
    const alert = this.alertCtrl.create({
      title: 'Info',
      subTitle: 'Pastikan GPS Anda sudah Aktif',
      buttons: ['OK']
    });
    alert.present();
    this.navCtrl.push(FormoutletpwaPage)
  }

  history() {
    this.navCtrl.push(HistoryPage)
  }

  performa() {
    this.storage.get('user_session_storage').then((res) => {
      if (res[0].jenis_outlet == 'distributor') {
        if (this.level == '4') {
          this.navCtrl.push(PerformaPage)
        } else if (this.level == '2' || this.level == '10') {
          this.navCtrl.push(UsersdistributorPage, { kdoutlet: res[0].kdoutlet, kdcabang: res[0].cabang_id, performa: 'Yes', close: 'No', bisa_pilih: 'No', reset_password: 'No' });
        } else {
          this.navCtrl.push(DetaillistingsPage, { kdoutlet: '', kdcabang: '', storages: 'Yes', close: 'No', bisa_pilih: 'No' });
        }
      } else {
        const alert = this.alertCtrl.create({
          title: 'Mohon Maaf',
          subTitle: 'Anda Tidak Mempunyai Akses.',
          buttons: ['OK']
        });
        alert.present();
      }
    });

  }

  settings() {
    this.navCtrl.push(SettingsPage)
  }

  admin_page() {
    this.navCtrl.push(AdminPage)

  }

  listing() {
    // this.navCtrl.push(ListingPage);
    this.storage.get('user_session_storage').then((res) => {
      //2 dan 10
      if (res[0].user_level == '2' || res[0].user_level == '10') {
        this.navCtrl.push(DetailbangetlistingsPage, { kdoutlet: res[0].kdoutlet, kdcabang: res[0].cabang_id, kdsales: '', viewuser: 'Yes', title: '', close: 'No', bisa_pilih: 'No' });
      } else {
        this.navCtrl.push(DetaillistingsPage, { kdoutlet: '', kdcabang: '', storages: 'Yes', close: 'No', bisa_pilih: 'No' })
      }
    });
  }

  activities() {
    this.navCtrl.push(ActivitiesPage);
    // const alert = this.alertCtrl.create({
    //   title: 'Mohon Maaf',
    //   subTitle: 'Masih Dalam Tahap Pengembangan',
    //   buttons: ['OK']
    // });
    // alert.present();
  }

  allOutlets() {
    this.storage.get('user_session_storage').then((res) => {

      //admin dan kedist
      if (res[0].user_level == '2' || res[0].user_level == '10') {
        this.navCtrl.push(
          PetatokoPage, {
          kdcabang: res[0].cabang_id,
          kdoutlet: res[0].kdoutlet,
          kdsales: '',
          distributor_name: res[0].distributor_name,
          title: res[0].distributor_name,
          sales_name: 'Semua',
          legend_type: '2'
        });
        //regional
      } else if (res[0].user_level == '6') {
        this.navCtrl.push(
          PetatokoPage, {
          kdcabang: res[0].cabang_id,
          kdoutlet: '',
          kdsales: '',
          distributor_name: 'Semua Distributor Regional',
          title: 'Semua Distributor Regional',
          sales_name: 'Semua',
          legend_type: '2'
        });
      } else if (res[0].user_level == '-1' || res[0].user_level == '9') {
        this.navCtrl.push(
          PetatokoPage, {
          kdcabang: '',
          kdoutlet: '',
          kdsales: '',
          distributor_name: 'Semua Distributor Nasional',
          title: 'Semua Distributor Nasional',
          sales_name: 'Semua',
          legend_type: '2'
        });
      }



    });
  }

  Logs() {
    this.navCtrl.push(LogsPage);
  }

  bypass() {
    this.navCtrl.push(BypassPage);
  }

  traffic() {
    this.navCtrl.push(TraficPage);
  }

  luar_rute() {
    this.storage.get('user_session_storage').then((res) => {

      this.navCtrl.push(TokoplusPage, { from_storage: 'No', kdcabang: res[0].cabang_id, user_level: res[0].user_level, kdoutlet: res[0].kdoutlet, kdsales: res[0].kdsales, performa: 'No' });

    });
  }

  rutesales() {
    this.storage.get('user_session_storage').then((res) => {

      if (this.level == '2' || this.level == '10') {
        this.navCtrl.push(RutesalesPage, { kdoutlet: res[0].kdoutlet, kdcabang: res[0].cabang_id });
      } else {
        this.navCtrl.push(RutesalesPage, { kdoutlet: '', kdcabang: '' });
      }

    });
  }

  analytics() {
    // this.navCtrl.push(AnalyticsPage);
    this.storage.get('user_session_storage').then((res) => {
      //2 dan 10
      if (res[0].user_level == '2' || res[0].user_level == '10') {
        this.navCtrl.push(AnalyticsPage, { kdoutlet: res[0].kdoutlet, kdcabang: res[0].cabang_id, kdsales: '', viewuser: 'Yes', title: '', close: 'No', bisa_pilih: 'No' });
      } else {
        this.navCtrl.push(AnalyticsPage, { kdoutlet: '', kdcabang: '', kdsales: '', storages: 'Yes', close: 'No', bisa_pilih: 'No' })
      }
    });
  }

  noo() {
    this.storage.get('user_session_storage').then((res) => {
      this.navCtrl.push(NoolistPage, { from_storage: 'No', kdcabang: res[0].cabang_id, user_level: res[0].user_level, kdoutlet: res[0].kdoutlet, kdsales: '' });
    });
  }
}
