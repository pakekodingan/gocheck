import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController } from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { AlertController } from 'ionic-angular';
import { ViewController } from 'ionic-angular';
import { NavParams } from 'ionic-angular';
import { PopoverController } from 'ionic-angular';
import { ModalController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { Provider } from '../../config/provider';
import { FormoutletPage } from '../../pages/formoutlet/formoutlet';
import { Storage } from '@ionic/storage';

declare var google;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  @ViewChild('map') mapElement: ElementRef;

  qrData = null;
  createdCode = null;
  scannedCode = null;

  lat: any;
  lng: any;
  outlet: any;
  name: any;
  address: any;
  note: any;
  kode_distributor: any;
  kode_sales: any;

  constructor(
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    public barcodeScanner: BarcodeScanner,
    public popoverCtrl: PopoverController,
    public geolocation: Geolocation,
    public viewCtrl: ViewController,
    public navParams: NavParams,
    public postPvdr: Provider,
    public storage: Storage,
    private locationAccuracy: LocationAccuracy,
    private androidPermissions: AndroidPermissions,
    public modalCtrl: ModalController
  ) {

    this.scanCodex(navParams.get("lat"), navParams.get("lng"), navParams.get("outlet"), navParams.get("name"), navParams.get("address"), navParams.get("note"), navParams.get("kode_distributor"), navParams.get("kode_sales"));

    this.lat = navParams.get("lat");
    this.lng = navParams.get("lng");
    this.outlet = navParams.get("outlet");
    this.name = navParams.get("name");
    this.address = navParams.get("address");
    this.note = navParams.get("note");
    this.kode_distributor = navParams.get("kode_distributor");
    this.kode_sales = navParams.get("kode_sales");

  }

  ionViewDidLoad() {
    this.initMap();
  }

  //Check if application having GPS access permission  
  checkGPSPermission() {

    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
      result => {
        console.log('status harus melewati permission : ', result.hasPermission);
        if (result.hasPermission) {

          //If having permission show 'Turn On GPS' dialogue
          this.askToTurnOnGPS();
          console.log('masuk ke pertanyaan permission');
        } else {

          //If not having permission ask for permission
          this.requestGPSPermission();
          console.log('masuk ke request permission');
        }
      },
      err => {
        console.log(err);
      }
    );

  }

  requestGPSPermission() {
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {
      if (canRequest) {
        console.log("4");
      } else {
        //Show 'GPS Permission Request' dialogue
        this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
          .then(
            () => {
              // call method to turn on GPS
              this.askToTurnOnGPS();
              console.log('nyalakan GPS');
            },
            error => {
              //Show alert if user click on 'No Thanks'
              console.log('requestPermission Error requesting location permissions ' + error)
            }
          );
      }
    });
  }

  askToTurnOnGPS() {
    this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
      () => {
        // When GPS Turned ON call method to get Accurate location coordinates
        console.log('masuk ke pencarian koordinat');
        this.initMap();

      },
      error => console.log('Error requesting location permissions ' + JSON.stringify(error))
    );
  }

  // initMap() {

  //   var markerArray = [];

  //   // Instantiate a directions service.
  //   var directionsService = new google.maps.DirectionsService;


  //   this.geolocation.getCurrentPosition().then(pos => {
  //     // this.latitude = pos.coords.latitude;
  //     // this.longitude = pos.coords.longitude;

  //     var myLatlng = { lat: pos.coords.latitude, lng: pos.coords.longitude };
  //     var map = new google.maps.Map(this.mapElement.nativeElement, {
  //       zoom: 15,
  //       center: myLatlng,
  //       rotateControl: false,
  //       fullscreenControl: false,
  //       mapTypeControl: false,
  //       scaleControl: false,
  //       streetViewControl: false,
  //       streetViewControlOptions: {
  //         position: google.maps.ControlPosition.LEFT_TOP
  //       },
  //       zoomControl: false,
  //       zoomControlOptions: {
  //         position: google.maps.ControlPosition.LEFT_TOP
  //       }
  //     });

  //     // Create a renderer for directions and bind it to the map.
  //     var directionsRenderer = new google.maps.DirectionsRenderer({ map: map });

  //     // Instantiate an info window to hold step text.
  //     var stepDisplay = new google.maps.InfoWindow;

  //     // Display the route between the initial start and end selections.
  //     this.calculateAndDisplayRoute(directionsRenderer, directionsService, markerArray, stepDisplay, map, pos.coords.latitude, pos.coords.longitude);

  //     //marker mylocation
  //     // var image = 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-red.png';
  //     // var marker = new google.maps.Marker({
  //     //   position: myLatlng,
  //     //   map: map,
  //     //   icon: image
  //     // });

  //     // var contentString =
  //     //   '<h4>test&nbsp;&nbsp;&nbsp;&nbsp;</h4>' +
  //     //   '<div>' +
  //     //   '<p>test&nbsp;&nbsp;&nbsp;&nbsp;' +
  //     //   '</p>' +
  //     //   '<div id="pic"><img src="https://lh6.googleusercontent.com/_rhuioznu6Us/TU-MTMhCYGI/AAAAAAAAAQs/WJw-khN02AI/s144/vasto-1967.jpeg" width="268" height="188" />' +
  //     //   '<p>Jakarta, 2019</p></div>' +
  //     //   '<input type="submit" id="butSubmit" value="Detail" onclick="detail()"><br><br>' +
  //     //   '</div>';

  //     // marker.content = contentString;

  //     // var infoWindow = new google.maps.InfoWindow();
  //     // google.maps.event.addListener(marker, 'click', function () {
  //     //   infoWindow.setContent(this.content);
  //     //   infoWindow.open(this.getMap(), this);

  //     // });

  //   }).catch(err => console.log(err));

  // }

  initMap() {
    var myLatlng = { lat: -6.1741808, lng: 106.8079343 };
    var map = new google.maps.Map(this.mapElement.nativeElement, {
      zoom: 13,
      center: myLatlng,
      zoomControl: false,
      mapTypeControl: false,
      scaleControl: false,
      streetViewControl: false,
      rotateControl: false,
      fullscreenControl: false
    });
  }

  calculateAndDisplayRoute(directionsRenderer, directionsService,
    markerArray, stepDisplay, map, lat, lng) {
    // First, remove any existing markers from the map.
    for (var i = 0; i < markerArray.length; i++) {
      markerArray[i].setMap(null);
    }

    // Retrieve the start and end locations and create a DirectionsRequest using
    // DRIVING directions.
    directionsService.route({
      origin: 'duta indah karya',
      destination: { lat: lat, lng: lng },
      travelMode: 'DRIVING',
      avoidTolls: true
    }, function (response, status) {
      // Route the directions and pass the response to a function to create
      // markers for each step.
      if (status === 'OK') {

        directionsRenderer.setDirections(response);

        // For each step, place a marker, and add the text to the marker's infowindow.
        // Also attach the marker to an array so we can keep track of it and remove it
        // when calculating new routes.
        var myRoute = response.routes[0].legs[0];
        for (var i = 0; i < myRoute.steps.length; i++) {
          var marker = markerArray[i] = markerArray[i] || new google.maps.Marker;

          //untuk memunculkan step ( titik langkkah )
          //marker.setMap(map);
          marker.setPosition(myRoute.steps[i].start_location);


          google.maps.event.addListener(marker, 'click', function () {
            // Open an info window when the marker is clicked on, containing the text
            // of the step.
            stepDisplay.setContent(myRoute.steps[i].instructions);
            stepDisplay.open(map, marker);
          });


        }

      } else {
        // window.alert('Directions request failed due to ' + status);
      }
    });
  }


  detail() {
    // alert('test')
  }

  mylocationPage(marker) {

    google.maps.event.addListener(marker, 'click', () => {

    });
  }

  createCode() {
    this.createdCode = this.qrData;
  }

  // scanCode() {
  //   this.barcodeScanner.scan().then(barcodeData => {
  //     this.scannedCode = barcodeData.text;
  //   }).catch(err => {
  //     console.log('Error', err);
  //   });
  // }

  scanCode() {
    this.barcodeScanner.scan().then(barcodeData => {

      var strArray = barcodeData.text.split("#");

      const prompt = this.alertCtrl.create({
        title: 'Info!',
        message: "Result QR Code Scanner",
        inputs: [
          {
            name: 'outlet_name',
            placeholder: 'outlet name',
            value: strArray[0]
          },
          {
            name: 'address',
            placeholder: 'address',
            value: strArray[1]
          },
          {
            name: 'latitude',
            placeholder: 'latitude',
            value: strArray[2]
          },
          {
            name: 'longitude',
            placeholder: 'longitude',
            value: strArray[3]
          },
        ],
        buttons: [
          {
            text: 'Cancel',
            handler: data => {
              console.log('Cancel clicked');
            }
          },
          {
            text: 'View',
            handler: data => {
              console.log('Saved clicked');
              var myLatlng = { lat: parseFloat(data.latitude), lng: parseFloat(data.longitude) };
              var map = new google.maps.Map(this.mapElement.nativeElement, {
                zoom: 19,
                center: myLatlng,
                rotateControl: false,
                fullscreenControl: false,
                mapTypeControl: false,
                scaleControl: false,
                streetViewControl: true,
                streetViewControlOptions: {
                  position: google.maps.ControlPosition.LEFT_TOP
                },
                zoomControl: true,
                zoomControlOptions: {
                  position: google.maps.ControlPosition.LEFT_TOP
                }
              });

              var image = 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-red.png';
              var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                icon: image
              });

              var contentString =
                '<h4>' + data.outlet_name + '&nbsp;&nbsp;&nbsp;&nbsp;</h4>' +
                '<div>' +
                '<p>' + data.address + '&nbsp;&nbsp;&nbsp;&nbsp;' +
                '</p>' +
                // '<div id="pic"><img src="https://www.biruindonesia.com/images/articles/project/puri-indah-financiall-towrerrr.jpg" width="188" height="168" />' +
                '<input type="submit" id="butSubmit" value="Detail" onclick="detail()"><br><br>' +
                '</div>';

              marker.content = contentString;

              var infoWindow = new google.maps.InfoWindow();
              google.maps.event.addListener(marker, 'click', function () {
                infoWindow.setContent(this.content);
                infoWindow.open(this.getMap(), this);
              });

              google.maps.event.addListener(marker, 'click', () => {
                this.detailLocation('', '', '', '', '', '', '', '', '');
              });

            }
          }
        ]
      });
      prompt.present();

    }).catch(err => {
      console.log('Error', err);
    });
  }

  scanCodex(lat, lng, outlet, name, address, note, kode_distributor, kode_sales) {
    const prompt = this.alertCtrl.create({
      title: 'Info!',
      message: "Result QR Code Scanner",
      inputs: [
        {
          name: 'outlet_name',
          placeholder: 'outlet name',
          value: name
        },
        {
          name: 'address',
          placeholder: 'address',
          value: address
        },
        {
          name: 'latitude',
          placeholder: 'latitude',
          value: lat
        },
        {
          name: 'longitude',
          placeholder: 'longitude',
          value: lng
        },
        {
          name: 'note',
          placeholder: 'note',
          value: note
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'View',
          handler: data => {
            console.log('Saved clicked');
            var myLatlng = { lat: parseFloat(lat), lng: parseFloat(lng) };
            var map = new google.maps.Map(this.mapElement.nativeElement, {
              zoom: 17,
              center: myLatlng,
              rotateControl: false,
              fullscreenControl: false,
              mapTypeControl: false,
              scaleControl: false,
              streetViewControl: true,
              streetViewControlOptions: {
                position: google.maps.ControlPosition.LEFT_TOP
              },
              zoomControl: true,
              zoomControlOptions: {
                position: google.maps.ControlPosition.LEFT_TOP
              }
            });

            // var map = new google.maps.Map(this.mapElement.nativeElement, {
            //   center: myLatlng,
            //   zoom: 14
            // });

            var image = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';
            var marker = new google.maps.Marker({
              position: myLatlng,
              map: map,
              icon: image
            });

            //untuk botton save
            //google.maps.event.addListener(marker, 'click', () => {
            //  this.detailLocation(outlet, lat, lng , name, address,'','');
            //});

            var contentString =
              '<h4>' + data.outlet_name + '&nbsp;&nbsp;&nbsp;&nbsp;</h4>' +
              '<div>' +
              '<p>' + data.address + '&nbsp;&nbsp;&nbsp;&nbsp;' +
              '</p>' +
              '</div>';

            marker.content = contentString;

            var infoWindow = new google.maps.InfoWindow();
            google.maps.event.addListener(marker, 'click', function () {
              infoWindow.setContent(this.content);
              infoWindow.open(this.getMap(), this);
            });

            var panorama = new google.maps.StreetViewPanorama(
              document.getElementById('pano'), {
              position: myLatlng,
              fullscreenControl: false,
              zoomControl: false,
              panControl: false,
              pov: {
                heading: 34,
                pitch: 10
              }
            });
            map.setStreetView(panorama);



          }
        }
      ]
    });
    prompt.present();


  }

  detailLocation(outlet, lat, lng, name, address, typevalidate, note, kode_distributor, kode_sales) {

    var tipe;
    if (typevalidate == '1') {
      tipe = "Validate ";
    } else {
      tipe = "No Validate ";
    }

    const prompt = this.alertCtrl.create({
      title: tipe,
      message: tipe + "this location. save to server.",
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: data => {


            const prompts = this.alertCtrl.create({
              title: 'Info',
              message: "Add Note For Information",
              inputs: [
                {
                  name: 'sebab',
                  placeholder: 'Note Here ...'
                },
              ],
              buttons: [
                {
                  text: 'Cancel',
                  handler: data => {
                    console.log('Cancel clicked');
                  }
                },
                {
                  text: 'Send',
                  handler: data => {


                    this.storage.get('user_session_storage').then((res) => {
                      let body = {
                        outlet: outlet,
                        kdcabang: res[0].cabang_id,
                        lat: lat,
                        lng: lng,
                        name: name,
                        address: address,
                        note: note,
                        notevalidate: data.sebab,
                        status: typevalidate,
                        marker: kode_sales,
                        kdsales: kode_sales,
                        validator: res[0].kdsales + '-' + res[0].employee_name,
                        kode_distributor: kode_distributor
                      };

                      this.postPvdr.postData(body, 'outlet/updateoutlet_fromwhatsapp').subscribe((data) => {

                        const alert = this.alertCtrl.create({
                          title: 'Info',
                          subTitle: data.subtitle,
                          message: data.msg,
                          buttons: ['OK']
                        });
                        alert.present();

                        const prompt = this.alertCtrl.create({
                          title: 'Info',
                          subTitle: data.subtitle,
                          message: data.msg,
                          buttons: [
                            {
                              text: 'Save',
                              handler: data => {
                                this.navCtrl.pop();
                              }
                            }
                          ]
                        });
                        prompt.present();

                      }, error => {

                        const alert = this.alertCtrl.create({
                          title: 'Info',
                          subTitle: error,
                          message: 'Not Connected To Server',
                          buttons: ['OK']
                        });
                        alert.present();
                      });
                    });


                  }
                }
              ]
            });
            prompts.present();


          }
        }
      ]
    });
    prompt.present();

  }

  more(ev) {
    const popover = this.popoverCtrl.create(PopoverPage);
    popover.present({
      ev: ev
    });
  }

  close() {
    this.viewCtrl.dismiss();
  }

}


@Component({
  templateUrl: 'popover.html'
})
export class PopoverPage {

  constructor(
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    public viewCtrl: ViewController,
    public modalCtrl: ModalController
  ) {
  }

  ionViewDidLoad() {

  }

  newOutlet() {
    const modal = this.modalCtrl.create(FormoutletPage);
    modal.present();
  }



}

