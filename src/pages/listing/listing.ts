import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewController, ModalController } from 'ionic-angular';
import { DetaillistingsPage } from '../../pages/detaillistings/detaillistings';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the ListingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-listing',
  templateUrl: 'listing.html',
})
export class ListingPage {

  constructor(
    public viewCtrl: ViewController,
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    public storage: Storage,
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListingPage');
  }

  detaillisting() {
    // const modal = this.modalCtrl.create(DetaillistingsPage);
    // modal.present();
    this.storage.get('user_session_storage').then((res) => {
      this.navCtrl.push(DetaillistingsPage, { kdoutlet: res[0].kdoutlet, kdcabang: res[0].cabang_id, storages: 'Yes', close: 'No', bisa_pilih: 'No' })
    });
  }

}
