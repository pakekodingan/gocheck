import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormoutletPage } from './formoutlet';

@NgModule({
  declarations: [
    FormoutletPage,
  ],
  imports: [
    IonicPageModule.forChild(FormoutletPage),
  ],
})
export class FormoutletPageModule { }
