import { Component, Input, ViewChild, Renderer2, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewController, AlertController } from 'ionic-angular';
import { ModalController, LoadingController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { File, IWriteOptions } from '@ionic-native/file';
import { Storage } from '@ionic/storage';
import { SocialSharing } from '@ionic-native/social-sharing';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { Geolocation } from '@ionic-native/geolocation';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { Platform } from 'ionic-angular';

import { Provider } from '../../config/provider';

import { ListoutletPage } from '../../pages/listoutlet/listoutlet';

declare var google;
declare var require: any;
let QRCode = require('qrcode');
const STORAGE_KEY = 'IMAGE_LIST';

@IonicPage()
@Component({
  selector: 'page-formoutlet',
  templateUrl: 'formoutlet.html',
})
export class FormoutletPage {

  @ViewChild('map') mapElement: ElementRef;
  @ViewChild('imageCanvas1') canvas1: any;
  canvasElement1: any;

  upload_url_image = 'http://api.vci.co.id/index.php/gocheck/uploadimage';
  photo: any;
  nama_image: any;
  outlet_existing: any = false;
  kode_outlet = null;
  kode_distributor = null;
  kode_sales = null;
  outlet_name = null;
  address = null;
  note = null;
  latitude = null;
  longitude = null;
  createdCode = null;
  photo_status: String = '';
  storedImages = [];

  @Input('qrc-element-type') elementType: 'url' | 'img' | 'canvas' = 'img';
  @Input('qrc-class') cssClass = 'qrcode';
  @Input('qrc-value') value = 'https://www.techiediaries.com';
  @Input('qrc-version') version: '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' | '10' | '11' | '12' | '13' | '14' | '15' | '16' | '17' | '18' | '19' | '20' | '21' | '22' | '23' | '24' | '25' | '26' | '27' | '28' | '29' | '30' | '31' | '32' | '33' | '34' | '35' | '36' | '37' | '38' | '39' | '40' | '' = '';

  @Input('qrc-errorCorrectionLevel') errorCorrectionLevel: 'L' | 'M' | 'Q' | 'H' = 'M';
  @Input('qrc-margin') margin = 3;
  @Input('qrc-scale') scale = 3;
  @Input('qrc-width') width = 10;
  @Input('qrc-colorDark') colorDark = '#000000';
  @Input('qrc-colorLight') colorLight = '#ffffff';

  @ViewChild('qrcElement') qrcElement: ElementRef;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    private camera: Camera,
    public barcodeScanner: BarcodeScanner,
    private file: File,
    public plt: Platform,
    public loadingCtrl: LoadingController,
    private storage: Storage,
    public renderer: Renderer2,
    public transfer: FileTransfer,
    public postPvdr: Provider,
    private socialSharing: SocialSharing,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    private locationAccuracy: LocationAccuracy,
    private androidPermissions: AndroidPermissions,
    public geolocation: Geolocation,
  ) {

    this.storage.get('user_session_storage').then((res) => {
      this.kode_distributor = res[0].kdoutlet;
      this.kode_sales = res[0].kdsales;
      ;
    });

    if (this.plt.is('android')) {
      console.log('android')
    } else {
      console.log('web')
    }

    this.storage.ready().then(() => {
      this.storage.get(STORAGE_KEY).then(data => {
        if (data != undefined) {
          this.storedImages = data;
        }
      });
    });

    //this.checkGPSPermission();
  }

  ionViewDidLoad() {
    this.canvasElement1 = this.canvas1.nativeElement;
    this.canvasElement1.width = 720;
    this.canvasElement1.height = 965;
    this.getLocation();
  }

  //Check if application having GPS access permission  
  checkGPSPermission() {

    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
      result => {
        if (result.hasPermission) {

          //If having permission show 'Turn On GPS' dialogue
          this.askToTurnOnGPS();

        } else {

          //If not having permission ask for permission
          this.requestGPSPermission();

        }
      },
      err => {
        console.log(err);
      }
    );

  }

  requestGPSPermission() {
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {
      if (canRequest) {
        console.log("4");
      } else {
        //Show 'GPS Permission Request' dialogue
        this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
          .then(
            () => {
              // call method to turn on GPS
              this.askToTurnOnGPS();

            },
            error => {
              //Show alert if user click on 'No Thanks'
              console.log('requestPermission Error requesting location permissions ' + error)
            }
          );
      }
    });
  }

  askToTurnOnGPS() {
    this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
      () => {
        // When GPS Turned ON call method to get Accurate location coordinates

      },
      error => console.log('Error requesting location permissions ' + JSON.stringify(error))
    );
  }

  getLocation() {
    this.geolocation.getCurrentPosition().then(pos => {
      this.latitude = pos.coords.latitude;
      this.longitude = pos.coords.longitude;

      var myLatlng = { lat: pos.coords.latitude, lng: pos.coords.longitude };
      var map = new google.maps.Map(this.mapElement.nativeElement, {
        zoom: 17,
        center: myLatlng,
        zoomControl: false,
        mapTypeControl: false,
        scaleControl: false,
        streetViewControl: false,
        rotateControl: false,
        fullscreenControl: false
      });

      // marker mylocation
      var icon = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';
      var marker = new google.maps.Marker({
        position: myLatlng,
        draggable: true,
        map: map,
        icon: icon
      });

      google.maps.event.addListener(marker, 'dragend', () => {
        this.latitude = marker.getPosition().lat();
        this.longitude = marker.getPosition().lng();

        //find address draggable
        var geocoder = new google.maps.Geocoder;
        var latlng = { lat: this.latitude, lng: this.longitude };
        geocoder.geocode({ 'location': latlng }, function (results, status) {
          var inputElement = <HTMLInputElement>document.getElementById('map-search');
          inputElement.value = results[0].formatted_address;

          var PerubahanLokasi = (document.getElementById('RLocate') as HTMLInputElement).value;
          if (PerubahanLokasi == '1') {
            var inputElements = <HTMLInputElement>document.getElementById('ReLocate');
            inputElements.value = '1';
          }

        });

      });

      //find address fixed

      var geocoder = new google.maps.Geocoder;
      var latlng = { lat: this.latitude, lng: this.longitude };
      geocoder.geocode({ 'location': latlng }, function (results, status) {
        var inputElement = <HTMLInputElement>document.getElementById('map-search');
        inputElement.value = results[0].formatted_address;
      });

    }).catch(err => console.log('Error Here : ', err));
  }

  addForm() {

    if ((typeof (this.latitude) === "undefined" || this.latitude == null || this.latitude == '') || (typeof (this.longitude) === "undefined" || this.longitude == null || this.longitude == '')) {
      const alert = this.alertCtrl.create({
        title: 'Info!',
        subTitle: 'Harus mendapatkan titik lokasi. Pastikan GPS Handphone anda sudah aktif.',
        buttons: ['OK']
      });
      alert.present();
      return false;
    }

    const prompt = this.alertCtrl.create({
      title: 'Info',
      message: "Sebelum isi formulir Tag Lokasi Toko, Pastikan titik lokasi toko sudah tepat.",
      buttons: [
        {
          text: 'Ulangi',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Ya',
          handler: data => {
            console.log('Saved clicked');
            this.tampilkanForm();
          }
        }
      ]
    });
    prompt.present();
  }

  tampilkanForm() {



    this.address = (document.getElementById('map-search') as HTMLInputElement).value;
    document.getElementById('addForm').style.display = '';
    document.getElementById('peta').style.display = 'none';
    document.getElementById('map').style.display = 'none';
    document.getElementById('add').style.display = 'none';
    document.getElementById('maps').style.display = '';
    document.getElementById('tombol_kirim').style.display = '';

    var PerubahanLokasi = (document.getElementById('ReLocate') as HTMLInputElement).value;
    if (PerubahanLokasi == '1') {
      //hilangkan image
      document.getElementById('hasil_photo').style.display = 'none';
      //nol-kan lagi
      this.photo_status = '';
      const prompt = this.alertCtrl.create({
        title: 'Info',
        message: "Anda ada perubahan titik lokasi, kami menyarankan ambil ulang photo",
        buttons: [
          {
            text: 'Ya',
            handler: data => {
              this.takePhoto();
            }
          }
        ]
      });
      prompt.present();
    }

  }

  addMaps() {

    var inputElements = <HTMLInputElement>document.getElementById('ReLocate');
    inputElements.value = '0';

    var inputElements = <HTMLInputElement>document.getElementById('RLocate');
    inputElements.value = '1';

    document.getElementById('addForm').style.display = 'none';
    document.getElementById('peta').style.display = '';
    document.getElementById('map').style.display = '';
    document.getElementById('add').style.display = '';
    document.getElementById('maps').style.display = 'none';
    document.getElementById('tombol_kirim').style.display = 'none';
  }

  myChange($event) {
    if (this.outlet_existing) {
      const modal = this.modalCtrl.create(ListoutletPage);
      modal.present();
      modal.onDidDismiss(val => {
        if (val) {
          this.outlet_existing = true;
          this.kode_outlet = val.kdoutlet;
          this.outlet_name = val.nama;
          this.address = val.alamat;
          document.getElementById('kode_outlet').style.display = '';
        } else {
          this.outlet_existing = false;
          document.getElementById('kode_outlet').style.display = 'none';
        }
      });
    } else {
      document.getElementById('kode_outlet').style.display = 'none';
      this.kode_outlet = '';
      this.outlet_name = '';
      this.address = '';
    }
  }

  getImages() {

    var ctx = this.canvasElement1.getContext("2d");
    var img = document.getElementById("take_photo");
    var img2 = document.getElementById("qi_ar_code");
    ctx.drawImage(img, 3, 3);
    ctx.drawImage(img2, 3, 3);
  }

  createCode() {
    this.createdCode = this.kode_outlet + '#' + this.outlet_name + '#' + this.address + '#' + this.latitude + '#' + this.longitude + '#' + this.note + '#' + this.kode_distributor + '#' + this.kode_sales;
  }


  takePhoto() {

    if (typeof (this.outlet_name) === "undefined" || this.outlet_name == null || this.outlet_name == '') {
      const alert = this.alertCtrl.create({
        title: 'Info!',
        subTitle: 'Nama Toko Tidak Boleh Kosong.',
        buttons: ['OK']
      });
      alert.present();
      return false;
    }

    if (typeof (this.note) === "undefined" || this.note == null || this.note == '') {
      const alert = this.alertCtrl.create({
        title: 'Info!',
        subTitle: 'Catatan Harus Diisi, jika tidak ada input strip (-) saja.',
        buttons: ['OK']
      });
      alert.present();
      return false;
    }

    //generate QR Code
    this.createCode();
    this.createQRCode();

    this.photo_status = '1';
    //camera
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      //saveToPhotoAlbum: true,
      targetWidth: 720,
      targetHeight: 1560,
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      this.photo = imageData;

      //to canvas
      const prompt = this.alertCtrl.create({
        title: 'Info',
        message: "Tekan Ok Untuk Melihat Gambar.",
        buttons: [
          {
            text: 'Ok',
            handler: data => {
              this.getImages();
            }
          }
        ]
      });
      prompt.present();

    }, (err) => {
      console.log('error : ' + err);
    });
  }

  saveCanvasImage() {
    var lukisan = this.canvasElement1;
    var dataUrl = lukisan.toDataURL();
    let ctx = lukisan.getContext('2d');
    //ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height); // Clears the canvas

    let name = new Date().getTime() + '.jpg';

    let path = this.file.dataDirectory;
    let options: IWriteOptions = { replace: true };

    var data = dataUrl.split(',')[1];
    let blob = this.b64toBlob(data, 'image/jpg');

    this.file.writeFile(path, name, blob, options).then(res => {
      this.storeImage(name);
    }, err => {
      console.log('error: ' + err);
    });

  }

  saveJoinImage() {
    var lukisan = this.canvasElement1;
    var dataUrl = lukisan.toDataURL();
    let ctx = lukisan.getContext('2d');
    //ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height); // Clears the canvas

    let name = new Date().getTime() + '.jpg';

    //untuk file upload
    this.nama_image = name;

    let path = this.file.dataDirectory;
    let options: IWriteOptions = { replace: true };

    var data = dataUrl.split(',')[1];
    let blob = this.b64toBlob(data, 'image/jpg');

    this.file.writeFile(path, name, blob, options).then(res => {
      this.storeImageServer(name);
    }, err => {
      console.log('error: ' + err);
    });

  }


  b64toBlob(b64Data, contentType) {
    contentType = contentType || '';
    var sliceSize = 512;
    var byteCharacters = atob(b64Data);
    var byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      var slice = byteCharacters.slice(offset, offset + sliceSize);

      var byteNumbers = new Array(slice.length);
      for (var i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      var byteArray = new Uint8Array(byteNumbers);

      byteArrays.push(byteArray);
    }

    var blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  storeImage(imageName) {
    let saveObj = { img: imageName };
    this.storedImages.push(saveObj);
    this.storage.set(STORAGE_KEY, this.storedImages).then(() => {
      setTimeout(() => {
        this.shareImg(imageName);
      }, 500);
    });
  }

  storeImageServer(imageName) {
    let saveObj = { img: imageName };
    this.storedImages.push(saveObj);
    this.storage.set(STORAGE_KEY, this.storedImages).then(() => {
      setTimeout(() => {

      }, 500);
    });
  }

  shareImg(fileImage) {
    // let imageName = "1568069382318.bmp";
    let imageName = fileImage;
    // const ROOT_DIRECTORY = 'file:///sdcard//';
    const ROOT_DIRECTORY = 'file:///data/user/0/io.ionic.gocheck/';
    const downloadFolderName = 'tempDownloadFolder';

    //Create a folder in memory location
    this.file.createDir(ROOT_DIRECTORY, downloadFolderName, true)
      .then((entries) => {

        //Copy our asset/img/FreakyJolly.jpg to folder we created
        //console.log('direktori ', this.file.applicationDirectory);
        // this.file.copyFile(this.file.applicationDirectory + "www/assets/imgs/", imageName, ROOT_DIRECTORY + downloadFolderName + '//', imageName)
        this.file.copyFile("file:///data/user/0/io.ionic.gocheck/files/", imageName, ROOT_DIRECTORY + downloadFolderName + '//', imageName)
          .then((entries) => {

            //Common sharing event will open all available application to share
            this.socialSharing.share("Message", "Subject", ROOT_DIRECTORY + downloadFolderName + "/" + imageName, imageName)
              .then((entries) => {
                //console.log('success ' + JSON.stringify(entries));
                this.navCtrl.pop();
              })
              .catch((error) => {
                console.log('error 1 ' + JSON.stringify(error));
              });
          })
          .catch((error) => {
            console.log('error 2 ' + JSON.stringify(error));
          });
      })
      .catch((error) => {
        console.log('error 3 ' + JSON.stringify(error));
      });
  }







  toDataURL() {
    return new Promise((resolve, reject) => {
      QRCode.toDataURL(this.value,
        {
          version: this.version,
          errorCorrectionLevel: this.errorCorrectionLevel,
          margin: this.margin,
          scale: this.scale,
          width: this.width,
          color: {
            dark: this.colorDark,
            light: this.colorLight
          }
        }, function (err, url) {
          if (err) {
            console.error(err);
            reject(err);
          } else {
            //console.log(url);
            resolve(url);
          }
        })
    });
  }
  toCanvas(canvas) {
    return new Promise((resolve, reject) => {
      QRCode.toCanvas(canvas, this.value, {
        version: this.version,
        errorCorrectionLevel: this.errorCorrectionLevel,
        margin: this.margin,
        scale: this.scale,
        width: this.width,
        color: {
          dark: this.colorDark,
          light: this.colorLight
        }
      }, function (error) {
        if (error) {
          //console.error(error);
          reject(error);
        }
        else {
          //console.log('success!');
          resolve("success");
        }
      })
    });
  }
  renderElement(element) {
    for (let node of this.qrcElement.nativeElement.childNodes) {
      this.renderer.removeChild(this.qrcElement.nativeElement, node);
    }
    this.renderer.appendChild(this.qrcElement.nativeElement, element);
  }
  createQRCode() {
    if (!this.value) {
      return;
    };

    this.value = this.kode_outlet + '#' + this.outlet_name + '#' + this.address + '#' + this.latitude + '#' + this.longitude + '#' + this.note + '#' + this.kode_distributor + '#' + this.kode_sales;
    let element: Element;

    // switch (this.elementType) {
    //console.log(this.elementType)
    switch (this.elementType) {

      case 'canvas':
        console.log('empty canvas');
        break;

      case 'url':
        element = this.renderer.createElement('canvas');
        this.toCanvas(element).then((v) => {
          this.renderElement(element);
        }).catch((e) => {
          console.error(e);
        });
        break;

      case 'img':

        element = this.renderer.createElement('img');
        this.toDataURL().then((v: string) => {
          console.log('666', v);
          console.log('999', element);
          element.setAttribute("src", v);
          element.setAttribute("id", "qi_ar_code");
          this.renderElement(element);
        }).catch((e) => {
          console.error(e);
        })
        break;

      default:

        element = this.renderer.createElement('img');
        this.toDataURL().then((v: string) => {

          element.setAttribute("src", v);
          this.renderElement(element);
        }).catch((e) => {
          console.error(e);
        })

    }


  }

  metode_upload() {

    if (typeof (this.photo_status) === "undefined" || this.photo_status == null || this.photo_status == '') {
      const alert = this.alertCtrl.create({
        title: 'Info!',
        subTitle: 'Harus Melampirkan Photo.',
        buttons: ['OK']
      });
      alert.present();
      return false;
    }

    //save image dulu
    this.saveJoinImage();

    const loader = this.loadingCtrl.create({
      content: "Cek Server Dulu...",
    });
    loader.present();



    let body = {
      ping: 'ping'
    };

    this.postPvdr.postData(body, 'ping').subscribe((data) => {

      if (data.success) {
        loader.dismiss();
        //recommended server
        this.upload_data('Disarankan', false, true, true, false);
      }

    }, error => {
      loader.dismiss();
      //recommended whatsapp
      this.upload_data('Tdk Disarankan', true, false, false, true);

    });

  }

  upload_data(msg, server, whatsapp, s, w) {
    var status = '(' + msg + ')';
    let status_server = server;
    let status_whatsapp = whatsapp;

    let alert = this.alertCtrl.create();
    alert.setTitle('Metode Pengiriman');

    alert.addInput({
      type: 'radio',
      label: 'Server VCI ' + status,
      value: 'server',
      checked: s,
      disabled: false
    });

    alert.addInput({
      type: 'radio',
      label: 'WhatsApp',
      value: 'whatsapp',
      checked: w,
      disabled: false
    });

    alert.addButton('Cancel');
    alert.addButton({
      text: 'OK',
      handler: data => {
        if (data == 'whatsapp') {
          this.saveCanvasImage();
        } else if (data == 'server') {
          this.sendToServer();
        }
      }
    });
    alert.present();
  }

  sendToServer() {

    //upload image
    this.uploadImage(this.nama_image);

    this.storage.get('user_session_storage').then((res) => {

      let body = {
        'kode_outlet': this.kode_outlet,
        'outlet_name': this.outlet_name,
        'address': this.address,
        'note': this.note,
        'latitude': this.latitude,
        'longitude': this.longitude,
        'images': this.nama_image,
        'marker': res[0].kdsales + ' - ' + res[0].employee_name,
        'kdcabang': res[0].cabang_id,
        'username': res[0].username,
        'kdsales': this.kode_sales,
        'kddistributor': this.kode_distributor
      };
      this.postPvdr.postData(body, 'outlet/savedata2').subscribe((data) => {
        if (data.success) {
          const prompt = this.alertCtrl.create({
            title: 'Info',
            message: "Data Berhasil di kirim.",
            buttons: [
              {
                text: 'Ok',
                handler: data => {
                  this.navCtrl.pop();
                }
              }
            ]
          });
          prompt.present();
        } else {
          const prompt = this.alertCtrl.create({
            title: 'Info',
            message: "Data Gagal Dikirim.",
            buttons: [
              {
                text: 'Ok',
                handler: data => {
                  // this.navCtrl.pop();
                }
              }
            ]
          });
          prompt.present();
        }
      }, error => {
        const prompt = this.alertCtrl.create({
          title: 'Info',
          message: "Tidak konek ke sever, silahkan di coba kembali.",
          buttons: [
            {
              text: 'Ok',
              handler: data => {
                // this.navCtrl.pop();
              }
            }
          ]
        });
        prompt.present();
        return;
      });

    });
  }

  uploadImage(imageName) {

    const fileTransfer: FileTransferObject = this.transfer.create();

    let options: FileUploadOptions = {
      fileKey: 'file',
      fileName: imageName,
      chunkedMode: false,
      mimeType: "image/png",
      headers: {}
    }

    fileTransfer.upload("file:///data/user/0/io.ionic.gocheck/files/" + imageName, this.upload_url_image, options)
      .then((data) => {
        //sukses
      }, (err) => {
        console.log("Error This Image: " + err);
      });
  }

}
