import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Provider } from '../../config/provider';
import { App } from 'ionic-angular';
import { AppUpdate } from '@ionic-native/app-update';
import { ToastController } from 'ionic-angular';
import { BypassPage } from '../../pages/bypass/bypass';

/**
 * Generated class for the SettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
import { LoginPage } from '../../pages/login/login';
@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  version: String = '150320';
  update: String = 'No';
  public press: number = 0;
  view_change_password: any;

  constructor(
    public storage: Storage,
    public alertCtrl: AlertController,
    public app: App,
    public toastCtrl: ToastController,
    private appUpdate: AppUpdate,
    public postPvdr: Provider,
    public viewCtrl: ViewController,
    public loadingCtrl: LoadingController,
    public navCtrl: NavController,
    public navParams: NavParams) {

    this.storage.get('user_session_storage').then((res) => {
      if (res[0].jenis_outlet == "distributor" || res[0].username == 'irfan1404') {
        this.view_change_password = "Yes";
      } else {
        this.view_change_password = "No";
      }
    });
    this.updates();

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingsPage');
  }

  updates() {
    this.storage.get('user_session_storage').then((res) => {
      let body = {
        username: res[0].username
      };

      this.postPvdr.postData(body, 'login/cek_update').subscribe((data) => {
        console.log('update version : ', data)
        if (data.success) {
          if (data.result != this.version) {
            this.update = 'Yes';
          } else {
            this.update = 'No';
          }
        } else {
          this.update = 'No';
        }

      }, error => {
        this.update = 'No';
      });
    });
  }

  check_update() {
    const updateUrl = 'http://api.vci.co.id/public/ota/update.xml';
    this.appUpdate.checkAppUpdate(updateUrl).then(() => { console.log('Update available') });
  }

  logout() {
    this.viewCtrl.dismiss();
    this.storage.clear();
    this.app.getRootNav().setRoot(LoginPage);
  }

  change_password() {
    this.storage.get('user_session_storage').then((res) => {
      const prompt = this.alertCtrl.create({
        title: 'Ganti Password',
        message: "Silahkan mengganti password anda.",
        inputs: [
          {
            name: 'old_password',
            type: 'password',
            placeholder: 'Password Lama'
          },
          {
            name: 'new_password',
            type: 'password',
            placeholder: 'Password Baru'
          }
        ],
        buttons: [
          {
            text: 'Batal',
            handler: data => {
              console.log('Cancel clicked');
            }
          },
          {
            text: 'Simpan',
            handler: data => {
              console.log('Saved clicked');
              if (data.old_password != res[0].password) {
                const alert = this.alertCtrl.create({
                  title: 'Info',
                  subTitle: 'Password Lama Anda Salah. Silahkan Ulangi Lagi.',
                  buttons: ['OK']
                });
                alert.present();
              } else {

                const loader = this.loadingCtrl.create({
                  content: "Mohon Menunggu..."
                });
                loader.present();

                let body = {
                  kdcabang: res[0].cabang_id,
                  kdoutlet: res[0].kdoutlet,
                  kdsales: res[0].kdsales,
                  username: res[0].username,
                  new_password: data.new_password
                };

                this.postPvdr.postData(body, 'login/change_password').subscribe((data) => {


                  if (data.success) {
                    loader.dismiss();
                    const prompts = this.alertCtrl.create({
                      title: 'Info',
                      subTitle: 'Ganti Password Sukses. silahkan login kembali.',
                      buttons: [
                        {
                          text: 'Oke',
                          handler: res => {
                            this.logout();
                          }
                        }
                      ]
                    });
                    prompts.present();
                    return false;
                  } else {
                    loader.dismiss();

                    const alert = this.alertCtrl.create({
                      title: 'Info',
                      subTitle: 'Ganti Password Gagal.',
                      message: '',
                      buttons: ['OK']
                    });
                    alert.present();

                    return false;
                  }

                }, error => {
                  loader.dismiss();
                  const alert = this.alertCtrl.create({
                    title: 'Mohon Maaf',
                    subTitle: 'Sedang tidak terkoneksi dengan server. Silahkan coba kembali beberapa saat',
                    buttons: ['OK']
                  });
                  alert.present();
                  return false;
                });

              }
            }
          }
        ]
      });
      prompt.present();
    });
  }


  developer_option(e) {

    var a = this.press++;

    if (a == 3) {
      const prompt = this.alertCtrl.create({
        title: 'Developer Access',
        message: "Enter Your PIN Access Developer",
        inputs: [
          {
            name: 'developerid',
            type: 'password',
            placeholder: 'ID Developer'
          },
        ],
        buttons: [
          {
            text: 'Cancel',
            handler: data => {
              //console.log('Cancel clicked');
            }
          },
          {
            text: 'Access',
            handler: data => {
              if (data.developerid == '120187') {
                this.navCtrl.push(BypassPage);
              } else {
                const toast = this.toastCtrl.create({
                  message: 'Opps Sorry, you do not have access.',
                  duration: 3000,
                  position: 'top'
                });
                toast.present();
              }
            }
          }
        ]
      });
      prompt.present();

    }
  }

}
