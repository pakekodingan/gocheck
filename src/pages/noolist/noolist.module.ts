import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NoolistPage } from './noolist';

@NgModule({
  declarations: [
    NoolistPage,
  ],
  imports: [
    IonicPageModule.forChild(NoolistPage),
  ],
})
export class NoolistPageModule {}
