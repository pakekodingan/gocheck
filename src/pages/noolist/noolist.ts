import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { PopoverController } from 'ionic-angular';
import { Provider } from '../../config/provider';
import { Storage } from '@ionic/storage';

import { DetailhistoryPage } from '../../pages/detailhistory/detailhistory';
import { PetanooPage } from '../../pages/petanoo/petanoo';
import { THROW_IF_NOT_FOUND } from '@angular/core/src/di/injector';
/**
 * Generated class for the NoolistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-noolist',
  templateUrl: 'noolist.html',
})
export class NoolistPage {

  listNoo: Array<any>;
  src: any;
  first: number;

  from_storage: any;
  user_level: any;
  kdoutlet: any;
  kdcabang: any;
  kdsales: any;

  constructor(public navCtrl: NavController,
    public viewCtrl: ViewController,
    public postPvdr: Provider,
    public storage: Storage,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private popoverCtrl: PopoverController,
    public navParams: NavParams) {

    this.from_storage = navParams.get("from_storage");
    this.kdoutlet = navParams.get("kdoutlet");
    this.kdcabang = navParams.get("kdcabang");
    this.kdsales = navParams.get("kdsales");
    this.user_level = navParams.get("user_level");

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NoolistPage');
    this.listNoo = new Array();
    this.getData('', 0, 10, 'No');
  }

  form_search() {
    const prompt = this.alertCtrl.create({
      title: 'Cari',
      message: "Masukan kata untuk mencari Toko N.O.O",
      inputs: [
        {
          name: 'src',
          placeholder: 'Apa yang anda cari...'
        },
      ],
      buttons: [
        {
          text: 'Batal',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Cari',
          handler: data => {
            console.log('Saved clicked');
            this.getlistNoo(data.src);
          }
        }
      ]
    });
    prompt.present();
  }


  getlistNoo(src) {
    if (src) {
      src = src;
    } else {
      src = '';
    }
    this.listNoo = new Array();
    this.getData(src, 0, 10, 'No');
  }

  getData(src, first, offset, infinite) {

    this.first = first;
    this.src = src;


    const loader = this.loadingCtrl.create({
      content: "Mohon Menunggu..."
    });

    if (infinite == 'No') {
      loader.present();
    }


    this.storage.get('user_session_storage').then((res) => {
      let body;

      if (this.from_storage == 'No') {
        body = {
          first: first,
          offset: offset,
          keyword: src,
          kdcabang: this.kdcabang,
          user_level: this.user_level,
          kdoutlet: this.kdoutlet,
          kdsales: this.kdsales,
          username: res[0].username
        };
      } else {
        body = {
          first: first,
          offset: offset,
          keyword: src,
          kdcabang: res[0].cabang_id,
          user_level: res[0].user_level,
          kdoutlet: res[0].kdoutlet,
          kdsales: res[0].kdsales,
          username: res[0].username
        };
      }

      this.postPvdr.postData(body, 'outlet/getNoo').subscribe((data) => {


        if (data.success) {
          loader.dismiss();
          for (let i = 0; i < data.result.length; i++) {

            var tipedata;
            if (data.result[i]['TypeData'] == 'S') {
              tipedata = "Server";
            } else {
              tipedata = "WhatsApp";
            }

            this.listNoo.push(
              {
                id: data.result[i]['id'],
                KdOutlet: data.result[i]['KdOutlet'],
                OutletName: data.result[i]['OutletName'],
                Address: data.result[i]['Address'],
                UserUpload: data.result[i]['UserUpload'],
                DateUpload: data.result[i]['DateUpload'],
                UserValidate: data.result[i]['UserValidate'],
                DateValidate: data.result[i]['DateValidate'],
                Lat: data.result[i]['Lat'],
                Lng: data.result[i]['Lng'],
                Status: data.result[i]['Status'],
                Images: data.result[i]['Images'],
                tipedata: tipedata,
                Note: data.result[i]['Note'],
                NoteValidator: data.result[i]['NoteValidator']
              });
          }
        } else {
          loader.dismiss();
          if (infinite == 'No') {
            const alert = this.alertCtrl.create({
              title: 'Info',
              subTitle: 'Mohon Maaf Data Tidak Di Temukan',
              message: '',
              buttons: ['OK']
            });
            alert.present();
          }
          return false;
        }

      }, error => {
        loader.dismiss();
        const alert = this.alertCtrl.create({
          title: 'Mohon Maaf',
          subTitle: 'Sedang tidak terkoneksi dengan server. Silahkan coba kembali beberapa saat',
          buttons: ['OK']
        });
        alert.present();
        return false;
      });

    });

  }

  doRefresh(refresher) {
    this.listNoo = new Array();
    this.getData('', 0, 10, 'No');
    setTimeout(() => {
      refresher.complete();
    }, 1000);
  }

  doInfinite(infiniteScroll, src, first, filter) {

    first = first + 10;

    console.log('Begin async operation');

    setTimeout(() => {
      this.getData(src, first, 10, 'Yes');
      infiniteScroll.complete();
    }, 1000);

  }

  aksi(ev, id, lat, lng, name_outlet) {
    //popover
    let popover = this.popoverCtrl.create(AksiPage, { id: id, lat: lat, lng: lng, name_noo: name_outlet });
    popover.present({
      ev: ev
    });

    //ketika popover di tutup
    popover.onDidDismiss(res => {

      if (res) {
        if (res.edit) {
          this.edit(id);
        }
      }
    })
  }

  edit(id) {
    const prompt = this.alertCtrl.create({
      title: 'Edit N.O.O',
      message: "Masukkan Kode Toko Resmi Untuk Toko Ini.",
      inputs: [
        {
          name: 'new_kdoutlet',
          placeholder: 'Kode Toko'
        },
      ],
      buttons: [
        {
          text: 'Batal',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Simpan',
          handler: data => {
            this.save_edit(id, data.new_kdoutlet);
          }
        }
      ]
    });
    prompt.present();
  }

  save_edit(id, new_kdoutlet) {

    const loader = this.loadingCtrl.create({
      content: "Proses Simpan Toko Baru..."
    });
    loader.present();

    this.storage.get('user_session_storage').then((res) => {
      let body = {
        id: id,
        new_kdoutlet: new_kdoutlet,
        username: res[0].username
      };

      this.postPvdr.postData(body, 'outlet/saveNewOutlet').subscribe((data) => {

        if (data.success) {
          loader.dismiss();

          const confirm = this.alertCtrl.create({
            title: 'Info',
            message: 'Penyimpanan Kode Toko Baru Berhasil',
            buttons: [
              {
                text: 'Oke',
                handler: () => {
                  this.ionViewDidLoad();
                }
              }
            ]
          });
          confirm.present();
          return false;

        } else {
          loader.dismiss();
          const alert = this.alertCtrl.create({
            title: 'Info',
            subTitle: 'Penyimpanan Kode Toko Baru Gagal',
            message: '',
            buttons: ['OK']
          });
          alert.present();
          return false;
        }

      }, error => {
        loader.dismiss();
        const alert = this.alertCtrl.create({
          title: 'Mohon Maaf',
          subTitle: 'Sedang tidak terkoneksi dengan server. Silahkan coba kembali beberapa saat',
          buttons: ['OK']
        });
        alert.present();
        return false;
      });
    });

  }


}

@Component({
  templateUrl: 'aksi.html'
})
export class AksiPage {
  id: any;
  lat: any;
  lng: any;
  name_noo: any;
  hak_meresmikan_noo: any;

  constructor(
    public alertCtrl: AlertController,
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    public navParams: NavParams,
    public postPvdr: Provider,
    public storage: Storage,
    public loadingCtrl: LoadingController
  ) {
    this.id = navParams.get("id");
    this.lat = navParams.get("lat");
    this.lng = navParams.get("lng");
    this.name_noo = navParams.get("name_noo");

    this.storage.get('user_session_storage').then((res) => {
      if ((res[0].user_level == '-1' && res[0].developer == '1') || res[0].user_level == '2') {
        this.hak_meresmikan_noo = 'Yes';
      } else {
        this.hak_meresmikan_noo = 'No';
      }
    });

  }

  ionViewDidLoad() {
    //
  }

  close() {
    this.viewCtrl.dismiss();
  }

  peta() {
    this.navCtrl.push(PetanooPage, { lat: this.lat, lng: this.lng, name_noo: this.name_noo });
    this.close();
  }

  detail(id) {
    this.navCtrl.push(DetailhistoryPage, { id: this.id });
    this.close();
  }

  edit() {
    this.viewCtrl.dismiss({ edit: true });
  }


}
