import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController } from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { AlertController } from 'ionic-angular';
import { ViewController } from 'ionic-angular';
import { NavParams } from 'ionic-angular';
import { ModalController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { Provider } from '../../config/provider';
import { FormoutletPage } from '../../pages/formoutlet/formoutlet';
import { Storage } from '@ionic/storage';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { Platform } from 'ionic-angular';

declare var google;

@Component({
  selector: 'page-detaillocationoutlet',
  templateUrl: 'detaillocationoutlet.html',
})
export class DetaillocationoutletPage {

  @ViewChild('map') mapElement: ElementRef;

  base_url = 'http://api.vci.co.id/public/outlets/';

  qrData = null;
  createdCode = null;
  scannedCode = null;

  id: any;
  lat: any;
  lng: any;
  outlet: any;
  name: any;
  address: any;
  images: any;

  constructor(
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    public barcodeScanner: BarcodeScanner,
    public geolocation: Geolocation,
    public viewCtrl: ViewController,
    public navParams: NavParams,
    public postPvdr: Provider,
    public storage: Storage,
    public plt: Platform,
    private screenOrientation: ScreenOrientation,
    private locationAccuracy: LocationAccuracy,
    private androidPermissions: AndroidPermissions,
    public modalCtrl: ModalController
  ) {


    this.scanCodex(navParams.get("id"), navParams.get("lat"), navParams.get("lng"), navParams.get("outlet"), navParams.get("name"), navParams.get("address"));

    this.id = navParams.get("id");
    this.lat = navParams.get("lat");
    this.lng = navParams.get("lng");
    this.outlet = navParams.get("outlet");
    this.name = navParams.get("name");
    this.address = navParams.get("address");
    this.images = this.base_url + navParams.get("images");

    //if (this.plt.is('android')) {
    //    this.lockScreenOrientation();
    //}

  }

  ionViewDidLoad() {
    this.initMap();
  }

  async lockScreenOrientation() {

    try {
      this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.LANDSCAPE);
    } catch (error) {
      console.log(error);
    }

  }

  //Check if application having GPS access permission  
  checkGPSPermission() {

    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
      result => {
        console.log('status harus melewati permission : ', result.hasPermission);
        if (result.hasPermission) {

          //If having permission show 'Turn On GPS' dialogue
          this.askToTurnOnGPS();
          console.log('masuk ke pertanyaan permission');
        } else {

          //If not having permission ask for permission
          this.requestGPSPermission();
          console.log('masuk ke request permission');
        }
      },
      err => {
        console.log(err);
      }
    );

  }

  requestGPSPermission() {
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {
      if (canRequest) {
        console.log("4");
      } else {
        //Show 'GPS Permission Request' dialogue
        this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
          .then(
            () => {
              // call method to turn on GPS
              this.askToTurnOnGPS();
              console.log('nyalakan GPS');
            },
            error => {
              //Show alert if user click on 'No Thanks'
              console.log('requestPermission Error requesting location permissions ' + error)
            }
          );
      }
    });
  }

  askToTurnOnGPS() {
    this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
      () => {
        // When GPS Turned ON call method to get Accurate location coordinates
        console.log('masuk ke pencarian koordinat');
        this.initMap();

      },
      error => console.log('Error requesting location permissions ' + JSON.stringify(error))
    );
  }

  initMap() {
    var myLatlng = { lat: -6.1741808, lng: 106.8079343 };
    var map = new google.maps.Map(this.mapElement.nativeElement, {
      zoom: 13,
      center: myLatlng,
      zoomControl: false,
      mapTypeControl: false,
      scaleControl: false,
      streetViewControl: false,
      rotateControl: false,
      fullscreenControl: false
    });
  }

  mylocationPage(marker) {

    google.maps.event.addListener(marker, 'click', () => {
    });
  }

  createCode() {
    this.createdCode = this.qrData;
  }


  scanCodex(id, lat, lng, outlet, name, address) {
    const prompt = this.alertCtrl.create({
      title: 'Info!',
      message: "Result QR Code Scanner",
      inputs: [
        {
          name: 'outlet_name',
          placeholder: 'outlet name',
          value: name
        },
        {
          name: 'address',
          placeholder: 'address',
          value: address
        },
        {
          name: 'latitude',
          placeholder: 'latitude',
          value: lat
        },
        {
          name: 'longitude',
          placeholder: 'longitude',
          value: lng
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'View',
          handler: data => {
            console.log('Saved clicked');
            var myLatlng = { lat: parseFloat(lat), lng: parseFloat(lng) };
            var map = new google.maps.Map(this.mapElement.nativeElement, {
              zoom: 17,
              center: myLatlng,
              rotateControl: false,
              fullscreenControl: false,
              mapTypeControl: false,
              scaleControl: false,
              streetViewControl: true,
              streetViewControlOptions: {
                position: google.maps.ControlPosition.LEFT_TOP
              },
              zoomControl: true,
              zoomControlOptions: {
                position: google.maps.ControlPosition.LEFT_TOP
              }
            });

            // var map = new google.maps.Map(this.mapElement.nativeElement, {
            //   center: myLatlng,
            //   zoom: 14
            // });

            var image = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';
            var marker = new google.maps.Marker({
              position: myLatlng,
              map: map,
              icon: image
            });

            //untuk button validasi
            //google.maps.event.addListener(marker, 'click', () => {
            //  this.detailLocation(id,outlet, lat, lng , name, address,'');
            //});

            var contentString =
              '<h4>' + data.outlet_name + '&nbsp;&nbsp;&nbsp;&nbsp;</h4>' +
              '<div>' +
              '<p>' + data.address + '&nbsp;&nbsp;&nbsp;&nbsp;' +
              '</p>' +
              '</div>';

            marker.content = contentString;

            var infoWindow = new google.maps.InfoWindow();
            google.maps.event.addListener(marker, 'click', function () {
              infoWindow.setContent(this.content);
              infoWindow.open(this.getMap(), this);
            });

            var panorama = new google.maps.StreetViewPanorama(
              document.getElementById('pano'), {
              position: myLatlng,
              fullscreenControl: false,
              pov: {
                heading: 34,
                pitch: 10
              }
            });
            map.setStreetView(panorama);



          }
        }
      ]
    });
    prompt.present();


  }

  detailLocation(id, outlet, lat, lng, name, address, typevalidate) {

    var tipe;
    if (typevalidate == '1') {
      tipe = "Validate ";
    } else {
      tipe = "No Validate ";
    }

    const prompt = this.alertCtrl.create({
      title: tipe,
      message: tipe + "this location. save to server.",
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: data => {


            const prompts = this.alertCtrl.create({
              title: 'Info',
              message: "Add Note For Information",
              inputs: [
                {
                  name: 'sebab',
                  placeholder: 'Note Here ...'
                },
              ],
              buttons: [
                {
                  text: 'Cancel',
                  handler: data => {
                    console.log('Cancel clicked');
                  }
                },
                {
                  text: 'Send',
                  handler: data => {


                    this.storage.get('user_session_storage').then((res) => {
                      let body = {
                        id: id,
                        outlet: outlet,
                        kdcabang: res[0].cabang_id,
                        lat: lat,
                        lng: lng,
                        name: name,
                        address: address,
                        notevalidate: data.sebab,
                        status: typevalidate,
                        marker: res[0].kdsales + ' - ' + res[0].employee_name
                      };

                      this.postPvdr.postData(body, 'outlet/updateoutlet').subscribe((data) => {

                        const alert = this.alertCtrl.create({
                          title: 'Info',
                          subTitle: data.subtitle,
                          message: data.msg,
                          buttons: ['OK']
                        });
                        alert.present();

                      }, error => {

                        const alert = this.alertCtrl.create({
                          title: 'Info',
                          subTitle: error,
                          message: 'Not Connected To Server',
                          buttons: ['OK']
                        });
                        alert.present();
                      });
                    });


                  }
                }
              ]
            });
            prompts.present();


          }
        }
      ]
    });
    prompt.present();

  }


  close() {
    this.viewCtrl.dismiss();
  }

}


