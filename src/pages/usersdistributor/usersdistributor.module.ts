import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UsersdistributorPage } from './usersdistributor';

@NgModule({
  declarations: [
    UsersdistributorPage,
  ],
  imports: [
    IonicPageModule.forChild(UsersdistributorPage),
  ],
})
export class UsersdistributorPageModule {}
