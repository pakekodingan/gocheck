import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { Provider } from '../../config/provider';
import { Storage } from '@ionic/storage';

import { PerformaPage } from '../../pages/performa/performa';
/**
 * Generated class for the UsersdistributorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-usersdistributor',
  templateUrl: 'usersdistributor.html',
})
export class UsersdistributorPage {

  kdoutlet: any;
  kdcabang: any;
  close_page: any;
  bisa_pilih: any;
  reset_password: any;
  hak_reset_password: any;
  performance: any;

  listUsers: Array<any>;
  src: any;
  filter: String = 'all';
  first: number;

  constructor(
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    public postPvdr: Provider,
    public alertCtrl: AlertController,
    public storage: Storage,
    public loadingCtrl: LoadingController,
    public navParams: NavParams) {
    this.kdoutlet = navParams.get("kdoutlet");
    this.kdcabang = navParams.get("kdcabang");
    this.close_page = navParams.get("close");
    this.bisa_pilih = navParams.get("bisa_pilih");
    this.reset_password = navParams.get("reset_password");
    console.log('reset password : ', this.reset_password)
    this.storage.get('user_session_storage').then((res) => {
      if (res[0].user_level == '4' || res[0].user_level == '2' || res[0].user_level == '10') {
        this.performance = navParams.get("performa");
      } else {
        this.performance = 'Yes';
      };

      if (res[0].user_level == '2' || (res[0].user_level == '-1' && res[0].devloper == '1')) {
        this.hak_reset_password = 'Yes';
      } else {
        this.hak_reset_password = 'No';
      };

    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UsersdistributorPage');
    this.listUsers = new Array();
    this.getData('', 0, 10, 'all', 'No');
  }

  form_search() {
    const prompt = this.alertCtrl.create({
      title: 'Cari',
      message: "Masukan kata untuk mencari kode atau nama user",
      inputs: [
        {
          name: 'src',
          placeholder: 'Apa yang anda cari...'
        },
      ],
      buttons: [
        {
          text: 'Batal',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Cari',
          handler: data => {
            console.log('Saved clicked');
            this.getListUserDistributor(data.src);
          }
        }
      ]
    });
    prompt.present();
  }

  getListUserDistributor(src) {
    this.listUsers = new Array();
    if (src) {
      src = src;
    } else {
      src = '';
    }
    this.getData(src, 0, 10, 'all', 'No');
  }

  getData(src, first, offset, filter, infinite) {

    this.first = first;
    this.src = src;

    const loader = this.loadingCtrl.create({
      content: "Mohon Menunggu..."
    });
    if (infinite == 'No') {
      loader.present();
    }

    this.storage.get('user_session_storage').then((res) => {
      let body = {
        kdcabang: this.kdcabang,
        first: first,
        offset: offset,
        filter: filter,
        keyword: src,
        kdoutlet: this.kdoutlet,
        user_level: res[0].user_level,
        username: res[0].username
      };

      this.postPvdr.postData(body, 'outlet/getListUserDistributor2').subscribe((data) => {


        if (data.success) {
          loader.dismiss();
          for (let i = 0; i < data.result.length; i++) {
            this.listUsers.push(
              {
                username: data.result[i]['username'],
                name: data.result[i]['name'],
                kdsales: data.result[i]['kdsales'],
                kdoutlet: data.result[i]['kdoutlet'],
                kdcabang: data.result[i]['kdcabang']
              });

          }
        } else {
          loader.dismiss();
          if (infinite == 'No') {
            const alert = this.alertCtrl.create({
              title: 'Info',
              subTitle: 'Mohon Maaf Data Tidak Di Temukan',
              message: '',
              buttons: ['OK']
            });
            alert.present();
          }
          return false;
        }

      }, error => {

        loader.dismiss();
        const alert = this.alertCtrl.create({
          title: 'Mohon Maaf',
          subTitle: 'Sedang tidak terkoneksi dengan server. Silahkan coba kembali beberapa saat',
          buttons: ['OK']
        });
        alert.present();
        return false;

      });

    });
  }

  doRefresh(refresher) {
    this.listUsers = new Array();
    this.getData('', 0, 10, 'all', 'No');
    setTimeout(() => {
      refresher.complete();
    }, 1000);
  }

  doInfinite(infiniteScroll, src, first, filter) {

    first = first + 10;

    console.log('filter : ', filter)
    console.log('Begin async operation');

    setTimeout(() => {
      this.getData(src, first, 10, filter, 'Yes');
      infiniteScroll.complete();
    }, 1000);

  }

  performa(kdcabang, kdoutlet, kdsales, name) {
    this.navCtrl.push(PerformaPage, { kdcabang: kdcabang, storage: 'No', user_level: '4', kdoutlet: kdoutlet, kdsales: kdsales, sales_name: name })
  }

  close() {
    this.viewCtrl.dismiss();
  }

  pilih_ini(kdsales, Nama) {
    this.viewCtrl.dismiss({ kdsales: kdsales, sales_name: Nama });
  }

  resetPassword(name, username, kdsales, kdcabang, kdoutlet) {
    const confirm = this.alertCtrl.create({
      title: 'Info',
      message: 'Apakah anda yakin akan mereset password sales ' + name + '?',
      buttons: [
        {
          text: 'Batal',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Ya',
          handler: () => {
            this.reseting_password(username, kdsales, kdcabang, kdoutlet);
          }
        }
      ]
    });
    confirm.present();
  }

  reseting_password(username, kdsales, kdcabang, kdoutlet) {
    const prompt = this.alertCtrl.create({
      title: 'Reset Password',
      message: "Silahkan Masukkan Password Baru",
      inputs: [
        {
          name: 'password',
          placeholder: 'Password'
        },
      ],
      buttons: [
        {
          text: 'Batal',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Simpan',
          handler: data => {
            this.action_reset_password(data.password, username, kdsales, kdcabang, kdoutlet);
          }
        }
      ]
    });
    prompt.present();
  }

  action_reset_password(new_password, username, kdsales, kdcabang, kdoutlet) {
    const loader = this.loadingCtrl.create({
      content: "Proses Reset..."
    });
    loader.present();


    this.storage.get('user_session_storage').then((res) => {
      let body = {
        new_password: new_password,
        username_sales: username,
        kdsales: kdsales,
        kdcabang: kdcabang,
        kdoutlet: kdoutlet,
        username_resetor: res[0].username
      };

      this.postPvdr.postData(body, 'login/resetPassword').subscribe((data) => {

        if (data.success) {
          loader.dismiss();
          const alert = this.alertCtrl.create({
            title: 'Info',
            subTitle: 'Reset Password Berhasil.',
            message: '',
            buttons: ['OK']
          });
          alert.present();
          return false;
        } else {
          loader.dismiss();
          const alert = this.alertCtrl.create({
            title: 'Info',
            subTitle: 'Reset Password Gagal.',
            message: '',
            buttons: ['OK']
          });
          alert.present();
          return false;
        }

      }, error => {

        loader.dismiss();
        const alert = this.alertCtrl.create({
          title: 'Mohon Maaf',
          subTitle: 'Sedang tidak terkoneksi dengan server. Silahkan coba kembali beberapa saat',
          buttons: ['OK']
        });
        alert.present();
        return false;

      });

    });
  }

}
