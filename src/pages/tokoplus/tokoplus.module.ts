import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TokoplusPage } from './tokoplus';

@NgModule({
  declarations: [
    TokoplusPage,
  ],
  imports: [
    IonicPageModule.forChild(TokoplusPage),
  ],
})
export class TokoplusPageModule {}
