import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { Provider } from '../../config/provider';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the TokoplusPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tokoplus',
  templateUrl: 'tokoplus.html',
})
export class TokoplusPage {


  listTokoplus: Array<any>;
  src: any;
  first: number;

  from_storage: any;
  user_level: any;
  kdoutlet: any;
  kdcabang: any;
  kdsales: any;
  performa: any;

  hak_swtiching: any;

  constructor(public navCtrl: NavController,
    public viewCtrl: ViewController,
    public postPvdr: Provider,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public storage: Storage,
    public navParams: NavParams) {

    this.from_storage = navParams.get("from_storage");
    this.kdoutlet = navParams.get("kdoutlet");
    this.kdcabang = navParams.get("kdcabang");
    this.kdsales = navParams.get("kdsales");
    this.user_level = navParams.get("user_level");
    this.performa = navParams.get("performa");

    this.storage.get('user_session_storage').then((res) => {
      if ((res[0].user_level == '-1' && res[0].developer == '1') || res[0].user_level == '2') {
        this.hak_swtiching = 'Yes';
      } else {
        this.hak_swtiching = 'No';
      }
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NoolistPage');
    this.listTokoplus = new Array();
    this.getData('', 0, 10, 'No');
  }

  form_search() {
    const prompt = this.alertCtrl.create({
      title: 'Cari',
      message: "Masukan kata untuk mencari Toko Plus",
      inputs: [
        {
          name: 'src',
          placeholder: 'Apa yang anda cari...'
        },
      ],
      buttons: [
        {
          text: 'Batal',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Cari',
          handler: data => {
            console.log('Saved clicked');
            this.getlistTokoplus(data.src);
          }
        }
      ]
    });
    prompt.present();
  }


  getlistTokoplus(src) {
    if (src) {
      src = src;
    } else {
      src = '';
    }
    this.listTokoplus = new Array();
    this.getData(src, 0, 10, 'No');
  }

  getData(src, first, offset, infinite) {

    this.first = first;
    this.src = src;


    const loader = this.loadingCtrl.create({
      content: "Mohon Menunggu..."
    });

    if (infinite == 'No') {
      loader.present();
    }


    this.storage.get('user_session_storage').then((res) => {
      let body;

      if (this.from_storage == 'No') {
        body = {
          first: first,
          offset: offset,
          keyword: src,
          performa: this.performa,
          kdcabang: this.kdcabang,
          user_level: this.user_level,
          kdoutlet: this.kdoutlet,
          kdsales: this.kdsales,
          username: res[0].username
        };
      } else {
        body = {
          first: first,
          offset: offset,
          keyword: src,
          performa: this.performa,
          kdcabang: res[0].cabang_id,
          user_level: res[0].user_level,
          kdoutlet: res[0].kdoutlet,
          kdsales: res[0].kdsales,
          username: res[0].username
        };
      }

      this.postPvdr.postData(body, 'outlet/getTokoplus').subscribe((data) => {


        if (data.success) {
          loader.dismiss();
          for (let i = 0; i < data.result.length; i++) {

            var tipedata;
            if (data.result[i]['TypeData'] == 'S') {
              tipedata = "Server";
            } else {
              tipedata = "WhatsApp";
            }

            this.listTokoplus.push(
              {
                id: data.result[i]['id'],
                KdOutlet: data.result[i]['KdOutlet'],
                KdCabang: data.result[i]['KdCabang'],
                KdDistributor: data.result[i]['KdDistributor'],
                OutletName: data.result[i]['OutletName'],
                Address: data.result[i]['Address'],
                UserUpload: data.result[i]['UserUpload'],
                DateUpload: data.result[i]['DateUpload'],
                UserValidate: data.result[i]['UserValidate'],
                DateValidate: data.result[i]['DateValidate'],
                Lat: data.result[i]['Lat'],
                Lng: data.result[i]['Lng'],
                Status: data.result[i]['Status'],
                Images: data.result[i]['Images'],
                tipedata: tipedata,
                Note: data.result[i]['Note'],
                NoteValidator: data.result[i]['NoteValidator'],
                sales_asli: data.result[i]['sales_asli'],
                kdsales_asli: data.result[i]['kdsales_asli'],
                KdSales: data.result[i]['KdSales']
              });
          }
        } else {
          loader.dismiss();
          if (infinite == 'No') {
            const alert = this.alertCtrl.create({
              title: 'Info',
              subTitle: 'Mohon Maaf Data Tidak Di Temukan',
              message: '',
              buttons: ['OK']
            });
            alert.present();
          }
          return false;
        }

      }, error => {
        loader.dismiss();
        const alert = this.alertCtrl.create({
          title: 'Mohon Maaf',
          subTitle: 'Sedang tidak terkoneksi dengan server. Silahkan coba kembali beberapa saat',
          buttons: ['OK']
        });
        alert.present();
        return false;
      });

    });

  }

  doRefresh(refresher) {
    this.listTokoplus = new Array();
    this.getData('', 0, 10, 'No');
    setTimeout(() => {
      refresher.complete();
    }, 1000);
  }

  doInfinite(infiniteScroll, src, first, filter) {

    first = first + 10;

    console.log('Begin async operation');

    setTimeout(() => {
      this.getData(src, first, 10, 'Yes');
      infiniteScroll.complete();
    }, 1000);

  }

  switching(KdOutlet, KdCabang, KdDistributor, KdSales, kdsales_asli, UserUpload, sales_asli) {

    const confirm = this.alertCtrl.create({
      title: 'Info',
      message: 'Apakah anda yakin akan melakukan swtiching toko ini dari ' + sales_asli + ' Ke ' + UserUpload + '?',
      buttons: [
        {
          text: 'Batal',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Ya',
          handler: () => {

            this.switch(KdOutlet, KdSales, kdsales_asli, KdCabang, KdDistributor);

          }
        }
      ]
    });
    confirm.present();


  }

  switch(kdoutlet, kdsales, kodesales, kdcabang, kddistributor) {
    const loader = this.loadingCtrl.create({
      content: "Proses Swtiching..."
    });
    loader.present();


    this.storage.get('user_session_storage').then((res) => {
      let body = {
        kdcabang: kdcabang,
        kdsales: kdsales,
        kodesales: kodesales,
        kdoutlet: kdoutlet,
        kddistributor: kddistributor,
        username: res[0].username
      };

      this.postPvdr.postData(body, 'outlet/swtichingRuteSales').subscribe((data) => {


        if (data.success) {
          loader.dismiss();
          const confirm = this.alertCtrl.create({
            title: 'Info',
            message: 'Swtiching Berhasil',
            buttons: [
              {
                text: 'Oke',
                handler: () => {
                  this.ionViewDidLoad();
                }
              }
            ]
          });
          confirm.present();
        } else {
          loader.dismiss();

          const alert = this.alertCtrl.create({
            title: 'Info',
            subTitle: 'Swtiching Gagal',
            message: '',
            buttons: ['OK']
          });
          alert.present();

          return false;
        }

      }, error => {

        loader.dismiss();
        const alert = this.alertCtrl.create({
          title: 'Mohon Maaf',
          message: 'Sedang tidak terkoneksi dengan server. Silahkan coba kembali beberapa saat',
          buttons: ['OK']
        });
        alert.present();
        return false;

      });

    });
  }


}

