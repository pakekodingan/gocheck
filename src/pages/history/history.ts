import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { Provider } from '../../config/provider';
import { Storage } from '@ionic/storage';
import { DetailhistoryPage } from '../../pages/detailhistory/detailhistory';

/**
 * Generated class for the HistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-history',
  templateUrl: 'history.html',
})
export class HistoryPage {

  listOutlet: Array<any>;
  src: any;
  filter: String = 'all';
  first: number;

  pending: Number = 0;
  validate: Number = 0;
  reject: Number = 0;

  constructor(
    public navCtrl: NavController,
    public postPvdr: Provider,
    public storage: Storage,
    public viewCtrl: ViewController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public navParams: NavParams
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HistoryPage');
    this.listOutlet = new Array();
    this.getData('', 0, 10, 'all', 'No');
  }

  form_search() {
    const prompt = this.alertCtrl.create({
      title: 'Cari',
      message: "Masukan kata untuk mencari kode atau nama toko",
      inputs: [
        {
          name: 'src',
          placeholder: 'Apa yang anda cari...'
        },
      ],
      buttons: [
        {
          text: 'Batal',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Cari',
          handler: data => {
            console.log('Saved clicked');
            this.getOutlets(data.src);
          }
        }
      ]
    });
    prompt.present();
  }

  getOutlets(src) {
    this.listOutlet = new Array();
    if (src) {
      src = src;
    } else {
      src = '';
    }
    this.getData(src, 0, 10, 'all', 'No');
  }

  getData(src, first, offset, filter, infinite) {

    this.first = first;
    this.filter = filter;
    this.src = src;

    const loader = this.loadingCtrl.create({
      content: "Mohon Menunggu..."
    });
    if (infinite == 'No') {
      loader.present();
    }

    this.storage.get('user_session_storage').then((res) => {
      let body = {
        kdsales: res[0].kdsales,
        kdcabang: res[0].cabang_id,
        user_level: res[0].user_level,
        kdoutlet: res[0].kdoutlet,
        username: res[0].username,
        first: first,
        offset: offset,
        filter: filter,
        keyword: src
      };

      this.postPvdr.postData(body, 'outlet/getlistoutlet2').subscribe((data) => {

        if (data.success) {
          loader.dismiss();

          this.pending = data.pending;
          this.validate = data.validate;
          this.reject = data.reject;

          for (let i = 0; i < data.result.length; i++) {
            var tipedata;
            if (data.result[i]['TypeData'] == 'S') {
              tipedata = "Server";
            } else {
              tipedata = "WhatsApp";
            }
            this.listOutlet.push(
              {
                id: data.result[i]['id'],
                KdOutlet: data.result[i]['KdOutlet'],
                OutletName: data.result[i]['OutletName'],
                Address: data.result[i]['Address'],
                UserUpload: data.result[i]['UserUpload'],
                DateUpload: data.result[i]['DateUpload'],
                UserValidate: data.result[i]['UserValidate'],
                DateValidate: data.result[i]['DateValidate'],
                Lat: data.result[i]['Lat'],
                Lng: data.result[i]['Lng'],
                Status: data.result[i]['Status'],
                Images: data.result[i]['Images'],
                tipedata: tipedata,
                Note: data.result[i]['Note'],
                NoteValidator: data.result[i]['NoteValidator']
              });

          }
        } else {
          loader.dismiss();
          if (infinite == 'No') {
            const alert = this.alertCtrl.create({
              title: 'Info',
              subTitle: 'Mohon Maaf Data Tidak Di Temukan',
              message: '',
              buttons: ['OK']
            });
            alert.present();
          }
          return false;
        }


      }, error => {
        loader.dismiss();
        const alert = this.alertCtrl.create({
          title: 'Mohon Maaf',
          subTitle: 'Sedang tidak terkoneksi dengan server. Silahkan coba kembali beberapa saat',
          buttons: ['OK']
        });
        alert.present();
        return false;
      });
    });
  }

  doRefresh(refresher) {
    this.listOutlet = new Array();
    this.getData('', 0, 10, 'all', 'No');
    setTimeout(() => {
      refresher.complete();
    }, 1000);
  }

  doInfinite(infiniteScroll, src, first, filter) {

    first = first + 10;

    console.log('Begin async operation');

    setTimeout(() => {
      this.getData(src, first, 10, filter, 'Yes');
      infiniteScroll.complete();
    }, 1000);
  }

  filtering() {
    let alert = this.alertCtrl.create();
    alert.setTitle('Filter');

    alert.addInput({
      type: 'radio',
      label: 'All',
      value: 'all',
      checked: false
    });

    alert.addInput({
      type: 'radio',
      label: 'Pending',
      value: 'pending',
      checked: false
    });

    alert.addInput({
      type: 'radio',
      label: 'Validate',
      value: 'validate',
      checked: false
    });

    alert.addInput({
      type: 'radio',
      label: 'Reject',
      value: 'reject',
      checked: false
    });

    alert.addButton('Cancel');
    alert.addButton({
      text: 'OK',
      handler: data => {
        this.listOutlet = new Array();
        this.getData('', 0, 10, data, 'No');
      }
    });
    alert.present();
  }

  detail(id) {
    this.navCtrl.push(DetailhistoryPage, { id: id })
  }

  dashboard(filter) {
    this.listOutlet = new Array();
    this.getData('', 0, 10, filter, 'No');
  }

}
