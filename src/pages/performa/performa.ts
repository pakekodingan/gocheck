import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Provider } from '../../config/provider';
import { Storage } from '@ionic/storage';

import chartJs from 'chart.js';

import { PetatokoPage } from '../../pages/petatoko/petatoko';
import { DetailbangetlistingsPage } from '../../pages/detailbangetlistings/detailbangetlistings';
import { NoolistPage } from '../../pages/noolist/noolist';
import { TokoplusPage } from '../../pages/tokoplus/tokoplus';

/**
 * Generated class for the PerformaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-performa',
  templateUrl: 'performa.html',
})
export class PerformaPage {

  @ViewChild('doughnutCanvas1') doughnutCanvas1;
  @ViewChild('doughnutCanvas2') doughnutCanvas2;
  doughnutChart1: any;
  doughnutChart2: any;

  toko_sudah_terpetakan: any;
  toko_belum_terpetakan: any;
  toko_pegangan: any;
  toko_diluar_pegangan_sudah_terpetakan: any;
  toko_noo: any;

  seluruh_toko: any;
  seluruh_toko_sudah_terpetakan: any;
  seluruh_toko_belum_terpetakan: any;
  seluruh_toko_noo: any;

  user_level: any;
  performa: any;
  from_storage: any;
  kdoutlet: any;
  kdcabang: any;
  kdsales: any;
  sales_name: any;


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public postPvdr: Provider,
    public storage: Storage
  ) {

    this.from_storage = navParams.get("storage");
    this.kdoutlet = navParams.get("kdoutlet");
    this.kdcabang = navParams.get("kdcabang");
    this.kdsales = navParams.get("kdsales");
    this.user_level = navParams.get("user_level");

    if (this.from_storage == 'No') {

      this.sales_name = navParams.get("sales_name");

    } else {
      this.storage.get('user_session_storage').then((res) => {
        this.sales_name = res[0].employee_name;
      });
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PerformaPage');
    this.getData();
  }

  getData() {

    this.storage.get('user_session_storage').then((res) => {
      let body;
      if (this.from_storage == 'No') {
        body = {
          kdcabang: this.kdcabang,
          user_level: this.user_level,
          kdoutlet: this.kdoutlet,
          kdsales: this.kdsales,
          username: res[0].username
        };
      } else {
        body = {
          kdcabang: res[0].cabang_id,
          user_level: res[0].user_level,
          kdoutlet: res[0].kdoutlet,
          kdsales: res[0].kdsales,
          username: res[0].username
        };

        //tergantikan
        this.from_storage = 'Yes';
        this.kdoutlet = res[0].kdoutlet;
        this.kdcabang = res[0].cabang_id;
        this.kdsales = res[0].kdsales;
        this.user_level = res[0].user_level;
      }


      this.postPvdr.postData(body, 'outlet/getchart2').subscribe((data) => {

        if (data.success) {

          this.toko_sudah_terpetakan = data.toko_sudah_terpetakan;
          this.toko_belum_terpetakan = data.toko_belum_terpetakan;
          this.toko_pegangan = data.toko_pegangan;
          this.toko_diluar_pegangan_sudah_terpetakan = data.toko_diluar_pegangan_sudah_terpetakan;
          this.toko_noo = data.toko_noo;
          this.getDoughnutChart1(parseInt(this.toko_sudah_terpetakan), parseInt(this.toko_belum_terpetakan));

          //performa
          this.performa = (parseInt(this.toko_sudah_terpetakan) / parseInt(this.toko_pegangan) * 100).toFixed(1);

          this.seluruh_toko = data.seluruh_toko;
          this.seluruh_toko_sudah_terpetakan = data.seluruh_toko_sudah_terpetakan;
          this.seluruh_toko_belum_terpetakan = data.seluruh_toko_belum_terpetakan;
          this.seluruh_toko_noo = data.seluruh_toko_noo;
          this.getDoughnutChart2(parseInt(this.seluruh_toko_sudah_terpetakan), parseInt(this.seluruh_toko_belum_terpetakan));

        } else {
          //failed
          return false;
        }

      }, error => {
        //not connect
        return false;
      });

    });

  }

  listOutlet() {
    var kdcabang;
    var kdoutlet;
    this.storage.get('user_session_storage').then((res) => {
      if (res[0].user_level == '-1' || res[0].user_level == '9') {
        kdcabang = '';
        kdoutlet = '';
      } else {
        kdcabang = res[0].cabang_id;
        kdoutlet = res[0].kdoutlet;
      }
      this.navCtrl.push(DetailbangetlistingsPage, { kdoutlet: kdoutlet, kdcabang: kdcabang, kdsales: this.kdsales, viewuser: 'No', title: this.sales_name, close: 'No', bisa_pilih: 'No' });
    });
  }

  ngAfterViewInit() {
    setTimeout(() => {
      // this.doughnutChart1 = this.getDoughnutChart1();
      // this.doughnutChart2 = this.getDoughnutChart2();
    }, 250)
  }

  getChart(context, chartType, data, options?) {
    return new chartJs(context, {
      data,
      options,
      type: chartType
    })
  }

  getDoughnutChart1(sudah, belum) {
    const data = {
      display: true,
      labels: ['Sudah', 'Belum'],
      datasets: [{
        data: [sudah, belum],
        backgroundColor: [
          'rgb(255, 120, 0)',
          'rgb(37, 39, 43)'
        ]
      }]
    }

    const options = {
      legend: {
        display: true,
        position: 'bottom'
      },
      title: {
        display: false,
        text: 'Chart Data Terkumpul',
        position: 'bottom'
      }
    }

    return this.getChart(this.doughnutCanvas1.nativeElement, 'doughnut', data, options);
  }

  getDoughnutChart2(sudah, belum) {
    const data = {
      display: true,
      labels: ['Sudah', 'Belum'],
      datasets: [{
        data: [sudah, belum],
        backgroundColor: [
          'rgb(255, 120, 0)',
          'rgb(37, 39, 43)'
        ]
      }]
    }

    const options = {
      legend: {
        display: true,
        position: 'bottom'
      },
      title: {
        display: false,
        text: 'Chart Data Terkumpul',
        position: 'bottom'
      }
    }

    return this.getChart(this.doughnutCanvas2.nativeElement, 'doughnut', data, options);
  }

  peta_sendiri(kdcabang, kdoutlet, kdsales) {
    this.storage.get('user_session_storage').then((res) => {
      this.navCtrl.push(
        PetatokoPage, {
        kdcabang: kdcabang,
        kdoutlet: kdoutlet,
        kdsales: kdsales,
        distributor_name: res[0].distributor_name,
        title: this.sales_name,
        sales_name: this.sales_name,
        legend_type: '1'
      });
    });
  }

  peta_distributor(kdcabang, kdoutlet) {
    this.storage.get('user_session_storage').then((res) => {
      this.navCtrl.push(
        PetatokoPage, {
        kdcabang: kdcabang,
        kdoutlet: kdoutlet,
        kdsales: '',
        distributor_name: res[0].distributor_name,
        title: res[0].distributor_name,
        sales_name: 'Semua',
        legend_type: '2'
      });
    });
  }

  noo() {
    this.storage.get('user_session_storage').then((res) => {

      if (this.from_storage == 'No') {
        this.navCtrl.push(NoolistPage, { from_storage: this.from_storage, kdcabang: this.kdcabang, user_level: this.user_level, kdoutlet: this.kdoutlet, kdsales: this.kdsales });
      } else {
        this.navCtrl.push(NoolistPage, { from_storage: 'Yes', kdcabang: res[0].cabang_id, user_level: res[0].user_level, kdoutlet: res[0].kdoutlet, kdsales: res[0].kdsales });
      }
    });
  }

  tokoplus() {
    this.storage.get('user_session_storage').then((res) => {

      if (this.from_storage == 'No') {
        this.navCtrl.push(TokoplusPage, { from_storage: this.from_storage, kdcabang: this.kdcabang, user_level: this.user_level, kdoutlet: this.kdoutlet, kdsales: this.kdsales, performa: 'Yes' });
      } else {
        this.navCtrl.push(TokoplusPage, { from_storage: 'Yes', kdcabang: res[0].cabang_id, user_level: res[0].user_level, kdoutlet: res[0].kdoutlet, kdsales: res[0].kdsales, performa: 'Yes' });
      }
    });
  }



}