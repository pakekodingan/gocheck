import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PerformaPage } from './performa';

@NgModule({
  declarations: [
    PerformaPage,
  ],
  imports: [
    IonicPageModule.forChild(PerformaPage),
  ],
})
export class PerformaPageModule {}
