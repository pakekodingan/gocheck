import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PetatokoPage } from './petatoko';

@NgModule({
  declarations: [
    PetatokoPage,
  ],
  imports: [
    IonicPageModule.forChild(PetatokoPage),
  ],
})
export class PetatokoPageModule {}
