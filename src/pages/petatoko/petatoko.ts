import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { ModalController } from 'ionic-angular';
import { Provider } from '../../config/provider';
import { Storage } from '@ionic/storage';

import { SearchPage } from '../../pages/search/search';
declare var google;

/**
 * Generated class for the PetatokoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-petatoko',
  templateUrl: 'petatoko.html',
})
export class PetatokoPage {

  base_url = 'http://api.vci.co.id/public/outlets/';

  kdoutlet: any;
  kdcabang: any;
  kdsales: any;
  title: any;
  distributor_name: any;
  sales_name: any;
  legend_type: any;
  markers = [];

  toko_sudah_terpetakan: any;
  toko_belum_terpetakan: any;
  toko_pegangan: any;
  toko_diluar_pegangan_sudah_terpetakan: any;
  toko_noo: any;

  listOutlet: any;
  view_filter: String = 'No';

  @ViewChild('map') mapElement: ElementRef;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public postPvdr: Provider,
    public modalCtrl: ModalController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public storage: Storage
  ) {
    this.kdoutlet = navParams.get("kdoutlet");
    this.kdcabang = navParams.get("kdcabang");
    this.kdsales = navParams.get("kdsales");
    this.title = navParams.get("title");
    this.distributor_name = navParams.get("distributor_name");
    this.sales_name = navParams.get("sales_name");
    this.legend_type = navParams.get("legend_type");

    this.storage.get('user_session_storage').then((res) => {

      if (res[0].user_level != '4') {
        this.view_filter = 'Yes';
      }

    });


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PetatokoPage');
    // this.initMap();
    this.getMaps();
    this.getData(this.legend_type);

  }

  getMaps() {

    this.storage.get('user_session_storage').then((res) => {
      //super user dan nasional
      if (res[0].user_level == '-1' || res[0].user_level == '9') {
        this.initMap('nasional', 5.69);
        //regional
      } else if (res[0].user_level == '6') {
        this.initMap('regional', 7);

        //admin dan kepala distribusi
      } else if (res[0].user_level == '2' || res[0].user_level == '4' || res[0].user_level == '10') {
        this.initMap('distributor', 11);
      }
    });

  }

  initMap(area, zoom) {

    const loader = this.loadingCtrl.create({
      content: "Mohon Menunggu..."
    });
    loader.present();
    this.storage.get('user_session_storage').then((res) => {
      let body = {
        kdcabang: this.kdcabang,
        kdsales: this.kdsales,
        kdoutlet: this.kdoutlet,
        username: res[0].username
      };

      this.postPvdr.postData(body, 'outlet/getLocationTagSales2').subscribe((data) => {

        if (data.success) {
          loader.dismiss();

          var myLatlng;

          if (area == "nasional") {
            myLatlng = { lat: -0.620093, lng: 115.3094217 };
          } else if (area == "regional") {
            myLatlng = { lat: parseFloat(data.result[0]['Lat']), lng: parseFloat(data.result[0]['Lng']) };
          } else if (area == "distributor") {
            myLatlng = { lat: parseFloat(data.result[0]['Lat']), lng: parseFloat(data.result[0]['Lng']) };
          }

          var map = new google.maps.Map(this.mapElement.nativeElement, {
            zoom: zoom,
            center: myLatlng,
            zoomControl: true,
            zoomControlOptions: {
              position: google.maps.ControlPosition.LEFT_TOP
            },
            mapTypeControl: false,
            scaleControl: false,
            streetViewControl: false,
            rotateControl: false,
            fullscreenControl: false
          });


          for (let i = 0; i < data.result.length; i++) {

            // this.listOutlet.push(
            //   {
            //     id: data.result[i]['id'],
            //     KdOutlet: data.result[i]['KdOutlet'],
            //     OutletName: data.result[i]['OutletName'],
            //     Address: data.result[i]['Address'],
            //     UserUpload: data.result[i]['UserUpload'],
            //     DateUpload: data.result[i]['DateUpload'],
            //     UserValidate: data.result[i]['UserValidate'],
            //     DateValidate: data.result[i]['DateValidate'],
            //     Lat: data.result[i]['Lat'],
            //     Lng: data.result[i]['Lng'],
            //     Status: data.result[i]['Status'],
            //     NOO: data.result[i]['NOO'],
            //     Images: this.base_url + data.result[i]['Images'],
            //     tipedata: data.result[i]['TypeData'],
            //     Note: data.result[i]['Note'],
            //     NoteValidator: data.result[i]['NoteValidator'],
            //     toko_plus: data.result[i]['toko_plus']
            //   });

            //pilih icon
            var icon;
            if (data.result[i]['toko_plus'] == 'Yes' && data.result[i]['NOO'] == '0') {
              icon = 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-blue.png';
            } else if (data.result[i]['toko_plus'] == 'No' && data.result[i]['NOO'] == '0') {
              icon = 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-red.png';
            } else if (data.result[i]['NOO'] == '1') {
              icon = 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-green.png';
            }

            var Latlng = { lat: parseFloat(data.result[i]['Lat']), lng: parseFloat(data.result[i]['Lng']) };
            //marker
            var marker = new google.maps.Marker({
              position: Latlng,
              animation: google.maps.Animation.DROP,
              map: map,
              icon: icon
            });
            this.markers.push(marker);

            var contentString =
              '<h4>' + data.result[i]['OutletName'] + '&nbsp;&nbsp;</h4>' +
              '<div>' +
              '<p>' + data.result[i]['Address'] + '&nbsp;&nbsp;' + '<br>' +
              'Tanggal Upload : ' + data.result[i]['DateUpload'] + '&nbsp;&nbsp;' + '<br>' +
              'Catatan Sales :' + data.result[i]['Note'] + '&nbsp;&nbsp;' + '<br>' +
              'Tanggal Valid : ' + data.result[i]['DateValidate'] + '&nbsp;&nbsp;' + '<br>' +
              'Catatan Valid : ' + data.result[i]['NoteValidator'] + '&nbsp;&nbsp;' + '</p>' +
              '<div id="pic"><img src="' + this.base_url + data.result[i]['Images'] + '" width="200" height="250" />' +
              '</div>';

            marker.content = contentString;

            var infoWindow = new google.maps.InfoWindow();
            // google.maps.event.addListener(marker, 'mouseover', function () {
            google.maps.event.addListener(marker, 'click', function () {
              infoWindow.setContent(this.content);
              infoWindow.open(this.getMap(), this);
            });

          }

        } else {
          loader.dismiss();
          const alert = this.alertCtrl.create({
            title: 'Info',
            subTitle: 'Mohon Maaf Data Tidak Di Temukan',
            message: '',
            buttons: ['OK']
          });
          alert.present();
          this.indonesiaMap();
          return false;
        }

      }, error => {
        loader.dismiss();
        const alert = this.alertCtrl.create({
          title: 'Mohon Maaf',
          subTitle: 'Sedang tidak terkoneksi dengan server. Silahkan coba kembali beberapa saat',
          buttons: ['OK']
        });
        alert.present();
        this.indonesiaMap();
        return false;
      });
    });
  }

  indonesiaMap() {

    var myLatlng = { lat: -0.620093, lng: 115.3094217 };
    var map = new google.maps.Map(this.mapElement.nativeElement, {
      zoom: 5.69,
      center: myLatlng,
      zoomControl: false,
      mapTypeControl: false,
      scaleControl: false,
      streetViewControl: false,
      rotateControl: false,
      fullscreenControl: false
    });
  }

  getData(legend_type) {
    console.log('legendnya ini : ', legend_type);
    this.storage.get('user_session_storage').then((res) => {
      let body;
      body = {
        kdcabang: this.kdcabang,
        kdsales: this.kdsales,
        kdoutlet: this.kdoutlet,
        user_level: res[0].user_level,
        username: res[0].username
      };

      this.postPvdr.postData(body, 'outlet/getchart2').subscribe((data) => {

        if (data.success) {

          if (legend_type == '1') {
            this.toko_sudah_terpetakan = data.toko_sudah_terpetakan;
            this.toko_belum_terpetakan = data.toko_belum_terpetakan;
            this.toko_pegangan = data.toko_pegangan;
            this.toko_diluar_pegangan_sudah_terpetakan = data.toko_diluar_pegangan_sudah_terpetakan;
            this.toko_noo = data.toko_noo;
          } else if (legend_type == '2') {
            this.toko_sudah_terpetakan = data.seluruh_toko_sudah_terpetakan;
            this.toko_belum_terpetakan = data.seluruh_toko_belum_terpetakan;
            this.toko_pegangan = data.seluruh_toko;
            this.toko_diluar_pegangan_sudah_terpetakan = 0;
            this.toko_noo = data.seluruh_toko_noo;
          }

        } else {
          //failed
          return false;
        }

      }, error => {
        //not connect
        return false;
      });

    });

  }

  filter() {
    var area;
    var zooming;

    this.storage.get('user_session_storage').then((res) => {

      if (res[0].user_level == '-1' || res[0].user_level == '9') {
        const modal = this.modalCtrl.create(SearchPage, { kdcabang: this.kdcabang, kdsales: this.kdsales, kdoutlet: this.kdoutlet, distributor_name: this.distributor_name, sales_name: this.sales_name });
        modal.present();
        modal.onDidDismiss(val => {
          console.log('val : ', val)
          if (val) {
            if (val.distributor_name == 'Semua Distributor Nasional' && val.sales_name == 'Semua') {
              console.log('Ngapa Kemari Mulu Ya 1 : ', val.distributor_name);
              this.kdsales = val.kdsales;
              this.sales_name = val.sales_name;
              this.kdcabang = val.kdcabang;
              this.kdoutlet = val.kdoutlet;
              this.distributor_name = val.distributor_name;
              this.title = val.distributor_name;
              this.legend_type = '2';
              area = "nasional";
              zooming = 5.69;
            } else if (val.distributor_name != 'Semua Distributor Nasional' && val.sales_name == 'Semua') {
              console.log('Ngapa Kemari Mulu Ya 2 : ', val.distributor_name);
              this.kdsales = val.kdsales;
              this.sales_name = val.sales_name;
              this.kdcabang = val.kdcabang;
              this.kdoutlet = val.kdoutlet;
              this.distributor_name = val.distributor_name;
              this.title = val.distributor_name;
              this.legend_type = '2';
              area = "distributor";
              zooming = 11;
            } else {
              console.log('Ngapa Kemari Mulu Ya 3 : ', val.distributor_name);
              this.kdsales = val.kdsales;
              this.sales_name = val.sales_name;
              this.kdcabang = val.kdcabang;
              this.kdoutlet = val.kdoutlet;
              this.distributor_name = val.distributor_name;
              this.title = val.distributor_name;
              this.legend_type = '1';
              area = "distributor";
              zooming = 11;
            }
            this.getData(this.legend_type);
            this.initMap(area, zooming);
            this.clearMarker();
          } else {
            //coding if else or empty
          }
        });
      }

      if (res[0].user_level == '6') {
        const modal = this.modalCtrl.create(SearchPage, { kdcabang: this.kdcabang, kdsales: this.kdsales, kdoutlet: this.kdoutlet, distributor_name: this.distributor_name, sales_name: this.sales_name });
        modal.present();
        modal.onDidDismiss(val => {
          if (val) {
            if (val.distributor_name == 'Semua Distributor Regional' && val.sales_name == 'Semua') {
              this.kdsales = val.kdsales;
              this.sales_name = val.sales_name;
              this.kdcabang = val.kdcabang;
              this.kdoutlet = val.kdoutlet;
              this.distributor_name = val.distributor_name;
              this.title = val.distributor_name;
              this.legend_type = '2';
              area = "regional";
              zooming = 7;
            } else if (val.distributor_name != 'Semua Distributor Regional' && val.sales_name == 'Semua') {
              this.kdsales = val.kdsales;
              this.sales_name = val.sales_name;
              this.kdcabang = val.kdcabang;
              this.kdoutlet = val.kdoutlet;
              this.distributor_name = val.distributor_name;
              this.title = val.distributor_name;
              this.legend_type = '2';
              area = "distributor";
              zooming = 11;
            } else {
              this.kdsales = val.kdsales;
              this.sales_name = val.sales_name;
              this.kdcabang = val.kdcabang;
              this.kdoutlet = val.kdoutlet;
              this.distributor_name = val.distributor_name;
              this.title = val.distributor_name;
              this.legend_type = '1';
              area = "distributor";
              zooming = 11;
            }
            this.getData(this.legend_type);
            this.initMap(area, zooming);
            this.clearMarker();
          } else {
            //coding if else or empty
          }
        });
      }

      if (res[0].user_level == '2' || res[0].user_level == '10') {

        const modal = this.modalCtrl.create(SearchPage, { kdcabang: this.kdcabang, kdsales: this.kdsales, kdoutlet: this.kdoutlet, distributor_name: this.distributor_name, sales_name: this.sales_name });
        modal.present();
        modal.onDidDismiss(val => {
          if (val) {
            if (val.sales_name != 'Semua') {
              this.kdsales = val.kdsales;
              this.sales_name = val.sales_name;
              this.kdcabang = val.kdcabang;
              this.kdoutlet = val.kdoutlet;
              this.distributor_name = val.distributor_name;
              this.title = val.distributor_name;
              this.legend_type = '1';
              area = "distributor";
              zooming = 11;
            } else {
              this.kdsales = val.kdsales;
              this.sales_name = val.sales_name;
              this.kdcabang = val.kdcabang;
              this.kdoutlet = val.kdoutlet;
              this.distributor_name = val.distributor_name;
              this.title = val.distributor_name;
              this.legend_type = '2';
              area = "distributor";
              zooming = 11;
            }
            this.getData(this.legend_type);
            this.initMap(area, zooming);
            this.clearMarker();
          } else {
            //coding if else or empty
          }
        });
      }




    });
  }

  clearMarker() {
    this.setMapOnAll(null);
  }

  setMapOnAll(map) {
    for (var i = 0; i < this.markers.length; i++) {
      this.markers[i].setMap(map);
    }
  }

  tampilkan() {
    document.getElementById('legend').style.display = '';
    document.getElementById('fab').style.display = 'none';
  }

  sembunyikan() {
    document.getElementById('legend').style.display = 'none';
    document.getElementById('fab').style.display = '';
  }

}
