import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MainPage } from '../../pages/main/main';
import { Storage } from '@ionic/storage';
import { Provider } from '../../config/provider';
import { AlertController } from 'ionic-angular';
import { Platform } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  username: any;
  password: any;
  platform: String = 'M';

  constructor(
    public navCtrl: NavController,
    public storage: Storage,
    public plt: Platform,
    public alertCtrl: AlertController,
    public postPvdr: Provider,
    public navParams: NavParams) {

    if (this.plt.is('android')) {
      this.platform = 'M';
    } else {
      this.platform = 'W';
    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  login_procces() {
    let body = {
      username: this.username,
      password: this.password,
      platform: this.platform,
      action: 'cekLogin'
    };

    this.postPvdr.postData(body, 'login/cekuser').subscribe((data) => {


      if (data.success) {
        this.storage.set('user_session_storage', data.result);
        this.navCtrl.setRoot(MainPage);
      } else {
        const alert = this.alertCtrl.create({
          title: 'Mohon Maaf',
          subTitle: 'Anda Tidak Mempunyai Akses.',
          buttons: ['OK']
        });
        alert.present();
      }

    }, error => {
      const alert = this.alertCtrl.create({
        title: 'Mohon Maaf',
        subTitle: 'Sedang tidak terkoneksi dengan server. Silahkan coba kembali beberapa saat',
        buttons: ['OK']
      });
      alert.present();
    });
  }

}
