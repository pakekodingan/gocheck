import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Provider } from '../../config/provider';
import { Storage } from '@ionic/storage';
import { AlertController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';

declare var google;
/**
 * Generated class for the DetailhistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detailhistory',
  templateUrl: 'detailhistory.html',
})
export class DetailhistoryPage {

  // @ViewChild('map') mapElement: ElementRef;

  base_url = 'http://api.vci.co.id/public/outlets/';

  id: any;

  KdOutlet: any;
  OutletName: any;
  Address: any;
  UserUpload: any;
  DateUpload: any;
  UserValidate: any;
  DateValidate: any;
  Lat: any;
  Lng: any;
  Status: any;
  Images: any;
  tipedata: any;
  Note: any;
  NoteValidator: any;

  constructor(
    public navCtrl: NavController,
    public postPvdr: Provider,
    public storage: Storage,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public navParams: NavParams) {
    this.id = navParams.get("id");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailhistoryPage');
    this.getData(this.id);
  }

  getData(id) {

    const loader = this.loadingCtrl.create({
      content: "Mohon Menunggu..."
    });
    loader.present();


    let body = {
      id: id
    };

    this.postPvdr.postData(body, 'outlet/getdetailhistory').subscribe((data) => {

      if (data.success) {
        loader.dismiss();

        var tipedata;
        if (data.result[0]['TypeData'] == 'S') {
          tipedata = "Server";
        } else {
          tipedata = "WhatsApp";
        }


        this.KdOutlet = data.result[0]['KdOutlet'];
        this.OutletName = data.result[0]['OutletName'];
        this.Address = data.result[0]['Address'];
        this.UserUpload = data.result[0]['UserUpload'];
        this.DateUpload = data.result[0]['DateUpload'];
        this.UserValidate = data.result[0]['UserValidate'];
        this.DateValidate = data.result[0]['DateValidate'];
        this.Lat = data.result[0]['Lat'];
        this.Lng = data.result[0]['Lng'];
        this.Status = data.result[0]['Status'];
        this.Images = this.base_url + data.result[0]['Images'];
        this.tipedata = tipedata;
        this.Note = data.result[0]['Note'];
        this.NoteValidator = data.result[0]['NoteValidator'];


        // var myLatlng = { lat: parseFloat(data.result[0]['Lat']), lng: parseFloat(data.result[0]['Lng']) };
        // var map = new google.maps.Map(this.mapElement.nativeElement, {
        //   zoom: 13,
        //   center: myLatlng,
        //   zoomControl: false,
        //   mapTypeControl: false,
        //   scaleControl: false,
        //   streetViewControl: false,
        //   rotateControl: false,
        //   fullscreenControl: false
        // });
        // var icon = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';
        // var marker = new google.maps.Marker({
        //   position: myLatlng,
        //   animation: google.maps.Animation.DROP,
        //   map: map,
        //   icon: icon
        // });



      } else {
        loader.dismiss();

        const alert = this.alertCtrl.create({
          title: 'Info',
          subTitle: 'Mohon Maaf Data Tidak Di Temukan',
          message: '',
          buttons: ['OK']
        });
        alert.present();

        return false;
      }


    }, error => {
      loader.dismiss();
      const alert = this.alertCtrl.create({
        title: 'Mohon Maaf',
        subTitle: 'Sedang tidak terkoneksi dengan server. Silahkan coba kembali beberapa saat',
        buttons: ['OK']
      });
      alert.present();
      return false;
    });

  }

}
