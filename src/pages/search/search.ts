import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { ModalController } from 'ionic-angular';
import { Provider } from '../../config/provider';
import { Storage } from '@ionic/storage';

import { DetailbangetlistingsPage } from '../../pages/detailbangetlistings/detailbangetlistings';
import { UsersdistributorPage } from '../../pages/usersdistributor/usersdistributor';
import { DetaillistingsPage } from '../../pages/detaillistings/detaillistings';

/**
 * Generated class for the SearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {

  distributor_name: any;
  kdoutlet: any;
  kdcabang: any;
  kdsales: any;
  sales_name: any;
  cari_distribusi: any;
  cari_sales: any;


  constructor(
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    public alertCtrl: AlertController,
    public postPvdr: Provider,
    public storage: Storage,
    public modalCtrl: ModalController,
    public loadingCtrl: LoadingController,
    public navParams: NavParams) {

    this.distributor_name = navParams.get("distributor_name");
    this.kdcabang = navParams.get("kdcabang");
    this.kdoutlet = navParams.get("kdoutlet");
    this.kdsales = navParams.get("kdsales");
    this.sales_name = navParams.get("sales_name");


    this.storage.get('user_session_storage').then((res) => {
      if (res[0].user_level == '2' || res[0].user_level == '10') {
        this.cari_distribusi = "No";
        this.cari_sales = "Yes";
      } else {
        this.cari_distribusi = "Yes";
        this.cari_sales = "Yes";
      }

    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchPage');

  }


  close() {
    this.viewCtrl.dismiss();
  }

  search_distributor(kdoutlet, kdcabang) {
    const modal = this.modalCtrl.create(DetaillistingsPage, { kdoutlet: kdoutlet, kdcabang: kdcabang, storages: 'No', close: 'Yes', bisa_pilih: 'Yes' });
    modal.present();
    modal.onDidDismiss(val => {
      console.log('val ', val)
      if (val) {
        this.distributor_name = val.distributor_name;
        this.kdoutlet = val.kdoutlet;
        this.kdsales = '';
        this.sales_name = 'Semua';
      } else {
        //coding if else or empty
      }
    });
  }

  search_sales(kdoutlet, kdcabang) {
    // untuk mencari toko
    // const modal = this.modalCtrl.create(DetailbangetlistingsPage, { kdoutlet: kdoutlet, kdcabang: kdcabang, kdsales: '', viewuser: 'No', title: '', close: 'Yes', bisa_pilih: 'Yes' });
    // modal.present();
    const modal = this.modalCtrl.create(UsersdistributorPage, { kdoutlet: kdoutlet, kdcabang: kdcabang, performa: 'No', close: 'Yes', bisa_pilih: 'Yes', reset_password: 'No' });
    modal.present();
    modal.onDidDismiss(val => {
      if (val) {
        this.kdsales = val.kdsales;
        this.sales_name = val.sales_name;
      } else {
        //coding if else or empty
      }
    });

  }

  terapkan() {
    this.viewCtrl.dismiss({ kdcabang: this.kdcabang, kdoutlet: this.kdoutlet, distributor_name: this.distributor_name, kdsales: this.kdsales, sales_name: this.sales_name });
  }

}
