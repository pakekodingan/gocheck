import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SearchactivitiesPage } from './searchactivities';

@NgModule({
  declarations: [
    SearchactivitiesPage,
  ],
  imports: [
    IonicPageModule.forChild(SearchactivitiesPage),
  ],
})
export class SearchactivitiesPageModule {}
