import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { Provider } from '../../config/provider';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the RutesalesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

import { DetailrutesalesPage } from '../../pages/detailrutesales/detailrutesales';


@IonicPage()
@Component({
  selector: 'page-rutesales',
  templateUrl: 'rutesales.html',
})
export class RutesalesPage {

  listUsers: Array<any>;
  src: any;
  first: number;

  kdoutlet: any;
  kdcabang: any;

  constructor(
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    public postPvdr: Provider,
    public alertCtrl: AlertController,
    public storage: Storage,
    public loadingCtrl: LoadingController,
    public navParams: NavParams) {
    this.kdoutlet = navParams.get("kdoutlet");
    this.kdcabang = navParams.get("kdcabang");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RutesalesPage');
    this.listUsers = new Array();
    this.getData('', 0, 10, 'No');
  }

  form_search() {
    const prompt = this.alertCtrl.create({
      title: 'Cari',
      message: "Masukan kata untuk mencari kode atau nama user",
      inputs: [
        {
          name: 'src',
          placeholder: 'Apa yang anda cari...'
        },
      ],
      buttons: [
        {
          text: 'Batal',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Cari',
          handler: data => {
            console.log('Saved clicked');
            this.getListUserDistributor(data.src);
          }
        }
      ]
    });
    prompt.present();
  }

  getListUserDistributor(src) {
    this.listUsers = new Array();
    if (src) {
      src = src;
    } else {
      src = '';
    }
    this.getData(src, 0, 10, 'No');
  }

  getData(src, first, offset, infinite) {

    this.first = first;
    this.src = src;

    const loader = this.loadingCtrl.create({
      content: "Mohon Menunggu..."
    });
    if (infinite == 'No') {
      loader.present();
    }

    this.storage.get('user_session_storage').then((res) => {
      let body = {
        kdcabang: this.kdcabang,
        first: first,
        offset: offset,
        filter: '',
        keyword: src,
        kdoutlet: this.kdoutlet,
        user_level: res[0].user_level,
        username: res[0].username
      };

      this.postPvdr.postData(body, 'outlet/getListUserDistributor2').subscribe((data) => {


        if (data.success) {
          loader.dismiss();
          for (let i = 0; i < data.result.length; i++) {
            this.listUsers.push(
              {
                username: data.result[i]['username'],
                name: data.result[i]['name'],
                kdsales: data.result[i]['kdsales'],
                kdoutlet: data.result[i]['kdoutlet'],
                kdcabang: data.result[i]['kdcabang'],
                jml_toko: data.result[i]['jml_toko']
              });

          }
        } else {
          loader.dismiss();
          if (infinite == 'No') {
            const alert = this.alertCtrl.create({
              title: 'Info',
              subTitle: 'Mohon Maaf Data Tidak Di Temukan',
              message: '',
              buttons: ['OK']
            });
            alert.present();
          }
          return false;
        }

      }, error => {

        loader.dismiss();
        const alert = this.alertCtrl.create({
          title: 'Mohon Maaf',
          subTitle: 'Sedang tidak terkoneksi dengan server. Silahkan coba kembali beberapa saat',
          buttons: ['OK']
        });
        alert.present();
        return false;

      });

    });
  }

  doRefresh(refresher) {
    this.listUsers = new Array();
    this.getData('', 0, 10, 'No');
    setTimeout(() => {
      refresher.complete();
    }, 1000);
  }

  doInfinite(infiniteScroll, src, first) {

    first = first + 10;

    setTimeout(() => {
      this.getData(src, first, 10, 'Yes');
      infiniteScroll.complete();
    }, 1000);

  }

  detail_rute(kdsales, kdcabang, kdoutlet, name_sales) {
    this.navCtrl.push(DetailrutesalesPage, { kdoutlet: kdoutlet, kdcabang: kdcabang, kdsales: kdsales, title: name_sales });
  }

}
