import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RutesalesPage } from './rutesales';

@NgModule({
  declarations: [
    RutesalesPage,
  ],
  imports: [
    IonicPageModule.forChild(RutesalesPage),
  ],
})
export class RutesalesPageModule {}
